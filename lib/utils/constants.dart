class Constants {
  static const String appName = 'Santa Clara';
  static const String logoTag = 'near.huscarl.loginsample.logo';
  static const String titleTag = 'near.huscarl.loginsample.title';
  static const String apiUrl = 'http://stage.santaclara.com.py:8086/api/';
}
import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:intl/intl.dart' as intl;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:santa_clara/src/modules/authBloc.dart';
import 'package:santa_clara/src/modules/mainBloc.dart';
import 'package:santa_clara/src/modules/appProvider.dart';
import 'package:santa_clara/src/modules/turnoBloc.dart';
import 'package:santa_clara/src/modules/userBloc.dart';
import 'package:santa_clara/utils/userPreferences.dart';

class UiHelper {
  static BuildContext ctx;
  static Size size;
  static Color colorBtnPrimary = Color.fromRGBO(0, 159, 227, 1);
  static Color scCeleste = Color.fromRGBO(0, 159, 227, 1);
  static Color scAzul = Color.fromRGBO(21, 61, 138, 1);
  static Color scAzulOscuro = Color.fromRGBO(6, 41, 109, 1);
  static Color scAzulOscuroBorde = Color.fromRGBO(8, 39, 98, 1);
  static Color scGris = Color.fromRGBO(51, 51, 51, 1);
  static Color greenContainer = Color.fromRGBO(100, 255, 100, 0.5);
  static Color scBgActionAlert = Color.fromRGBO(255, 204, 0, 1);
  static Color scBgActionLocation = Color.fromRGBO(242, 153, 74, 1);
  static Color scSeparadorCeleste = Color.fromRGBO(217, 244, 255, 1);
  static Color scGrisTexto = Color.fromRGBO(90, 90, 90, 1);
  static Color scAmarilloHora = Color.fromRGBO(253, 255, 229, 1);
  static Color scBordeCeleste = Color.fromRGBO(148, 204, 227, 1);
  static Color scHoraGris = Color.fromRGBO(249, 249, 249, 1);
  static Color scBtnGris = Color.fromRGBO(150, 150, 150, 1);
  static Color scCheckGris = Color.fromRGBO(240, 240, 240, 1);
  static Color scCheckRojo = Color.fromRGBO(235, 87, 87, 1);

  static double layoutMarginLeft = 30.0;
  static String noProfilePic = 'assets/images/sc-profile-pic.png';

  final BoxShadow shadowCeleste = BoxShadow(
      color: Color.fromRGBO(0, 159, 227, 0.25),
      offset: new Offset(0.0, 0.0),
      blurRadius: 10.0);

  final BoxShadow shadowGris = BoxShadow(
      color: Color.fromRGBO(100, 100, 100, 0.5),
      offset: new Offset(1.0, 1.0),
      blurRadius: 3.0);

  static Color bgAppbar = Colors.transparent;
  static TextStyle titleStyle =
      TextStyle(fontSize: 38, color: scAzul, fontWeight: FontWeight.bold);
  static TextStyle normalStyle =
      TextStyle(fontSize: 12, color: scGris, fontWeight: FontWeight.normal);
  static TextStyle btnTextStyle = TextStyle(
      fontSize: 14,
      color: scAzul,
      fontWeight: FontWeight.bold,
      decoration: TextDecoration.underline);
  static TextStyle boldText = TextStyle(fontWeight: FontWeight.bold);

  final BoxDecoration boxDecoCitaCard = BoxDecoration(
    color: Color.fromRGBO(219, 244, 255, 1),
    border: Border.all(color: Color.fromRGBO(175, 227, 249, 1), width: 1.0),
    boxShadow: [
      BoxShadow(
          color: Color.fromRGBO(175, 227, 249, 1),
          offset: Offset(0, 0),
          blurRadius: 3.0),
    ],
    borderRadius: BorderRadius.circular(5.0),
  );

  final BoxDecoration boxDecoProfesionalCard = BoxDecoration(
    color: Colors.white,
    border: Border.all(color: Color.fromRGBO(175, 227, 249, 1), width: 1.0),
    boxShadow: [
      BoxShadow(
          color: Color.fromRGBO(175, 227, 249, 1),
          offset: Offset(0, 0),
          blurRadius: 5.0),
    ],
    borderRadius: BorderRadius.circular(5.0),
  );

  void setTitleFontSize(double size) {
    titleStyle =
        TextStyle(fontSize: size, color: scAzul, fontWeight: FontWeight.bold);
  }

  void setNormalFontSize(double size) {
    normalStyle =
        TextStyle(fontSize: size, color: scGris, fontWeight: FontWeight.normal);
  }

  void setContext(BuildContext pContext) {
    ctx = pContext;
    this.setSize(MediaQuery.of(pContext).size);
  }

  void setSize(Size pSize) {
    size = pSize;
  }

  Size get getSize => size;

  final List<String> listaOpRecordatorio = [
    "30 min.",
    "60 min.",
    "90 min.",
  ];

  final List<String> listaOpFrecuencia = [
    "1 hora",
    "3 horas",
    "4 horas",
    "6 horas",
    "8 horas",
    "12 horas",
    "24 horas"
  ];

  Widget logoSantaClara() {
    final logo = new Image(
      image: new AssetImage('assets/images/logo-santa-clara.png'),
      width: (this.getSize.width * 0.505),
      fit: BoxFit.contain,
    );
    return logo;
  }

  final Map<String, String> dropdownItems = {
    "UNICA_VEZ": "Única vez",
    "PROGRAMADO": "Programado",
    "INDEFINIDO": "Indefinido",
  };

  Widget backgroundLayout({bool sinLogo = false}) {
    final logoSantaClara = this.logoSantaClara();

    List<Widget> bgWidgetList = [this.getBackground()];
    if (!sinLogo) {
      bgWidgetList.add(
        Positioned(
            top: this.getSize.height * 0.0845, right: 30, child: logoSantaClara),
      );
    }
    return Stack(
      children: bgWidgetList,
    );
  }

  Container getDrawerBackground() {
    final bg = Container(
        height: this.getSize.height,
        width: this.getSize.width,
        decoration: new BoxDecoration(
            image: new DecorationImage(
                image: new AssetImage('assets/images/bg-drawer.png'),
                fit: BoxFit.fill)));
    return bg;
  }

  Container getBackground() {
    Image bgImage = Image(
        image: new AssetImage('assets/images/bg-screen.png'),
        repeat: ImageRepeat.repeatY,
        height: this.getSize.height);
    final bg = Container(
        alignment: Alignment.centerLeft,
        height: this.getSize.height,
        width: this.getSize.width,
        color: Colors.white,
        child:
            SizedBox(width: 30.0, height: this.getSize.height, child: bgImage));
    return bg;
  }

  Align crearTituloGrande({String caption, double textSize = 0}) {
    if (textSize > 0) {
      this.setTitleFontSize(textSize);
    }

    return Align(
        alignment: Alignment.centerLeft,
        child: Container(
            margin: EdgeInsets.only(left: 20.0, right: 30.0),
            child: Text(
              caption,
              style: titleStyle,
              textAlign: TextAlign.left,
            )));
  }

  Align crearTexto({String text, double textSize = 0}) {
    if (textSize > 0) {
      this.setNormalFontSize(textSize);
    }

    return Align(
      alignment: Alignment.centerLeft,
      child: Container(
          margin: EdgeInsets.only(left: 20.0, right: 30.0),
          child: Text(
            text,
            style: normalStyle,
            textAlign: TextAlign.left,
          )),
    );
  }

  /// ------------------------------------------------------
  /// widget base de los campos
  /// ------------------------------------------------------

  Widget emailField(
      {Stream<String> fieldStream,
      Function(String) evtOnChanged,
      String label = 'Correo electrónico',
      String hint = 'usuario@mail.com',
      String text,
      bool withStreamBuilder = true,
      TextEditingController controller}) {
    Widget rWidget;
    if (withStreamBuilder) {
      rWidget = StreamBuilder(
          stream: fieldStream,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            return Container(
                margin: EdgeInsets.only(bottom: 10),
                child: TextFormField(
                  controller: controller,
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                      labelText: label,
                      hintText: hint,
                      border: InputBorder.none,
                      errorText: snapshot.error,
                      errorStyle: TextStyle(height: 0.1)),
                  onChanged: evtOnChanged,
                ));
          });
    } else {
      rWidget = Container(
          margin: EdgeInsets.only(bottom: 10),
          child: TextFormField(
            controller: controller,
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
                labelText: label, hintText: hint, border: InputBorder.none),
            onChanged: evtOnChanged,
          ));
    }
    return rWidget;
  }

  Widget passwordConfirmField(
      {Stream<String> fieldStream,
      Function(String) evtOnChanged,
      Function evtOnPressed,
      String caption = 'Contraseña',
      bool withStreamBuilder = true,
      bool showPass = false}) {
    print('passwordConfirmField - showPass:$showPass');
    Widget rWidget;
    if (withStreamBuilder) {
      rWidget = StreamBuilder(
          stream: fieldStream,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            return Container(
                margin: EdgeInsets.only(bottom: 5),
                child: TextField(
                  obscureText: showPass,
                  decoration: InputDecoration(
                      suffix: SizedBox(
                        child: IconButton(
                          icon: Icon(
                              (!showPass)
                                  ? FontAwesomeIcons.eyeSlash
                                  : FontAwesomeIcons.eye,
                              size: 18.0,
                              color: scAzul),
                          onPressed: evtOnPressed,
                        ),
                        width: 30.0,
                        height: 20.0,
                      ),
                      labelText: caption,
                      border: InputBorder.none,
                      errorText: snapshot.error,
                      errorStyle: TextStyle(height: 0.1)),
                  onChanged: evtOnChanged,
                ));
          });
    } else {
      rWidget = Container(
          margin: EdgeInsets.only(bottom: 5),
          child: TextField(
            obscureText: showPass,
            decoration: InputDecoration(
                suffix: SizedBox(
                  child: IconButton(
                    icon: Icon(FontAwesomeIcons.eye, size: 18.0, color: scAzul),
                    onPressed: evtOnPressed,
                  ),
                  width: 30.0,
                  height: 20.0,
                ),
                labelText: caption,
                border: InputBorder.none,
                errorStyle: TextStyle(height: 0.1)),
            onChanged: evtOnChanged,
          ));
    }
    return rWidget;
  }

  Widget passwordField(
      {Stream<String> fieldStream,
      Function(String) evtOnChanged,
      Function evtOnPressed,
      String caption = 'Contraseña',
      bool withStreamBuilder = true,
      bool showPass = false}) {
    print('passwordField - showPass:$showPass');
    Widget rWidget;
    if (withStreamBuilder) {
      rWidget = StreamBuilder(
          stream: fieldStream,
          builder: (BuildContext context, AsyncSnapshot snapshot) {

            return Container(
                margin: EdgeInsets.only(bottom: 5),
                child: TextFormField(
                  obscureText: showPass,
                  decoration: InputDecoration(
                      suffix: SizedBox(
                        child: IconButton(
                          icon: Icon(
                              (!showPass)
                                  ? FontAwesomeIcons.eyeSlash
                                  : FontAwesomeIcons.eye,
                              size: 18.0,
                              color: scAzul),
                          onPressed: evtOnPressed,
                        ),
                        width: 30.0,
                        height: 20.0,
                      ),
                      labelText: caption,
                      border: InputBorder.none,
                      errorText: snapshot.error,
                      errorStyle: TextStyle(height: 0.1)),
                  onChanged: evtOnChanged,
                ));
          });
    } else {
      rWidget = Container(
          margin: EdgeInsets.only(bottom: 5),
          child: TextFormField(
            initialValue: '456789',
            obscureText: showPass,
            decoration: InputDecoration(
                suffix: SizedBox(
                  child: IconButton(
                    icon: Icon(FontAwesomeIcons.eye, size: 18.0, color: scAzul),
                    onPressed: evtOnPressed,
                  ),
                  width: 30.0,
                  height: 20.0,
                ),
                labelText: caption,
                border: InputBorder.none,
                errorStyle: TextStyle(height: 0.1)),
            onChanged: evtOnChanged,
          ));
    }
    return rWidget;
  }

  Widget textField(
      {Stream<String> fieldStream,
      Function(String) evtOnChanged,
      Function evtOnPressed,
      String label = 'Etiqueta del campo',
      String hint = 'Texto de ayuda',
      String text,
      bool withStreamBuilder = true,
      TextEditingController controller}) {
    Widget rWidget;
    if (withStreamBuilder) {
      rWidget = StreamBuilder(
          stream: fieldStream,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            return Container(
                margin: EdgeInsets.only(bottom: 10),
                child: TextField(
                  controller: controller,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                      labelText: label,
                      hintText: hint,
                      border: InputBorder.none,
                      errorText: snapshot.error,
                      errorStyle: TextStyle(height: 1)),
                  onChanged: evtOnChanged,
                ));
          });
    } else {
      rWidget = Container(
          margin: EdgeInsets.only(bottom: 10),
          child: TextFormField(
            controller: controller,
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
                labelText: label,
                hintText: hint,
                border: InputBorder.none,
                errorStyle: TextStyle(height: 1)),
            onChanged: evtOnChanged,
          ));
    }
    return rWidget;
  }

  Widget numberField(
      {Stream<String> fieldStream,
      Function(String) evtOnChanged,
      Function evtOnPressed,
      String label = 'Etiqueta del campo',
      String hint = '###.###.###',
      String text,
      bool withStreamBuilder = true,
      TextEditingController controller}) {
    Widget rWidget;
    if (withStreamBuilder) {
      rWidget = StreamBuilder(
          stream: fieldStream,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            return Container(
                margin: EdgeInsets.only(bottom: 10),
                child: TextField(
                  controller: controller,
                  keyboardType: TextInputType.numberWithOptions(signed: true),
                  decoration: InputDecoration(
                      labelText: label,
                      hintText: hint,
                      border: InputBorder.none,
                      errorText: snapshot.error,
                      errorStyle: TextStyle(height: 1)),
                  onChanged: evtOnChanged,
                ));
          });
    } else {
      rWidget = Container(
          margin: EdgeInsets.only(bottom: 10),
          child: TextField(
            controller: controller,
            keyboardType: TextInputType.numberWithOptions(signed: true),
            decoration: InputDecoration(
                labelText: label,
                hintText: hint,
                border: InputBorder.none,
                errorStyle: TextStyle(height: 1)),
            onChanged: evtOnChanged,
          ));
    }
    return rWidget;
  }

  /// ------------------------------------------------------
  /// campos de login y registro
  /// ------------------------------------------------------

  Widget campoCorreoElectronico({AuthBloc bloc}) =>
      emailField(fieldStream: bloc.emailStream, evtOnChanged: bloc.changeEmail);

  Widget campoCorreoElectronicoRecovery({AuthBloc bloc}) => emailField(
      fieldStream: bloc.emailRecoveryStream,
      evtOnChanged: bloc.changeEmailRecovery);

  Widget campoPassword(
          {String caption = "Contraseña",
          AuthBloc bloc,
          Function evtOnPressed,
          bool showPass = false}) =>
      passwordField(
          fieldStream: bloc.passwordStream,
          evtOnChanged: bloc.changePassword,
          evtOnPressed: evtOnPressed,
          showPass: showPass);

  Widget campoPasswordConfirm(
          {String caption = "Contraseña Contraseña",
          AuthBloc bloc,
          Function evtOnPressed,
          bool showPass = false}) =>
      passwordConfirmField(
          fieldStream: bloc.passwordConfirmStream,
          evtOnChanged: bloc.changePasswordConfirm,
          evtOnPressed: evtOnPressed,
          showPass: showPass);

  Widget campoNombreApellido(
          {AuthBloc bloc,
          String label = 'Nombre y Apellido',
          String hint = 'Ingrese su nombre completo'}) =>
      textField(
          fieldStream: bloc.nombreApellidoStream,
          evtOnChanged: bloc.changeNombreApellido,
          label: label,
          hint: hint);

  Widget campoNroDocumento(
          {AuthBloc bloc,
          String label = "Nro. de cédula",
          String hint = "###.###.###"}) =>
      numberField(
          fieldStream: bloc.nroDocumentoStream,
          evtOnChanged: bloc.changeNroDocumento,
          label: label,
          hint: hint);

  Widget campoNroTelefono(
          {AuthBloc bloc,
          String label = "Nro. de teléfono",
          String hint = "+### ### #######"}) =>
      numberField(
          fieldStream: bloc.nroTelefonoStream,
          evtOnChanged: bloc.changeNroTelefono,
          label: label,
          hint: hint);

  /// ------------------------------------------------------
  /// campos de perfil de usuario
  /// ------------------------------------------------------

  Widget perfilPassword(
          {UserBloc bloc,
          String caption = "Contraseña",
          Function evtOnPressed,
          bool streamBuilder = true,
          bool showPass = false}) =>
      passwordField(
          fieldStream: bloc.passwordStream,
          evtOnChanged: bloc.changePassword,
          evtOnPressed: evtOnPressed,
          withStreamBuilder: streamBuilder,
          showPass: showPass);

  Widget perfilPasswordConfirm(
          {UserBloc bloc,
          String caption = "Contraseña Contraseña",
          Function evtOnPressed,
          bool streamBuilder = true,
          bool showPass = false}) =>
      passwordConfirmField(
          fieldStream: bloc.passwordConfirmStream,
          evtOnChanged: bloc.changePasswordConfirm,
          evtOnPressed: evtOnPressed,
          withStreamBuilder: streamBuilder,
          showPass: showPass);

  Widget perfilNombreApellido(
      {UserBloc bloc,
      String label = 'Nombre y Apellido',
      String hint = 'Ingrese su nombre completo',
      String value,
      bool streamBuilder = true,
      Function(String) evtOnChanged,
      TextEditingController controller}) {
    if (streamBuilder) {
      return textField(
          fieldStream: bloc.nombreApellidoStream,
          evtOnChanged:
              (evtOnChanged != null) ? evtOnChanged : bloc.changeNombreApellido,
          label: label,
          hint: hint,
          text: value,
          withStreamBuilder: streamBuilder,
          controller: controller);
    } else {
      return textField(
          evtOnChanged: evtOnChanged,
          label: label,
          hint: hint,
          text: value,
          withStreamBuilder: streamBuilder,
          controller: controller);
    }
  }

  Widget perfilCorreoElectronico(
      {UserBloc bloc,
      String value,
      bool streamBuilder = true,
      Function(String) evtOnChanged,
      TextEditingController controller}) {
    if (streamBuilder) {
      return emailField(
          fieldStream: bloc.emailStream,
          evtOnChanged:
              (evtOnChanged != null) ? evtOnChanged : bloc.changeEmail,
          text: value,
          withStreamBuilder: streamBuilder,
          controller: controller);
    } else {
      return emailField(
          evtOnChanged: evtOnChanged,
          text: value,
          withStreamBuilder: streamBuilder,
          controller: controller);
    }
  }

  Widget perfilNroDocumento(
      {UserBloc bloc,
      String label = "Nro. de cédula",
      String hint = "###.###.###",
      String value,
      bool streamBuilder = true,
      Function(String) evtOnChanged,
      TextEditingController controller}) {
    if (streamBuilder) {
      return numberField(
          fieldStream: bloc.nroDocumentoStream,
          evtOnChanged:
              (evtOnChanged != null) ? evtOnChanged : bloc.changeNroDocumento,
          label: label,
          hint: hint,
          text: value,
          withStreamBuilder: streamBuilder,
          controller: controller);
    } else {
      return numberField(
          evtOnChanged: evtOnChanged,
          label: label,
          hint: hint,
          text: value,
          withStreamBuilder: streamBuilder,
          controller: controller);
    }
  }

  Widget perfilNroTelefono(
      {UserBloc bloc,
      String label = "Nro. de teléfono",
      String hint = "#########",
      String value,
      bool streamBuilder = true,
      Function(String) evtOnChanged,
      TextEditingController controller}) {
    if (streamBuilder) {
      return textField(
          fieldStream: bloc.nroTelefonoStream,
          evtOnChanged:
              (evtOnChanged != null) ? evtOnChanged : bloc.changeNroTelefono,
          label: label,
          hint: hint,
          text: value,
          withStreamBuilder: streamBuilder,
          controller: controller);
    } else {
      return textField(
          evtOnChanged: evtOnChanged,
          label: label,
          hint: hint,
          text: value,
          withStreamBuilder: streamBuilder,
          controller: controller);
    }
  }

  /// ------------------------------------------------------
  /// campos de perfil de usuario
  /// ------------------------------------------------------

  Widget turnoNroTelefono(
      {TurnoBloc bloc,
      String label = "Nro. de teléfono",
      String hint = "#########",
      String value,
      bool streamBuilder = true,
      Function(String) evtOnChanged,
      TextEditingController controller}) {
    if (streamBuilder) {
      return textField(
          fieldStream: bloc.nroTelefonoStream,
          evtOnChanged:
              (evtOnChanged != null) ? evtOnChanged : bloc.changeNroTelefono,
          label: label,
          hint: hint,
          text: value,
          withStreamBuilder: streamBuilder,
          controller: controller);
    } else {
      return textField(
          evtOnChanged: evtOnChanged,
          label: label,
          hint: hint,
          text: value,
          withStreamBuilder: streamBuilder,
          controller: controller);
    }
  }

  Widget turnoCorreoElectronico(
      {TurnoBloc bloc,
      String value,
      bool streamBuilder = true,
      Function(String) evtOnChanged,
      TextEditingController controller}) {
    if (streamBuilder) {
      return emailField(
          fieldStream: bloc.emailStream,
          evtOnChanged:
              (evtOnChanged != null) ? evtOnChanged : bloc.changeEmail,
          text: value,
          withStreamBuilder: streamBuilder,
          controller: controller);
    } else {
      return emailField(
          evtOnChanged: evtOnChanged,
          text: value,
          withStreamBuilder: streamBuilder,
          controller: controller);
    }
  }

  ///

  Widget campoTitulo(
      {TurnoBloc bloc,
      TextEditingController controller,
      Function evtOnChanged}) {
    Widget rWidget;
    if (controller != null) {
      rWidget = Container(
          margin: EdgeInsets.only(bottom: 10),
          child: TextField(
              controller: controller,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                  labelText: 'Título de la tarea',
                  hintText: '---',
                  border: InputBorder.none,
                  errorStyle: TextStyle(height: 1)),
              onChanged: evtOnChanged));
    } else {
      rWidget = StreamBuilder(
          stream: bloc.tituloStream,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            return Container(
                margin: EdgeInsets.only(bottom: 10),
                child: TextField(
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                      labelText: 'Título de la tarea',
                      hintText: '---',
                      border: InputBorder.none,
                      //counterText: snapshot.data,
                      errorText: snapshot.error,
                      errorStyle: TextStyle(height: 1)),
                  onChanged: bloc.changeTitulo,
                ));
          });
    }

    return rWidget;
  }

  Widget campoDosificacion({TurnoBloc bloc}) {
    return StreamBuilder(
        stream: bloc.dosificacionStream,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return Container(
              margin: EdgeInsets.only(bottom: 10),
              child: TextField(
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                    labelText: 'Dosis de la medicación',
                    hintText: '---',
                    border: InputBorder.none,
                    errorText: snapshot.error,
                    errorStyle: TextStyle(height: 1)),
                onChanged: bloc.changeDosificacion,
              ));
        });
  }

  Widget campoDescripcion(
      {TurnoBloc bloc,
      int lines = 1,
      TextEditingController controller,
      Function evtOnChanged}) {
    Widget rWidget;
    if (controller != null) {
      rWidget = Container(
          margin: EdgeInsets.only(bottom: 10),
          child: TextField(
            controller: controller,
            keyboardType:
                (lines > 1) ? TextInputType.multiline : TextInputType.text,
            maxLines: lines,
            decoration: InputDecoration(
                labelText: 'Descripción adicional',
                alignLabelWithHint: true,
                hintText: '---',
                border: InputBorder.none,
                errorStyle: TextStyle(height: 1)),
            onChanged: evtOnChanged,
          ));
    } else {
      rWidget = StreamBuilder(
          stream: bloc.descripcionStream,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            return Container(
                margin: EdgeInsets.only(bottom: 10),
                child: TextField(
                  keyboardType: (lines > 1)
                      ? TextInputType.multiline
                      : TextInputType.text,
                  maxLines: lines,
                  decoration: InputDecoration(
                      labelText: 'Descripción adicional',
                      alignLabelWithHint: true,
                      hintText: '---',
                      border: InputBorder.none,
                      errorText: snapshot.error,
                      errorStyle: TextStyle(height: 1)),
                  onChanged: bloc.changeDescripcion,
                ));
          });
    }
    return rWidget;
  }

  Widget inputWrapperHelper({Widget child, BuildContext context, width}) {
    return new Container(
      margin: EdgeInsets.symmetric(horizontal: 0.0),
      padding: EdgeInsets.only(left: 10, right: 10),
      width: width,
      child: Container(
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                const Color.fromRGBO(230, 245, 252, 1),
                const Color(0xFFFFFFFF)
              ],
            ),
            border: Border(bottom: BorderSide(width: 2.0, color: scAzul)),
          ),
          child: child,
        )
      )
    );
  }

  Widget inputWrapper(
      {Widget field,
      bool withBoxShadow = false,
      double paddingRight = 15,
      double paddingLeft = 15}) {
    List<BoxShadow> listBoxShadow = [];
    if (withBoxShadow) {
      listBoxShadow.add(BoxShadow(
          color: Color.fromRGBO(100, 100, 100, 0.4),
          offset: new Offset(0.0, 1.0),
          blurRadius: 3.0));
    }

    final BoxDecoration bgInputWrapper = BoxDecoration(
      color: Colors.white,
      gradient: LinearGradient(
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
        colors: [
          const Color.fromRGBO(230, 245, 252, 1),
          const Color(0xFFFFFFFF)
        ],
      ),
      boxShadow: listBoxShadow,
      border: Border(bottom: BorderSide(width: 2.0, color: scAzul)),
    );

    return Stack(
      children: <Widget>[
        Container(
            margin: EdgeInsets.symmetric(horizontal: 0.0),
            padding: EdgeInsets.only(left: paddingLeft, right: paddingRight),
            child: Container(
                child: Container(
              decoration: bgInputWrapper,
              child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 10.0), child: field),
            ))),
      ],
    );
  }

  Widget crearBotoneraRegistro({List<Widget> listButtons}) {
    return Container(
        margin: EdgeInsets.only(right: 0.0, bottom: 5.0, left: 45.0),
        width: size.width - 80,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.end,
          children: listButtons,
        ));
  }

  Widget botonCuadrado({IconData icon, Function evtOnPressed}) {
    final BoxDecoration boxDeco = BoxDecoration(
      boxShadow: [shadowGris],
      borderRadius: BorderRadius.circular(10.0),
    );

    return Container(
        width: 55.0,
        decoration: boxDeco,
        child: ButtonTheme(
            minWidth: 50.0,
            height: 55.0,
            child: RaisedButton(
                child: Container(
                    child: FaIcon(
                  icon,
                  size: 24.0,
                )),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
                elevation: 0.0,
                color: colorBtnPrimary,
                textColor: Colors.white,
                onPressed: evtOnPressed)));
  }

  Widget crearBotonTexto(
      {String caption,
      IconData icon,
      double textSize = 0.0,
      double maxSize = 0.0,
      Color color,
      bool underline = false,
      bool iconRight = false,
      Function evtOnTap}) {
    TextStyle textStyle = TextStyle(
      fontSize: (textSize > 0.0) ? textSize : 14,
      color: (color == null) ? scAzul : color,
      fontWeight: FontWeight.bold,
      decoration: (underline) ? TextDecoration.underline : TextDecoration.none,
    );

    Container fieldContainer =
        Container(child: Text(caption, style: textStyle));
    if (maxSize > 0) {
      fieldContainer =
          Container(width: maxSize, child: Text(caption, style: textStyle));
    }

    List<Widget> buttonElements = [
      SizedBox(
          width: 20.0,
          height: 24.0,
          child: Align(
              alignment: Alignment.center,
              child: Icon(
                icon,
                size: 12,
                color: (color == null) ? scAzul : color,
              ))),
      fieldContainer
    ];
    MainAxisAlignment buttonElementsAlignment = MainAxisAlignment.start;
    if (iconRight) {
      buttonElements = [
        fieldContainer,
        SizedBox(
            width: 20.0,
            height: 24.0,
            child: Align(
                alignment: Alignment.center,
                child: Icon(
                  icon,
                  size: 12,
                  color: (color == null) ? scAzul : color,
                ))),
      ];
      buttonElementsAlignment = MainAxisAlignment.end;
    }

    return InkWell(
      child: Container(
          padding: EdgeInsets.symmetric(vertical: 4),
          child: Row(
            mainAxisAlignment: buttonElementsAlignment,
            children: buttonElements,
          )),
      onTap: evtOnTap,
    );
  }

  Widget safeArea(double pHeight) {
    return SafeArea(
      child: Container(
          margin: EdgeInsets.all(0.0),
          padding: EdgeInsets.all(0.0),
          height: pHeight),
    );
  }

  Widget customDrawerNavItem(
      {IconData icon, String text, Function evtPressed}) {
    return Align(
        alignment: Alignment.centerLeft,
        heightFactor: 0.7,
        child: ButtonTheme(
            child: FlatButton(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                child: Align(
                    alignment: Alignment.centerLeft,
                    child: FaIcon(icon, color: Colors.white, size: 14.0)),
                width: 30.0,
                height: 30.0,
              ),
              Text(text,
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 14.0)),
            ],
          ),
          onPressed: evtPressed,
        )));
  }

  Widget userProfileBox(
      {String nombreCompleto,
      Function evtPressed,
      String userProfilePic = '',
      double profileComplete = 0}) {
    ImageProvider profileImage = new AssetImage(noProfilePic);
    if (userProfilePic.length > 0) {
      profileImage = (isUri(userProfilePic))
          ? NetworkImage(userProfilePic, scale: 1)
          : FileImage(File(userProfilePic));
      // (isUri(userPrefs.imagenPerfil))?NetworkImage(userProfilePic):FileImage(File(userProfilePic)),
    }

    return Align(
        alignment: Alignment.centerLeft,
        child: Container(
          width: UiHelper.size.width * 0.7,
          margin: EdgeInsets.only(top: 20.0, right: 40.0, left: 20.0),
          child: FlatButton(
            padding: EdgeInsets.all(0.0),
            child: Row(
              children: <Widget>[
                Container(
                    margin: EdgeInsets.only(right: 10.0),
                    padding: EdgeInsets.all(0.0),
                    height: 70.0,
                    width: 70.0,
                    child: ClipRRect(
                        borderRadius: BorderRadius.circular(5.0),
                        child: AspectRatio(
                            aspectRatio: 1,
                            child: Image(
                              image: profileImage,
                              fit: BoxFit.cover,
                            )))),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      nombreCompleto,
                      textAlign: TextAlign.left,
                      style: TextStyle(color: Colors.white, fontSize: 14.0),
                    ),
                    Padding(
                        padding: EdgeInsets.only(top: 2.0, bottom: 5.0),
                        child: Text('Ver y editar el perfil',
                            style: TextStyle(color: UiHelper.scCeleste))),
                    Stack(children: <Widget>[
                      Container(
                        decoration: BoxDecoration(color: UiHelper.scAzulOscuro),
                        height: 10.0,
                        width: 100.0,
                      ),
                      Container(
                        decoration: BoxDecoration(color: UiHelper.scCeleste),
                        height: 10.0,
                        width: profileComplete,
                      )
                    ]),
                    Padding(
                        padding: EdgeInsets.only(top: 5.0),
                        child: Text('$profileComplete% del perfil completado',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                color: Colors.grey,
                                fontWeight: FontWeight.bold,
                                fontSize: 11.0)))
                  ],
                ),
              ],
            ),
            onPressed: evtPressed,
          ),
        ));
  }

  Widget userPlanBox({Function evtOnpressed, List<dynamic> planes}) {
    List<Widget> listaPlanes = new List<Widget>();
    for (var item in planes) {
      listaPlanes.add(
        boxPlan(
            title: item['desc_plan'],
            icon: FontAwesomeIcons.users,
            color: Colors.brown),
      );
    }
    listaPlanes.add(Text('Ver detalles de la cobertura',
        textAlign: TextAlign.left,
        style: TextStyle(
            color: UiHelper.scCeleste,
            fontWeight: FontWeight.bold,
            fontSize: 13.0)));

    return Align(
        alignment: Alignment.centerLeft,
        child: Container(
            width: this.getSize.width * 0.7,
            margin: EdgeInsets.only(right: 40.0, left: 20.0),
            child: FlatButton(
              padding: EdgeInsets.all(0.0),
              child: Container(
                padding: EdgeInsets.all(10.0),
                decoration: BoxDecoration(
                    color: Color.fromRGBO(0, 0, 0, 0.2),
                    borderRadius: BorderRadius.circular(10.0),
                    border: Border.all(color: scAzulOscuroBorde, width: 3.0)),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: listaPlanes,
                ),
              ),
              onPressed: evtOnpressed,
            )));
  }

  Widget boxPlan({String title, IconData icon, Color color}) {
    return Container(
      padding: EdgeInsets.only(top: 5.0, bottom: 5.0, right: 10.0, left: 10.0),
      margin: EdgeInsets.only(bottom: 5.0),
      decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(5.0),
          border: Border.all(color: scCeleste, width: 1.0)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(
              width: 10.0,
              child:
                  Icon(FontAwesomeIcons.users, size: 12, color: Colors.white)),
          SizedBox(width: 10.0, height: 10.0),
          Text(title,
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 11.0)),
        ],
      ),
    );
  }

  Widget separadorDrawer() {
    return Container(
      margin: EdgeInsets.only(right: 40.0, left: 20.0, top: 20.0, bottom: 20),
      width: this.getSize.width * 0.7,
      height: 2.0,
      decoration: BoxDecoration(color: UiHelper.scAzulOscuro),
    );
  }

  Widget scAppBar(UserPreferences userPrefs) {
    final logoSantaClaraAppBar = Image(
      image: new AssetImage('assets/images/logo-santa-clara.png'),
      width: (this.getSize.width * 0.10),
      fit: BoxFit.contain,
    );

    final customLeadingIconButton = Builder(
      builder: (BuildContext context) {
        return IconButton(
          icon: const Icon(FontAwesomeIcons.bars),
          onPressed: () {
            Scaffold.of(context).openDrawer();
          },
          tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
        );
      },
    );

    return PreferredSize(
        preferredSize: Size.fromHeight(70.0),
        child: Container(
            padding: EdgeInsets.only(top: 10.0, bottom: 10.0, left: 45.0),
            child: AppBar(
                elevation: 0.0,
                titleSpacing: 0.0,
                automaticallyImplyLeading: true,
                leading: customLeadingIconButton,
                title: Container(
                    //color:UiHelper.greenContainer,
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                        child: Text(
                      'Hola, ' + userPrefs.nombreCompleto,
                      style: TextStyle(
                        color: UiHelper.scAzulOscuro,
                        fontSize: 14.0,
                      ),
                    )),
                    Padding(
                        padding: EdgeInsets.only(right: 10.0),
                        child: SizedBox(
                            width: 100.0, child: logoSantaClaraAppBar)),
                  ],
                )))));
  }

  // *****************************
  // dashboard
  // *****************************

  Widget campoBuscarDashboard(
      {String caption = 'Qué estás buscando...?',
      Function evtOnPressed,
      MainBloc bloc}) {
    return StreamBuilder(
        stream: bloc.buscarStream,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return TextField(
            decoration: InputDecoration(
                suffix: SizedBox(
                  child: IconButton(
                    icon: Icon(FontAwesomeIcons.search,
                        size: 18.0, color: scAzul),
                    onPressed: evtOnPressed,
                  ),
                  width: 30.0,
                  height: 20.0,
                ),
                labelText: caption,
                border: InputBorder.none,
                errorText: snapshot.error),
            onChanged: bloc.changeBuscar,
          );
        });
  }

  Widget campoBuscarResultados(
      {String caption = 'Filtrar por código',
        Function evtOnPressed,
        MainBloc bloc}) {
    return StreamBuilder(
        stream: bloc.buscarStream,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return TextField(
            decoration: InputDecoration(
                suffix: SizedBox(
                  child: IconButton(
                    icon: Icon(FontAwesomeIcons.search,
                        size: 18.0, color: scAzul),
                    onPressed: evtOnPressed,
                  ),
                  width: 30.0,
                  height: 20.0,
                ),
                labelText: caption,
                border: InputBorder.none,
                errorText: snapshot.error),
            onChanged: bloc.changeBuscar,
          );
        });
  }

  Widget boxDashboard({IconData icon, String text, onTap}) {
    return new GestureDetector(
      onTap: onTap,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 5.0),
        constraints: new BoxConstraints(
          minHeight: 80,
        ),
        decoration: BoxDecoration(
          border:
          Border.all(color: Color.fromRGBO(138, 158, 196, 1.0), width: 1.0),
          color: Colors.white,
          borderRadius: BorderRadius.circular(5.0),
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(0, 159, 227, 0.25),
              offset: new Offset(0.0, 0.0),
              blurRadius: 10.0
            )
          ],
        ),
        child: Padding(
            padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 35.0,
                  child: Icon(
                    icon,
                    color: UiHelper.scAzul,
                    size: 30.0,
                  )
                ),
                Padding(
                    padding: EdgeInsets.only(top: 5.0),
                    child: FittedBox(
                      fit: BoxFit.cover,
                      child: Text(text,
                        style: TextStyle(
                          color: Colors.grey,
                          fontWeight: FontWeight.bold,
                        )
                      )
                    )
                )
              ],
            )),
      )
    );
  }

  Widget badgetItems({double items, Color color}) {
    String formatedItems =
        items.toStringAsFixed(items.truncateToDouble() == items ? 0 : 2);
    return Positioned(
      right: 10.0,
      top: 0.0,
      child: Container(
          decoration: BoxDecoration(
              color: color, borderRadius: BorderRadius.circular(20.0)),
          width: 20.0,
          height: 20.0,
          child: Align(
              alignment: Alignment.center,
              child: Text(formatedItems,
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 12.0)))),
    );
  }

  Widget cardPlanes(
      {Color color,
      String title,
      String url,
      bool lastItem = false,
      Function evtOnPressed}) {
    return Container(
        width: 210.0,
        margin: EdgeInsets.only(
            left: 15.0, top: 5.0, bottom: 5.0, right: (lastItem) ? 10.0 : 0.0),
        decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.2),
                  offset: Offset(0, 2),
                  blurRadius: 2.0),
            ],
            borderRadius: BorderRadius.circular(5.0)),
        child: Column(
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                // foto-icono-plan
                Container(
                    width: 60.0,
                    height: 58.0,
                    padding: EdgeInsets.all(10.0),
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(width: 2.0, color: color),
                          borderRadius: BorderRadius.circular(5.0)),
                      child: CircleAvatar(
                          radius: 20,
                          backgroundImage: NetworkImage(url)
                      ),
                    )),

                // titulo-plan
                Flexible(
                  child: Padding(
                    padding: EdgeInsets.only(right: 10.0),
                    child: Text(
                      title,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 14.0,
                        color: Color.fromRGBO(100, 100, 100, 1),
                      ),
                    ),
                  ),
                ),
              ],
            ),

            // footer cardPlan
            cardFooterPlanes(color: color, evtOnPressed: evtOnPressed),
          ],
        ));
  }

  Widget cardFooterPlanes({Function evtOnPressed, Color color}) {
    return Container(
      child: Stack(
        children: <Widget>[
          Container(
              height: 32.0,
              width: 210.0,
              decoration: BoxDecoration(
                  color: color.withOpacity(0.5),
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(5.0),
                    bottomRight: Radius.circular(5.0),
                  ))),
          Container(height: 2.0, color: color),
          Align(
              alignment: Alignment.centerRight,
              child: crearBotonTexto(
                  evtOnTap: evtOnPressed,
                  caption: 'Ver más',
                  icon: FontAwesomeIcons.angleDoubleRight,
                  color: Colors.white,
                  textSize: 12.0,
                  iconRight: true)),
        ],
      ),
    );
  }

  Widget headerSliderPlanes(
      {String title, Function evtOnPressed, bool withActionButton = true}) {
    return Container(
      padding: EdgeInsets.only(top: 10.0, bottom: 5.0),
      child: Row(children: <Widget>[
        Expanded(
          child: Padding(
              padding: EdgeInsets.only(left: 20.0),
              child: Text(title,
                  style: TextStyle(
                      color: UiHelper.scGris, fontWeight: FontWeight.bold))),
        ),
        Align(
          alignment: Alignment.centerLeft,
          heightFactor: 0.4,
          child: (withActionButton)
              ? FlatButton(
                  onPressed: evtOnPressed,
                  child: Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 10.0, vertical: 3.0),
                      margin: EdgeInsets.all(0.0),
                      decoration: BoxDecoration(
                        color: UiHelper.scCeleste,
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Text('VER TODOS',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 10.0))))
              : null,
        ),
      ]),
    );
  }

  ///***********************
  /// widget card turno
  ///***********************

  Widget rowTitleTagCard(
      {String title,
      String tag = "",
      Color tagColor = const Color.fromRGBO(153, 153, 153, 1)}) {
    return Container(
      margin: EdgeInsets.only(top: 10.0),
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      child:
          Row(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
        Flexible(
          flex: (tag.length > 0) ? 7 : 1,
          fit: FlexFit.tight,
          child: Container(
              padding: EdgeInsets.symmetric(vertical: 3.0),
              child: Container(
                  child: Text(
                title,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0),
                overflow: TextOverflow.ellipsis,
                softWrap: true,
                maxLines: 1,
                textDirection: TextDirection.ltr,
              ))),
        ),
        (tag.length > 0)
            ? Flexible(
                flex: 5,
                fit: FlexFit.tight,
                child: Container(
                  child: Align(
                      alignment: Alignment.centerRight,
                      child: ButtonTheme(
                        child: Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 10.0, vertical: 4.0),
                            decoration: BoxDecoration(
                              color: tagColor,
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child: Text(
                              tag,
                              style: TextStyle(
                                  color: Colors.white, fontSize: 11.0),
                              maxLines: 1,
                              softWrap: true,
                              overflow: TextOverflow.ellipsis,
                              textDirection: TextDirection.ltr,
                            )),
                      )),
                ),
              )
            : Container(child: null),
      ]),
    );
  }

  Widget rowTitleCard({
    String title,
  }) {
    return Container(
      margin: EdgeInsets.only(top: 10.0),
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      child:
          Row(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
        Flexible(
          flex: 1,
          fit: FlexFit.tight,
          child: Container(
              padding: EdgeInsets.symmetric(vertical: 3.0),
              child: Container(
                  child: Text(title,
                      style:
                          TextStyle(color: UiHelper.scGris, fontSize: 12.0)))),
        ),
      ]),
    );
  }

  Widget rowBeneficiarioCard({
    String text,
  }) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 10.0),
        width: double.infinity,
        child: Text(text,
            style: TextStyle(
                color: UiHelper.scAzul,
                fontWeight: FontWeight.bold,
                fontSize: 12.0)));
  }

  Widget rowDireccionCard({String text}) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 10.0),
        width: double.infinity,
        child: Text(
          text,
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0),
          overflow: TextOverflow.ellipsis,
          softWrap: true,
          maxLines: 1,
          textDirection: TextDirection.ltr,
        ));
  }

  Widget btnActionCard(
      {IconData icon, Color iconColor, Color btnColor, Function evtOnPressed}) {
    return Padding(
        padding: EdgeInsets.only(left: 5.0, top: 3.0, bottom: 3.0),
        child: ButtonTheme(
            child: Align(
                heightFactor: 0.7,
                widthFactor: 0.55,
                child: IconButton(
                  padding: EdgeInsets.all(0.0),
                  icon: Container(
                      height: 24.0,
                      width: 24.0,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          color: btnColor),
                      child: Icon(
                        icon,
                        color: iconColor,
                        size: 12.0,
                      )),
                  onPressed: evtOnPressed,
                ))));
  }

  Widget footerCitaCard({String pTime, String pDate, List<Widget> actionList}) {
    final BoxDecoration boxBottomLeftFooter = BoxDecoration(
      border: Border(
          top: BorderSide(color: Color.fromRGBO(170, 228, 253, 1), width: 1.0),
          right:
              BorderSide(color: Color.fromRGBO(170, 228, 253, 1), width: 1.0)),
    );

    return Container(
        margin: EdgeInsets.only(top: 10.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Flexible(
              flex: 5,
              fit: FlexFit.tight,
              child: Container(
                height: 40.0,
                padding: EdgeInsets.symmetric(horizontal: 10.0),
                decoration: boxBottomLeftFooter,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(pTime,
                          style: TextStyle(
                              fontSize: 12.0,
                              color: UiHelper.scGris,
                              fontWeight: FontWeight.bold)),
                      Text(pDate,
                          style: TextStyle(
                              fontSize: 10.0, color: UiHelper.scGris)),
                    ]),
              ),
            ),
            Flexible(
                flex: 4,
                fit: FlexFit.tight,
                child: Container(
                    height: 40.0,
                    padding: EdgeInsets.symmetric(horizontal: 10.0),
                    decoration: BoxDecoration(
                      border: Border(
                        top: BorderSide(
                            color: Color.fromRGBO(170, 228, 253, 1),
                            width: 1.0),
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: actionList,
                    ))),
          ],
        ));
  }

  Widget customCitaCard(
      {String title,
      String tag,
      String beneficiario,
      String lugar,
      String time,
      String date,
      bool actionLocation = true,
      bool actionCancel = true,
      bool actionAlert = true,
      BuildContext ctx,
      Function evtOnTap}) {
    List<Widget> acciones = [];
    if (actionAlert) {
      acciones.add(this.btnActionCard(
        icon: FontAwesomeIcons.solidBell,
        iconColor: UiHelper.scAzulOscuro,
        btnColor: UiHelper.scBgActionAlert,
        evtOnPressed: () {},
      ));
    }
    if (actionLocation) {
      acciones.add(this.btnActionCard(
        icon: FontAwesomeIcons.mapMarkedAlt,
        iconColor: UiHelper.scAzulOscuro,
        btnColor: UiHelper.scBgActionLocation,
        evtOnPressed: () {},
      ));
    }

    if (actionCancel) {
      acciones.add(this.btnActionCard(
        icon: FontAwesomeIcons.ban,
        iconColor: Colors.white,
        btnColor: UiHelper.scAzulOscuro,
        evtOnPressed: () {},
      ));
    }

    return InkWell(
      child: Container(
        margin: EdgeInsets.only(left: 15.0, top: 5.0, bottom: 5.0, right: 15.0),
        decoration: boxDecoCitaCard,
        child: Column(
          children: <Widget>[
            // titulo+tag
            rowTitleTagCard(title: title, tag: tag),

            // beneficiario
            rowBeneficiarioCard(text: beneficiario),

            // dirección
            rowDireccionCard(text: lugar),

            // footer-fecha-botonera-acciones
            footerCitaCard(
              pTime: time,
              pDate: date,
              actionList: acciones,
            ),
          ],
        ),
      ),
      onTap: evtOnTap,
    );
  }

  Widget customTareaCard(
      {String title,
      String beneficiario,
      String lugar,
      String tag = "",
      String time,
      String date,
      bool actionLocation = true,
      bool actionCancel = true,
      bool actionAlert = true,
      BuildContext ctx,
      Function evtOnTap}) {
    List<Widget> acciones = [];
    if (actionAlert) {
      acciones.add(this.btnActionCard(
        icon: FontAwesomeIcons.solidBell,
        iconColor: UiHelper.scAzulOscuro,
        btnColor: UiHelper.scBgActionAlert,
        evtOnPressed: () {},
      ));
    }
    if (actionLocation) {
      acciones.add(this.btnActionCard(
        icon: FontAwesomeIcons.mapMarkedAlt,
        iconColor: UiHelper.scAzulOscuro,
        btnColor: UiHelper.scBgActionLocation,
        evtOnPressed: () {},
      ));
    }

    if (actionCancel) {
      acciones.add(this.btnActionCard(
        icon: FontAwesomeIcons.ban,
        iconColor: Colors.white,
        btnColor: UiHelper.scAzulOscuro,
        evtOnPressed: () {},
      ));
    }

    return InkWell(
      child: Container(
        margin: EdgeInsets.only(left: 15.0, top: 5.0, bottom: 5.0, right: 15.0),
        decoration: boxDecoCitaCard,
        child: Column(
          children: <Widget>[
            // titulo+tag
            rowTitleTagCard(title: title, tag: tag),

            // beneficiario
            rowBeneficiarioCard(text: beneficiario),

            // dirección
            rowDireccionCard(text: lugar),

            // footer-fecha-botonera-acciones
            footerCitaCard(
              pTime: time,
              pDate: date,
              actionList: acciones,
            ),
          ],
        ),
      ),
      onTap: evtOnTap,
    );
  }

  ///***********************
  /// widget datos-usuario
  ///***********************

  Widget separadorCampos(
      {IconData icon,
      String title,
      double marginTop = 20.0,
      Widget actionButton}) {
    EdgeInsetsGeometry margin = EdgeInsets.only(top: marginTop, left: 1.0);

    final Widget btnAction = (actionButton != null)
        ? actionButton
        : SizedBox(
            width: 1,
          );

    return Container(
      color: Color.fromRGBO(246, 246, 246, 1),
      margin: margin,
      padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
      child: Row(
        children: [
          SizedBox(
              width: 30.0, child: Icon(icon, color: UiHelper.scAzul, size: 16)),
          Expanded(
            flex: 1,
              child: Text(title,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0,
                      color: UiHelper.scAzul))),
          btnAction
        ],
      ),
    );
  }

  Widget btnCustomPageViewNav(
      {String caption, bool activo = false, Function evtOnPressed}) {
    return ButtonTheme(
      padding: EdgeInsets.all(0.0),
      child: FlatButton(
          onPressed: evtOnPressed,
          child: Container(
            margin: EdgeInsets.zero,
            padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 15.0),
            decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(
                        color: (activo)
                            ? UiHelper.scAzulOscuro
                            : Colors.transparent,
                        width: 3.0))),
            child: Text(
              caption,
              style: TextStyle(
                  fontWeight: (activo) ? FontWeight.bold : FontWeight.normal,
                  fontSize: 12.0),
            ),
          )),
    );
  }

  ///******************************
  /// widget item-miembros-familia
  ///******************************
  Widget itemGrupoFamiliar({
    String title,
    String data,
    String dataID,
    bool inactive = false,
    bool altBg = false,
    GestureTapCallback evtOnTap,
  }) {
    final Widget activeTrailing =
        Icon(FontAwesomeIcons.solidCheckCircle, color: Colors.green, size: 24);

    final Widget inactiveTrailing = Container(
        width: 24,
        height: 24,
        decoration: BoxDecoration(
            color: Color.fromRGBO(150, 150, 150, 1),
            borderRadius: BorderRadius.circular(24.0)),
        child: Icon(FontAwesomeIcons.ban,
            color: Color.fromRGBO(60, 60, 60, 1), size: 14));

    return Container(
      padding: EdgeInsets.symmetric(vertical: 10.0),
      margin: EdgeInsets.zero,
      decoration: BoxDecoration(
          color:
              (altBg) ? Color.fromRGBO(249, 249, 249, 1) : Colors.transparent,
          border: Border(
              bottom: BorderSide(
                  color: Color.fromRGBO(233, 233, 233, 1), width: 1.0))),
      child: ListTile(
        onTap: evtOnTap,
        trailing: (inactive) ? inactiveTrailing : activeTrailing,
        leading: Container(
            width: 40.0,
            height: 40.0,
            decoration: BoxDecoration(
              color: Color.fromRGBO(235, 255, 229, 1),
              borderRadius: BorderRadius.circular(40.0),
              border: Border.all(color: UiHelper.scAzul, width: 2.0),
            ),
            child: Icon(FontAwesomeIcons.solidUser,
                color: Color.fromRGBO(80, 80, 80, 1), size: 16)),
        title: Text(title,
            style: TextStyle(fontSize: 16.0, color: UiHelper.scAzul)),
        subtitle:
            Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text(data,
              style: TextStyle(
                  fontSize: 11.0, color: Color.fromRGBO(90, 90, 90, 1))),
          Text(dataID,
              style: TextStyle(
                  fontSize: 12.0,
                  color: Color.fromRGBO(90, 90, 90, 1),
                  fontWeight: FontWeight.bold)),
        ]),
      ),
    );
  }

  Widget itemTransaccion(
      {String title,
      String data,
      String dataID,
      bool altBg = false,
      bool inactive = false,
      EstadoTsx status = EstadoTsx.Pendiente,
      Function evtOnTap}) {
    BoxDecoration trailingBoxDeco = BoxDecoration(
        color: Colors.orangeAccent, borderRadius: BorderRadius.circular(24.0));
    Color iconColor = Color.fromRGBO(55, 55, 55, 1);
    switch (status) {
      case EstadoTsx.Pendiente:
        break;
      case EstadoTsx.Realizada:
        trailingBoxDeco = BoxDecoration(
            color: Colors.green, borderRadius: BorderRadius.circular(24.0));
        iconColor = Colors.white;
        break;
      case EstadoTsx.Vencida:
        trailingBoxDeco = BoxDecoration(
            color: Colors.red, borderRadius: BorderRadius.circular(24.0));
        iconColor = Colors.white;
        break;
    }

    final Widget itemTrailing = Container(
        width: 24,
        height: 24,
        decoration: trailingBoxDeco,
        child: Icon(FontAwesomeIcons.eye, color: iconColor, size: 14));

    return Container(
      padding: EdgeInsets.symmetric(vertical: 10.0),
      margin: EdgeInsets.zero,
      decoration: BoxDecoration(
          color:
              (altBg) ? Color.fromRGBO(249, 249, 249, 1) : Colors.transparent,
          border: Border(
              bottom: BorderSide(
                  color: Color.fromRGBO(233, 233, 233, 1), width: 1.0))),
      child: ListTile(
        trailing: itemTrailing,
        leading: Container(
            width: 40.0,
            height: 40.0,
            decoration: BoxDecoration(
              color: Color.fromRGBO(235, 255, 229, 1),
              borderRadius: BorderRadius.circular(40.0),
              border: Border.all(color: UiHelper.scAzul, width: 2.0),
            ),
            child: Icon(FontAwesomeIcons.moneyBillAlt,
                color: Color.fromRGBO(80, 80, 80, 1), size: 16)),
        title: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text(data,
              style: TextStyle(
                  fontSize: 11.0, color: Color.fromRGBO(90, 90, 90, 1))),
          Text(title,
              style: TextStyle(
                  fontSize: 16.0,
                  color: UiHelper.scAzul,
                  fontWeight: FontWeight.bold)),
          Text(dataID,
              style: TextStyle(
                  fontSize: 12.0,
                  color: Color.fromRGBO(90, 90, 90, 1),
                  fontWeight: FontWeight.bold)),
        ]),
        onTap: evtOnTap,
      ),
    );
  }

  // *****************************
  // agenda-turno
  // *****************************

  Widget headerLocation({String caption}) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(color: UiHelper.scSeparadorCeleste),
      padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
      child: Text(
        caption,
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0),
      ),
    );
  }

  Widget stepTurno({String step, String caption}) {
    return Container(
        padding: EdgeInsets.symmetric(vertical: 10.0),
        child: Row(
          children: [
            Flexible(
                flex: 2,
                child: Center(
                    child: Container(
                  height: 30.0,
                  width: 30.0,
                  decoration: BoxDecoration(
                    color: UiHelper.scAzul,
                    borderRadius: BorderRadius.circular(30.0),
                  ),
                  alignment: Alignment.center,
                  margin: EdgeInsets.zero,
                  child: Text(
                    step,
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                ))),
            Flexible(
                flex: 9,
                child: Container(
                    child: Text(caption,
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: UiHelper.scGris)))),
          ],
        ));
  }

  Widget campoBuscarProfesional(
      {String caption = 'Qué estas buscando...?',
      Function evtOnPressed,
      Function evtOnChanged,
      TurnoBloc bloc,
      TextEditingController controller}) {
    if (bloc != null) {
      return StreamBuilder(
          stream: bloc.buscarStream,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            return TextField(
              decoration: InputDecoration(
                  labelText: caption,
                  border: InputBorder.none,
                  errorText: snapshot.error),
              onChanged: bloc.changeBuscar,
            );
          });
    } else {
      return TextField(
        controller: controller,
        decoration:
            InputDecoration(labelText: caption, border: InputBorder.none),
        onChanged: (value) {
          evtOnChanged(value);
        },
      );
    }
  }

  Widget dropdownBeneficiario(
      {Map<String, String> listItems, Function evtOnChanged}) {
    Function _evtOnChanged = evtOnChanged;
    Map<String, String> listItems = {
      'OP1': 'Opción #1',
      'OP2': 'Opción #2',
      'OP3': 'Opción #3',
    };
    if (listItems.length > 0) {
      listItems = listItems;
    }

    AppProvider provider = new AppProvider();
    return FutureBuilder(
      future: provider.loadDataGrupoFamiliar(),
      initialData: [],
      builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
        listItems = new Map<String, String>();
        snapshot.data.forEach((item) {
          listItems[item['nro-documento']] = item['nombre-y-apellido'];
        });
        return DropdownButton<String>(
            hint: Text('Recordatorio previo'),
            isExpanded: true,
            icon: SizedBox(
                width: 24.0,
                child: Icon(
                  FontAwesomeIcons.angleDown,
                  color: UiHelper.scAzul,
                  size: 16,
                )),
            iconSize: 16,
            elevation: 16,
            style: TextStyle(color: Color.fromRGBO(70, 70, 70, 1)),
            underline: Container(color: Colors.transparent),
            onChanged: (opt) {
              _evtOnChanged(opt);
            },
            items: listItems
                .map((String _k, String _v) {
                  return MapEntry(
                      _k, DropdownMenuItem<String>(value: _k, child: Text(_v)));
                })
                .values
                .toList());
      },
    );
  }

  Widget dropdownOpciones({List<String> listItems}) {
    List<String> dropdownItems = [
      'Contrato #1',
      'Contrato #2',
      'Contrato #3',
    ];
    if (listItems.length > 0) {
      dropdownItems = listItems;
    }
    String dropdownValue = dropdownItems[0];

    return DropdownButton<String>(
      hint: Text('Recordatorio previo'),
      value: dropdownValue,
      isExpanded: true,
      icon: SizedBox(
          width: 24.0,
          child: Icon(
            FontAwesomeIcons.angleDown,
            color: UiHelper.scAzul,
            size: 16,
          )),
      iconSize: 16,
      elevation: 16,
      style: TextStyle(color: Color.fromRGBO(70, 70, 70, 1)),
      underline: Container(color: Colors.transparent),
      onChanged: (String newValue) {
        dropdownValue = newValue;
      },
      items: dropdownItems.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }

  Widget dropdownEspecialidades({Map<String, String> listItems}) {
    Map<String, String> dropdownItems = {
      '0': '---',
    };
    if (listItems.length > 0) {
      dropdownItems = listItems;
    }
    String dropdownValue = dropdownItems[0];

    return DropdownButton<String>(
        hint: Text('Recordatorio previo'),
        value: dropdownValue,
        isExpanded: true,
        icon: SizedBox(
            width: 24.0,
            child: Icon(
              FontAwesomeIcons.angleDown,
              color: UiHelper.scAzul,
              size: 16,
            )),
        iconSize: 16,
        elevation: 16,
        style: TextStyle(color: Color.fromRGBO(70, 70, 70, 1)),
        underline: Container(color: Colors.transparent),
        onChanged: (String newValue) {
          dropdownValue = newValue;
        },
        items: listItems
            .map((String _k, String _v) {
              return MapEntry(
                  _k, DropdownMenuItem<String>(value: _k, child: Text(_v)));
            })
            .values
            .toList());
  }

  Widget customProfesionalCard(
      {String title,
      String tag,
      String lugar,
      String telefono,
      bool btnCall = true,
      bool btnCheck = true,
      bool hasPicture = false,
      BuildContext ctx,
      bool selected = false,
      Function evtOnCheckPressed,
      Function evtOnCallpressed}) {
    List<Widget> acciones = [];
    double _boxMarginTop = 5.0;
    double _boxPaddingTop = 0.0;
    Widget _pictureBox = Container(child: null);

    if (btnCall) {
      acciones.add(this.btnActionCard(
        icon: FontAwesomeIcons.phone,
        iconColor: Colors.white,
        btnColor: Colors.green,
        evtOnPressed: evtOnCallpressed,
      ));
    }
    if (btnCheck) {
      acciones.add(this.btnActionCard(
        icon: FontAwesomeIcons.check,
        iconColor: (!selected) ? UiHelper.scGris : UiHelper.scCheckRojo,
        btnColor: (!selected) ? UiHelper.scBtnGris : Colors.white,
        evtOnPressed: evtOnCheckPressed,
      ));
    }

    if (hasPicture) {
      _boxMarginTop = 50.0;
      _boxPaddingTop = 15.0;
      _pictureBox = Align(
        child: Container(
            width: 70.0,
            height: 70.0,
            decoration: BoxDecoration(
              color: UiHelper.scBtnGris,
              border: Border.all(width: 3.0, color: UiHelper.scAzul),
              borderRadius: BorderRadius.circular(10.0),
            ),
            child:
                Icon(FontAwesomeIcons.user, color: UiHelper.scGris, size: 24)),
      );
    }

    return Stack(children: [
      Container(
        margin: EdgeInsets.only(
            left: 15.0, top: _boxMarginTop, bottom: 5.0, right: 15.0),
        decoration: boxDecoProfesionalCard,
        child: Column(
          children: <Widget>[
            // titulo+tag
            Padding(
                padding: EdgeInsets.only(top: _boxPaddingTop),
                child: rowTitleCard(title: title)),

            // dirección
            Padding(
              padding: EdgeInsets.only(top: 10.0),
              child: rowDireccionCard(text: lugar),
            ),

            // footer-fecha-botonera-acciones
            footerProfesionalCard(
              direccion: lugar,
              telefono: telefono,
              tag: tag,
              actionList: acciones,
            ),
          ],
        ),
      ),
      _pictureBox
    ]);
  }

  Widget footerProfesionalCard(
      {String direccion,
      String telefono,
      String tag,
      Color tagColor = const Color.fromRGBO(153, 153, 153, 1),
      List<Widget> actionList}) {
    final BoxDecoration boxBottomFooter = BoxDecoration(
      border: Border(
        top: BorderSide(color: Color.fromRGBO(230, 230, 230, 1), width: 1.0),
      ),
    );

    return Container(
        margin: EdgeInsets.only(top: 10.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Flexible(
              flex: 15,
              fit: FlexFit.tight,
              child: Container(
                height: 40.0,
                padding: EdgeInsets.symmetric(horizontal: 10.0),
                decoration: boxBottomFooter,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        child: Align(
                            alignment: Alignment.centerLeft,
                            child: ButtonTheme(
                              child: Container(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 10.0, vertical: 4.0),
                                  decoration: BoxDecoration(
                                    color: tagColor,
                                    borderRadius: BorderRadius.circular(15.0),
                                  ),
                                  child: Text(
                                    tag,
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 11.0),
                                    maxLines: 1,
                                    softWrap: true,
                                    overflow: TextOverflow.ellipsis,
                                    textDirection: TextDirection.ltr,
                                  )),
                            )),
                      ),
                      (telefono != null)
                          ? Text(telefono,
                              style: TextStyle(
                                  fontSize: 12.0,
                                  color: UiHelper.scAzul,
                                  fontWeight: FontWeight.bold))
                          : SizedBox(width: 10),
                    ]),
              ),
            ),
            Flexible(
                flex: 5,
                fit: FlexFit.tight,
                child: Container(
                    height: 40.0,
                    padding: EdgeInsets.only(right: 10.0),
                    decoration: boxBottomFooter,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: actionList,
                    ))),
          ],
        ));
  }

  Widget headerWizardTurno({String title, String step}) {
    List<Widget> listWidgets = [];
    if(step.length > 0){
      listWidgets = [
        Flexible(
          flex: 2,
          child: Center(
              child: Container(
              height: 30.0,
              width: 30.0,
              decoration: BoxDecoration(
                color: UiHelper.scAzul,
                borderRadius: BorderRadius.circular(30.0),
              ),
              alignment: Alignment.center,
              margin: EdgeInsets.zero,
              child: Text(
                step,
                style: TextStyle(
                    fontSize: 16,
                    color: Colors.white,
                    fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
            ))),
          Flexible(
            flex: 9,
            child: Container(
              child: Text(title,
                  style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      color: UiHelper.scGris)))),
      ];
    }else{
      listWidgets = [
        Flexible(
          flex: 1,
          child: Container(
              padding: EdgeInsets.only(left:15.0),
              child: Text(title,
                  style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      color: UiHelper.scGris)))),
      ];
    }
    return Container(
        padding: EdgeInsets.symmetric(vertical: 10.0),
        child: Row( children: listWidgets )
    );
  }

  Widget iconBoxButton(
      {IconData icon,
      String label,
      Function evtOnTap,
      double iconSize = 16,
      double width = 55,
      double height = 55,
      Color color}) {
    Color iconBoxColor = (color != null) ? color : scAzul;
    List<Widget> _children = [
      Padding(
          padding: EdgeInsets.symmetric(vertical: 5.0),
          child: Icon(
            icon,
            color: Colors.white,
            size: iconSize,
          ))
    ];
    if (label.length > 0) {
      _children = [
        Padding(
            padding: EdgeInsets.symmetric(vertical: 5.0),
            child: Icon(
              icon,
              color: Colors.white,
              size: iconSize,
            )),
        Text(label, style: TextStyle(color: Colors.white, fontSize: 11))
      ];
    }

    Widget rWidget;
    if (evtOnTap != null) {
      rWidget = InkWell(
        onTap: evtOnTap,
        child: Container(
            width: width,
            height: height,
            margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 10.0),
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
            decoration: BoxDecoration(
              color: iconBoxColor,
              borderRadius: BorderRadius.circular(10.0),
              boxShadow: [
                BoxShadow(
                    color: Colors.grey, offset: Offset(1, 1), blurRadius: 1.0),
              ],
            ),
            child: Column(children: _children)),
      );
    } else {
      rWidget = InkWell(
        onTap: evtOnTap,
        child: Container(
            width: width,
            height: height,
            margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 10.0),
            padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
            decoration: BoxDecoration(
              color: scGrisTexto,
              borderRadius: BorderRadius.circular(10.0),
              boxShadow: [
                BoxShadow(
                    color: Colors.grey, offset: Offset(1, 1), blurRadius: 1.0),
              ],
            ),
            child: Column(children: _children)),
      );
    }

    return rWidget;
  }

  Widget btnAccionVisor({String label, Function evtOnTap, IconData icon}) {
    Widget rWidget = InkResponse(
        onTap: evtOnTap,
        child: Container(
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
          decoration: BoxDecoration(
              color: UiHelper.scAzul,
              borderRadius: BorderRadius.circular(10.0)),
          child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
            SizedBox(
                width: 20.0,
                height: 15.0,
                child: Icon(icon, color: Colors.white, size: 10.0)),
            Text(label, style: TextStyle(color: Colors.white, fontSize: 12))
          ]),
        ));
    return rWidget;
  }

  Widget tagVisor({String label}) {
    final BoxDecoration boxDeco = BoxDecoration(
        color: UiHelper.scCheckGris,
        borderRadius: BorderRadius.circular(15.0),
        border: Border.all(color: Colors.grey));

    Widget rWidget = Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
      decoration: boxDeco,
      child: Row(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              label,
              style: TextStyle(
                  color: UiHelper.scGrisTexto,
                  fontSize: 10,
                  fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            )
          ]),
    );
    return rWidget;
  }

  Widget btnLimpiarSeleccion({String label, Function evtOnTap}) {
    Widget rWidget = InkResponse(
        onTap: evtOnTap,
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 3.0),
          decoration: BoxDecoration(
              color: UiHelper.scAzul,
              borderRadius: BorderRadius.circular(10.0)),
          child: Row(children: [
            SizedBox(
                width: 14.0,
                child: Icon(FontAwesomeIcons.solidCheckCircle,
                    color: Colors.white, size: 10.0)),
            Text(label, style: TextStyle(color: Colors.white, fontSize: 10))
          ]),
        ));
    return rWidget;
  }

  Widget btnTag({String label, Function evtOnTap}) {
    final BoxDecoration boxDeco = BoxDecoration(
        color: UiHelper.scCheckGris, borderRadius: BorderRadius.circular(15.0));

    Widget rWidget = InkResponse(
        onTap: evtOnTap,
        child: Container(
          padding: EdgeInsets.only(right: 10.0, top: 5.0, bottom: 5.0),
          decoration: boxDeco,
          child: Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                    width: 25.0,
                    child: Icon(FontAwesomeIcons.checkCircle,
                        color: UiHelper.scGrisTexto, size: 12.0)),
                Text(label,
                    style: TextStyle(
                        color: UiHelper.scGrisTexto,
                        fontSize: 10,
                        fontWeight: FontWeight.bold))
              ]),
        ));
    return rWidget;
  }

  Widget msgAlert({AuthBloc bloc}) {
    return StreamBuilder(
      stream: bloc.requestStatusStream,
      builder: (context, AsyncSnapshot snapshot) {
        Widget alertWidget;
        String requestStatusType =
            (bloc.requestStatusType != null) ? bloc.requestStatusType : 'info';
        if (snapshot.hasData) {
          // el request tiene mensaje que desplegar
          alertWidget = Container(
            margin: null,
          );
          if (bloc.requestStatusMsg.length > 0) {
            alertWidget = _msgStatus(
                msg: bloc.requestStatusMsg,
                type: (!snapshot.data) ? 'info' : requestStatusType);
          }
        } else {
          // el request no tiene datos que mostrar
          alertWidget = Container(
            margin: null,
          );
        }
        return alertWidget;
      },
    );
  }

  Widget msgUserAlert({UserBloc bloc}) {
    return StreamBuilder(
      stream: bloc.requestStatusStream,
      builder: (context, AsyncSnapshot snapshot) {
        Widget alertWidget;
        String requestStatusType =
            (bloc.requestStatusType != null) ? bloc.requestStatusType : 'info';
        if (snapshot.hasData) {
          // el request tiene mensaje que desplegar
          alertWidget = Container(
            margin: null,
          );
          if (bloc.requestStatusMsg.length > 0) {
            alertWidget = _msgStatus(
                msg: bloc.requestStatusMsg,
                type: (!snapshot.data) ? 'info' : requestStatusType);
          }
        } else {
          // el request no tiene datos que mostrar
          alertWidget = Container(
            margin: null,
          );
        }
        return alertWidget;
      },
    );
  }

  Widget msgTurnoAlert({TurnoBloc bloc}) {
    return StreamBuilder(
      stream: bloc.requestStatusStream,
      builder: (context, AsyncSnapshot snapshot) {
        Widget alertWidget;
        String requestStatusType =
            (bloc.requestStatusType != null) ? bloc.requestStatusType : 'info';
        if (snapshot.hasData) {
          // el request tiene mensaje que desplegar
          alertWidget = Container(
            margin: null,
          );
          if (bloc.requestStatusMsg.length > 0) {
            alertWidget = _msgStatus(
                msg: bloc.requestStatusMsg,
                type: (!snapshot.data) ? 'info' : requestStatusType);
          }
        } else {
          // el request no tiene datos que mostrar
          alertWidget = Container(
            margin: null,
          );
        }
        return alertWidget;
      },
    );
  }

  Widget _msgStatus({String msg, String type = 'warning'}) {
    Icon _icon;
    switch (type) {
      case 'loading':
        _icon =
            Icon(FontAwesomeIcons.fileUpload, color: UiHelper.scAzul, size: 16);
        break;
      case 'info':
        _icon = Icon(FontAwesomeIcons.exclamationTriangle,
            color: Colors.orangeAccent, size: 16);
        break;
      case 'success':
      default:
        _icon = Icon(FontAwesomeIcons.solidCheckCircle,
            color: Colors.greenAccent, size: 16);
        break;
    }

    return Container(
        margin: EdgeInsets.symmetric(horizontal: 15.0),
        child: Row(
          children: [
            SizedBox(width: 24, child: _icon),
            Expanded(
              flex: 1,
              child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 5.0),
                  child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Text(msg,
                          style: TextStyle(color: UiHelper.scGrisTexto)))),
            )
          ],
        ));
  }

  void resetAuthRequestStatus(AuthBloc bloc) {
    // mensaje de alerta del request
    bloc.requestStatusController.value = false;
    bloc.requestStatusMsgController.value = "";
    // control de estado del request
    bloc.requestingController.value = false;
  }

  void resetUserRequestStatus(UserBloc bloc) {
    // mensaje de alerta del request
    bloc.requestStatusController.value = false;
    bloc.requestStatusMsgController.value = "";
    // control de estado del request
    bloc.requestingController.value = false;
  }

  void resetTurnoRequestStatus(TurnoBloc bloc) {
    // mensaje de alerta del request
    bloc.requestStatusController.value = false;
    bloc.requestStatusMsgController.value = "";
    // control de estado del request
    bloc.requestingController.value = false;
  }

  void clearAuthMsgAlert(AuthBloc bloc) {
    const duracion = const Duration(seconds: 6);
    new Timer(duracion, () => UiHelper().resetAuthRequestStatus(bloc));
  }

  void clearUserMsgAlert(UserBloc bloc) {
    const duracion = const Duration(seconds: 6);
    new Timer(duracion, () => UiHelper().resetUserRequestStatus(bloc));
  }

  void clearTurnoMsgAlert(TurnoBloc bloc) {
    const duracion = const Duration(seconds: 6);
    new Timer(duracion, () => UiHelper().resetTurnoRequestStatus(bloc));
  }

  formatoFechaTurno({String fecha, bool invertido = true}) {
    List<String> arrFecha = fecha.split('-');
    DateTime dtFechaTurno;
    if (invertido) {
      dtFechaTurno = new DateTime(int.parse(arrFecha[2]),
          int.parse(arrFecha[1]), int.parse(arrFecha[0]));
    } else {
      dtFechaTurno = new DateTime(int.parse(arrFecha[0]),
          int.parse(arrFecha[1]), int.parse(arrFecha[2]));
    }

    intl.DateFormat _formatoFecha = intl.DateFormat('EEEE dd/MM/yyyy', 'es');
    String rFecha = _formatoFecha.format(dtFechaTurno);
    return rFecha;
  }

  /// -------------------------------------------------------
  /// MISCELANEOS - UTILES
  /// -------------------------------------------------------

  bool isUri(String value) {
    Pattern pattern =
        r"^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$";
    RegExp regExp = new RegExp(pattern);
    return regExp.hasMatch(value);
  }

  /// -------------------------------------------------------
  /// DIALOGOS - ALERTAS
  /// -------------------------------------------------------
  
  void alertaSiNo( { String title, String txtMessage, BuildContext ctx, Function yesAction } ) {

    showDialog(
      context: ctx,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Text(txtMessage),
          actions: <Widget>[
            FlatButton(
              child: Text("SI"),
              onPressed: () {
                yesAction();
              },
            ),
            FlatButton(
              child: Text("NO"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text("CANCELAR"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }


}

enum EstadoTsx { Pendiente, Realizada, Vencida }

import 'package:shared_preferences/shared_preferences.dart';

class UserPreferences {

  static const int defaultId = 1;
  static const String defaultIdCliente = '0';
  static const String defaultEmail = 'usuario@mail.com';
  static const String defaultNombreCompleto = '--- ---';
  static const String defaultCi = '######';
  static const String defaultTelefono = '';
  static const String defaultImagenPerfil = '';
  static const String defaultEstadoZonaAsegurados = '...';
  List<dynamic> userPlanes;

  static final UserPreferences _instance = new UserPreferences._internal();

  factory UserPreferences(){
    return _instance;
  }
  UserPreferences._internal();

  SharedPreferences _prefs;

  initPrefs() async {
    _prefs = await SharedPreferences.getInstance();
  }

  String get id { return _prefs.getString('id') ?? defaultId; }
  String get email { return _prefs.getString('email')?? defaultEmail; }
  String get nombreCompleto { return _prefs.getString('nombre-completo') ?? defaultNombreCompleto; }
  String get ci { return _prefs.getString('ci') ?? defaultCi; }
  String get telefono { return _prefs.getString('telefono') ?? defaultTelefono; }
  String get estadoZonaAsegurados { return _prefs.getString('estado-zona-asegurados') ?? defaultEstadoZonaAsegurados; }
  String get imagenPerfil { return _prefs.getString('imagen-perfil') ?? defaultImagenPerfil; }
  String get idCliente { return _prefs.getString('id-cliente') ?? defaultIdCliente; }

  set id (String value){ _prefs.setString('id', value); }
  set email (String value){ _prefs.setString('email', value); }
  set nombreCompleto (String value){ _prefs.setString('nombre-completo', value); }
  set ci (String value){ _prefs.setString('ci', value); }
  set telefono (String value){ _prefs.setString('telefono', value); }
  set estadoZonaAsegurados (String value){ _prefs.setString('estado-zona-asegurados', value); }
  set imagenPerfil (String value){ _prefs.setString('imagen-perfil', value); }
  set idCliente (String value){ _prefs.setString('id-cliente', value); }

  setPrefData(Map<String,dynamic> dataSet) async{
    
    SharedPreferences prefs = await SharedPreferences.getInstance();

    Map<String,dynamic> meta = dataSet['meta'];
    userPlanes = new List<dynamic>();
    userPlanes = meta['planes'];
    
    prefs.setString('id', dataSet['id']);
    prefs.setString('nombre-completo', dataSet['nombre']);
    prefs.setString('ci', dataSet['ci']);
    prefs.setString('email', dataSet['email']);
    prefs.setString('telefono', dataSet['telefono']);
    prefs.setString('id-cliente', meta['id']);
  }

  double userPrefsComplete(){
    double totalData = 5;
    double countFilledData = 0;
    if(email != defaultEmail){
      countFilledData++;
    }
    if(nombreCompleto != defaultNombreCompleto){
      countFilledData++;
    }
    if(ci != defaultCi){
      countFilledData++;
    }
    if(telefono != defaultTelefono){
      countFilledData++;
    }
    if(imagenPerfil != defaultImagenPerfil){
      countFilledData++;
    }
    return (countFilledData * 100)/totalData;
  }

}
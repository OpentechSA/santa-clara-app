import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:santa_clara/utils/userPreferences.dart';
import 'src/app.dart';

Future main() async{

  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  await SystemChrome.setEnabledSystemUIOverlays([]);
  // cargamos las preferencias del usuario
  final userPrefs = new UserPreferences();
  await userPrefs.initPrefs();
  runApp(new App());
}
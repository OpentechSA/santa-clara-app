import 'dart:convert';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:santa_clara/src/models/agendaModels.dart';
import 'package:santa_clara/src/modules/authBloc.dart';
import 'package:santa_clara/src/modules/mainBloc.dart';
import 'package:santa_clara/src/modules/turnoBloc.dart';
import 'package:santa_clara/src/modules/userBloc.dart';
import 'package:santa_clara/utils/uiHelper.dart';
export 'package:santa_clara/src/modules/authBloc.dart'; 

class AppProvider extends InheritedWidget{
  
  final authBloc = AuthBloc();
  final mainBloc = MainBloc();
  final turnoBloc = TurnoBloc();
  final userBloc = UserBloc();

  AppProvider({Key key,Widget child}) : super(key:key,child:child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

  static AuthBloc getAuthBloc (BuildContext context){
    return context.dependOnInheritedWidgetOfExactType<AppProvider>().authBloc;
  }
  
  static MainBloc getMainBloc (BuildContext context){
    return context.dependOnInheritedWidgetOfExactType<AppProvider>().mainBloc;
  }

  static TurnoBloc getTurnoBloc (BuildContext context){
    return context.dependOnInheritedWidgetOfExactType<AppProvider>().turnoBloc;
  }

  static UserBloc getUserBloc (BuildContext context){
    return context.dependOnInheritedWidgetOfExactType<AppProvider>().userBloc;
  }

  List<dynamic> listaGrupoFamiliar = [];

  Future<List<dynamic>> loadDataGrupoFamiliar() async{

    final respuesta = await rootBundle.loadString('data/grupoFamiliar.json');  
    Map dataMap = json.decode(respuesta);
    listaGrupoFamiliar.clear();
    listaGrupoFamiliar = dataMap['items'];
    return listaGrupoFamiliar;

  }

  Future<List<DropdownMenuItem<String>>> loadModalidadesCita() async {

    final respuesta = await rootBundle.loadString('data/modalidadesCitas.json');
    List<DropdownMenuItem<String>> _listItems = [];
    Map dataMap = json.decode(respuesta);
    dataMap['items'].forEach((element) { 
      _listItems.add(new DropdownMenuItem<String>(
          child: Text(element['text']),
          value: element['id'],
        )
      );
    });
    return _listItems;

  }

  Future<List<DropdownMenuItem<String>>> loadOpcionesRecordatorio() async {

    final respuesta = await rootBundle.loadString('data/opcionesRecordatorios.json');
    List<DropdownMenuItem<String>> _listItems = [];
    Map dataMap = json.decode(respuesta);
    dataMap['items'].forEach((element) { 
      _listItems.add(new DropdownMenuItem<String>(
          child: Text(element['text']),
          value: element['id'],
        )
      );
    });
    return _listItems;
    
  }

  Future<List<DropdownMenuItem<String>>> loadOpcionesTipoRecordatorio() async {

    final respuesta = await rootBundle.loadString('data/opcionesTiposRecordatorios.json');
    List<DropdownMenuItem<String>> _listItems = [];
    Map dataMap = json.decode(respuesta);
    dataMap['items'].forEach((element) { 
      _listItems.add(new DropdownMenuItem<String>(
          child: Text(element['text']),
          value: element['id'],
        )
      );
    });
    return _listItems;
    
  }

  Future<List<DropdownMenuItem<String>>> loadOpcionesFrecuencia() async {

    final respuesta = await rootBundle.loadString('data/opcionesFrecuencia.json');
    List<DropdownMenuItem<String>> _listItems = [];
    Map dataMap = json.decode(respuesta);
    dataMap['items'].forEach((element) { 
      _listItems.add(new DropdownMenuItem<String>(
          child: Text(element['text']),
          value: element['id'],
        )
      );
    });
    return _listItems;
    
  }

  List<dynamic> listaTags = [];

  Future<List<dynamic>> loadTagsTareas() async{

    final respuesta = await rootBundle.loadString('data/tagsTareas.json');  
    Map dataMap = json.decode(respuesta);
    listaTags.clear();
    listaTags = dataMap['items'];
    return listaTags;

  }

  List<dynamic> listaDiasSemana = [];

  Future<List<dynamic>> loadDiasSemana() async{

    final respuesta = await rootBundle.loadString('data/diasSemana.json');  
    Map dataMap = json.decode(respuesta);
    listaDiasSemana.clear();
    listaDiasSemana = dataMap['items'];
    return listaDiasSemana;

  }

  Future<File> createFileOfPdfUrl({String pUri}) async {
    final url = pUri;
    final filename = url.substring(url.lastIndexOf("=") + 1);
    var request = await HttpClient().getUrl(Uri.parse(url));
    var response = await request.close();
    var bytes = await consolidateHttpClientResponseBytes(response);
    String dir = (await getApplicationDocumentsDirectory()).path;
    File file = new File('$dir/factura-$filename.pdf');
    await file.writeAsBytes(bytes);
    return file;
  }

  Future<File> createFileOfHtmlUrl({String pUri}) async {
    final url = pUri;
    final filename = url.substring(url.lastIndexOf("=") + 1);
    var request = await HttpClient().getUrl(Uri.parse(url));
    var response = await request.close();
    var bytes = await consolidateHttpClientResponseBytes(response);
    String dir = (await getApplicationDocumentsDirectory()).path;
    File file = new File('$dir/factura-$filename.html');
    await file.writeAsBytes(bytes);
    return file;
  }

  static int getSucursalEspecialidad(List<Especialidad> list,String especialidad){

    int result = 0;    
    list.forEach((element) { 
      if(element.idEspecialidad == especialidad){
        result = element.codSucursal;
      }
    });
    return result;

  }

  static Especialidad getEspecialidad(List<Especialidad> list,String especialidad){

    Especialidad result = new Especialidad();    
    list.forEach((element) { 
      if(element.idEspecialidad == especialidad){
        result = element;
      }
    });
    return result;

  }

  static String getTagEspecialidad(List<Especialidad> list,String especialidad){

    String result = '---';    
    list.forEach((element) { 
      if(element.idEspecialidad == especialidad){
        result = element.descripcion;
      }
    });
    return result;

  }

  static String getTagFecha(List<Fecha> list,String fecha){

    String result = '---';    
    list.forEach((element) { 
      if(element.fecha == fecha){
        result = element.descDia;
      }
    });
    return result;

  }

  static String getTagHora(List<Hora> list,String hora){

    String result = '---';    
    list.forEach((element) { 
      if(element.idRes == hora){
        result = element.descHora;
      }
    });
    return result;

  }

  showToast(String texto,BuildContext context){
    Widget pIconToastWidget = Container(
      margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft+10,right:10.0),
      padding: EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        color:Color.fromRGBO(0, 0, 0,100),
        borderRadius: BorderRadius.circular(10.0),
      ),
      child:Row(
        children: [
          SizedBox(width: 30,child:Icon(FontAwesomeIcons.exclamationTriangle,size: 20,color: Colors.yellow)),
          Text(texto,style: TextStyle(color:Colors.white),),
          Spacer(flex: 1,)
        ],
      )
    );
    showToastWidget(
      pIconToastWidget,
      context: context,
      position: StyledToastPosition.center,
      animation: StyledToastAnimation.fade,
      reverseAnimation: StyledToastAnimation.fade,
      duration: Duration(seconds: 3),
      animDuration: Duration(milliseconds: 250),
      curve: Curves.easeIn,
      reverseCurve: Curves.linear
    );
  }

}
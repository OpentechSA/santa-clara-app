import 'dart:async';
import 'package:rxdart/rxdart.dart';
import 'package:santa_clara/src/modules/validators.dart';

class TurnoBloc with Validators {

  final emailController = BehaviorSubject<String>();
  final nroTelefonoController = BehaviorSubject<String>();
  final buscarController = BehaviorSubject<String>();
  final tituloController = BehaviorSubject<String>();
  final descripcionController = BehaviorSubject<String>();
  final fechaController = BehaviorSubject<String>();
  final tipoRecordatorioController = BehaviorSubject<String>();
  final horaController = BehaviorSubject<String>();
  final dosificacionController = BehaviorSubject<String>();
  final requestStatusController = BehaviorSubject<bool>();
  final requestingController = BehaviorSubject<bool>();
  final requestStatusMsgController = BehaviorSubject<String>();
  final requestStatusTypeController = BehaviorSubject<String>();

  Stream<String> get emailStream              => emailController.stream.transform( validarEmail );
  Stream<String> get nroTelefonoStream        => nroTelefonoController.transform( validarNroTelefono );
  Stream<String> get buscarStream             => buscarController.stream;
  Stream<String> get tituloStream             => tituloController.stream;
  Stream<String> get descripcionStream        => descripcionController.stream;
  Stream<String> get fechaStream              => fechaController.stream;
  Stream<String> get tipoRecordatorioStream   => tipoRecordatorioController.stream;
  Stream<String> get horaStream               => horaController.stream;
  Stream<String> get dosificacionStream       => tituloController.stream;
  Stream<String> get requestStatusMsgStream   => requestStatusMsgController;
  Stream<String> get requestStatusTypeStream  => requestStatusTypeController;
  Stream<bool> get requestStatusStream        => requestStatusController;
  Stream<bool> get requestingStream           => requestStatusController;

  Function(String) get changeEmail            => emailController.sink.add;
  Function(String) get changeNroTelefono      => nroTelefonoController.sink.add;
  Function(String) get changeBuscar           => buscarController.sink.add;
  Function(String) get changeTitulo           => tituloController.sink.add;
  Function(String) get changeDescripcion      => descripcionController.sink.add;
  Function(String) get changeFecha            => fechaController.sink.add;
  Function(String) get changeTipoRecordatorio => tipoRecordatorioController.sink.add;
  Function(String) get changeHora             => horaController.sink.add;
  Function(String) get changeDosificacion     => dosificacionController.sink.add;

  Stream<bool> get validConfirmForm           => CombineLatestStream.combine2(emailStream, nroTelefonoStream, (a, b) => true);

  String get email              => emailController.value;
  String get nroTelefono        => nroTelefonoController.value;
  String get buscar             => buscarController.value;
  String get titulo             => tituloController.value;
  String get dosificacion       => dosificacionController.value;
  String get descripcion        => descripcionController.value;
  String get requestStatusMsg   => requestStatusMsgController.value;
  String get requestStatusType  => requestStatusTypeController.value;
  bool get requestStatus        => requestStatusController.value;
  bool get requestingStatus     => requestingController.value;

  dispose(){
    emailController?.close();
    nroTelefonoController?.close();
    buscarController?.close();
    tituloController?.close();
    descripcionController?.close();
    fechaController?.close();
    tipoRecordatorioController?.close();
    horaController?.close();
    dosificacionController?.close();
    requestStatusController?.close();
    requestingController?.close();
    requestStatusMsgController?.close();
    requestStatusTypeController?.close();
  }

}
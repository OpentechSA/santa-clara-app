import 'dart:async';

class Validators{

  final validarPassword = StreamTransformer<String,String>.fromHandlers(
    handleData: (password,sink){
      if(password.length >= 6){
        sink.add (password);
      }else{
        sink.addError('El password es muy corto - Mínimo 6 caracteres');
      }
    }
  );

  final validarNombreApellido = StreamTransformer<String,String>.fromHandlers(
    handleData: (nombreApellido,sink){
      if(nombreApellido.length >= 6){
        sink.add (nombreApellido);
      }else{
        sink.addError('El Nombre y Apellido es muy corto - Mínimo 6 caracteres');
      }
    }
  );

  final validarNroTelefono = StreamTransformer<String,String>.fromHandlers(
    handleData: (nroTelefono,sink){
      // Pattern pattern = r'^[+][0-9]*[ ][0-9]*[ ][0-9]*$';
      // RegExp regExp = new RegExp(pattern);
      // if(regExp.hasMatch(nroTelefono)){
      //   sink.add (nroTelefono);
      // }else{
      //   sink.addError('El teléfono no es válido { +### #### ####### }');
      // }
      if(nroTelefono.length >= 6){
        sink.add (nroTelefono);
      }else{
        sink.addError('El Teléfono es muy corto - Mínimo 9 caracteres');
      }
    }
  );

  final validarNroDocumento = StreamTransformer<String,String>.fromHandlers(
    handleData: (nroDocumento,sink){
      Pattern pattern = r'^[1-9]\d{5,10}';
      RegExp regExp = new RegExp(pattern);

      if(regExp.hasMatch(nroDocumento)){
        sink.add (nroDocumento);
      }else{
        sink.addError('La cédula no es válida {###.###.###}');
      }
    }
  );

  final validarEmail = StreamTransformer<String,String>.fromHandlers(
    handleData: (email,sink){

      Pattern pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
      RegExp regExp = new RegExp(pattern);

      if(regExp.hasMatch(email)){
        sink.add (email);
      }else{
        sink.addError('El email no es válido');
      }
    }
  );

  final validarEmailRecovery = StreamTransformer<String,String>.fromHandlers(
    handleData: (email,sink){
      if(Validators().isValidEmail(email)){
        sink.add (email);
      }else{
        sink.addError('El email no es válido');
      }
    }
  );

  bool isValidEmail(String email){
    Pattern pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(pattern);
    return (regExp.hasMatch(email));
  }

}
import 'dart:async';
import 'package:santa_clara/src/modules/validators.dart';
import 'package:rxdart/rxdart.dart';

class AuthBloc with Validators {

  ValidateAllStreamsHaveDataAndNoErrors validateAllStreamsHaveDataAndNoErrors;

  AuthBloc(){
    validateAllStreamsHaveDataAndNoErrors = ValidateAllStreamsHaveDataAndNoErrors()
    ..listen([
      //nroTelefonoStream,
      emailStream,
      nombreApellidoStream,
      nroDocumentoStream,
      passwordStream,
      passwordConfirmStream,
    ]);
  }

  final emailRecoveryController     = BehaviorSubject<String>();
  final emailController             = BehaviorSubject<String>();
  final passwordController          = BehaviorSubject<String>();
  final passwordConfirmController   = BehaviorSubject<String>();
  final nombreApellidoController    = BehaviorSubject<String>();
  final nroDocumentoController      = BehaviorSubject<String>();
  final nroTelefonoController       = BehaviorSubject<String>();

  final requestStatusMsgController    = BehaviorSubject<String>();
  final requestStatusTypeController   = BehaviorSubject<String>();
  final requestStatusController       = BehaviorSubject<bool>();
  final requestingController          = BehaviorSubject<bool>();

  Stream<String> get emailStream              => emailController.stream.transform( validarEmail );
  Stream<String> get emailRecoveryStream      => emailRecoveryController.stream.transform( validarEmailRecovery );
  Stream<String> get passwordStream           => passwordController.stream.transform( validarPassword ).doOnData((String pass ){
    String passConfirm = passwordConfirmController.value;
    print("$pass - $passConfirm");
    if(0 != passwordController.value.compareTo(passConfirm)){
      passwordController.addError("Las contraseñas no coinciden");
    }
  });
  Stream<String> get passwordConfirmStream    => passwordConfirmController.stream.transform ( validarPassword ).doOnData((String passConfirm ){
    String pass = passwordController.value;
    print("$pass - $passConfirm");
    if(0 != passwordController.value.compareTo(passConfirm)){
      passwordConfirmController.addError("Las contraseñas no coinciden");
    }
  });
  Stream<String> get nombreApellidoStream     => nombreApellidoController.transform( validarNombreApellido );
  Stream<String> get nroDocumentoStream       => nroDocumentoController.transform( validarNroDocumento );
  Stream<String> get nroTelefonoStream        => nroTelefonoController.transform( validarNroTelefono );
  Stream<String> get requestStatusMsgStream   => requestStatusMsgController;
  Stream<String> get requestStatusTypeStream  => requestStatusTypeController;
  Stream<bool> get requestStatusStream        => requestStatusController;
  Stream<bool> get requestingStream           => requestStatusController;

  Stream<bool> get validRegisterForm          => validateAllStreamsHaveDataAndNoErrors?.status;
  Stream<bool> get validRequestStatus         => requestStatusStream;
  Stream<bool> get validLoginForm             => CombineLatestStream.combine2(emailStream, passwordStream, (a, b) => true);
  Stream<bool> get validUserDataForm          => CombineLatestStream.combine3(nombreApellidoStream, emailStream, nroDocumentoStream, (a, b, c) => true);
  Stream<bool> get validPasswordForm          => CombineLatestStream.combine2(passwordStream, passwordConfirmStream, (a, b) { 
    return (0 != passwordController.value.compareTo(passwordConfirmController.value)) ; 
  });

  Function(String) get changeEmail            => emailController.sink.add;
  Function(String) get changeEmailRecovery    => emailRecoveryController.sink.add;
  Function(String) get changePassword         => passwordController.sink.add;
  Function(String) get changePasswordConfirm  => passwordConfirmController.sink.add;
  Function(String) get changeNombreApellido   => nombreApellidoController.sink.add;
  Function(String) get changeNroDocumento     => nroDocumentoController.sink.add;
  Function(String) get changeNroTelefono      => nroTelefonoController.sink.add;

  String get email              => emailController.value;
  String get emailRecovery      => emailRecoveryController.value;
  String get password           => passwordController.value;
  String get nroDocumento       => nroDocumentoController.value;
  String get nombreApellido     => nombreApellidoController.value;
  String get nroTelefono        => nroTelefonoController.value;
  String get requestStatusMsg   => requestStatusMsgController.value;
  String get requestStatusType  => requestStatusTypeController.value;
  bool get requestStatus        => requestStatusController.value;
  bool get requestingStatus     => requestingController.value;

  dispose(){
    emailController?.close();
    passwordController?.close();
    passwordConfirmController?.close();
    nombreApellidoController?.close();
    nroDocumentoController?.close();
    nroTelefonoController?.close();
    requestStatusMsgController?.close();
    requestStatusTypeController?.close();
    requestStatusController?.close();
    requestingController?.close();
    emailRecoveryController?.close();
  }

}

class ValidateAllStreamsHaveDataAndNoErrors {
  
  List<bool> errors;
  StreamController<bool> _controller;
 
  void listen(List<Stream> streams){
    
    _controller = StreamController<bool>.broadcast();
    errors = List.generate(streams.length, (_) => true);
    List.generate(streams.length, (int index) {
      return streams[index].listen(
        (data){
          // Data was emitted => this is not an error
          errors[index] = false;
          _validate();
        },
        onError: (_){
          // Error was emitted
          errors[index] = true;
          _validate();
        },
      );
    });

  }
 
  void _validate(){
    bool hasNoErrors = (errors.firstWhere((b) => b == true, orElse: () => null) == null);
    _controller.sink.add(hasNoErrors);
  }

  Stream<bool> get status => _controller.stream;

  void dispose(){
    _controller?.close();
  }

}
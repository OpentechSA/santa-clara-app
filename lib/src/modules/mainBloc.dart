import 'dart:async';
import 'package:santa_clara/src/modules/validators.dart';

class MainBloc with Validators {

  final buscarController = StreamController<String>.broadcast();

  Stream<String> get buscarStream => buscarController.stream;

  Function(String) get changeBuscar => buscarController.sink.add;

  dispose(){
    buscarController?.close();
  }

}
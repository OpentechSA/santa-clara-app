import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:santa_clara/src/components/camposPerfilUsuario.dart';
import 'package:santa_clara/src/components/menuDrawer.dart';
import 'package:santa_clara/src/components/scAppBar.dart';
import 'package:santa_clara/src/models/agendaModels.dart';
import 'package:santa_clara/src/models/genericModels.dart';
import 'package:santa_clara/src/models/registrationModels.dart';
import 'package:santa_clara/src/modules/appProvider.dart';
import 'package:santa_clara/src/modules/userBloc.dart';
import 'package:santa_clara/src/pages/viewer/factura.dart';
import 'package:santa_clara/src/services/authenticationService.dart';
import 'package:santa_clara/src/services/bookingService.dart';
import 'package:santa_clara/src/services/userService.dart';
import 'package:santa_clara/utils/uiHelper.dart';
import 'package:santa_clara/utils/userPreferences.dart';

class IndexPerfilUsuario extends StatefulWidget {
  
  IndexPerfilUsuario({Key key}) : super(key: key);
  final IndexPerfilUsuarioState _state = new IndexPerfilUsuarioState();
  @override
  IndexPerfilUsuarioState createState() => _state;

}

class IndexPerfilUsuarioState extends State<IndexPerfilUsuario> with UiHelper{
  
  int _currentPage = 0;
  PageController _pageController = new PageController(initialPage: 0);
  int idxFactura = 0;
  
  String nombreApellido = ""; 
  String email = "";
  String nroDocumento = "";
  String nroTelefono = "";
  bool _showPassNew;
  bool _showPassConfirm;
  
  TextEditingController nombreApellidoController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  TextEditingController nroDocumentoController = new TextEditingController();
  TextEditingController nroTelefonoController = new TextEditingController();

  final TextEditingController maxWidthController = TextEditingController();
  final TextEditingController maxHeightController = TextEditingController();
  final TextEditingController qualityController = TextEditingController();

  static UserBloc blocUser;
  static Auth auth = new Auth();
  static Turnos turnos = new Turnos();
  static UserProfile user = new UserProfile();
  final UserPreferences userPrefs = new UserPreferences();

  final picker = ImagePicker();
  PickedFile _imageFile;
  dynamic _pickImageError;  
  String _imageProfile;

  final List<dynamic> clientIds = new List<dynamic>();
  final List<FacturaCliente> facturas = new List<FacturaCliente>();

  @override
  void initState() { 
    super.initState(); 
    userPrefs.initPrefs();
    loadInitData();
    nombreApellidoController.text = nombreApellido;
    emailController.text = email;
    nroDocumentoController.text = nroDocumento;
    nroTelefonoController.text = nroTelefono;
    _showPassNew = true;
    _showPassConfirm = true;
  }

  @override
  Widget build(BuildContext context) {

    setContext(context);
    blocUser = AppProvider.getUserBloc(context);
    resetUserRequestStatus(blocUser);
    
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: ScAppBar(),
      drawer: MenuDrawer(),
      body: Stack(
        children:<Widget>[
          backgroundLayout(sinLogo: true),
          _mainContent(context)
        ]
      )
    );

  }

  loadInitData(){
    nombreApellido = userPrefs.nombreCompleto;
    email = userPrefs.email;
    nroDocumento = userPrefs.ci;
    nroTelefono = userPrefs.telefono;
  }

  Widget _mainContent(BuildContext ctx){

    return Container(
      margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
      padding: EdgeInsets.all(0.0),
      width: getSize.width-UiHelper.layoutMarginLeft,
      child: Container(
        child:Column(
          children: [

            // header perfil-usuario
            Container(
                padding: EdgeInsets.only(top:50),
                decoration:BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      const Color(0xFFFFFFFF),
                      const Color.fromRGBO(230, 248, 255, 1),
                    ],
                  ),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [

                    // user-box
                    userBox(),
                    // botonera 
                    botoneraPages()

                  ],
                )
              ),

            // container perfil-usuario-content
            Flexible(
              flex:1,
              child:Container(
                child: PageView(
                  controller: _pageController,
                  onPageChanged: (value){
                    print('onPageChanged: $value');
                    setState(() { _currentPage = value; });
                  },
                  children: [
                    // mis datos
                    _misDatos(ctx),
                    _miGrupoFamiliar(ctx),
                    _misTransacciones(ctx),
                  ],
                ),
              ),
            ),

          ],
        )
      )
    );

  }

  Widget userBox(){

    BoxDecoration _boxDecoProfilePic = BoxDecoration(
      image: DecorationImage(
          image: AssetImage(UiHelper.noProfilePic),
          fit:BoxFit.cover
        ),
      border:Border.all(width:2.0, color:UiHelper.scAzul ),
      borderRadius: BorderRadius.circular(5.0)
    );
    Color iconColor = Colors.transparent;

    if(userPrefs.imagenPerfil.length > 0){
      _boxDecoProfilePic = BoxDecoration(
        image: DecorationImage(
          image: (isUri(userPrefs.imagenPerfil))?NetworkImage(userPrefs.imagenPerfil,scale: 1):FileImage(File(userPrefs.imagenPerfil)),
          fit:BoxFit.cover
        ),
        border:Border.all(width:2.0, color:UiHelper.scAzul ),
        borderRadius: BorderRadius.circular(5.0)
      );
      iconColor = Colors.transparent;
    }

    return Column(
      children: [
        
        // profile-pic and picker
        Stack(
          children: [
            // profile-pic
            Container(
              width: 80.0,
              height: 80.0,
              margin:EdgeInsets.symmetric(vertical:15.0),
              padding:EdgeInsets.all(10.0),
              child: Container(
                decoration: _boxDecoProfilePic,
                child:IconButton(
                  icon:Icon(FontAwesomeIcons.image,color:iconColor,size:24.0),
                  onPressed: (){
                    _selectPicture(ImageSource.gallery,context:context);                    
                  }
                )
              ),
            ),
            // image-picker-btn
            Positioned(
              bottom: 0.0,
              child: Container(
                width: 80,
                child:IconButton(
                  icon:Container(
                    width:28.0,
                    height:28.0,
                    decoration: BoxDecoration(
                      color:UiHelper.scCeleste,
                      borderRadius: BorderRadius.circular(28.0),
                    ),
                    child:Icon(FontAwesomeIcons.camera,color:Colors.white,size:13.0)
                  ),
                  onPressed: (){
                    _selectPicture(ImageSource.camera,context:context);
                  },
                ),
              ),
            ),
          ],
        ),

        // nombre-completo
        Container(
          padding: EdgeInsets.symmetric(horizontal:15.0),
          child:Text(
            userPrefs.nombreCompleto,
            style:TextStyle(
              color:UiHelper.scAzul,
              fontSize: 14.0,
              fontWeight: FontWeight.bold,
            ),
            textAlign: TextAlign.center,
            overflow:TextOverflow.ellipsis,
            maxLines: 1,
          )
        ),
        
        // email
        Container(
          padding: EdgeInsets.symmetric(horizontal:15.0),
          child:Text(
            userPrefs.email,
            style:TextStyle(
              color:UiHelper.scAzul,
              fontSize: 14.0,
            ),
            textAlign: TextAlign.center,
            overflow:TextOverflow.ellipsis,
            maxLines: 1,
          )
        ),
      
      ],
    );
  }

  Widget botoneraPages(){

    List<BtnPageViewNav> navItems = [
      BtnPageViewNav( caption:'Mis Datos',index:0, activo:(this.currentPage == 0), parentState: this),
      BtnPageViewNav( caption:'Mi Grupo Familiar', activo:(this.currentPage == 1),index:1, parentState: this),
      BtnPageViewNav( caption:'Transacciones',index:2, activo:(this.currentPage == 2), parentState: this),
      BtnPageViewNav( caption:'Métodos de pago',index:3, activo:(this.currentPage == 3), parentState: this, evtOnPressed: null ),
    ];

    PageViewNav pageViewNav = new PageViewNav(items:navItems);
    
    return Container(
      height: 50.0,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
          const Color.fromRGBO(230, 248, 255, 1),
          const Color.fromRGBO(206, 236, 252, 1),
          ]
        ),
      ),
      child: pageViewNav
    );
  }

  Widget _misDatos(BuildContext context){

    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [

            separadorCampos(title:'Datos Personales',icon:FontAwesomeIcons.userEdit),
            
            SizedBox(height: 15.0,),

            inputWrapper(
              field:perfilNombreApellido(
                bloc:blocUser,
                streamBuilder: false,
                controller: nombreApellidoController,
                evtOnChanged: (value){
                  userPrefs.nombreCompleto = value;
                  blocUser.nombreApellidoController.value = value;
                }
              ),
              withBoxShadow: true
            ),

            SizedBox(height: 10.0,),

            inputWrapper(
              field:perfilCorreoElectronico(
                bloc:blocUser,
                controller: emailController,
                evtOnChanged: (value){
                  userPrefs.email = value;
                  blocUser.emailController.value = value;
                }
              ),
              withBoxShadow: true
            ),

            SizedBox(height: 10.0,),

            inputWrapper(
              field:perfilNroDocumento(
                bloc:blocUser,
                controller: nroDocumentoController,
                evtOnChanged: (value){
                  userPrefs.ci = value;
                  blocUser.nroDocumentoController.value = value;
                }
              ),
              withBoxShadow: true
            ),

            SizedBox(height: 10.0,),

            inputWrapper(
              field:perfilNroTelefono(
                bloc:blocUser,
                controller: nroTelefonoController,
                evtOnChanged: (value){
                  userPrefs.telefono = value;
                  blocUser.nroTelefonoController.value = value;
                }
              ),
              withBoxShadow: true
            ),

            SizedBox(height: 10.0,),

            Row(
              children: [
                Expanded(child:msgUserAlert(bloc: blocUser),),
                Container( 
                  margin:EdgeInsets.only(right:10.0), 
                  alignment: Alignment.centerRight, 
                  child:iconBoxButton(
                    label:'Actualizar',
                    icon:FontAwesomeIcons.solidSave,
                    evtOnTap: (){ updateDatos(); },
                    iconSize: 24, 
                    width: 70, 
                    height:60
                  )
                ),
              ]
            ),

            separadorCampos(title:'Seguridad',icon:FontAwesomeIcons.shieldAlt),

            SizedBox(height: 10.0,),

            Container(
              padding: EdgeInsets.symmetric(horizontal:15.0),
              margin:EdgeInsets.only(bottom: 10.0),
              child:Row(
                children: [
                  Flexible(
                    flex:9,
                    fit:FlexFit.tight,
                    child:Text(
                      'Olvide mi clave actual, enviar enlace de cambio de clave por correo',
                      style:TextStyle(fontSize:12.0),
                    )
                  ),
                  Flexible(
                    flex:1,
                    fit:FlexFit.loose,
                    child:Container(
                      width: 28.0,
                      height: 28.0,
                      decoration:BoxDecoration(color:UiHelper.scAzul,borderRadius: BorderRadius.circular(20.0)),
                      child:IconButton(icon:Icon(FontAwesomeIcons.externalLinkAlt,color:Colors.white,size:12.0),onPressed: (){},)
                    )
                  ),
                ],
              ),
            ),

            inputWrapper(field:perfilPassword(bloc:blocUser,showPass: _showPassNew, evtOnPressed: (){
              setState(() { _showPassNew = !_showPassNew; });
            }),withBoxShadow: true),

            SizedBox(height: 10.0,),

            inputWrapper(field:perfilPasswordConfirm(caption: 'Confirmar Contraseña',bloc: blocUser,showPass: _showPassConfirm, evtOnPressed: (){
              setState(() { _showPassConfirm = !_showPassConfirm; });
            }),withBoxShadow: true),

            SizedBox(height: 10.0,),

            Row(
              children: [
                Expanded(child:msgUserAlert(bloc: blocUser),),
                Container( 
                  margin:EdgeInsets.only(right:10.0), 
                  alignment: Alignment.centerRight, 
                  child:iconBoxButton(
                    label:'Guardar',
                    icon:FontAwesomeIcons.solidSave,
                    evtOnTap: (){ updatePassword(); },
                    iconSize: 24, 
                    width: 70, 
                    height:60
                  )
                ),
              ],
            ),

            SizedBox(height: 30.0,),

          ],
      ),
      ),
    );

  }

  Widget _miGrupoFamiliar(BuildContext context){

    return FutureBuilder(
      future: turnos.getBeneficiarios(userPrefs.idCliente),
      builder: (context,AsyncSnapshot<AdherentesResponse> snapshot) {

        if(snapshot.hasData){

          List<Widget> _listItems = [];
          bool altBg = false;
          snapshot.data.data.forEach((element) { 
            _listItems.add(itemGrupoFamiliar(
              title: element['nombres'],
              data: 'Cédula: '+element['ci']+' - Secuencia: '+element['secuencia'],
              dataID: 'Plan: '+element['descripcion'],
              altBg: altBg
            ));
            altBg = !altBg;
          });

          return Container(
            child:Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                headerSliderPlanes(title: 'Miembros del contrato',withActionButton: false),
                Expanded(
                  child: Align(
                    alignment: Alignment.topCenter,
                    child:SingleChildScrollView(child:
                      Column( children: _listItems ),
                    )
                  ),
                ),

              ],
            )
          );
        }else{
          return Center(child: CircularProgressIndicator());
        }

    });

  }

  Widget _misTransacciones(BuildContext context){

    return FutureBuilder(
      future: user.getFacturas(userPrefs.idCliente),
      builder: (context,AsyncSnapshot<FacturasResponse> snapshot) {

        // List<String> dropdownItems = [
        //   'Contrato #1',
        //   'Contrato #2',
        //   'Contrato #3',
        // ];
        // String dropdownValue = dropdownItems[0];

        String foundItems = '0';
        List<Widget> transacciones = new List<Widget>();
        bool altBg = false;
        if(snapshot.hasData){
          int idx = 0;
          facturas.clear();
          for(Map<String,dynamic> item in snapshot.data.data){
            facturas.add(new FacturaCliente.fromJson(item));
            transacciones.add(
              ItemTransaccion(
                index   :idx,
                title   :'Nro: '+item['nro_fact'],
                data    :'Fecha: '+item['fecha'],
                dataID  :'Saldo: '+item['saldo'].toString()+' - '+item['txt_estado'],
                status:(item['txt_estado'] == 'CANCELADO')?EstadoTsx.Realizada:EstadoTsx.Pendiente,
                altBg:altBg,
                evtOnTap: (value){ openDocument(value); },
              )
            );
            altBg = !altBg;
            idx++;
          }
          foundItems = transacciones.length.toString();
        }else{
          transacciones = [];
        }

        return Container(
          child:Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [

              // SizedBox(height: 20.0),
              // inputWrapper(
              //   field:DropdownButton<String>(
              //     value: dropdownValue,
              //     isExpanded: true,
              //     icon: SizedBox(width:24.0,child:Icon(FontAwesomeIcons.angleDown,color:UiHelper.scAzul,size: 16,)),
              //     iconSize: 16,
              //     elevation: 16,
              //     style: TextStyle( color: Color.fromRGBO(70, 70, 70, 1) ),
              //     underline: Container(color:Colors.transparent),
              //     onChanged: (String newValue) { dropdownValue = newValue; },
              //     items: dropdownItems.map<DropdownMenuItem<String>>((String value) {
              //         return DropdownMenuItem<String>(
              //           value: value,
              //           child: Text(value),
              //         );
              //       })
              //       .toList(),
              //   ),
              //   withBoxShadow: true
              // ),
              headerSliderPlanes(title: 'Se encontraon $foundItems transacciones',withActionButton: false),
              Expanded(
                child: Align(
                  alignment: Alignment.topCenter,
                  child:SingleChildScrollView(child:
                    Column(
                      children: transacciones
                    ),
                  ),
                ),
              ),

            ]
          )
        );
        
      }
    );    
        
  }

  void setCurrentPage(int value){
    setState(() { _currentPage = value; });
    _pageController.animateToPage(_currentPage, duration: Duration(milliseconds: 500), curve: Curves.easeInOut);
  }
  
  void _selectPicture(ImageSource source, {BuildContext context}) async {

    final double maxWidth = 300;
    final double maxHeight = 300;
    final int quality = 100;
    
    try {
      final pickedFile = await picker.getImage(
        source: source,
        maxWidth: maxWidth,
        maxHeight: maxHeight,
        imageQuality: quality,
      );
      
      _imageFile = pickedFile;
      setState(() { _imageProfile = _imageFile.path;  });
      _uploadProfilePic();
      
    } catch (e) {
      setState(() { _pickImageError = e; });
      print("_selectPicture error: "+_pickImageError.toString());
    }

  }

  void _uploadProfilePic() async{

    DefaultResponse uploadStatus;
    uploadStatus = await user.uploadUserAvatar(File(_imageProfile),userPrefs.id);
    if(uploadStatus.status){
      List<dynamic> files = uploadStatus.data['files'];
      Map<String,dynamic> uploadedFile = files[0];
      print(uploadedFile['fileUri']);
      setState(() { _imageProfile = uploadedFile['fileUri']; });
      userPrefs.imagenPerfil = _imageProfile; 
    }else{
      userPrefs.imagenPerfil = _imageProfile;
    }
    
  }

  int get currentPage => _currentPage;

  updateDatos() async {

    if(blocUser.validUserDataForm != null){
      
      String uid          = userPrefs.id;
      String email        = userPrefs.email;
      String nombre       = userPrefs.nombreCompleto;
      String nroDocumento = userPrefs.ci;
      String nroTelefono  = userPrefs.telefono;

      print("$uid - $email - $nombre - $nroDocumento - $nroTelefono ");

      if(validateUserEdit()){
        if(!blocUser.requestingStatus){
        
          // activamos el control de request
          blocUser.requestingController.value = true;
          setBlocRequestStatus(msg:'Verificando, aguarde ...!',status: true,type:'loading',bloc:blocUser);
          
          EditUserRequest editRequest = new EditUserRequest(
            uid       : uid,
            ci        : nroDocumento,
            email     : email,
            nombre    : nombre,
            telefono  : nroTelefono,
          );

          // update request
          EditUserResponse respuesta = await auth.editUser(editRequest);
          
          if(respuesta.status){
            blocUser.passwordController.value = "";
            blocUser.passwordConfirmController.value = "";
            setBlocRequestStatus(msg:respuesta.message,status:respuesta.status,type:'success',bloc:blocUser);
            clearUserMsgAlert(blocUser);
          }else{
            // algo paso y te mostramos un mensaje
            setBlocRequestStatus(msg:respuesta.message,status:respuesta.status,bloc:blocUser);
            clearUserMsgAlert(blocUser);
          }
          
        }
      }else{
        setBlocRequestStatus(msg:'Los campos de usuario son obligatorios!',bloc:blocUser);
        clearUserMsgAlert(blocUser);
      }
      
    }else{
      setBlocRequestStatus(msg:'Ingresar datos de usuario!',bloc:blocUser);
    }

  }

  updatePassword() async {

    if(blocUser.validPasswordForm != null){
      
      String password     = blocUser.password;
      String passwordConf = blocUser.passwordConfirm;
      String uid          = userPrefs.id;

      print("$uid - $password - $passwordConf ");

      if(validatePasswordEdit()){

        if(!blocUser.requestingStatus){
        
          // activamos el control de request
          blocUser.requestingController.value = true;
          setBlocRequestStatus(msg:'Verificando, aguarde ...',type:'loading',status:true,bloc:blocUser);
          
          ChangePassRequest editRequest = new ChangePassRequest(
            uid       : uid,
            password  : blocUser.password
          );

          // update request
          EditUserResponse respuesta = await auth.changePass(editRequest);
          
          if(respuesta.status){
            setBlocRequestStatus(msg:respuesta.message,status:respuesta.status,type: 'success',bloc:blocUser);
            clearUserMsgAlert(blocUser);
          }else{
            // algo paso y te mostramos un mensaje
            setBlocRequestStatus(msg:respuesta.message,status:respuesta.status,bloc:blocUser);
            clearUserMsgAlert(blocUser);
          }
          
        }

      }else{
        setBlocRequestStatus(msg:'Los campos de password son obligatorios!',bloc:blocUser);
        clearUserMsgAlert(blocUser);
      }
      
    }else{
      setBlocRequestStatus(msg:'Ingresar datos de usuario!',bloc:blocUser);
    }

  }

  void setBlocRequestStatus({String msg,String type = 'info', bool status = false, UserBloc bloc}){
    bloc.requestStatusMsgController.value = msg;
    bloc.requestStatusTypeController.value = type;
    bloc.requestStatusController.value = status;
  }

  validateUserEdit(){
    if(nombreApellido != null && email != null && nroDocumento != null ){
      if(nombreApellido.length > 0 && email.length > 0 && nroDocumento.length > 0){
        return true;
      }else{
        return false;
      }
    }else{
      return false;
    }
  }

  validatePasswordEdit(){
    if(blocUser.password != null && blocUser.passwordConfirm != null ){
      if( blocUser.password.length > 0 && blocUser.passwordConfirm.length > 0 ){
        return true;
      }else{
        return false;
      }
    }else{
      return false;
    }
  }

  openDocument(int idx){
    if(facturas[idx].estado == 'CANCELADO'){
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => Factura(factura: facturas[idx]),
        ),
      );
    }else{
      AppProvider().showToast('La transacción no generó comprobante',context);
    }
    
  }

}


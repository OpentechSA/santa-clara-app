import 'dart:convert';

import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:santa_clara/src/models/genericModels.dart';
import 'package:santa_clara/src/pages/guiaMedica/info_guia_medica.dart';
import 'package:santa_clara/src/pages/guiaMedica/marker_guiaMedica.dart';
import 'package:santa_clara/src/pages/resultados/resultado_detail.dart';
import 'package:santa_clara/src/services/userService.dart';
import 'package:santa_clara/utils/uiHelper.dart';
import 'package:santa_clara/utils/constants.dart';
import 'package:santa_clara/src/modules/appProvider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:santa_clara/utils/userPreferences.dart';

class CarnetUsuario extends StatefulWidget {

  CarnetUsuario({Key key}) : super(key: key);
  @override
  createState() => CarnetUsuarioState();
}


class CarnetUsuarioState extends State<CarnetUsuario> with UiHelper {

  var userData;
  String urlImage;
  var userJson = {};
  bool userBool = false;

  final UserPreferences userPrefs = new UserPreferences();
  static UserProfile user = new UserProfile();

  final picker = ImagePicker();
  PickedFile _imageFile;
  dynamic _pickImageError;
  String _imageProfile;

  Future<void> onCarnet() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();

    var queryParameters = {
      'uid': prefs.getString("uidUser")
    };

    final response = await http.post(
        Constants.apiUrl+'usuarios/carnetDigital',
        body : queryParameters
    );

    if(response?.statusCode != 200){
      var statusCode = response?.statusCode.toString();
      print(response.body);

    }else{

      var decodeResp = json.decode(response.body);

      if(decodeResp['status'] != false) {
        dynamic datas = decodeResp['data'];

        setState(() {
          userData = datas;
          urlImage = "http://stage.santaclara.com.py:8086/api/usuarios/qrcode?uid=${prefs.getString("uidUser")}";
          userJson['ciUser'] = prefs.getString("ci_user").toString();
          userJson['idCliente'] = prefs.getString("id-cliente").toString();
          userJson['email'] = prefs.getString("email").toString();
          userBool = true;
        });
      } else {
        setState(() {
          userData = null;
          urlImage = "http://stage.santaclara.com.py:8086/api/usuarios/qrcode?uid=${prefs.getString("uidUser")}";
          userJson['ciUser'] = prefs.getString("ci_user").toString();
          userJson['idCliente'] = prefs.getString("id-cliente").toString();
          userJson['email'] = prefs.getString("email").toString();
          userBool = true;
        });
      }

    }
    return "Listo";

  }


  @override
  void initState() {
    onCarnet();
    userPrefs.initPrefs();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext context) {

    setContext(context);

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(70.0),
          child: Container(
              padding:EdgeInsets.only(top:10.0,bottom:10.0,left: 45.0),
              child: AppBar(
                  elevation: 0.0,
                  titleSpacing: 0.0,
                  automaticallyImplyLeading: true,
                  title: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                              child:Text(
                                "Volver",
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color:UiHelper.scAzulOscuro,
                                  fontSize: 14.0,
                                ),
                              )
                          ),
                          Padding(padding:EdgeInsets.only(right:10.0),child: SizedBox(width: 100.0,child:Image(
                            image: new AssetImage('assets/images/logo-santa-clara.png'),
                            width: (this.getSize.width * 0.10),
                            fit: BoxFit.contain,
                          ))),
                        ],
                      )
                  )
              )
          )
      ),
      body: Stack(
          children:<Widget>[
            backgroundLayout(sinLogo: true),
            _mainContent(context),
          ]
      ),
    );

  }

  void _selectPicture(ImageSource source, {BuildContext context}) async {

    final double maxWidth = 300;
    final double maxHeight = 300;
    final int quality = 100;

    try {
      final pickedFile = await picker.getImage(
        source: source,
        maxWidth: maxWidth,
        maxHeight: maxHeight,
        imageQuality: quality,
      );

      _imageFile = pickedFile;
      setState(() { _imageProfile = _imageFile.path;  });
      _uploadProfilePic();

    } catch (e) {
      setState(() { _pickImageError = e; });
      print("_selectPicture error: "+_pickImageError.toString());
    }

  }

  void _uploadProfilePic() async{

    DefaultResponse uploadStatus;
    uploadStatus = await user.uploadUserAvatar(File(_imageProfile),userPrefs.id);
    if(uploadStatus.status){
      List<dynamic> files = uploadStatus.data['files'];
      Map<String,dynamic> uploadedFile = files[0];
      print(uploadedFile['fileUri']);
      setState(() { _imageProfile = uploadedFile['fileUri']; });
      userPrefs.imagenPerfil = _imageProfile;
    }else{
      userPrefs.imagenPerfil = _imageProfile;
    }

  }

  Widget userBox(){

    BoxDecoration _boxDecoProfilePic = BoxDecoration(
        image: DecorationImage(
            image: AssetImage(UiHelper.noProfilePic),
            fit:BoxFit.cover
        ),
        border:Border.all(width:2.0, color:UiHelper.scAzul ),
        borderRadius: BorderRadius.circular(5.0)
    );
    Color iconColor = Colors.transparent;

    if(userPrefs.imagenPerfil.length > 0){
      _boxDecoProfilePic = BoxDecoration(
          image: DecorationImage(
              image: (isUri(userPrefs.imagenPerfil))?NetworkImage(userPrefs.imagenPerfil,scale: 1):FileImage(File(userPrefs.imagenPerfil)),
              fit:BoxFit.cover
          ),
          border:Border.all(width:2.0, color:UiHelper.scAzul ),
          borderRadius: BorderRadius.circular(5.0)
      );
      iconColor = Colors.transparent;
    }

    return Column(
      children: [

        // profile-pic and picker
        Stack(
          children: [
            // profile-pic
            Container(
              width: 80.0,
              height: 80.0,
              margin:EdgeInsets.symmetric(vertical:15.0),
              padding:EdgeInsets.all(10.0),
              child: Container(
                  decoration: _boxDecoProfilePic,
                  child:IconButton(
                      icon:Icon(FontAwesomeIcons.image,color:iconColor,size:24.0),
                      onPressed: (){
                        _selectPicture(ImageSource.gallery,context:context);
                      }
                  )
              ),
            ),
            // image-picker-btn
            Positioned(
              bottom: 0.0,
              child: Container(
                width: 80,
                child:IconButton(
                  icon:Container(
                      width:28.0,
                      height:28.0,
                      decoration: BoxDecoration(
                        color:UiHelper.scCeleste,
                        borderRadius: BorderRadius.circular(28.0),
                      ),
                      child:Icon(FontAwesomeIcons.camera,color:Colors.white,size:13.0)
                  ),
                  onPressed: (){
                    _selectPicture(ImageSource.camera,context:context);
                  },
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

  String _radioValue = 'profesionales'; //Initial definition of radio button value
  String choice;

  static Color scAzul = Color.fromRGBO(21, 61, 138, 1);

  var selectedString;

  Widget _mainContent(BuildContext ctx){

    final blocMain = AppProvider.getMainBloc(ctx);
    String image = "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/QR_code_for_mobile_English_Wikipedia.svg/1200px-QR_code_for_mobile_English_Wikipedia.svg.png";

    return Container(
        margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
        padding: EdgeInsets.all(0.0),
        width: getSize.width-45.0,
        //decoration: BoxDecoration( color:UiHelper.greenContainer, ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[

            // campo-busqueda
            SafeArea(
              child: Padding(
                padding:EdgeInsets.only(bottom:0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage("assets/images/background.png"),
                              fit: BoxFit.fill
                          )
                      ),
                      padding: EdgeInsets.only(bottom: 20),
                      child: new Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              userBox(),
                              new Text("CARNET DIGITAL",
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.black
                                ),
                              ),
                            ],
                          )
                        ],
                      )
                    ),
                  ],
                ),
              ),
            ),

            userBool ? new Flexible(
              flex: 20,
              child: SingleChildScrollView(
                  child: new Column(
                    children: [
                      // inputWrapper(
                      //   field: StatefulBuilder(
                      //     builder: (BuildContext context, setState) {
                      //       return new DropdownButtonFormField(
                      //         validator: (value) => value == null ? 'Es requerido' : null,
                      //         hint: new Text("Seleccionar grupo familiar"),
                      //         isExpanded: true,
                      //         items: ["Grupo 1", "Grupo 2"].map((data) {
                      //           return new DropdownMenuItem(
                      //             child: new Text(data, style: TextStyle(fontSize: 15.0),),
                      //             value: data,
                      //           );
                      //         }).toList(),
                      //         onChanged: (value) {
                      //           print(value);
                      //         },
                      //         value: selectedString,
                      //       );
                      //     }
                      //   ),
                      //   withBoxShadow: false
                      // ),
                      // new Padding(padding: EdgeInsets.all(10)),
                      Container(
                        padding: EdgeInsets.all(20),
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height * 0.7,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage("assets/carnet/carnet.png"),
                            fit: BoxFit.contain
                          ),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                                margin: EdgeInsets.only(left: 50, right: 50, top: 100),
                                child: Image.network(urlImage,
                                  fit: BoxFit.fill,
                                )
                            ),
                            new Container(
                              margin: EdgeInsets.only(top: 20),
                              width: MediaQuery.of(context).size.width * 0.5,
                              height: 5,
                              decoration: BoxDecoration(
                                color: Colors.black,
                                border: Border.all(
                                  color: Colors.black
                                )
                              ),
                            ),
                            new Padding(padding: EdgeInsets.all(10)),
                            new Text("Romina Labiste",
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w600
                              ),
                            ),
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                new Text("CI:",
                                  style: TextStyle(
                                    fontWeight: FontWeight.w600
                                  ),
                                ),
                                new Text(" ${userJson['ciUser']}")
                              ],
                            ),
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                new Text("ID:",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600
                                  ),
                                ),
                                new Text(" ${userJson['idCliente']}")
                              ],
                            ),
                          ],
                        ),
                      )
                    ],
                  )
              ),
            ) : Center(child: CircularProgressIndicator())
          ],
        )
    );
  }

}
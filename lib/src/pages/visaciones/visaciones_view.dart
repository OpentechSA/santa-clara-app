import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:santa_clara/src/pages/guiaMedica/info_guia_medica.dart';
import 'package:santa_clara/src/pages/guiaMedica/marker_guiaMedica.dart';
import 'package:santa_clara/src/pages/resultados/resultado_detail.dart';
import 'package:santa_clara/src/pages/viewer/document_viewer.dart';
import 'package:santa_clara/utils/uiHelper.dart';
import 'package:santa_clara/utils/constants.dart';
import 'package:santa_clara/src/modules/appProvider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';

class VisacionesView extends StatefulWidget {

  final item;

  VisacionesView({Key key, this.item}) : super(key: key);
  @override
  createState() => VisacionesViewState();
}


class VisacionesViewState extends State<VisacionesView> with UiHelper {

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext context) {

    setContext(context);

    print(widget.item.toString());

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(70.0),
          child: Container(
              padding:EdgeInsets.only(top:10.0,bottom:10.0,left: 45.0),
              child: AppBar(
                  elevation: 0.0,
                  titleSpacing: 0.0,
                  automaticallyImplyLeading: true,
                  title: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                              child:Text(
                                "Volver",
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color:UiHelper.scAzulOscuro,
                                  fontSize: 14.0,
                                ),
                              )
                          ),
                          Padding(padding:EdgeInsets.only(right:10.0),child: SizedBox(width: 100.0,child:Image(
                            image: new AssetImage('assets/images/logo-santa-clara.png'),
                            width: (this.getSize.width * 0.10),
                            fit: BoxFit.contain,
                          ))),
                        ],
                      )
                  )
              )
          )
      ),
      body: Stack(
          children:<Widget>[
            backgroundLayout(sinLogo: true),
            _mainContent(context),
          ]
      ),
    );

  }

  String _radioValue = 'profesionales'; //Initial definition of radio button value
  String choice;

  static Color scAzul = Color.fromRGBO(21, 61, 138, 1);


  Widget _mainContent(BuildContext ctx){

    final blocMain = AppProvider.getMainBloc(ctx);

    return Container(
        margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
        padding: EdgeInsets.all(0.0),
        width: getSize.width-45.0,
        //decoration: BoxDecoration( color:UiHelper.greenContainer, ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[

            // campo-busqueda
            SafeArea(
              child: Padding(
                padding:EdgeInsets.only(bottom:0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage("assets/images/background.png"),
                              fit: BoxFit.fill
                          )
                      ),
                      height: 50,
                      padding: EdgeInsets.only(right: 12, left: 12, bottom: 5),
                      child: new Row(
                        children: <Widget>[
                          new Image.asset("assets/visaciones/visaciones.png", width: 30,),
                          new Padding(padding: EdgeInsets.all(5)),
                          new Text("SOLICITUD DE VISACION",
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                                color: Colors.black
                            ),
                          )
                        ],
                      )
                    ),
                    new Padding(
                      padding: EdgeInsets.only(top: 15, right: 15, left: 15, bottom: 12),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          new Text("Estado de la solicitud",
                            style: TextStyle(
                              fontSize: 18,
                              color: Colors.black,
                              fontWeight: FontWeight.w300
                            )
                          ),
                          new Container(
                            decoration: BoxDecoration(
                              color: Color(0xffff2d55),
                              borderRadius: BorderRadius.circular(20)
                            ),
                            padding: EdgeInsets.all(10),
                            child: new Row(
                              children: [
                                new Icon(
                                  Icons.cancel,
                                  color: Colors.white,
                                  size: 12,
                                ),
                                new Text(" No disponible",
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w600
                                  ),
                                )
                              ],
                            )
                          ),
                        ],
                      )
                    ),
                    new Padding(
                      padding: EdgeInsets.only(right: 12, left: 12, top: 10),
                      child: new Row(
                        children: [
                          new Image.asset("assets/visaciones/person.png",
                            width: 30,
                          ),
                          new Text(" Datos de la solicitud",
                            style: TextStyle(
                              fontSize: 20,
                              color: scAzul,
                              fontWeight: FontWeight.w600
                            )
                          )
                        ],
                      )
                    ),
                  ],
                ),
              ),
            ),

            new Flexible(
              flex: 20,
              child: SingleChildScrollView(
                child: new Column(
                  children: [
                    inputWrapper(
                      field: StatefulBuilder(
                        builder: (BuildContext context, setState) {
                          return Container(
                            width: MediaQuery.of(context).size.width,
                            child: TextFormField(
                              initialValue: widget.item['name'],
                              readOnly: true,
                              decoration: InputDecoration(
                                labelText: 'Nombre y Apellido',
                                labelStyle: TextStyle(
                                    fontSize: 12
                                ),
                                border: InputBorder.none,
                              ),
                            ),
                          );
                        }
                      ),
                      withBoxShadow: false
                    ),
                    inputWrapper(
                      field: StatefulBuilder(
                        builder: (BuildContext context, setState) {
                          return Container(
                            width: MediaQuery.of(context).size.width,
                            child: TextFormField(
                              initialValue: widget.item['ci'],
                              readOnly: true,
                              decoration: InputDecoration(
                                labelText: 'Nro de cédula',
                                labelStyle: TextStyle(
                                    fontSize: 12
                                ),
                                border: InputBorder.none,
                              ),
                            ),
                          );
                        }
                      ),
                      withBoxShadow: false
                    ),
                    inputWrapper(
                      field: StatefulBuilder(
                        builder: (BuildContext context, setState) {
                          return Container(
                            width: MediaQuery.of(context).size.width,
                            child: TextFormField(
                              initialValue: widget.item['email'],
                              readOnly: true,
                              decoration: InputDecoration(
                                labelText: 'Correo Electrónico',
                                labelStyle: TextStyle(
                                    fontSize: 12
                                ),
                                border: InputBorder.none,
                              ),
                            ),
                          );
                        }
                      ),
                      withBoxShadow: false
                    ),
                    inputWrapper(
                      field: StatefulBuilder(
                        builder: (BuildContext context, setState) {
                          return Container(
                            width: MediaQuery.of(context).size.width,
                            child: TextFormField(
                              initialValue: widget.item['telefono'],
                              readOnly: true,
                              decoration: InputDecoration(
                                labelText: 'Celular o Telefono',
                                labelStyle: TextStyle(
                                    fontSize: 12
                                ),
                                border: InputBorder.none,
                              ),
                            ),
                          );
                        }
                      ),
                      withBoxShadow: false
                    ),
                    inputWrapper(
                      field: StatefulBuilder(
                        builder: (BuildContext context, setState) {
                          return Container(
                            width: MediaQuery.of(context).size.width,
                            child: TextFormField(
                              initialValue: widget.item['consulta'],
                              readOnly: true,
                              maxLines: 7,
                              decoration: InputDecoration(
                                labelText: 'Texto de la consulta',
                                labelStyle: TextStyle(
                                    fontSize: 12
                                ),
                                border: InputBorder.none,
                              ),
                            ),
                          );
                        }
                      ),
                      withBoxShadow: false
                    ),
                    new Padding(
                      padding: EdgeInsets.only(top: 30, right: 12, left: 12, bottom: 30),
                      child: new Row(
                        children: [
                          new Image.asset("assets/visaciones/file.png",
                            width: 30,
                          ),
                          new Text("  Adjuntos",
                            style: TextStyle(
                              fontSize: 20,
                              color: scAzul,
                              fontWeight: FontWeight.w700
                            )
                          )
                        ],
                      )
                    ),
                    ListView.builder(
                      padding: EdgeInsets.zero,
                      shrinkWrap: true,
                      itemCount: widget.item['adjunto'].length,
                      itemBuilder: (BuildContext context, i) {
                        return inputWrapper(
                          field: StatefulBuilder(
                            builder: (BuildContext context, setState) {

                              List valueUri = [];

                              if (widget.item['adjunto'] != null) {
                                Uri uri = Uri.parse(widget.item['adjunto'][i]['fileUri']);
                                valueUri = uri.pathSegments;
                              }

                              return Container(
                                width: MediaQuery.of(context).size.width,
                                child: TextFormField(
                                  readOnly: true,
                                  initialValue: widget.item['adjunto'] != null ? valueUri[3] : "No disponible",
                                  decoration: InputDecoration(
                                    suffix: SizedBox(
                                      child: new InkWell(
                                        onTap: () async {
                                          if (widget.item['adjunto'] != null) {
                                            if (widget.item['adjunto'][i]['fileUri'].toString().contains('.png') || widget.item['adjunto'][i]['fileUri'].toString().contains('.jpg')) {
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) => DocumentViewer(
                                                      documentType: 'png',
                                                      document: widget.item['adjunto'][i]['fileUri'],
                                                    ),
                                                  )
                                              );
                                            } else if (widget.item['adjunto'][i]['fileUri'].toString().contains('.pdf')) {
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) => DocumentViewer(
                                                      documentType: 'pdf',
                                                      document: widget.item['adjunto'][i]['fileUri'],
                                                    ),
                                                  )
                                              );
                                            } else {
                                              alertaSiNo(
                                                title: 'Error',
                                                txtMessage: 'No se puede obtener la vista previa del archivo..',
                                                ctx: context,
                                              );
                                            }
                                          } else {
                                            alertaSiNo(
                                              title: 'Error',
                                              txtMessage: 'No se puede obtener la vista previa del archivo..',
                                              ctx: context,
                                            );
                                          }
                                        },
                                        child: new Container(
                                          decoration: BoxDecoration(
                                              color: scAzul,
                                              borderRadius: BorderRadius.circular(30)
                                          ),
                                          padding: EdgeInsets.all(5),
                                          child: new Icon(
                                            Icons.remove_red_eye,
                                            color: Colors.white,
                                            size: 20,
                                          ),
                                        )
                                      )
                                    ),
                                    labelText: 'Seleccionar archivo',
                                    labelStyle: TextStyle(
                                        fontSize: 12
                                    ),
                                    border: InputBorder.none,
                                  ),
                                ),
                              );
                            }
                          ),
                          withBoxShadow: false
                        );
                      },
                    ),
                    new Padding(
                      padding: EdgeInsets.all(20)
                    )
                  ],
                )
              ),
            )
          ],
        )
    );
  }

}
import 'dart:convert';
import 'dart:developer' as developer;

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:santa_clara/src/pages/guiaMedica/info_guia_medica.dart';
import 'package:santa_clara/src/pages/guiaMedica/marker_guiaMedica.dart';
import 'package:santa_clara/src/pages/resultados/resultado_detail.dart';
import 'package:santa_clara/src/pages/visaciones/visacion_solicitud.dart';
import 'package:santa_clara/src/pages/visaciones/visaciones_view.dart';
import 'package:santa_clara/utils/uiHelper.dart';
import 'package:santa_clara/utils/constants.dart';
import 'package:santa_clara/src/modules/appProvider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';

class VisacionesPage extends StatefulWidget {

  VisacionesPage({Key key}) : super(key: key);
  @override
  createState() => VisacionesPageState();
}


class VisacionesPageState extends State<VisacionesPage> with UiHelper {

  List visacionesList = List();

  bool resultadosListBool = false;
  bool search = false;

  Future<void> onListVisaciones() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();

    // var queryParameters = {
    //   'ci': prefs.getString("ci_user"),
    //   'uid': prefs.getString("uidUser"),
    // };

    var queryParameters = {
      'ci': "3723342",
      'uid': "ce83d7832ec8fe80e50a3af7a9e57279",
    };

    final response = await http.post(
        Constants.apiUrl+'solicitudes/visaciones',
        body : queryParameters
    );

    if(response?.statusCode != 200){

      var statusCode = response?.statusCode.toString();

      print(statusCode.toString());

    }else{

      var decodeResp = json.decode(response.body);

      if(decodeResp['status'] != false) {
        dynamic datas = decodeResp['data'];

        datas.sort((a, b) => int.parse(b['id']).compareTo(int.parse(a['id'])));

        setState(() {
          visacionesList = datas;
        });
      }
    }

    setState(() {
      resultadosListBool = true;
    });

    return "Listo";

  }

  @override
  void initState() {
    super.initState();
    onListVisaciones();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext context) {

    setContext(context);

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(70.0),
          child: Container(
              padding:EdgeInsets.only(top:10.0,bottom:10.0,left: 45.0),
              child: AppBar(
                  elevation: 0.0,
                  titleSpacing: 0.0,
                  automaticallyImplyLeading: true,
                  title: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                              child:Text(
                                "Volver",
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color:UiHelper.scAzulOscuro,
                                  fontSize: 14.0,
                                ),
                              )
                          ),
                          Padding(padding:EdgeInsets.only(right:10.0),child: SizedBox(width: 100.0,child:Image(
                            image: new AssetImage('assets/images/logo-santa-clara.png'),
                            width: (this.getSize.width * 0.10),
                            fit: BoxFit.contain,
                          ))),
                        ],
                      )
                  )
              )
          )
      ),
      body: Stack(
          children:<Widget>[
            backgroundLayout(sinLogo: true),
            _mainContent(context),
          ]
      ),
      floatingActionButton: new GestureDetector(
        onTap: () async => Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => VisacionesSolicitud(),
          ),
        ),
        child: Container(
            decoration: BoxDecoration(
                color: Colors.lightBlue,
                borderRadius: BorderRadius.circular(10)
            ),
            child: new Padding(
                padding: EdgeInsets.all(10),
                child: new Icon(
                  Icons.arrow_forward_ios,
                  size: 30,
                  color: Colors.white,
                )
            )
        ),
      ),
    );

  }

  String _radioValue = 'profesionales'; //Initial definition of radio button value
  String choice;

  static Color scAzul = Color.fromRGBO(21, 61, 138, 1);


  Widget _mainContent(BuildContext ctx){

    final blocMain = AppProvider.getMainBloc(ctx);

    return Container(
        margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
        padding: EdgeInsets.all(0.0),
        width: getSize.width-45.0,
        //decoration: BoxDecoration( color:UiHelper.greenContainer, ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[

            // campo-busqueda
            SafeArea(
              child: Padding(
                padding:EdgeInsets.only(bottom:0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage("assets/images/background.png"),
                              fit: BoxFit.fill
                          )
                      ),
                      height: 50,
                      padding: EdgeInsets.only(right: 12, left: 12, bottom: 5),
                      child: new Row(
                        children: <Widget>[
                          new Image.asset("assets/visaciones/visaciones.png", width: 30,),
                          new Padding(padding: EdgeInsets.all(5)),
                          new Text("SOLICITUDES DE VISACION",
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                                color: Colors.black
                            ),
                          )
                        ],
                      )
                    ),
                    new Padding(
                      padding: EdgeInsets.only(right: 12, left: 12, top: 10),
                      child: new Text("Solicitudes de asistencia",
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w600
                        )
                      )
                    ),
                  ],
                ),
              ),
            ),

            resultadosListBool == true ? new Flexible(
              flex: 20,
              child: visacionesList.length > 0 ? new ListView.builder(
                padding: EdgeInsets.zero,
                shrinkWrap: true,
                itemCount: visacionesList.length,
                itemBuilder: (ctx, i) {
                  return new GestureDetector(
                    onTap: () async => Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => VisacionesView(
                          item: visacionesList[i]
                        ),
                      ),
                    ),
                    child: new Card(
                      elevation: 1,
                      shadowColor: Colors.cyan,
                      margin: EdgeInsets.all(10),
                      child: new ListTile(
                        leading: new Padding(
                          padding: EdgeInsets.all(5),
                          child: new Image.asset("assets/visaciones/pendiente.png")
                        ),
                        title: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            new Row(
                              children: [
                                Expanded(
                                  child: new Text("Nro. de visación: ${visacionesList[i]['ci']}",
                                    style: TextStyle(
                                        fontSize: 8,
                                        fontWeight: FontWeight.w600
                                    ),
                                  ),
                                ),
                                // Expanded(
                                //   child: new Text("No disponible",
                                //     style: TextStyle(
                                //         fontSize: 8,
                                //         fontWeight: FontWeight.w600,
                                //         color: Colors.green
                                //     ),
                                //   ),
                                // ),
                              ],
                            ),
                            new Text("${visacionesList[i]['nombre']}",
                              style: TextStyle(
                                  fontSize: 13,
                                  fontWeight: FontWeight.w500,
                                  color: scAzul
                              ),
                            )
                          ],
                        ),
                        subtitle: new Row(
                          children: [
                            new Expanded(
                              child: new Text("Fecha solicitud: ",
                                style: TextStyle(
                                    fontSize: 13,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.grey
                                ),
                              )
                            ),
                            new Expanded(
                              child: new Text("No disponible",
                                style: TextStyle(
                                    fontSize: 13,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.black
                                ),
                              )
                            ),
                          ],
                        ),
                        trailing: new Icon(
                          Icons.arrow_forward_ios,
                          color: scAzul,
                        )
                      ),
                    )
                  );
                },
              ) : new Padding(
                padding: EdgeInsets.all(10),
                child: new Center(
                  child: new Text("No hay visaciones disponibles en este momento")
                )
              ),
            ) : new Center(child: CircularProgressIndicator())
          ],

        )
    );
  }

}
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:santa_clara/src/pages/guiaMedica/info_guia_medica.dart';
import 'package:santa_clara/src/pages/guiaMedica/marker_guiaMedica.dart';
import 'package:santa_clara/src/pages/resultados/resultado_detail.dart';
import 'package:santa_clara/utils/uiHelper.dart';
import 'package:santa_clara/utils/constants.dart';
import 'package:santa_clara/src/modules/appProvider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';

class SolicitudesView extends StatefulWidget {

  final solicitud;

  SolicitudesView({Key key, this.solicitud}) : super(key: key);
  @override
  createState() => SolicitudesViewState();
}

class SolicitudesViewState extends State<SolicitudesView> with UiHelper {

  var selectString;
  var solicitudResponse;
  bool listBool = true;
  List planesList = List();
  var userDatas;

  Future<void> buscarCliente() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();

    var queryParameters = {
      'ci': prefs.getString("ci_user"),
      'idCliente': widget.solicitud['ci']
    };

    final response = await http.post(
        Constants.apiUrl+'solicitudes/buscarCliente',
        body : queryParameters
    );

    if(response?.statusCode != 200){
      var statusCode = response?.statusCode.toString();
      print(response.body);

    }else{

      var decodeResp = json.decode(response.body);

      if (decodeResp['status'] == true) {
        dynamic datas = decodeResp['data'];

        setState(() {
          userDatas = datas;
          planesList = userDatas['planes'] != null ? userDatas['planes'] : List();
        });

        await onSolicitudes();
      } else {
        alertaSiNo(
          title: 'Error',
          txtMessage: 'No se pude obtener los datos..',
          ctx: context,
        );
      }

    }
    return "Listo";

  }

  Future<void> onSolicitudes() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();

    var queryParameters = {
      'expediente': widget.solicitud['expediente'].toString()
    };

    final response = await http.post(
        Constants.apiUrl+'solicitudes/reintegro',
        body : queryParameters
    );

    if(response?.statusCode != 200){
      var statusCode = response?.statusCode.toString();
      print(response.body);

    }else{

      var decodeResp = json.decode(response.body);

      dynamic datas = decodeResp['data'];

      setState(() {
        listBool = false;
        solicitudResponse = datas;

        _switchValue = solicitudResponse['devolucion'] == 0 ? false : true;
      });

    }
    return "Listo";

  }

  Future<void> anularReintegro(xHr) async {

    var queryParameters = {
      'xHr': xHr
    };

    final response = await http.post(
        Constants.apiUrl+'solicitudes/anularReintegro',
        body : queryParameters
    );

    if(response?.statusCode != 200){
      var statusCode = response?.statusCode.toString();
      print(response.body);

    }else{

      var decodeResp = json.decode(response.body);

      Navigator.pushReplacementNamed(context, 'solicitudes');

    }
    return "Listo";

  }

  @override
  void initState() {
    buscarCliente();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext context) {

    setContext(context);

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(70.0),
          child: Container(
              padding:EdgeInsets.only(top:10.0,bottom:10.0,left: 45.0),
              child: AppBar(
                  elevation: 0.0,
                  titleSpacing: 0.0,
                  automaticallyImplyLeading: true,
                  title: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                              child:Text(
                                "Volver",
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color:UiHelper.scAzulOscuro,
                                  fontSize: 14.0,
                                ),
                              )
                          ),
                          Padding(padding:EdgeInsets.only(right:10.0),child: SizedBox(width: 100.0,child:Image(
                            image: new AssetImage('assets/images/logo-santa-clara.png'),
                            width: (this.getSize.width * 0.10),
                            fit: BoxFit.contain,
                          ))),
                        ],
                      )
                  )
              )
          )
      ),
      body: Stack(
          children:<Widget>[
            backgroundLayout(sinLogo: true),
            _mainContent(context),
          ]
      ),
      floatingActionButton: new GestureDetector(
        onTap: () => alertaSiNo(
            title: 'Anular solicitud',
            txtMessage: 'Está seguro que desea eliminar la solicitud de reintegro?',
            ctx: context,
            yesAction:() {
              Navigator.of(context).pop();
              anularReintegro(solicitudResponse['sha_id']);
            },
        ),
        child: Container(
            decoration: BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.circular(10)
            ),
            child: new Padding(
                padding: EdgeInsets.all(10),
                child: new Icon(
                  Icons.block,
                  size: 30,
                  color: Colors.white,
                )
            )
        ),
      ),
    );

  }

  String _radioValue = 'profesionales'; //Initial definition of radio button value
  String choice;

  static Color scAzul = Color.fromRGBO(21, 61, 138, 1);

  bool _switchValue = true;

  var selectedString;


  Widget _mainContent(BuildContext ctx){

    final blocMain = AppProvider.getMainBloc(ctx);

    return Container(
        margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
        padding: EdgeInsets.all(0.0),
        width: getSize.width-45.0,
        //decoration: BoxDecoration( color:UiHelper.greenContainer, ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[

            // campo-busqueda
            SafeArea(
              child: Padding(
                padding:EdgeInsets.only(bottom:0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Container(
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage("assets/images/background.png"),
                                fit: BoxFit.fill
                            )
                        ),
                        height: 50,
                        padding: EdgeInsets.only(right: 12, left: 12, bottom: 5),
                        child: new Row(
                          children: <Widget>[
                            new Image.asset("assets/solicitudes/solicitudes.png", width: 30,),
                            new Padding(padding: EdgeInsets.all(5)),
                            new Text("SOLICITUD DE REINTEGRO",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.black
                              ),
                            )
                          ],
                        )
                    ),
                    new Padding(
                        padding: EdgeInsets.only(top: 20, right: 12, left: 12),
                        child: new Row(
                          children: [
                            new Image.asset("assets/visaciones/person.png",
                              width: 30,
                            ),
                            new Text(" Datos del solicitante",
                                style: TextStyle(
                                    fontSize: 20,
                                    color: scAzul,
                                    fontWeight: FontWeight.w600
                                )
                            )
                          ],
                        )
                    ),
                    !listBool ? new Padding(
                        padding: EdgeInsets.only(top: 15, right: 15, left: 15, bottom: 12),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            new Container(
                                decoration: BoxDecoration(
                                    color: solicitudResponse['estado'] == "APROBADO" ? Colors.green :
                                        solicitudResponse['estado'] == "RECHAZADO" ? Colors.red :
                                        Colors.lightBlue,
                                    borderRadius: BorderRadius.circular(20)
                                ),
                                padding: EdgeInsets.all(10),
                                child: new Row(
                                  children: [
                                    new Icon(
                                      Icons.cancel,
                                      color: Colors.white,
                                      size: 12,
                                    ),
                                    new Text(solicitudResponse['estado'],
                                      style: TextStyle(
                                          fontSize: 11,
                                          color: Colors.white,
                                          fontWeight: FontWeight.w600
                                      ),
                                    )
                                  ],
                                )
                            ),
                            new Text("Pedir devolución",
                                style: TextStyle(
                                    fontSize: 15,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w300
                                )
                            ),
                            CupertinoSwitch(
                              trackColor: Colors.red, // **INACTIVE STATE COLOR**
                              activeColor: Colors.green, // **ACTIVE STATE COLOR**
                              value: _switchValue,
                              onChanged: (bool value) {
                                setState(() =>  _switchValue = value);
                              },
                            )
                          ],
                        )
                    ) : new Padding(padding: EdgeInsets.zero),
                  ],
                ),
              ),
            ),

            new Flexible(
              flex: 20,
              child: SingleChildScrollView(
                  child: !listBool ? new Column(
                    children: [
                      inputWrapper(
                          field: StatefulBuilder(
                              builder: (BuildContext context, setState) {
                                return Container(
                                  width: MediaQuery.of(context).size.width,
                                  child: TextFormField(
                                    initialValue: widget.solicitud['ci'].toString(),
                                    readOnly: true,
                                    decoration: InputDecoration(
                                      labelText: 'Nro de cédula',
                                      labelStyle: TextStyle(
                                          fontSize: 12
                                      ),
                                      border: InputBorder.none,
                                    ),
                                  ),
                                );
                              }
                          ),
                          withBoxShadow: false
                      ),
                      new Padding(padding: EdgeInsets.all(5)),
                      inputWrapper(
                          field: StatefulBuilder(
                              builder: (BuildContext context, setState) {
                                return Container(
                                  width: MediaQuery.of(context).size.width,
                                  child: TextFormField(
                                    initialValue: solicitudResponse['nombre'].toString(),
                                    readOnly: true,
                                    decoration: InputDecoration(
                                      labelText: 'Nombre del Solicitante',
                                      labelStyle: TextStyle(
                                          fontSize: 12
                                      ),
                                      border: InputBorder.none,
                                    ),
                                  ),
                                );
                              }
                          ),
                          withBoxShadow: false
                      ),
                      new Padding(padding: EdgeInsets.all(5)),
                      inputWrapper(
                          field: StatefulBuilder(
                              builder: (BuildContext context, setState) {
                                return Container(
                                  width: MediaQuery.of(context).size.width,
                                  child: TextFormField(
                                    initialValue: solicitudResponse['apellido'].toString(),
                                    readOnly: true,
                                    decoration: InputDecoration(
                                      labelText: 'Apellido del solicitante',
                                      labelStyle: TextStyle(
                                          fontSize: 12
                                      ),
                                      border: InputBorder.none,
                                    ),
                                  ),
                                );
                              }
                          ),
                          withBoxShadow: false
                      ),
                      new Padding(padding: EdgeInsets.all(5)),
                      inputWrapper(
                          field: StatefulBuilder(
                              builder: (BuildContext context, setState) {
                                return Container(
                                  width: MediaQuery.of(context).size.width,
                                  child: TextFormField(
                                    initialValue: solicitudResponse['email'].toString(),
                                    readOnly: true,
                                    decoration: InputDecoration(
                                      labelText: 'Correo electrónico',
                                      labelStyle: TextStyle(
                                          fontSize: 12
                                      ),
                                      border: InputBorder.none,
                                    ),
                                  ),
                                );
                              }
                          ),
                          withBoxShadow: false
                      ),
                      new Padding(padding: EdgeInsets.all(5)),
                      inputWrapper(
                        field: StatefulBuilder(
                            builder: (BuildContext context, setState) {
                              return Container(
                                width: MediaQuery.of(context).size.width,
                                child: TextFormField(
                                  initialValue: solicitudResponse['telefono'].toString(),
                                  readOnly: true,
                                  decoration: InputDecoration(
                                    labelText: 'Telefono del solicitante',
                                    labelStyle: TextStyle(
                                        fontSize: 12
                                    ),
                                    border: InputBorder.none,
                                  ),
                                ),
                              );
                            }
                        ),
                        withBoxShadow: false
                      ),
                      new Padding(padding: EdgeInsets.all(5)),
                      inputWrapper(
                          field: new DropdownButtonFormField(
                            validator: (value) => value == null ? 'Es requerido' : null,
                            hint: new Text("Seleccionar plan del solicitante"),
                            isExpanded: true,
                            items: planesList.map((data) {
                              return new DropdownMenuItem(
                                child: new Text(data['plan'].toString(), style: TextStyle(fontSize: 15.0),),
                                value: data['id'].toString(),
                              );
                            }).toList(),
                            onChanged: (value) {
                              selectString = value;
                            },
                            onSaved: (value) {
                              selectString = value;
                            },
                            value: selectedString,
                          ),
                          withBoxShadow: false
                      ),
                      new Padding(
                          padding: EdgeInsets.only(top: 30, right: 12, left: 12, bottom: 10),
                          child: new Row(
                            children: [
                              new Image.asset("assets/solicitudes/solicitudes.png",
                                width: 30,
                              ),
                              new Text(" Datos del comprobante",
                                  style: TextStyle(
                                      fontSize: 20,
                                      color: scAzul,
                                      fontWeight: FontWeight.w700
                                  )
                              )
                            ],
                          )
                      ),
                      new Row(
                        children: [
                          new Container(
                            width: MediaQuery.of(context).size.width * 0.2,
                            child: inputWrapper(
                              paddingLeft: 12,
                              paddingRight: 0,
                              field: StatefulBuilder(
                                  builder: (BuildContext context, setState) {
                                    return Container(
                                      width: MediaQuery.of(context).size.width,
                                      child: TextFormField(
                                        initialValue: solicitudResponse['fact_sucursal'].toString(),
                                        readOnly: true,
                                        decoration: InputDecoration(
                                          labelText: 'Sucursal',
                                          labelStyle: TextStyle(
                                              fontSize: 12
                                          ),
                                          border: InputBorder.none,
                                        ),
                                      ),
                                    );
                                  }
                              ),
                              withBoxShadow: false
                            ),
                          ),
                          new Container(
                            width: MediaQuery.of(context).size.width * 0.2,
                            child: inputWrapper(
                              paddingLeft: 5,
                              paddingRight: 1,
                              field: StatefulBuilder(
                                  builder: (BuildContext context, setState) {
                                    return Container(
                                      width: MediaQuery.of(context).size.width,
                                      child: TextFormField(
                                        initialValue: solicitudResponse['fact_puesto_emisor'].toString(),
                                        readOnly: true,
                                        decoration: InputDecoration(
                                          labelText: 'Emisor',
                                          labelStyle: TextStyle(
                                              fontSize: 12
                                          ),
                                          border: InputBorder.none,
                                        ),
                                      ),
                                    );
                                  }
                              ),
                              withBoxShadow: false
                            ),
                          ),
                          new Container(
                            width: MediaQuery.of(context).size.width * 0.4,
                            child: inputWrapper(
                                paddingLeft: 5,
                                paddingRight: 1,
                                field: StatefulBuilder(
                                    builder: (BuildContext context, setState) {
                                      return Container(
                                        width: MediaQuery.of(context).size.width,
                                        child: TextFormField(
                                          initialValue: solicitudResponse['fact_nro'].toString(),
                                          readOnly: true,
                                          decoration: InputDecoration(
                                            labelText: 'Nro de factura',
                                            labelStyle: TextStyle(
                                                fontSize: 12
                                            ),
                                            border: InputBorder.none,
                                          ),
                                        ),
                                      );
                                    }
                                ),
                                withBoxShadow: false
                            ),
                          ),
                        ],
                      ),
                      new Row(
                        children: [
                          new Container(
                            width: MediaQuery.of(context).size.width * 0.4,
                            child: inputWrapper(
                                paddingLeft: 12,
                                paddingRight: 0,
                                field: StatefulBuilder(
                                    builder: (BuildContext context, setState) {
                                      return Container(
                                        width: MediaQuery.of(context).size.width,
                                        child: TextFormField(
                                          initialValue: solicitudResponse['fact_fecha'].toString(),
                                          readOnly: true,
                                          decoration: InputDecoration(
                                            suffix: SizedBox(
                                                child: new GestureDetector(
                                                    onTap: () => print(""),
                                                    child: new Icon(
                                                      Icons.calendar_today,
                                                      color: scAzul,
                                                      size: 15,
                                                    )
                                                )
                                            ),
                                            labelText: 'Fecha inicio',
                                            labelStyle: TextStyle(
                                                fontSize: 12
                                            ),
                                            border: InputBorder.none,
                                          ),
                                        ),
                                      );
                                    }
                                ),
                                withBoxShadow: false
                            ),
                          ),
                          new Container(
                            width: MediaQuery.of(context).size.width * 0.4,
                            child: inputWrapper(
                                paddingLeft: 5,
                                paddingRight: 1,
                                field: StatefulBuilder(
                                    builder: (BuildContext context, setState) {
                                      return Container(
                                        width: MediaQuery.of(context).size.width,
                                        child: TextFormField(
                                          initialValue: solicitudResponse['fact_monto'].toString(),
                                          readOnly: true,
                                          decoration: InputDecoration(
                                            labelText: 'Monto',
                                            labelStyle: TextStyle(
                                                fontSize: 12
                                            ),
                                            border: InputBorder.none,
                                          ),
                                        ),
                                      );
                                    }
                                ),
                                withBoxShadow: false
                            ),
                          ),
                        ],
                      ),
                      new Padding(
                          padding: EdgeInsets.only(top: 30, right: 12, left: 12, bottom: 10),
                          child: new Row(
                            children: [
                              new Image.asset("assets/solicitudes/file.png",
                                width: 30,
                              ),
                              new Text(" Adjuntos",
                                  style: TextStyle(
                                      fontSize: 20,
                                      color: scAzul,
                                      fontWeight: FontWeight.w700
                                  )
                              )
                            ],
                          )
                      ),
                      inputWrapper(
                          field: StatefulBuilder(
                              builder: (BuildContext context, setState) {
                                return GestureDetector(
                                    onTap: () => print("Hello"),
                                    child: Container(
                                      width: MediaQuery.of(context).size.width,
                                      child: TextFormField(
                                        readOnly: true,
                                        initialValue: solicitudResponse['fact_comprobante'],
                                        decoration: InputDecoration(
                                          suffix: SizedBox(
                                              child: new GestureDetector(
                                                  onTap: () => print(""),
                                                  child: new Container(
                                                    decoration: BoxDecoration(
                                                        color: scAzul,
                                                        borderRadius: BorderRadius.circular(30)
                                                    ),
                                                    padding: EdgeInsets.all(5),
                                                    child: new Icon(
                                                      Icons.remove_red_eye,
                                                      color: Colors.white,
                                                      size: 15,
                                                    ),
                                                  )
                                              )
                                          ),
                                          labelText: 'Seleccionar archivo',
                                          labelStyle: TextStyle(
                                              fontSize: 12
                                          ),
                                          border: InputBorder.none,
                                        ),
                                      ),
                                    )
                                );
                              }
                          ),
                          withBoxShadow: false
                      ),
                      inputWrapper(
                          field: StatefulBuilder(
                              builder: (BuildContext context, setState) {
                                return GestureDetector(
                                    onTap: () => print("Hello"),
                                    child: Container(
                                      width: MediaQuery.of(context).size.width,
                                      child: TextFormField(
                                        readOnly: true,
                                        initialValue: solicitudResponse['ord_medica_comprobante'],
                                        decoration: InputDecoration(
                                          suffix: SizedBox(
                                              child: new GestureDetector(
                                                  onTap: () => print(""),
                                                  child: new Container(
                                                    decoration: BoxDecoration(
                                                        color: scAzul,
                                                        borderRadius: BorderRadius.circular(30)
                                                    ),
                                                    padding: EdgeInsets.all(5),
                                                    child: new Icon(
                                                      Icons.remove_red_eye,
                                                      color: Colors.white,
                                                      size: 15,
                                                    ),
                                                  )
                                              )
                                          ),
                                          labelText: 'Seleccionar archivo',
                                          labelStyle: TextStyle(
                                              fontSize: 12
                                          ),
                                          border: InputBorder.none,
                                        ),
                                      ),
                                    )
                                );
                              }
                          ),
                          withBoxShadow: false
                      ),
                      new Padding(
                          padding: EdgeInsets.all(20)
                      )
                    ],
                  ) : new Padding(padding: EdgeInsets.zero)
              ),
            )
          ],
        )
    );
  }

}
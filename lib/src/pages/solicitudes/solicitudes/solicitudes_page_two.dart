import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:santa_clara/src/pages/solicitudes/solicitudes/solicitudes_page_three.dart';
import 'package:santa_clara/src/pages/viajeros/asistencia/viajeros_page_two.dart';
import 'package:santa_clara/utils/uiHelper.dart';
import 'package:santa_clara/src/modules/appProvider.dart';

class SolicitudesPageTwo extends StatefulWidget {

  final switchValue;
  final selectedPlan;
  final userDatas;

  SolicitudesPageTwo({Key key, this.switchValue, this.selectedPlan, this.userDatas}) : super(key: key);
  @override
  createState() => SolicitudesPageTwoState();
}


class SolicitudesPageTwoState extends State<SolicitudesPageTwo> with UiHelper {

  List<Map<String, dynamic>> pacientes = [];

  @override
  void initState() {
    pacientes.add({"id": 1, "secuencia": 1, "ci": "", "nombre": "", "apellido": ""});
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext context) {

    setContext(context);

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(70.0),
          child: Container(
              padding:EdgeInsets.only(top:10.0,bottom:10.0,left: 45.0),
              child: AppBar(
                  elevation: 0.0,
                  titleSpacing: 0.0,
                  automaticallyImplyLeading: true,
                  title: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                              child:Text(
                                "Volver",
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color:UiHelper.scAzulOscuro,
                                  fontSize: 14.0,
                                ),
                              )
                          ),
                          Padding(padding:EdgeInsets.only(right:10.0),child: SizedBox(width: 100.0,child:Image(
                            image: new AssetImage('assets/images/logo-santa-clara.png'),
                            width: (this.getSize.width * 0.10),
                            fit: BoxFit.contain,
                          ))),
                        ],
                      )
                  )
              )
          )
      ),
      body: Stack(
          children:<Widget>[
            backgroundLayout(sinLogo: true),
            _mainContent(context),
          ]
      ),
      floatingActionButton: new GestureDetector(
        onTap: () async => Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => SolicitudesPageThree(
              userDatas: widget.userDatas,
              switchValue: _switchValue,
              selectedPlan: widget.selectedPlan,
              pacientes: pacientes
            ),
          ),
        ),
        child: Container(
            decoration: BoxDecoration(
                color: Colors.lightBlue,
                borderRadius: BorderRadius.circular(10)
            ),
            child: new Padding(
                padding: EdgeInsets.all(10),
                child: new Icon(
                  Icons.arrow_forward_ios,
                  size: 30,
                  color: Colors.white,
                )
            )
        ),
      ),
    );

  }

  String _radioValue = 'profesionales'; //Initial definition of radio button value
  String choice;

  bool _switchValue = true;

  Widget pacientesAdd() {

    return new Container(
        padding: EdgeInsets.only(top: 10),
        child: new Column(
          children: [
            new Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                new Container(
                    padding: EdgeInsets.only(left: 8, right: 8, top: 3, bottom: 5),
                    margin: EdgeInsets.only(left: 10),
                    decoration: BoxDecoration(
                        color: Color(0xffFFCC00),
                        borderRadius: BorderRadius.circular(20)
                    ),
                    child: new Text("1",
                      style: TextStyle(
                          fontSize: 10,
                          fontWeight: FontWeight.w600,
                          color: scAzul
                      ),
                    )
                ),
                new Column(
                  children: [
                    inputWrapperHelper(
                        width: MediaQuery.of(context).size.width * 0.7,
                        context: context,
                        child: Container(
                            margin: EdgeInsets.symmetric(horizontal: 10.0),
                            child: StatefulBuilder(
                                builder: (BuildContext context, setState) {
                                  return Container(
                                    width: MediaQuery.of(context).size.width,
                                    child: TextField(
                                      decoration: InputDecoration(
                                        labelText: 'Nro de cédula',
                                        labelStyle: TextStyle(
                                            fontSize: 12
                                        ),
                                        border: InputBorder.none,
                                      ),
                                    ),
                                  );
                                }
                            )
                        )
                    ),
                    inputWrapperHelper(
                        width: MediaQuery.of(context).size.width * 0.7,
                        context: context,
                        child: Container(
                            margin: EdgeInsets.symmetric(horizontal: 10.0),
                            child: StatefulBuilder(
                                builder: (BuildContext context, setState) {
                                  return Container(
                                    width: MediaQuery.of(context).size.width,
                                    child: TextField(
                                      decoration: InputDecoration(
                                        labelText: 'Nombre del solicitante',
                                        labelStyle: TextStyle(
                                            fontSize: 12
                                        ),
                                        border: InputBorder.none,
                                      ),
                                    ),
                                  );
                                }
                            )
                        )
                    ),
                    inputWrapperHelper(
                        width: MediaQuery.of(context).size.width * 0.7,
                        context: context,
                        child: Container(
                            margin: EdgeInsets.symmetric(horizontal: 10.0),
                            child: StatefulBuilder(
                                builder: (BuildContext context, setState) {
                                  return Container(
                                    width: MediaQuery.of(context).size.width,
                                    child: TextField(
                                      decoration: InputDecoration(
                                        labelText: 'Apellido del solicitante',
                                        labelStyle: TextStyle(
                                            fontSize: 12
                                        ),
                                        border: InputBorder.none,
                                      ),
                                    ),
                                  );
                                }
                            )
                        )
                    ),
                  ],
                ),
              ],
            ),
            new Padding(
                padding: EdgeInsets.all(20)
            ),
            new Stack(
              alignment: Alignment(1, 0),
              children: [
                new Container(
                  color: Colors.grey,
                  width: MediaQuery.of(context).size.width,
                  height: 1,
                ),
                new Container(
                  decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(20)
                  ),
                  margin: EdgeInsets.only(right: 20),
                  padding: EdgeInsets.all(10),
                  child: new Icon(
                    Icons.restore_from_trash,
                    color: Colors.white,
                    size: 20,
                  ),
                ),
              ],
            ),
            new Padding(
                padding: EdgeInsets.all(20)
            )
          ],
        )
    );
  }

  static Color scAzul = Color.fromRGBO(21, 61, 138, 1);

  var selectedString;

  Widget _mainContent(BuildContext ctx){

    final blocMain = AppProvider.getMainBloc(ctx);

    return Container(
        margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
        padding: EdgeInsets.all(0.0),
        width: getSize.width-45.0,
        //decoration: BoxDecoration( color:UiHelper.greenContainer, ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[

            // campo-busqueda
            SafeArea(
              child: Padding(
                padding:EdgeInsets.only(bottom:0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Container(
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage("assets/images/background.png"),
                                fit: BoxFit.fill
                            )
                        ),
                        height: 50,
                        padding: EdgeInsets.only(right: 12, left: 12, bottom: 5),
                        child: new Row(
                          children: <Widget>[
                            new Image.asset("assets/viajeros/viajeros.png", width: 30,),
                            new Padding(padding: EdgeInsets.all(5)),
                            new Text("SOLICITUD DE REINTEGRO",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.black
                              ),
                            )
                          ],
                        )
                    ),
                    new Padding(
                        padding: EdgeInsets.only(top: 15, right: 15, left: 15, bottom: 5),
                        child: new Row(
                          children: [
                            new Container(
                                padding: EdgeInsets.only(left: 12, right: 12, top: 3, bottom: 5),
                                decoration: BoxDecoration(
                                    color: scAzul,
                                    borderRadius: BorderRadius.circular(20)
                                ),
                                child: new Text("2",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.white
                                  ),
                                )
                            ),
                            new Text(" Datos de pacientes",
                                style: TextStyle(
                                    fontSize: 18,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w600
                                )
                            ),
                          ],
                        )
                    ),
                    new Padding(
                        padding: EdgeInsets.only(top: 15, right: 15, left: 15),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            new Text("Pacientes",
                                style: TextStyle(
                                    fontSize: 16,
                                    color: scAzul,
                                    fontWeight: FontWeight.w600
                                )
                            ),
                            new InkWell(
                              onTap: () {
                                int value = pacientes.length;

                                setState(() {
                                  pacientes.add({"id": value + 1, "secuencia": value + 1, "ci": "", "nombre": "", "apellido": ""});
                                });
                              },
                              child: new Container(
                                  decoration: BoxDecoration(
                                      color: scAzul,
                                      borderRadius: BorderRadius.circular(20)
                                  ),
                                  padding: EdgeInsets.all(10),
                                  child: new Row(
                                    children: [
                                      new Icon(
                                        Icons.person_add,
                                        color: Colors.white,
                                        size: 12,
                                      ),
                                      new Text(" Agregar pacientes",
                                        style: TextStyle(
                                            fontSize: 12,
                                            color: Colors.white,
                                            fontWeight: FontWeight.w600
                                        ),
                                      )
                                    ],
                                  )
                              ),
                            ),
                          ],
                        )
                    ),
                  ],
                ),
              ),
            ),

            new Flexible(
              flex: 20,
              child: new Container(
                child: ListView.builder(
                  shrinkWrap: true,
                    itemCount: pacientes.length,
                    itemBuilder: (context, i) {

                      int index = i + 1;

                      return new Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          new Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              new Container(
                                  padding: EdgeInsets.only(left: 8, right: 8, top: 3, bottom: 5),
                                  margin: EdgeInsets.only(left: 10),
                                  decoration: BoxDecoration(
                                      color: Color(0xffFFCC00),
                                      borderRadius: BorderRadius.circular(20)
                                  ),
                                  child: new Text(index.toString(),
                                    style: TextStyle(
                                        fontSize: 10,
                                        fontWeight: FontWeight.w600,
                                        color: scAzul
                                    ),
                                  )
                              ),
                              new Column(
                                children: [
                                  inputWrapperHelper(
                                      width: MediaQuery.of(context).size.width * 0.7,
                                      context: context,
                                      child: Container(
                                          margin: EdgeInsets.symmetric(horizontal: 10.0),
                                          child: StatefulBuilder(
                                              builder: (BuildContext context, setState) {
                                                return Container(
                                                  width: MediaQuery.of(context).size.width,
                                                  child: TextFormField(
                                                    onChanged: (value) {
                                                      setState(() {
                                                        pacientes[i]['ci'] = value;
                                                      });
                                                    },
                                                    decoration: InputDecoration(
                                                      labelText: 'Nro de cédula',
                                                      labelStyle: TextStyle(
                                                          fontSize: 12
                                                      ),
                                                      border: InputBorder.none,
                                                    ),
                                                  ),
                                                );
                                              }
                                          )
                                      )
                                  ),
                                  inputWrapperHelper(
                                      width: MediaQuery.of(context).size.width * 0.7,
                                      context: context,
                                      child: Container(
                                          margin: EdgeInsets.symmetric(horizontal: 10.0),
                                          child: StatefulBuilder(
                                              builder: (BuildContext context, setState) {
                                                return Container(
                                                  width: MediaQuery.of(context).size.width,
                                                  child: TextFormField(
                                                    onChanged: (value) {
                                                      setState(() {
                                                        pacientes[i]['nombre'] = value;
                                                      });
                                                    },
                                                    decoration: InputDecoration(
                                                      labelText: 'Nombre del solicitante',
                                                      labelStyle: TextStyle(
                                                          fontSize: 12
                                                      ),
                                                      border: InputBorder.none,
                                                    ),
                                                  ),
                                                );
                                              }
                                          )
                                      )
                                  ),
                                  inputWrapperHelper(
                                      width: MediaQuery.of(context).size.width * 0.7,
                                      context: context,
                                      child: Container(
                                          margin: EdgeInsets.symmetric(horizontal: 10.0),
                                          child: StatefulBuilder(
                                              builder: (BuildContext context, setState) {
                                                return Container(
                                                  width: MediaQuery.of(context).size.width,
                                                  child: TextFormField(
                                                    onChanged: (value) {
                                                      setState(() {
                                                        pacientes[i]['apellido'] = value;
                                                      });
                                                    },
                                                    decoration: InputDecoration(
                                                      labelText: 'Apellido del solicitante',
                                                      labelStyle: TextStyle(
                                                          fontSize: 12
                                                      ),
                                                      border: InputBorder.none,
                                                    ),
                                                  ),
                                                );
                                              }
                                          )
                                      )
                                  ),
                                ],
                              ),
                            ],
                          ),
                          new Padding(
                              padding: EdgeInsets.all(20)
                          ),
                          new Stack(
                            alignment: Alignment(1, 0),
                            children: [
                              new Container(
                                color: Colors.grey,
                                width: MediaQuery.of(context).size.width,
                                height: 1,
                              ),
                              new InkWell(
                                onTap: () {
                                  setState(() {
                                    pacientes.remove(pacientes[i]);
                                  });
                                },
                                child: new Container(
                                  decoration: BoxDecoration(
                                      color: Colors.red,
                                      borderRadius: BorderRadius.circular(20)
                                  ),
                                  margin: EdgeInsets.only(right: 20),
                                  padding: EdgeInsets.all(10),
                                  child: new Icon(
                                    Icons.restore_from_trash,
                                    color: Colors.white,
                                    size: 20,
                                  ),
                                ),
                              )
                            ],
                          ),
                          new Padding(
                              padding: EdgeInsets.all(20)
                          )
                        ],
                      );
                    }
                )
              ),
            )
          ],
        )
    );
  }

}
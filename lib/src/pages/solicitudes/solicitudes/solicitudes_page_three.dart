import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:santa_clara/src/pages/viajeros/asistencia/viajeros_page_two.dart';
import 'package:santa_clara/utils/constants.dart';
import 'package:santa_clara/utils/uiHelper.dart';
import 'package:santa_clara/src/modules/appProvider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class SolicitudesPageThree extends StatefulWidget {

  final userDatas;
  final switchValue;
  final selectedPlan;
  final List pacientes;

  SolicitudesPageThree({Key key, this.userDatas, this.switchValue, this.selectedPlan, this.pacientes}) : super(key: key);
  @override
  createState() => SolicitudesPageThreeState();
}

class SolicitudesPageThreeState extends State<SolicitudesPageThree> with UiHelper {

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  File factura;
  File ordenMedica;

  String sucursal;
  String facturaNro;
  String fecha;
  String emisor;

  void uploadFactura() async {
    FilePickerResult result = await FilePicker.platform.pickFiles();

    if(result != null) {
      factura = File(result.files.single.path);
    }
  }

  void uploadOrdenMedica() async {
    FilePickerResult result = await FilePicker.platform.pickFiles();

    if(result != null) {
      ordenMedica = File(result.files.single.path);
    }
  }

  Future<void> onRequestSolicitudes() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();

    var dio = Dio();

    var queryParameters =  FormData.fromMap({
      'idCliente': widget.selectedPlan['id_cliente'].toString(),
      'secuencia': widget.selectedPlan['secuencia'].toString(),
      'plan': widget.selectedPlan['id'].toString(),
      'descPlan': widget.selectedPlan['plan'].toString(),
      'solicitanteCi': widget.userDatas['ci'].toString(),
      'solicitanteNombre': widget.userDatas['nombre'].toString(),
      'solicitanteApellido': widget.userDatas['apellido'].toString(),
      'email': widget.userDatas['email'] != null && widget.userDatas['email'] != '' ? widget.userDatas['email'].toString() : 'example@example.com',
      'telefono': widget.userDatas['celular'].toString(),
      'devolucion': widget.switchValue.toString() == "true" ? "1": '0',
      'motivoDevolucion': 'Sin comentarios',
      'facturaSucursal': sucursal.toString(),
      'facturaPuestoEmisor': emisor.toString(),
      'facturaNro': facturaNro.toString(),
      'facturaFecha': fecha.toString(),
      'facturaMonto': "1",
      'arcFacturaComprobante': await MultipartFile.fromFile('${factura.path}'),
      'arcOrdenComprobante': await MultipartFile.fromFile('${ordenMedica.path}'),
      'uid': prefs.getString('uidUser'),
      'pacientes': widget.pacientes.toString()
    });

    print(queryParameters.toString());

    Response response = await dio.post(Constants.apiUrl+'solicitudes/solicitarReintegro', data: queryParameters);

    print(response.statusCode);
    print(response.data.toString());

    // final response = await http.post(
    //     Constants.apiUrl+'solicitudes/solicitarReintegro',
    //     body : queryParameters
    // );

    // if(response?.statusCode != 200){
    //   var statusCode = response?.statusCode.toString();
    //
    // }else{
    //
    //   var decodeResp = json.decode(response.body);
    //
    //   if(decodeResp['status'] != false) {
    //     print(decodeResp.toString());
    //   } else {
    //     print(decodeResp.toString());
    //   }
    //
    // }
    return "Listo";

  }


  Widget build(BuildContext context) {

    setContext(context);

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(70.0),
          child: Container(
              padding:EdgeInsets.only(top:10.0,bottom:10.0,left: 45.0),
              child: AppBar(
                  elevation: 0.0,
                  titleSpacing: 0.0,
                  automaticallyImplyLeading: true,
                  title: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                              child:Text(
                                "Volver",
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color:UiHelper.scAzulOscuro,
                                  fontSize: 14.0,
                                ),
                              )
                          ),
                          Padding(padding:EdgeInsets.only(right:10.0),child: SizedBox(width: 100.0,child:Image(
                            image: new AssetImage('assets/images/logo-santa-clara.png'),
                            width: (this.getSize.width * 0.10),
                            fit: BoxFit.contain,
                          ))),
                        ],
                      )
                  )
              )
          )
      ),
      body: Stack(
          children:<Widget>[
            backgroundLayout(sinLogo: true),
            _mainContent(context),
          ]
      ),
      floatingActionButton: new GestureDetector(
        onTap: () => onRequestSolicitudes(),
        child: Container(
            decoration: BoxDecoration(
                color: scAzul,
                borderRadius: BorderRadius.circular(10)
            ),
            child: new Padding(
                padding: EdgeInsets.all(10),
                child: Image.asset("assets/solicitudes/save.png", width: 30,)
            )
        ),
      ),
    );

  }

  String _radioValue = 'profesionales'; //Initial definition of radio button value
  String choice;

  bool _switchValue = true;

  static Color scAzul = Color.fromRGBO(21, 61, 138, 1);

  var selectedString;

  Widget _mainContent(BuildContext ctx){

    final blocMain = AppProvider.getMainBloc(ctx);

    return Container(
        margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
        padding: EdgeInsets.all(0.0),
        width: getSize.width-45.0,
        //decoration: BoxDecoration( color:UiHelper.greenContainer, ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[

            // campo-busqueda
            SafeArea(
              child: Padding(
                padding:EdgeInsets.only(bottom:0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Container(
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage("assets/images/background.png"),
                                fit: BoxFit.fill
                            )
                        ),
                        height: 50,
                        padding: EdgeInsets.only(right: 12, left: 12, bottom: 5),
                        child: new Row(
                          children: <Widget>[
                            new Image.asset("assets/viajeros/viajeros.png", width: 30,),
                            new Padding(padding: EdgeInsets.all(5)),
                            new Text("SOLICITUD DE REINTEGRO",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.black
                              ),
                            )
                          ],
                        )
                    ),
                    new Padding(
                        padding: EdgeInsets.only(top: 15, right: 15, left: 15, bottom: 5),
                        child: new Row(
                          children: [
                            new Container(
                                padding: EdgeInsets.only(left: 10, right: 10, top: 3, bottom: 5),
                                decoration: BoxDecoration(
                                    color: scAzul,
                                    borderRadius: BorderRadius.circular(20)
                                ),
                                child: new Text("3",
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.white
                                  ),
                                )
                            ),
                            new Text("  Datos del comprobante",
                                style: TextStyle(
                                    fontSize: 18,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w600
                                )
                            ),
                          ],
                        )
                    ),
                  ],
                ),
              ),
            ),

            new Flexible(
              flex: 20,
              child: SingleChildScrollView(
                  child: new Column(
                    children: [
                      new Row(
                        children: [
                          new Container(
                            width: MediaQuery.of(context).size.width * 0.4,
                            child: inputWrapper(
                                paddingLeft: 12,
                                paddingRight: 0,
                                field: StatefulBuilder(
                                    builder: (BuildContext context, setState) {
                                      return Container(
                                        width: MediaQuery.of(context).size.width,
                                        child: TextFormField(
                                          onChanged: (value) {
                                            setState(() => sucursal = value);
                                          },
                                          decoration: InputDecoration(
                                            labelText: 'Sucursal',
                                            labelStyle: TextStyle(
                                                fontSize: 12
                                            ),
                                            border: InputBorder.none,
                                          ),
                                        ),
                                      );
                                    }
                                ),
                                withBoxShadow: false
                            ),
                          ),
                          // new Container(
                          //   width: MediaQuery.of(context).size.width * 0.2,
                          //   child: inputWrapper(
                          //       paddingLeft: 5,
                          //       paddingRight: 1,
                          //       field: StatefulBuilder(
                          //           builder: (BuildContext context, setState) {
                          //             return Container(
                          //               width: MediaQuery.of(context).size.width,
                          //               child: TextField(
                          //                 decoration: InputDecoration(
                          //                   labelText: 'Emisor',
                          //                   labelStyle: TextStyle(
                          //                       fontSize: 12
                          //                   ),
                          //                   border: InputBorder.none,
                          //                 ),
                          //               ),
                          //             );
                          //           }
                          //       ),
                          //       withBoxShadow: false
                          //   ),
                          // ),
                          new Container(
                            width: MediaQuery.of(context).size.width * 0.4,
                            child: inputWrapper(
                                paddingLeft: 5,
                                paddingRight: 1,
                                field: StatefulBuilder(
                                    builder: (BuildContext context, setState) {
                                      return Container(
                                        width: MediaQuery.of(context).size.width,
                                        child: TextFormField(
                                          onChanged: (value) {
                                            setState(() => facturaNro = value);
                                          },
                                          decoration: InputDecoration(
                                            labelText: 'Nro de factura',
                                            labelStyle: TextStyle(
                                                fontSize: 12
                                            ),
                                            border: InputBorder.none,
                                          ),
                                        ),
                                      );
                                    }
                                ),
                                withBoxShadow: false
                            ),
                          ),
                        ],
                      ),
                      new Padding(padding: EdgeInsets.all(5)),
                      new Row(
                        children: [
                          new Container(
                            width: MediaQuery.of(context).size.width * 0.4,
                            child: inputWrapper(
                                paddingLeft: 12,
                                paddingRight: 0,
                                field: StatefulBuilder(
                                    builder: (BuildContext context, setState) {
                                      return Container(
                                        width: MediaQuery.of(context).size.width,
                                        child: TextFormField(
                                          onChanged: (value) {
                                            setState(() => fecha = value);
                                          },
                                          initialValue: "28/06/2021",
                                          decoration: InputDecoration(
                                            suffix: SizedBox(
                                                child: new GestureDetector(
                                                    onTap: () => print(""),
                                                    child: new Icon(
                                                      Icons.calendar_today,
                                                      color: scAzul,
                                                      size: 15,
                                                    )
                                                )
                                            ),
                                            labelText: 'Fecha de inicio',
                                            labelStyle: TextStyle(
                                                fontSize: 12
                                            ),
                                            border: InputBorder.none,
                                          ),
                                        ),
                                      );
                                    }
                                ),
                                withBoxShadow: false
                            ),
                          ),
                          new Container(
                            width: MediaQuery.of(context).size.width * 0.4,
                            child: inputWrapper(
                                paddingLeft: 5,
                                paddingRight: 1,
                                field: StatefulBuilder(
                                    builder: (BuildContext context, setState) {
                                      return Container(
                                        width: MediaQuery.of(context).size.width,
                                        child: TextFormField(
                                          onChanged: (value) {
                                            setState(() => emisor = value);
                                          },
                                          decoration: InputDecoration(
                                            labelText: 'Emisor',
                                            labelStyle: TextStyle(
                                                fontSize: 12
                                            ),
                                            border: InputBorder.none,
                                          ),
                                        ),
                                      );
                                    }
                                ),
                                withBoxShadow: false
                            ),
                          ),
                        ],
                      ),
                      new Padding(
                          padding: EdgeInsets.only(top: 30, right: 12, left: 12, bottom: 10),
                          child: new Row(
                            children: [
                              new Image.asset("assets/solicitudes/file.png",
                                width: 25,
                              ),
                              new Text(" Adjuntos",
                                  style: TextStyle(
                                      fontSize: 20,
                                      color: scAzul,
                                      fontWeight: FontWeight.w700
                                  )
                              )
                            ],
                          )
                      ),
                      inputWrapper(
                          field: StatefulBuilder(
                              builder: (BuildContext context, setState) {
                                return Container(
                                  width: MediaQuery.of(context).size.width,
                                  child: TextFormField(
                                    readOnly: true,
                                    initialValue: "factura.pdf",
                                    decoration: InputDecoration(
                                      suffix: SizedBox(
                                        child: new GestureDetector(
                                          onTap: () => uploadFactura(),
                                          child: new Icon(
                                            FontAwesomeIcons.solidFolderOpen,
                                            color: Colors.grey[600],
                                            size: 15,
                                          )
                                        )
                                      ),
                                      labelText: 'Comprobante de factura',
                                      labelStyle: TextStyle(
                                          fontSize: 12
                                      ),
                                      border: InputBorder.none,
                                    ),
                                  ),
                                );
                              }
                          ),
                          withBoxShadow: false
                      ),
                      inputWrapper(
                          field: StatefulBuilder(
                              builder: (BuildContext context, setState) {
                                return Container(
                                  width: MediaQuery.of(context).size.width,
                                  child: TextFormField(
                                    readOnly: true,
                                    initialValue: "orden_medica.pdf",
                                    decoration: InputDecoration(
                                      suffix: SizedBox(
                                          child: new GestureDetector(
                                              onTap: () => uploadOrdenMedica(),
                                              child: new Icon(
                                                FontAwesomeIcons.solidFolderOpen,
                                                color: Colors.grey[600],
                                                size: 15,
                                              )
                                          )
                                      ),
                                      labelText: 'Comprobante de order médica',
                                      labelStyle: TextStyle(
                                          fontSize: 12
                                      ),
                                      border: InputBorder.none,
                                    ),
                                  ),
                                );
                              }
                          ),
                          withBoxShadow: false
                      ),
                      new Padding(
                          padding: EdgeInsets.all(20)
                      )
                    ],
                  )
              ),
            )
          ],
        )
    );
  }

}
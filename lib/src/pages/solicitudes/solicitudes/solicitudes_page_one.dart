import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:santa_clara/src/pages/guiaMedica/info_guia_medica.dart';
import 'package:santa_clara/src/pages/guiaMedica/marker_guiaMedica.dart';
import 'package:santa_clara/src/pages/resultados/resultado_detail.dart';
import 'package:santa_clara/src/pages/solicitudes/solicitudes/solicitudes_page_two.dart';
import 'package:santa_clara/src/pages/viajeros/asistencia/viajeros_page_two.dart';
import 'package:santa_clara/utils/uiHelper.dart';
import 'package:santa_clara/utils/constants.dart';
import 'package:santa_clara/src/modules/appProvider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';

class SolicitudesPageOne extends StatefulWidget {

  final solicitud;

  SolicitudesPageOne({Key key, this.solicitud}) : super(key: key);
  @override
  createState() => SolicitudesPageOneState();
}


class SolicitudesPageOneState extends State<SolicitudesPageOne> with UiHelper {

  List planesList = List();
  var userDatas;
  bool listBool = true;
  var selectString;

  Map<String, dynamic> planSelected;

  Future<void> buscarCliente() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();

    // var queryParameters = {
    //   'ci': prefs.getString("ci_user"),
    //   'idCliente': widget.solicitud['id_cliente'].toString()
    // };

    var queryParameters = {
      'ci': "1239180",
      'idCliente': "177768"
    };

    final response = await http.post(
        Constants.apiUrl+'solicitudes/buscarCliente',
        body : queryParameters
    );

    if(response?.statusCode != 200){
      var statusCode = response?.statusCode.toString();
      print(response.body);

    }else{

      var decodeResp = json.decode(response.body);

      print(decodeResp);

      if (decodeResp['status'] == true) {
        dynamic datas = decodeResp['data'];

        setState(() {
          userDatas = datas;
          listBool = false;
          planesList = userDatas['planes'] != null ? userDatas['planes'] : List();
        });
      } else {
        alertaSiNo(
          title: 'Error',
          txtMessage: 'No se pude obtener los datos..',
          ctx: context,
        );
      }

    }
    return "Listo";

  }

  @override
  void initState() {
    buscarCliente();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext context) {

    setContext(context);

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(70.0),
          child: Container(
              padding:EdgeInsets.only(top:10.0,bottom:10.0,left: 45.0),
              child: AppBar(
                  elevation: 0.0,
                  titleSpacing: 0.0,
                  automaticallyImplyLeading: true,
                  title: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                              child:Text(
                                "Volver",
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color:UiHelper.scAzulOscuro,
                                  fontSize: 14.0,
                                ),
                              )
                          ),
                          Padding(padding:EdgeInsets.only(right:10.0),child: SizedBox(width: 100.0,child:Image(
                            image: new AssetImage('assets/images/logo-santa-clara.png'),
                            width: (this.getSize.width * 0.10),
                            fit: BoxFit.contain,
                          ))),
                        ],
                      )
                  )
              )
          )
      ),
      body: Stack(
          children:<Widget>[
            backgroundLayout(sinLogo: true),
            _mainContent(context),
          ]
      ),
      floatingActionButton: new GestureDetector(
        onTap: () async {

          print(planSelected);

          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => SolicitudesPageTwo(
                  switchValue: _switchValue,
                  selectedPlan: planSelected,
                  userDatas: userDatas
              ),
            ),
          );
        },
        child: Container(
            decoration: BoxDecoration(
                color: Colors.lightBlue,
                borderRadius: BorderRadius.circular(10)
            ),
            child: new Padding(
                padding: EdgeInsets.all(10),
                child: new Icon(
                  Icons.arrow_forward_ios,
                  size: 30,
                  color: Colors.white,
                )
            )
        ),
      ),
    );

  }

  String _radioValue = 'profesionales'; //Initial definition of radio button value
  String choice;

  bool _switchValue = true;

  static Color scAzul = Color.fromRGBO(21, 61, 138, 1);

  var selectedString;

  Widget _mainContent(BuildContext ctx){

    final blocMain = AppProvider.getMainBloc(ctx);

    return Container(
        margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
        padding: EdgeInsets.all(0.0),
        width: getSize.width-45.0,
        //decoration: BoxDecoration( color:UiHelper.greenContainer, ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[

            // campo-busqueda
            SafeArea(
              child: Padding(
                padding:EdgeInsets.only(bottom:0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Container(
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage("assets/images/background.png"),
                                fit: BoxFit.fill
                            )
                        ),
                        height: 50,
                        padding: EdgeInsets.only(right: 12, left: 12, bottom: 5),
                        child: new Row(
                          children: <Widget>[
                            new Image.asset("assets/viajeros/viajeros.png", width: 30,),
                            new Padding(padding: EdgeInsets.all(5)),
                            new Text("SOLICITUD DE REINTEGRO",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.black
                              ),
                            )
                          ],
                        )
                    ),
                    new Padding(
                        padding: EdgeInsets.only(top: 15, right: 15, left: 15, bottom: 5),
                        child: new Row(
                          children: [
                            new Container(
                                padding: EdgeInsets.only(left: 12, right: 12, top: 3, bottom: 5),
                                decoration: BoxDecoration(
                                    color: scAzul,
                                    borderRadius: BorderRadius.circular(20)
                                ),
                                child: new Text("1",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.white
                                  ),
                                )
                            ),
                            new Text(" Datos del solicitante",
                                style: TextStyle(
                                    fontSize: 18,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w600
                                )
                            ),
                          ],
                        )
                    ),
                    new Padding(
                        padding: EdgeInsets.only(top: 15, right: 15, left: 15),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            new Text("Pedir devolución",
                              style: TextStyle(
                                fontSize: 15,
                                color: Colors.black,
                                fontWeight: FontWeight.w300
                              )
                            ),
                            CupertinoSwitch(
                              trackColor: Colors.red, // **INACTIVE STATE COLOR**
                              activeColor: Colors.green, // **ACTIVE STATE COLOR**
                              value: _switchValue,
                              onChanged: (bool value) {
                                setState(() =>  _switchValue = value);
                              },
                            )
                          ],
                        )
                    ),
                  ],
                ),
              ),
            ),

            new Flexible(
              flex: 20,
              child: !listBool ? SingleChildScrollView(
                  child: new Column(
                    children: [
                      inputWrapper(
                          field: StatefulBuilder(
                              builder: (BuildContext context, setState) {
                                return Container(
                                  width: MediaQuery.of(context).size.width,
                                  child: TextFormField(
                                    initialValue: userDatas['ci'],
                                    readOnly: true,
                                    decoration: InputDecoration(
                                      labelText: 'Nro de cédula',
                                      labelStyle: TextStyle(
                                          fontSize: 12
                                      ),
                                      border: InputBorder.none,
                                    ),
                                  ),
                                );
                              }
                          ),
                          withBoxShadow: false
                      ),
                      inputWrapper(
                          field: StatefulBuilder(
                              builder: (BuildContext context, setState) {
                                return Container(
                                  width: MediaQuery.of(context).size.width,
                                  child: TextFormField(
                                    initialValue: userDatas['nombre'],
                                    readOnly: true,
                                    decoration: InputDecoration(
                                      labelText: 'Nombre del solicitante',
                                      labelStyle: TextStyle(
                                          fontSize: 12
                                      ),
                                      border: InputBorder.none,
                                    ),
                                  ),
                                );
                              }
                          ),
                          withBoxShadow: false
                      ),
                      inputWrapper(
                          field: StatefulBuilder(
                              builder: (BuildContext context, setState) {
                                return Container(
                                  width: MediaQuery.of(context).size.width,
                                  child: TextFormField(
                                    initialValue: userDatas['apellido'],
                                    readOnly: true,
                                    decoration: InputDecoration(
                                      labelText: 'Apellido del solicitante',
                                      labelStyle: TextStyle(
                                          fontSize: 12
                                      ),
                                      border: InputBorder.none,
                                    ),
                                  ),
                                );
                              }
                          ),
                          withBoxShadow: false
                      ),
                      inputWrapper(
                          field: StatefulBuilder(
                              builder: (BuildContext context, setState) {
                                return Container(
                                  width: MediaQuery.of(context).size.width,
                                  child: TextFormField(
                                    initialValue: userDatas['email'],
                                    readOnly: userDatas['email'] == null ? false : true,
                                    decoration: InputDecoration(
                                      labelText: 'Correo Electrónico',
                                      labelStyle: TextStyle(
                                          fontSize: 12
                                      ),
                                      border: InputBorder.none,
                                    ),
                                  ),
                                );
                              }
                          ),
                          withBoxShadow: false
                      ),
                      inputWrapper(
                          field: StatefulBuilder(
                              builder: (BuildContext context, setState) {
                                return Container(
                                  width: MediaQuery.of(context).size.width,
                                  child: TextFormField(
                                    initialValue: userDatas['celular'],
                                    readOnly: userDatas['celular'] == null ? false : true,
                                    decoration: InputDecoration(
                                      labelText: 'Telefono del solicitante',
                                      labelStyle: TextStyle(
                                          fontSize: 12
                                      ),
                                      border: InputBorder.none,
                                    ),
                                  ),
                                );
                              }
                          ),
                          withBoxShadow: false
                      ),
                      inputWrapper(
                          field: new DropdownButtonFormField(
                            validator: (value) => value == null ? 'Es requerido' : null,
                            hint: new Text("Seleccionar plan del solicitante"),
                            isExpanded: true,
                            items: planesList.map((data) {
                              return new DropdownMenuItem(
                                child: new Text(data['plan'].toString(), style: TextStyle(fontSize: 15.0),),
                                value: data,
                              );
                            }).toList(),
                            onChanged: (value) {
                              setState(() {
                                selectString = value['id'];
                                planSelected = value;
                              });
                            },
                            onSaved: (value) {
                              setState(() {
                                selectString = value['id'];
                                planSelected = value;
                              });
                            },
                            value: selectedString,
                          ),
                          withBoxShadow: false
                      ),
                      new Padding(
                          padding: EdgeInsets.all(20)
                      )
                    ],
                  )
              ) : new Padding(padding: EdgeInsets.zero),
            )
          ],
        )
    );
  }

}
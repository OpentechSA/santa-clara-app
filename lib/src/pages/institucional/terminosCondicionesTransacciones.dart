
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:santa_clara/src/components/menuDrawer.dart';
import 'package:santa_clara/src/components/scAppBar.dart';
import 'package:santa_clara/utils/uiHelper.dart';

class TerminosCondicionesTransacciones extends StatelessWidget with UiHelper {
  
  @override
  Widget build(BuildContext context) {

    setContext(context);

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: ScAppBar(masterView: false,altCaption: 'Términos legales',),
      drawer: MenuDrawer(),
      body: Stack(
        children:<Widget>[
          backgroundLayout(sinLogo: true),
          _mainContent(context)
        ]
      )
    );

  }

  Widget _mainContent(BuildContext ctx){

    return Container(
      margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
      padding: EdgeInsets.all(0.0),
      width: getSize.width-UiHelper.layoutMarginLeft,
      child: Container(
        child:Column(
          children: [

            // header visor-recibo
            Container(
              width: UiHelper.size.width-UiHelper.layoutMarginLeft,
              padding: EdgeInsets.only(top:50),
              decoration:BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    const Color(0xFFFFFFFF),
                    const Color.fromRGBO(230, 248, 255, 1),
                  ],
                ),
              ),
              child: Container(
                margin: EdgeInsets.symmetric(vertical:20),
                child:crearTituloGrande(caption:'POLÍTICA DE COOKIES',textSize:28),
              )
            ),
            Container(
              decoration: BoxDecoration(
                border: Border(bottom:BorderSide(width: 2.0,color:UiHelper.scAzul)),
              )
            ),

            // content visor-factura
            Expanded(
              child:SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.only(top:15,right:15,left:15,bottom: 0),
                  child:RichText(
                    text:TextSpan(
                      style: TextStyle(fontSize: 16,color:UiHelper.scGrisTexto),
                      children:[

                        TextSpan(text:'Este contrato describe los términos y condiciones generales aplicables al uso de los servicios ofrecidos por Santa Clara dentro del Sitio. Cualquier persona (en adelante "USUARIO" o en plural "USUARIOS") que desee acceder y/o usar el sitio o los servicios podrá hacerlo sujetándose a los Términos Legales, junto con todas las demás políticas y principios que rigen y que son incorporados a la presente por referencia. En consecuencia, todas las visitas y transacciones que se realicen en este sitio, como asimismo sus efectos jurídicos, quedarán regidos por estas reglas y sometidas a la legislación aplicable en la República del Paraguay. \n\n'),
                        TextSpan(text:'a.\t',style:UiHelper.boldText),
                        TextSpan(text:'Información relevante\n'),
                        TextSpan(text:'El uso del Sitio implica la aceptación incondicional e irrevocable de que toda la información y actividades realizadas en 	el mismo se encuentran regidas por los Términos Legales.  \nLas formas oficiales de contacto son:	\n• Correo electrónico: info@santaclara.com.py	\n• Por medio de nuestra APP y/o página web www.santaclara.com.py	\n• Teléfono: +595 21 418 00 00.	\n• En nuestros diferentes locales ubicados en:	\n\t- O´Higgins Nº 631 c/ Sucre (Asunción)\n\t- Parapiti Nº 1220 c/ Avda. Rodriguez de Francia (Asunción)\n\t- Av. Gral. Bernardino Caballero esq. 29 de Setiembre (Ciudad del Este)\n\nMediante el uso del SITIO el USUARIO acepta los Términos Legales y declara bajo fe de juramento tener por lo menos 18 años de edad cumplidos. Si es menor de 18 años de edad, deberá abstenerse de usar el SITIO. Los padres, tutores o responsables de los menores de 18 años que utilicen el SITIO son plena y exclusivamente responsables por el uso del SITIO por parte de ellos, incluyendo, y sin limitar, cualquier cargo o costo en que se pueda incurrir como consecuencia de tal uso. Si usted no está de acuerdo en todos y cada uno de los términos legales, abstenerse de usar el SITIO. \n\n'),

                        TextSpan(text:'b.\t',style:UiHelper.boldText),
                        TextSpan(text:'Compra de servicios\n'),
                        TextSpan(text:'La compra de servicios ofrecidos en el SITIO se encuentra sujeta a disponibilidad de estos, de la aprobación de identidad el USUARIO, de la imputación correcta de la tarjeta de crédito o valores transferidos, como a la aceptación del USUARIO de estos Términos Legales. Las fotos de los servicios son meramente ilustrativas. Los servicios, características técnicas, precios, disponibilidad, y ofertas contenidas en este sitio pueden variar sin previo aviso. \nLímite de ventas de servicios a los usuarios: Santa Clara se reserva el derecho de restringir la cantidad de servicios vendidos a un Usuario registrado por motivos de restricciones comerciales, entre otros.\n\n'),

                        TextSpan(text:'c.\t',style:UiHelper.boldText),
                        TextSpan(text:'Condiciones de pago\n'),
                        TextSpan(text:'Las ofertas y promociones publicadas en el SITIO son sólo válidas dentro del período de vigencia de las mismas. El USUARIO podrá pagar su compra con tarjeta de crédito o cualquiera de las formas de pago ofrecidas durante el proceso final de la compra. Los pagos se realizarán únicamente en moneda nacional salvo indicación explícita.\nSanta Clara se reserva el derecho a cambiar las modalidades de pago, pudiendo crear nuevas o eliminar alguna de las existentes, sin que el USUARIO del SITIO pueda realizar reclamos por este motivo.\n\n'),

                        TextSpan(text:'d.\t',style:UiHelper.boldText),
                        TextSpan(text:'Pago con tarjetas\n'),
                        TextSpan(text:'En caso de pagos online, con el objetivo de dar la máxima seguridad al sistema de pago el SITIO utiliza sistemas de pago seguro de Bancard S.A. En este sentido, los datos confidenciales son transmitidos directamente y en formato encriptado (SSL) a la entidad financiera mencionada, el cual confiere plena seguridad a la transmisión de datos a través de la red. Sus datos gozan de total confidencialidad y protección.\nLas tarjetas de crédito se encuentran sujetas a autorizaciones por parte de la entidad emisora de las mismas, pero si dicha entidad no autorizase el pago, Santa Clara no se hace responsable ante la imposibilidad de pago.\n\n'),

                        TextSpan(text:'e.\t',style:UiHelper.boldText),
                        TextSpan(text:'Facturación\n'),
                        TextSpan(text:'Santa Clara entrega las facturas a los usuarios en el correo indicado por el usuario responsable de pago.\n\n'),

                      ]
                    )
                  )
                )
              ),
            ),

          ],
        )
      )
    );

  }

}
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:santa_clara/src/pages/institucional/advertenciaLegal.dart';
import 'package:santa_clara/src/pages/institucional/formasPago.dart';
import 'package:santa_clara/src/pages/institucional/informacionesGenerales.dart';
import 'package:santa_clara/src/pages/institucional/politicasCookies.dart';
import 'package:santa_clara/src/pages/institucional/reglasUso.dart';
import 'package:santa_clara/src/pages/institucional/politicaPrivacidad.dart';
import 'package:santa_clara/src/pages/institucional/terminosCondicionesTransacciones.dart';
import 'package:santa_clara/utils/uiHelper.dart';


class TerminosLegales extends StatefulWidget {
  
  final TerminosLegalesState _state = new TerminosLegalesState();
  TerminosLegales({Key key}) : super(key: key);
  
  @override
  TerminosLegalesState createState() => _state;
  
}

class TerminosLegalesState extends State<TerminosLegales> with UiHelper{

  @override
  void initState() {  super.initState(); }

  Widget build(BuildContext context) {

    setContext(context);
    
    List<Widget> listaFloatingActionButtons = [
      botonCuadrado(
        icon:Icons.arrow_back_ios,
        evtOnPressed: (){
          Navigator.popAndPushNamed(context, 'login');
        }
      )
    ];

    return Scaffold(
      body:Stack(
        children:<Widget>[
          backgroundLayout(sinLogo: true),
          _mainContent()
        ]
      ),
      floatingActionButton: crearBotoneraRegistro(listButtons:listaFloatingActionButtons ),
    );

  }

  Widget _mainContent(){
    return Container(
      margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
      padding: EdgeInsets.all(0.0),
      width: getSize.width-UiHelper.layoutMarginLeft,
      child: SingleChildScrollView(
        child: Container(
          child: _buildPanel(),
        ),
      )
    );
  }

  Widget _linkedlistItem({String caption,Function evtOnTap, IconData icon}){
    return InkWell(
      child: Container(
        padding: EdgeInsets.symmetric(vertical:5),
        child:Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children:[
            SizedBox(width:20,height: 24,child:Icon(icon,size: 15,color: UiHelper.scAzul,)),
            SizedBox(width: 5),
            Text(caption,textAlign:TextAlign.start,style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),)
          ]
        )
      ),
      onTap: evtOnTap,
    );
  }

  Widget _subItemlistItem({String caption,String prefix}){
    return Row( 
      crossAxisAlignment: CrossAxisAlignment.start,
      children:[ 
        SizedBox(width: 15,child:Text(prefix,style: TextStyle(fontWeight: FontWeight.bold),)), 
        SizedBox(width: 5), 
        Text(caption,textAlign:TextAlign.start,softWrap: true,style: TextStyle(fontSize: 14),) 
      ]
    );
  }

  Widget _buildPanel() {
    double maxWidthOptionContainer = getSize.width-UiHelper.layoutMarginLeft;
    return Column(children:[
      SizedBox(height: 40,),
      Container(alignment:Alignment.centerRight, padding: EdgeInsets.only(top:30,bottom:30,right:20),child:logoSantaClara()),
      crearTituloGrande(caption:'TERMINOS LEGALES',textSize: 28),
      Container(
        width: maxWidthOptionContainer,
        padding: EdgeInsets.only(left:20,right:10),
        child:Column(children: [
          _linkedlistItem(caption:'Informaciones Generales',icon:FontAwesomeIcons.angleRight,evtOnTap: (){
            Navigator.push( context, MaterialPageRoute( builder: (context) => InformacionesGenerales(), ), );
          }),
          _linkedlistItem(caption:'Formas de Pago',icon:FontAwesomeIcons.angleDown,evtOnTap: (){
            Navigator.push( context, MaterialPageRoute( builder: (context) => FormasPago(), ), );
          }),
          Container(
            width: maxWidthOptionContainer-15,
            margin: EdgeInsets.only(left:15),
            child:Column(children:[
              _subItemlistItem(prefix:'a. ',caption:'Tarjetas de crédito'),
              _subItemlistItem(prefix:'b. ',caption:'Aquí Pago'),
              _subItemlistItem(prefix:'a. ',caption:'Pago Express'),
              _subItemlistItem(prefix:'d. ',caption:'Efectivo, Débito y Crédito en las oficinas \nde Santa Clara'),
            ])
          ),
          _linkedlistItem(caption:'Reglas de Uso',icon:FontAwesomeIcons.angleDown,evtOnTap: (){
            Navigator.push( context, MaterialPageRoute( builder: (context) => ReglasUso(), ), );
          }),
          Container(
            margin: EdgeInsets.only(left:15),
            child:Column(children:[
              _subItemlistItem(prefix:'a. ',caption:'Generalidades sobre el uso'),
              _subItemlistItem(prefix:'b. ',caption:'Ámbito de Aplicación'),
              _subItemlistItem(prefix:'c. ',caption:'Información sobre Servicios'),
              _subItemlistItem(prefix:'d. ',caption:'Otros Contenidos'),
              _subItemlistItem(prefix:'e. ',caption:'Propiedad Intelectual e Industrial'),
              _subItemlistItem(prefix:'f. ',caption:'Menores de Edad'),
              _subItemlistItem(prefix:'g. ',caption:'Opción de no recepción de correo \nelectrónico'),
              _subItemlistItem(prefix:'h. ',caption:'Seguridad'),
            ])
          ),
          _linkedlistItem(caption:'Política de Privacidad',icon:FontAwesomeIcons.angleRight,evtOnTap: (){
            Navigator.push( context, MaterialPageRoute( builder: (context) => PoliticaPrivacidad(), ), );
          }),
          _linkedlistItem(caption:'Política de Cookies',icon:FontAwesomeIcons.angleRight,evtOnTap: (){
            Navigator.push( context, MaterialPageRoute( builder: (context) => PoliticaCookies(), ), );
          }),
          _linkedlistItem(caption:'Términos y Condiciones de \nTransacciones',icon:FontAwesomeIcons.angleDown,evtOnTap: (){
            Navigator.push( context, MaterialPageRoute( builder: (context) => TerminosCondicionesTransacciones(), ), );
          }),
          Container(
            margin: EdgeInsets.only(left:15),
            child:Column(children:[
              _subItemlistItem(prefix:'a. ',caption:'Información relevante'),
              _subItemlistItem(prefix:'b. ',caption:'Compra de servicios'),
              _subItemlistItem(prefix:'c. ',caption:'Condiciones de pago'),
              _subItemlistItem(prefix:'d. ',caption:'Pago con tarjetas'),
              _subItemlistItem(prefix:'e. ',caption:'Facturación'),
            ])
          ),
        ]),
      ),
      SizedBox(height: 10,),
      crearTituloGrande(caption:'ADVERTENCIA LEGAL',textSize: 28),
      Container(
        padding: EdgeInsets.only(left:20,right:10),
        child:Column(children: [
          _linkedlistItem(caption:'Advertencia Legal',icon:FontAwesomeIcons.angleDown,evtOnTap: (){
            Navigator.push( context, MaterialPageRoute( builder: (context) => AdvertenciaLegal(), ), );
          }),
          Container(
            margin: EdgeInsets.only(left:15),
            child:Column(children:[
              _subItemlistItem(prefix:'a. ',caption:'Uso indebido y responsabilidad'),
              _subItemlistItem(prefix:'b. ',caption:'Jurisdicción y legislación aplicable'),
            ])
          ),
        ],)
      ),
      SizedBox(height: 40,),
    ]);
  }

}
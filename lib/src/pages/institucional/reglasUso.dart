
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:santa_clara/src/components/menuDrawer.dart';
import 'package:santa_clara/src/components/scAppBar.dart';
import 'package:santa_clara/utils/uiHelper.dart';

class ReglasUso extends StatelessWidget with UiHelper {
  
  @override
  Widget build(BuildContext context) {

    setContext(context);

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: ScAppBar(masterView: false,altCaption: 'Términos legales',),
      drawer: MenuDrawer(),
      body: Stack(
        children:<Widget>[
          backgroundLayout(sinLogo: true),
          _mainContent(context)
        ]
      )
    );

  }

  Widget _mainContent(BuildContext ctx){

    return Container(
      margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
      padding: EdgeInsets.all(0.0),
      width: getSize.width-UiHelper.layoutMarginLeft,
      child: Container(
        child:Column(
          children: [

            // header visor-recibo
            Container(
              width: UiHelper.size.width-UiHelper.layoutMarginLeft,
              padding: EdgeInsets.only(top:50),
              decoration:BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    const Color(0xFFFFFFFF),
                    const Color.fromRGBO(230, 248, 255, 1),
                  ],
                ),
              ),
              child: Container(
                margin: EdgeInsets.symmetric(vertical:20),
                child:crearTituloGrande(caption:'REGLAS DE USO',textSize:28),
              )
            ),
            Container(
              decoration: BoxDecoration(
                border: Border(bottom:BorderSide(width: 2.0,color:UiHelper.scAzul)),
              )
            ),

            // content visor-factura
            Expanded(
              child:SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.only(top:15,right:15,left:15,bottom: 0),
                  child:RichText(
                    text:TextSpan(
                      style: TextStyle(fontSize: 16,color:UiHelper.scGrisTexto),
                      children:[

                        TextSpan(text:'a.\t',style:UiHelper.boldText),
                        TextSpan(text:'Generalidades sobre el uso\n'),
                        TextSpan(text:'Santa Clara S.A. Medicina Prepaga es una sociedad debidamente constituida conforme a las leyes de la República del	Paraguay y es titular del sitio web http://www.santaclara.com.py y de la APP SANTA CLARA MEDICINA PREPAGA.\nAl ingresar en el SITIO, los usuarios quedan sujetos a los Términos Legales publicados en el mismo. Recomendamos leer	atentamente los mismos. Advertimos que la utilización por los usuarios de la página www.santaclara.com.py y/o la 	APP SANTA CLARA MEDICINA PREPAGA (el "Sitio"), importa la automática conformidad con los mismos quedando sujetos 	a todos los términos, condiciones, y avisos que puedan encontrarse en estos Términos Legales.\n	Los Términos Legales pueden ser modificados o actualizados en cualquier momento y sin previo aviso. En cualquiera de estos casos, las modificaciones regirán a partir de su publicación en “el Sitio”, por lo que le sugerimos visitar periódicamente y leer atentamente los Términos Legales toda vez que ingrese en “el Sitio” a fin de imponerse de los alcances y contenidos de dichas modificaciones o actualizaciones, antes de hacer uso de los contenidos de “el Sitio”. En caso de que alguna parte de los Términos Legales sea considerada nula, ilegal o que no sea exigible judicialmente por algún tribunal u otro órgano competente, dicha parte será separada del resto de los Términos Legales, los cuales	seguirán teniendo validez y serán exigibles judicialmente. \n\n'),

                        TextSpan(text:'b.\t',style:UiHelper.boldText),
                        TextSpan(text:'Ámbito de Aplicación\n'),
                        TextSpan(text:'Los “Términos Legales” de Santa Clara se aplican solo a este Sitio, no así a los sitios de terceros a los que los usuarios accedan por medio de links. Santa Clara les sugiere que en estos casos se informen acerca de las condiciones de uso adoptadas en cada uno de esos sitios. \nEl acceso a los vínculos de cualquier otro sitio o página quedará bajo su exclusiva responsabilidad y Santa Clara no asume responsabilidad alguna por los daños que pueda causar dicha vinculación. \nExpresamente Santa Clara manifiesta que no asume responsabilidad por los errores, fallas o imposibilidad de acceso a los sitios o páginas en razón de hipervínculos u otras funciones de enlace incluidos en el sitio y si se proporcionan vínculos	con sitios de software para descargar será́ solo para la utilidad de los usuarios y no asumiremos ninguna responsabilidad	por las consecuencias asociadas a la descarga de software. \nEl Sitio puede brindar acceso a la información suministrada por terceros en el Sitio o vínculos de hipertexto con otras direcciones de Internet. \nLa información contenida en nuestro Sitio de ninguna manera manifiesta, ya sea expresa o implícitamente, la adhesión o aprobación del asesoramiento, las opiniones, la información, los productos o servicios de terceros. No controlamos, ni garantizamos, ni asumimos responsabilidad alguna por la exactitud, oportunidad, o incluso la disponibilidad continua o existencia de dichos contenidos, hipervínculos, sitios o páginas de terceros vinculados con este Sitio. \nQueda bajo la exclusiva responsabilidad de los usuarios el acceso a este Sitio desde una jurisdicción distinta de la República del Paraguay, y Santa Clara se desliga de toda responsabilidad por cualquier incumplimiento de leyes o regulaciones locales en que los usuarios puedan incurrir como consecuencia del uso de la página y el ingreso en la misma. \nLas personas que encontrándose fuera de este país accedan a estaspáginasdeberán requerir asesoramiento profesional independiente sobre la posibilidad de acceder a las mismas desde el lugar en que se encuentren.\n\n'),

                        TextSpan(text:'c.\t',style:UiHelper.boldText),
                        TextSpan(text:'Información sobre Servicios\n'),
                        TextSpan(text:'El sitio contiene información sobre los servicios que Santa Clara comercializa en el ámbito de la República del Paraguay. \nLa información contenida sobre los mismos no es una descripción completa de todos los términos y condiciones aplicables a los productos y/o servicios que se ofrecen y no se encuentra libre de imprecisiones técnicas, errores tipográficos, referencias a productos o servicios discontinuados o no disponibles u otros errores. DESTACAMOS QUE TODA LA INFORMACIÓN CONTENIDA EN ESTE SITIO ACERCA DE LOS PRODUCTOS Y SERVICIOS SE PROPORCIONA ÚNICAMENTE CON CARÁCTER INFORMATIVO. Para una descripción completa del alcance y limitaciones de las coberturas solicite información a nuestros asesores. \nQUEDA ENTENDIDO, CONVENIDO Y ACEPTADO POR EL USUARIO QUE LA INFORMACIÓN PROPORCIONADA POR EL SITIO SOBRE LOS PRODUCTOS Y SERVICIOS DE LA COMPAÑÍA NO CONSTITUYE UNA OFERTA, PROPUESTA O INVITACIÓN A CONTRATAR. \nLos productos y servicios están sujetos a los términos y condiciones del contrato correspondiente a cada uno de ellos. \nLa información sobre los productos y servicios se refiere exclusivamente a los que son comercializados en la República del Paraguay. Los productos descriptos en estas páginas no están destinados a ser comercializados en jurisdicciones distintas de la República del Paraguay. \n\n'),

                        TextSpan(text:'d.\t',style:UiHelper.boldText),
                        TextSpan(text:'Otros contenidos\n'),
                        TextSpan(text:'El resto de la información contenida en el sitio no debe ser interpretada como asesoramiento legal, fiscal o profesional 	de ningún tipo ni podrá entenderse que dicha información implica la adopción de posiciones u opiniones respecto de ningún tema. En consecuencia, Santa Clara no asume responsabilidad alguna al respecto. \n\n'),

                        TextSpan(text:'e.\t',style:UiHelper.boldText),
                        TextSpan(text:'Propiedad Intelectual e Industrial\n'),
                        TextSpan(text:'El derecho de propiedad intelectual sobre la página, los contenidos, las pantallas, el material utilizado para su preparación y toda otra obra científica, literaria o artística incluidas en este Sitio pertenece a Santa Clara o bien 	Santa Clara es licenciataria del propietario y tiene la facultad de publicarla, ejecutarla, exponerla en público, adaptarla o reproducirla. \nNo pueden ser modificados, copiados, reproducidos o distribuidos, transmitidos, divulgados, licenciados, cedidos, ni se encuentra permitida la creación de trabajos derivados del uso de la información, el software, los productos o servicios incluidos en el sitio sin el previo consentimiento por escrito de Santa Clara. \nSe permite imprimir, copiar, descargar y almacenar fragmentos de este Sitio exclusivamente para información personal de los usuarios, o para facilitar el uso de algún producto o servicio.	\nSe prohíbe cualquier otro uso a menos que se solicite y obtenga de Santa Clara el consentimiento escrito previo.	\nSanta Clara se reserva el derecho de efectuar, en cualquier momento y sin previo aviso, cuantas modificaciones, variaciones, supresiones o cancelaciones en los contenidos y en la forma de presentación de los mismos considere necesarias. \nTodas las marcas comerciales, dibujos, emblemas, imágenes, logotipos, isotipos, combinaciones de colores, combinación de letras y números, frases publicitarias y todo otro signo con capacidad distintiva de los productos o servicios de Santa Clara que aparezcan publicados en este sitio son propiedad de Santa Clara y se encuentran protegidos por las leyes que regulan la Propiedad Intelectual, las Marcas y Patentes. \nNingún contenido puede interpretarse como una concesión, bajo ninguna forma, de licencias o derechos para hacer uso de ningún material comercial exhibido en el sitio sin permiso escrito de Santa Clara. \n\n'),

                        TextSpan(text:'f.\t',style:UiHelper.boldText),
                        TextSpan(text:'Menores de Edad\n'),
                        TextSpan(text:'Los menores de 18 años de edad no deben utilizar las aplicaciones digitales de Santa Clara. Consideramos que, quien se registra y utiliza este sitio tiene por lo menos 18 años de edad. Santa Clara puede estudiar su actividad en el sitio Web y en la aplicación Móvil para hacer un seguimiento preciso de su cuenta y ofrecerle los productos y servicios de Santa Clara que corresponden a sus intereses. Santa Clara puede recopilar datos en forma global. Los datos globales son anónimos y no contienen ninguna información personal que identifique al usuario. \n\n'),

                        TextSpan(text:'g.\t',style:UiHelper.boldText),
                        TextSpan(text:'Opción de no recepción de correo electrónico\n'),
                        TextSpan(text:'Santa Clara ofrece la opción de no permitir el envío de correos electrónicos relacionados con promociones, sorteos y eventos. En caso que no desee recibir estas novedades, deberá responder el mail en la pantalla de cancelación de mails. \n\n'),

                        TextSpan(text:'h.\t',style:UiHelper.boldText),
                        TextSpan(text:'Seguridad\n'),
                        TextSpan(text:'Santa Clara dispone de diversas funciones operativas para proteger la confidencialidad de los datos personales. Sin embargo, la seguridad perfecta en Internet no existe y no se garantiza que el sitio sea impenetrable ni invulnerable.\n\n'),

                      ]
                    )
                  )
                )
              ),
            ),

          ],
        )
      )
    );

  }

}
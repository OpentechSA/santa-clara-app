
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:santa_clara/src/components/menuDrawer.dart';
import 'package:santa_clara/src/components/scAppBar.dart';
import 'package:santa_clara/utils/uiHelper.dart';

class InformacionesGenerales extends StatelessWidget with UiHelper {
  
  @override
  Widget build(BuildContext context) {

    setContext(context);

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: ScAppBar(masterView: false,altCaption: 'Términos legales',),
      drawer: MenuDrawer(),
      body: Stack(
        children:<Widget>[
          backgroundLayout(sinLogo: true),
          _mainContent(context)
        ]
      )
    );

  }

  Widget _mainContent(BuildContext ctx){

    return Container(
      margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
      padding: EdgeInsets.all(0.0),
      width: getSize.width-UiHelper.layoutMarginLeft,
      child: Container(
        child:Column(
          children: [

            // header visor-recibo
            Container(
              width: UiHelper.size.width-UiHelper.layoutMarginLeft,
              padding: EdgeInsets.only(top:50),
              decoration:BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    const Color(0xFFFFFFFF),
                    const Color.fromRGBO(230, 248, 255, 1),
                  ],
                ),
              ),
              child: Container(
                margin: EdgeInsets.symmetric(vertical:20),
                child:crearTituloGrande(caption:'INFORMACIONES GENERALES',textSize:28),
              )
            ),
            Container(
              decoration: BoxDecoration(
                border: Border(bottom:BorderSide(width: 2.0,color:UiHelper.scAzul)),
              )
            ),

            // content visor-factura
            Expanded(
              child:SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.only(top:15,right:15,left:15,bottom: 0),
                  child:RichText(
                    text:TextSpan(
                      style: TextStyle(fontSize: 16,color:UiHelper.scGrisTexto),
                      children:[
                        TextSpan(text:'a.\t',style:UiHelper.boldText),
                        TextSpan(text:'Santa Clara S.A. Medicina Prepaga  (en adelante, “Santa Clara”) es una firma cuyo domicilio está fijado en O´Higgins Nº 631 c/ Sucre, cuyos propietarios son los accionistas de la sociedad. El teléfono es +595 21 418 0000 y la direccion de correo electrónico de  contacto es Info@santaclara.com.py\n\n'),
                        TextSpan(text:'b.\t',style:UiHelper.boldText),
                        TextSpan(text:'Para acceder a datos relativos a su contrato con el precio del servicio, moneda, modalidades de pago, valor final, cualquier otro costo relacionado con la contratación, el plazo, condiciones y responsabilidades o la cancelación del mismo, puede comunicarse al teléfono +595 21 418 0000.\n\n'),
                        TextSpan(text:'c.\t',style:UiHelper.boldText),
                        TextSpan(text:'	El establecimiento cumple con lo establecido en la Ley Nro. 836/80 y sus reglamentaciones vigentes y reúne los 	requisitos exigidos conforme al Ministerio de Salud Pública y Bienestar Social, según tomo I, folio 5, registro Nro. 16 y como EL PROVEEDOR de Medicina Pre Paga en el Registro Nacional de Entidades Prestadoras de Servicios de salud conforme a la Ley Nro. 2319/06.\n\n'),
                        TextSpan(text:'d.\t',style:UiHelper.boldText),
                        TextSpan(text:'	Resulta aplicable a los servicios prestados por Santa Clara la Ley No. 1334 “De defensa del consumidor y del usuario”	y normas complementarias. El Ministerio de Industria y Comercio, por medio de la Dirección General de Firma Digital y Comercio Electrónico, se instituye como Autoridad de Aplicación de la Ley 4868/13 “De Comercio Electrónico”	Correo: info-dgfdce@mic.gov.py  Tel: (+595 21) 513.533.\n\n')
                      ]
                    )
                  )
                )
              ),
            ),

          ],
        )
      )
    );

  }

}
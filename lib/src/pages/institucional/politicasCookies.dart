
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:santa_clara/src/components/menuDrawer.dart';
import 'package:santa_clara/src/components/scAppBar.dart';
import 'package:santa_clara/utils/uiHelper.dart';

class PoliticaCookies extends StatelessWidget with UiHelper {
  
  @override
  Widget build(BuildContext context) {

    setContext(context);

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: ScAppBar(masterView: false,altCaption: 'Términos legales',),
      drawer: MenuDrawer(),
      body: Stack(
        children:<Widget>[
          backgroundLayout(sinLogo: true),
          _mainContent(context)
        ]
      )
    );

  }

  Widget _mainContent(BuildContext ctx){

    return Container(
      margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
      padding: EdgeInsets.all(0.0),
      width: getSize.width-UiHelper.layoutMarginLeft,
      child: Container(
        child:Column(
          children: [

            // header visor-recibo
            Container(
              width: UiHelper.size.width-UiHelper.layoutMarginLeft,
              padding: EdgeInsets.only(top:50),
              decoration:BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    const Color(0xFFFFFFFF),
                    const Color.fromRGBO(230, 248, 255, 1),
                  ],
                ),
              ),
              child: Container(
                margin: EdgeInsets.symmetric(vertical:20),
                child:crearTituloGrande(caption:'POLÍTICA DE COOKIES',textSize:28),
              )
            ),
            Container(
              decoration: BoxDecoration(
                border: Border(bottom:BorderSide(width: 2.0,color:UiHelper.scAzul)),
              )
            ),

            // content visor-factura
            Expanded(
              child:SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.only(top:15,right:15,left:15,bottom: 0),
                  child:RichText(
                    text:TextSpan(
                      style: TextStyle(fontSize: 16,color:UiHelper.scGrisTexto),
                      children:[

                        TextSpan(text:'Santa Clara ha desarrollado esta política para informarle qué es una cookie.\nSanta Clara, podría utilizar cookies para mejorar la experiencia de los usuarios. En caso de que Santa Clara utilizara cookies, de ninguna manera será para  identificar personalmente a un usuario/cliente, así como tampoco estas cookies podrán dañar su sistema o archivos. \nUna cookie puede definirse como un fichero de texto que un servidor web envía a su navegador desde el instante en el que se accede al mismo. La cookie o fichero de texto se implanta en el disco duro de su ordenador, incorporando información relativa al usuario. Una vez que su navegador finaliza una sesión, la cookie implantada en el disco duro de su ordenador cesa de funcionar. Es importante recordar que nuestro sitio web será capaz de recordar la información que le concierne o la relativa a sus preferencias hasta el momento en el que la sesión de su navegador finaliza (si la cookie es temporal) o hasta el instante en el que es definitivamente eliminada de su sistema. \nSu navegador sólo permite que los servidores web accedan a las cookies - y a la información que en ellas se contienen- en la medida en que ellos mismos la hubieran implantado en el disco duro de su ordenador, y no a aquellas otras que hubieran sido remitidas por otros sitios web. En cualquier caso, las cookies no pueden, bajo ninguna circunstancia, contener más información que la que el usuario voluntariamente proporciona estando, asimismo, imposibilitadas para invadir el disco duro de su ordenador y remitir subrepticiamente información personal o de otro género a nuestro sitio web. En caso de querer inhabilitar nuestras cookies o las de Google se podrá acceder al Administrador de preferencias de anuncios de Google o también puede inhabilitar el uso de cookies. \nNuestras cookies en ningún caso pueden ser ejecutadas como códigos o incluir virus en sus textos. \nSanta Clara no asume responsabilidad alguna en cuanto a las cookies ajenas que terceras partes pudieran instalar en el disco duro de su ordenador. \nEl uso que hacen estas terceras partes estará sujeto, en su caso, a sus propias políticas de privacidad y no a la Política de privacidad de Santa Clara.\nSanta Clara y otros proveedores, incluido Google, usan las cookies de origen (como la cookie de Google Analytics) y las cookies de terceros (como la cookie de DoubleClick) combinadas para informar sobre cómo se relacionan con las visitas a un sitio las impresiones de los anuncios, otros usos de los servicios publicitarios y las interacciones con estas impresiones de los anuncios y servicios publicitarios. \nPuede desactivar el uso de "cookies" de Google, haciendo clic en el enlace: Centro de Privacidad Google. Para obtener más información sobre esta tecnología de Google, por favor consulte la política de privacidad de Google. \nServir publicidad mediante el servicio de Google Adwords: Al acceder a la página web de Santa Clara se instala una cookie de Google Adwords en el ordenador del Usuario. Esta cookie se utilizará para mostrar publicidad, anuncios de texto, gráficos o videos, a los Usuarios que hayan visitado previamente nuestra página web. \nDicha cookie NO guarda en ningún caso información personal de los Usuarios. Tan solo información sobre sitios visitados e intereses mostrados mediante la navegación por Internet. \nEn caso de querer inhabilitar las cookies de Google Analytics para la publicidad de display y personalizar los anuncios de la Red de Display de Google puede acceder al Administrador de preferencias de anuncios de Google o también puede inhabilitar el uso de cookies de otros. \nConsulte los medios de inhabilitación disponibles actualmente de Google Analytics para la Web (https://tools.google.com/dlpage/gaoptout/). \n\n'),

                      ]
                    )
                  )
                )
              ),
            ),

          ],
        )
      )
    );

  }

}

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:santa_clara/src/components/menuDrawer.dart';
import 'package:santa_clara/src/components/scAppBar.dart';
import 'package:santa_clara/utils/uiHelper.dart';

class AdvertenciaLegal extends StatelessWidget with UiHelper {
  
  @override
  Widget build(BuildContext context) {

    setContext(context);

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: ScAppBar(masterView: false,altCaption: 'Términos legales',),
      drawer: MenuDrawer(),
      body: Stack(
        children:<Widget>[
          backgroundLayout(sinLogo: true),
          _mainContent(context)
        ]
      )
    );

  }

  Widget _mainContent(BuildContext ctx){

    return Container(
      margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
      padding: EdgeInsets.all(0.0),
      width: getSize.width-UiHelper.layoutMarginLeft,
      child: Container(
        child:Column(
          children: [

            // header visor-recibo
            Container(
              width: UiHelper.size.width-UiHelper.layoutMarginLeft,
              padding: EdgeInsets.only(top:50),
              decoration:BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    const Color(0xFFFFFFFF),
                    const Color.fromRGBO(230, 248, 255, 1),
                  ],
                ),
              ),
              child: Container(
                margin: EdgeInsets.symmetric(vertical:20),
                child:crearTituloGrande(caption:'POLÍTICA DE COOKIES',textSize:28),
              )
            ),
            Container(
              decoration: BoxDecoration(
                border: Border(bottom:BorderSide(width: 2.0,color:UiHelper.scAzul)),
              )
            ),

            // content visor-factura
            Expanded(
              child:SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.only(top:15,right:15,left:15,bottom: 0),
                  child:RichText(
                    text:TextSpan(
                      style: TextStyle(fontSize: 16,color:UiHelper.scGrisTexto),
                      children:[

                        TextSpan(text:'a.\t',style:UiHelper.boldText),
                        TextSpan(text:'Uso indebido y responsabilidad\n'),
                        TextSpan(text:'El usuario se obliga a no enviar ni transmitir documentos o software que contengan virus de cualquier naturaleza u otros elementos que pudieren causar daños al sitio o a sus usuarios y/o de alguna forma inhibir o restringir el uso del sitio por terceros. \nSanta Clara se reserva el derecho de deshabilitar sin previo aviso a los usuarios que posean clave de acceso a este SITIO si se detecta el uso indebido del mismo, hecho que queda a consideración y criterio de los administradores de Santa Clara. \nSanta Clara no garantiza que la información, el software o el material al que el usuario pudiere acceder a través del sitio se encuentre libre de virus de cualquier naturaleza u otros elementos que pudieren causar daños potenciales o reales a los usuarios o a su propiedad. En consecuencia, no se responsabiliza por ningún daño directo, indirecto, con causado u otro que pudiere resultar del uso, descarga y/o consulta de información, datos, texto, imágenes, videos, audio u otro material incluido en el sitio, sus páginas o las páginas o sitios conectados o relacionados por hipervínculos u otras funciones de enlace.\n\n'),

                        TextSpan(text:'b.\t',style:UiHelper.boldText),
                        TextSpan(text:'Jurisdicción y legislación aplicable\n'),
                        TextSpan(text:'El usuario acepta que todo conflicto de intereses o diferencia que pueda surgir como consecuencia del uso del sito será resuelto de acuerdo con las leyes de la República del Paraguay y será sometido a la jurisdicción y competencia de los Tribunales Ordinarios de la Ciudad de Asunción.\n\n'),

                      ]
                    )
                  )
                )
              ),
            ),

          ],
        )
      )
    );

  }

}
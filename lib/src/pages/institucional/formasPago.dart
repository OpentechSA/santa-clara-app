
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:santa_clara/src/components/menuDrawer.dart';
import 'package:santa_clara/src/components/scAppBar.dart';
import 'package:santa_clara/utils/uiHelper.dart';

class FormasPago extends StatelessWidget with UiHelper {
  
  @override
  Widget build(BuildContext context) {

    setContext(context);

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: ScAppBar(masterView: false,altCaption: 'Términos legales',),
      drawer: MenuDrawer(),
      body: Stack(
        children:<Widget>[
          backgroundLayout(sinLogo: true),
          _mainContent(context)
        ]
      )
    );

  }

  Widget _mainContent(BuildContext ctx){

    return Container(
      margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
      padding: EdgeInsets.all(0.0),
      width: getSize.width-UiHelper.layoutMarginLeft,
      child: Container(
        child:Column(
          children: [

            // header visor-recibo
            Container(
              width: UiHelper.size.width-UiHelper.layoutMarginLeft,
              padding: EdgeInsets.only(top:50),
              decoration:BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    const Color(0xFFFFFFFF),
                    const Color.fromRGBO(230, 248, 255, 1),
                  ],
                ),
              ),
              child: Container(
                margin: EdgeInsets.symmetric(vertical:20),
                child:crearTituloGrande(caption:'FORMAS DE PAGO',textSize:28),
              )
            ),
            Container(
              decoration: BoxDecoration(
                border: Border(bottom:BorderSide(width: 2.0,color:UiHelper.scAzul)),
              )
            ),

            // content visor-factura
            Expanded(
              child:SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.only(top:15,right:15,left:15,bottom: 0),
                  child:RichText(
                    text:TextSpan(
                      style: TextStyle(fontSize: 16,color:UiHelper.scGrisTexto),
                      children:[

                        TextSpan(text:'a.\t',style:UiHelper.boldText),
                        TextSpan(text:'Tarjetas de crédito Online\n'),
                        TextSpan(text:'I. Si seleccionas pagar con tarjeta de crédito online, clickea sobre el botón Pago Online\nII. Te redirigiremos a la página "consultar facturas pendientes", agrega tu número de cliente y clickea en "consultar"\nIII. Te estaremos informando del detalle de tu factura y a la derecha la opción de "pagar ahora" la cual te dirigirá a la página de Bancard donde puedes efectivizar tu pago. \nIV. Una vez en la página de Bancard, podrás realizar tu pago mediante tu cuenta de Pago Móvil o ingresando directamente	los datos de tu tarjeta de crédito en los campos correspondientes. \nV.	Una vez confirmado el pago, el sitio te volverá a redirigir a Santa Clara y te desplegaremos un resumen de tu pago. \n\n'),

                        TextSpan(text:'b.\t',style:UiHelper.boldText),
                        TextSpan(text:'Aquí Pago\n'),
                        TextSpan(text:'I. Podes pagar en todas las redes de Aquí Pago \nII. Solo tenés que indicar que querés pagar el servicio de Santa Clara o en el caso de los home banking buscar el logo de Santa Clara entre las opciones de pago de servicio. \nIII. Indicar al operador tus datos de beneficiario. IV. El operador de la red de pago te confirmará el monto a pagar. \nV. Realizar el pago correspondiente.\n\n'),

                        TextSpan(text:'c.\t',style:UiHelper.boldText),
                        TextSpan(text:'Pago Express\n'),
                        TextSpan(text:'I. Podes pagar en Pago Express o desde el Home Banking del Banco \nII.	Solo tenés que indicar que querés pagar el servicio de Santa Clara o en el caso de los home banking buscar el logo de Santa Clara entre las opciones de pago de servicio. \nIII. Indicar al operador tus datos de beneficiario. \nIV.	El operador de la red de pago te confirmará el monto a pagar o en el caso de hacerlo por Home Banking podrás verificarlo en pantalla. \nV. Realizar el pago correspondiente. \n\n'),

                        TextSpan(text:'d.\t',style:UiHelper.boldText),
                        TextSpan(text:'Efectivo, Débito o Crédito en las oficinas de Santa Clara\n'),
                        TextSpan(text:'I. Acercarse hasta cualquiera de las direcciones:\n\t - O\'higgnis 631 entre Lillo y Sucre (Asunción).\n\t - Parapiti 1220 c/ Avda. Rodriguez de Francia (Asunción).\nII. Indicar en caja los datos del beneficiario. \nIII. Realizar el pago correspondiente. \n\n'),

                      ]
                    )
                  )
                )
              ),
            ),

          ],
        )
      )
    );

  }

}
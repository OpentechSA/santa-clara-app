
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:santa_clara/src/components/menuDrawer.dart';
import 'package:santa_clara/src/components/scAppBar.dart';
import 'package:santa_clara/utils/uiHelper.dart';

class PoliticaPrivacidad extends StatelessWidget with UiHelper {
  
  @override
  Widget build(BuildContext context) {

    setContext(context);

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: ScAppBar(masterView: false,altCaption: 'Términos legales',),
      drawer: MenuDrawer(),
      body: Stack(
        children:<Widget>[
          backgroundLayout(sinLogo: true),
          _mainContent(context)
        ]
      )
    );

  }

  Widget _mainContent(BuildContext ctx){

    return Container(
      margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
      padding: EdgeInsets.all(0.0),
      width: getSize.width-UiHelper.layoutMarginLeft,
      child: Container(
        child:Column(
          children: [

            // header visor-recibo
            Container(
              width: UiHelper.size.width-UiHelper.layoutMarginLeft,
              padding: EdgeInsets.only(top:50),
              decoration:BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    const Color(0xFFFFFFFF),
                    const Color.fromRGBO(230, 248, 255, 1),
                  ],
                ),
              ),
              child: Container(
                margin: EdgeInsets.symmetric(vertical:20),
                child:crearTituloGrande(caption:'POLÍTICA DE PRIVACIDAD',textSize:28),
              )
            ),
            Container(
              decoration: BoxDecoration(
                border: Border(bottom:BorderSide(width: 2.0,color:UiHelper.scAzul)),
              )
            ),

            // content visor-factura
            Expanded(
              child:SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.only(top:15,right:15,left:15,bottom: 0),
                  child:RichText(
                    text:TextSpan(
                      style: TextStyle(fontSize: 16,color:UiHelper.scGrisTexto),
                      children:[

                        TextSpan(text:'De acuerdo con la legislación vigente en materia de protección de datos personales, Ley No. 1682/2001 y No. 1969/2002, el SITIO informa a los USUARIOS lo siguiente: hemos adoptado las medidas de seguridad adecuadas a los datos personales que nos facilite y ha instalado los medios y medidas técnicas para evitar la pérdida, mal uso, alteración, acceso no autorizado o robo de los mismos.\nEs necesario que el servidor del sitio perciba todos estos datos porque debe poder comunicarse con él para enviar aquello que el usuario pida a través de su navegador y poder verlo en su pantalla. Ni el servidor ni ningún miembro del Sitio, puede conocer ninguno de los datos personales del usuario si no ha sido él mismo quién libre y conscientemente los ha facilitado.\nEl relevamiento y tratamiento automatizado de tus datos personales tiene como finalidad la de facilitar la información y/o gestionar los servicios que solicites a el Sitio, quien, asimismo, puede enviarle -incluso por vía electrónica- cualquier tipo de información de la entidad que considere de su interés. Los datos personales que no facilite solamente podrán ser cedidos a un tercero cuando ello fuera estrictamente necesario para gestionar los servicios que solicites a el SITIO.\nSanta Clara, en su carácter de titular del SITIO, se compromete a guardar la máxima reserva y confidencialidad sobre la información que le sea facilitada y a utilizarla únicamente para los fines indicados, así como a adoptar los niveles de seguridad adecuados a los datos facilitados por sus clientes y, además, a instalar todos los medios y medidas a su alcance para evitar la pérdida, mal uso, alteración, acceso no autorizado y extracción de los mismos.\nConforme a lo establecido en la Ley de Protección de Datos de Carácter Personal, le informamos de que el cumplimiento de cualquier formulario existente en el Sitio o la remisión de un correo electrónico a cualquiera de nuestros buzones implica la aceptación de estos “Términos legales”, así como la autorización a Santa Clara para que trate los datos personales que nos facilite.\nLos usuarios se comprometen a utilizar en el Sitio y su contenido de forma correcta, no realizando ningún tipo de acción fraudulenta sobre la misma.\nSanta Clara recopila cierta información personal de los usuarios que acceden a nuestras aplicaciones, a fin de realizar consultas y trámites on-line. Dichos usuarios necesitan encontrarse previamente registrados. Santa Clara utiliza esta información internamente (para procesar su pedido y las facturas, para mantener o actualizar su cuenta en Santa Clara o para enviarle las actualizaciones de servicios nuevos, noticias y acontecimientos de interés). Al respecto, se aclara que:\n• La información que Santa Clara obtiene por parte de sus usuarios es de carácter estrictamente confidencial, en especial, aquella relacionada a su salud, tratamientos, diagnósticos y medicación.\n• La información de carácter económico y comercial de los usuarios también reviste el carácter de confidencial, por lo que su tratamiento deberá ser utilizado conforme la política de Santa Clara y las leyes vigentes.\n• Los empleados y asesores podrán utilizar la información confidencial de los socios exclusivamente durante el desempeño de las tareas habituales de la EL PROVEEDOR, encontrándose prohibido hacer uso de la misma fuera del ámbito laboral o con propósitos distintos a los necesarios para su desempeño.\n• Toda documentación que contenga información confidencial de un usuario, cualquiera sea el soporte en que se encuentre (físico, magnético o digital), deberá ser tratada con cuidado y diligencia.\n• Se encuentra rigurosamente prohibido brindar acceso a información de usuarios a terceros ajenos a la EL PROVEEDOR, con la única excepción de aquellas solicitudes efectuadas por autoridad competente. En ningún caso se le podrá brindar información a un usuario relacionada a otro usuario que no pertenezca a su grupo familiar.\n• En caso que un socio solicite información confidencial propia, deberá previamente constatarse la identidad del requirente por medio de preguntas que el personal considere necesaria a los efectos de constatar su identidad. \n\n'),

                      ]
                    )
                  )
                )
              ),
            ),

          ],
        )
      )
    );

  }

}
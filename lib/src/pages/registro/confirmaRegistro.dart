import 'package:flutter/material.dart';
import 'package:santa_clara/src/modules/authBloc.dart';
import 'package:santa_clara/src/modules/appProvider.dart';
import 'package:santa_clara/utils/uiHelper.dart';

class ConfirmaRegistro extends StatelessWidget with UiHelper {
  
  ConfirmaRegistro({Key key}) : super(key: key);
  static AuthBloc blocRegistro;
  
  @override
  Widget build(BuildContext context) {
    
    setContext(context);
    
    blocRegistro = AppProvider.getAuthBloc(context);
    blocRegistro.requestingController.value = false;

    List<Widget> listaFloatingActionButtons = [
      botonCuadrado(
        icon:Icons.arrow_forward_ios,
        evtOnPressed: (){ 
          resetAuthRequestStatus(blocRegistro);
          Navigator.pushNamed(context, 'login'); 
        }
      ),
    ];

    return Scaffold(
      body:Stack(
        children:<Widget>[
          backgroundLayout(sinLogo: true),
          _registerForm(context)
        ]
      ),
      floatingActionButton: crearBotoneraRegistro( listButtons: listaFloatingActionButtons ),
    );

  }

  Widget _registerForm(BuildContext ctx){

    return Center(
      child: Container(
        margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
        width: getSize.width-UiHelper.layoutMarginLeft,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[

            Container(alignment:Alignment.centerRight, padding: EdgeInsets.only(bottom:50,right:20),child:logoSantaClara()),
            crearTituloGrande(caption:'Bienvenido!'),
            crearTexto(text:'En breve recibirá un mensaje en su bandeja de entrada para finalizar el registro.'),
            SizedBox(height: 50.0,),
            
          ],
        ),
      ),
    );

  }

}
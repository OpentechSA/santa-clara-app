import 'package:flutter/material.dart';
import 'package:santa_clara/src/modules/appProvider.dart';
import 'package:santa_clara/utils/uiHelper.dart';

class IndexRegistro extends StatelessWidget with UiHelper{
  
  IndexRegistro({Key key}) : super(key: key);
  static AuthBloc blocRegistro;

  @override
  Widget build(BuildContext context) {

    setContext(context);
    blocRegistro = AppProvider.getAuthBloc(context);
    blocRegistro.requestStatusController.value = null;

    List<Widget> listaFloatingActionButtons = [
      botonCuadrado(
        icon:Icons.arrow_back_ios,
        evtOnPressed:(){
          resetAuthRequestStatus(blocRegistro);
          Navigator.pop(context);
        }
      ),
      SizedBox(width:5.0),
      StreamBuilder(
        stream: blocRegistro.validUserDataForm,
        builder: (context,AsyncSnapshot snapshot){
          return botonCuadrado(
            icon:Icons.arrow_forward_ios,
            evtOnPressed: (snapshot.hasData)? 
              (){  Navigator.pushNamed(context, 'registro-password');  }: 
              null
          );
        },
      ),
    ];

    return Scaffold(
      body:Stack(
        children:<Widget>[
          backgroundLayout(sinLogo: true),
          _registerForm(context)
        ]
      ),
      floatingActionButton: crearBotoneraRegistro( listButtons: listaFloatingActionButtons ),
    );

  }

  Widget _registerForm(BuildContext ctx){

    return Center(
      child:SingleChildScrollView(
        child: Column(
          children: <Widget>[

            Container(
              margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
              width: getSize.width-UiHelper.layoutMarginLeft,
              child: Column(
                children: <Widget>[

                  Container(alignment:Alignment.centerRight, padding: EdgeInsets.only(bottom:50,right:20),child:logoSantaClara()),
                  crearTituloGrande(caption:'Registro'),
                  crearTexto(text:'Complete los campos con su información básica'),
                  SizedBox(height: 15.0,),
                  inputWrapper(field:campoNombreApellido(bloc:blocRegistro),withBoxShadow: true),
                  SizedBox(height: 10.0,),
                  inputWrapper(field:campoCorreoElectronico(bloc:blocRegistro), withBoxShadow: true),
                  SizedBox(height: 10.0,),
                  inputWrapper(field:campoNroDocumento(bloc:blocRegistro), withBoxShadow: true),
                  SizedBox(height: 10.0,),
                  SafeArea(child:msgAlert(bloc: blocRegistro),bottom: true,),
                  SizedBox(height: 50.0,),
                  
                  
                ],
              ),
            )

          ],
        ),
      )
    );

  }

}
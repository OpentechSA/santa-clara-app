import 'package:flutter/material.dart';
import 'package:santa_clara/src/models/registrationModels.dart';
import 'package:santa_clara/src/modules/appProvider.dart';
import 'package:santa_clara/src/services/authenticationService.dart';
import 'package:santa_clara/utils/uiHelper.dart';

class ClavesRegistro extends StatefulWidget {
  
  ClavesRegistro({Key key}) : super(key: key);
  ClavesRegistroState _state = new ClavesRegistroState();
  @override
  ClavesRegistroState createState() => _state;

}
class ClavesRegistroState extends State<ClavesRegistro> with UiHelper{
  
  static Auth auth = new Auth();
  static AuthBloc blocRegistro;
  static bool _showPassNew;
  static bool _showPassConfirm;

  @override
  void initState() { 
    print(">>> initState");
    _showPassNew = true;
    _showPassConfirm = true;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    
    print(">>> build");
    setContext(context);
    blocRegistro = AppProvider.getAuthBloc(context);
    blocRegistro.requestingController.value = false;

    List<Widget> listaFloatingActionButtons = [
      botonCuadrado(
        icon:Icons.arrow_back_ios,
        evtOnPressed: (){ 
          resetAuthRequestStatus(blocRegistro);
          Navigator.pop(context); 
        }
      ),
      SizedBox(width:5.0), 
      StreamBuilder(
        stream: blocRegistro.validPasswordForm,
        builder: (context,AsyncSnapshot snapshot){
          return botonCuadrado(
            icon:Icons.arrow_forward_ios,
            evtOnPressed: (snapshot.hasData)?(){ registro(context); }:null
          );
        }
      ),
    ];

    return Scaffold(
      body:Stack(
        children:<Widget>[
          backgroundLayout(sinLogo: true),
          _registerForm(context)
        ]
      ),
      floatingActionButton: crearBotoneraRegistro( listButtons: listaFloatingActionButtons ),
    );

  }

  Widget _registerForm(BuildContext ctx){

    return Center(
      child:SingleChildScrollView(
        child: Column(
            children: <Widget>[

              Container(
                margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
                width: getSize.width-UiHelper.layoutMarginLeft,
                child: Column(
                  children: <Widget>[

                    Container(alignment:Alignment.centerRight, padding: EdgeInsets.only(bottom:50,right:20),child:logoSantaClara()),
                    crearTituloGrande(caption:'Seguridad'),
                    crearTexto(text:'Ingrese la clave para el acceso a la aplicación'),
                    SizedBox(height: 15.0,),
                    inputWrapper( 
                      field:campoPassword(
                        bloc:blocRegistro,
                        showPass: _showPassNew, 
                        evtOnPressed: (){
                          setState(() { _showPassNew = (_showPassNew)?false:true; });
                        }
                      ), 
                      withBoxShadow: true
                    ),
                    SizedBox(height: 10.0,),
                    inputWrapper( 
                      field:campoPasswordConfirm(
                        caption: 'Confirmar Contraseña',
                        bloc: blocRegistro,
                        showPass: _showPassConfirm, 
                        evtOnPressed: (){
                          setState(() { _showPassConfirm = (_showPassConfirm)?false:true; });
                        }
                      ), 
                      withBoxShadow: true 
                    ),
                    SizedBox(height: 10.0,),
                    msgAlert(bloc:blocRegistro),
                    SizedBox(height: 80.0,),
                    
                  ],
                ),
              )

            ],
          ),
      )
    );

  }

  registro(BuildContext context)async{

    if(blocRegistro.validRegisterForm != null){

      String email = blocRegistro.email;
      String password = blocRegistro.password;
      String nroDocumento = blocRegistro.nroDocumento;
      String nombre = blocRegistro.nombreApellido;

      print("$email - $password - $nroDocumento - $nombre");
      SignUpRequest request = new SignUpRequest(
        ci        : nroDocumento,
        email     : email,
        nombre    : nombre,
        password  : password
      );
      if(!blocRegistro.requestingStatus){

        // activamos el control de request
        blocRegistro.requestStatusTypeController.value = 'loading';
        blocRegistro.requestingController.value = true;
        blocRegistro.requestStatusController.value = true;
        blocRegistro.requestStatusMsgController.value = "Registrando datos ...";
        
        // registro request
        SignUpResponse respuesta = await auth.signUp(request);

        // procesamos la respuesta del request
        if(respuesta.status){
          Navigator.pushNamed(context, 'confirma-registro');
        }else{
          blocRegistro.requestStatusTypeController.value = 'info';
          blocRegistro.requestStatusMsgController.value = respuesta.message;
          blocRegistro.requestStatusController.value = respuesta.status;
        }
        clearAuthMsgAlert(blocRegistro);
        
      }
    }else{
      blocRegistro.requestStatusTypeController.value = 'info';
      blocRegistro.requestStatusMsgController.value = "Ingresar datos de usuario!";
      blocRegistro.requestStatusController.value = false;
    }

  }

}
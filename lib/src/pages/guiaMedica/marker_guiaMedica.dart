import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:latlong/latlong.dart';
import 'package:santa_clara/src/pages/resultados/resultado_detail.dart';
import 'package:santa_clara/utils/uiHelper.dart';
import 'package:santa_clara/utils/constants.dart';
import 'package:santa_clara/src/modules/appProvider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;


class MarkerGuiaMedica extends StatefulWidget {

  final typeBuilder;
  final jsonDataEncode;

  MarkerGuiaMedica({
    Key key,
    this.typeBuilder,
    this.jsonDataEncode
  }) : super(key: key);

  @override
  createState() => MarkerGuiaMedicaState();
}

class MarkerGuiaMedicaState extends State<MarkerGuiaMedica> with UiHelper {

  var jsonData;

  double calificacion = 0;

  void ratingData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    final response = await http.get(
      Constants.apiUrl+'guiaMedica/calificacion?idPrestador=${jsonData['xTg'].toString()}&uid=${prefs.getString("uidUser")}',
    );

    dynamic jsondecode = json.decode(response.body);

    if (response?.statusCode == 200) {
      setState(() => calificacion = jsondecode['data']['valor'] != null ? double.parse(jsondecode['data']['valor']) : 0);
    }
  }

  void newRatingSend() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    final response = await http.get(
      Constants.apiUrl+'guiaMedica/calificar?idPrestador=${jsonData['xTg'].toString()}&uid=${prefs.getString("uidUser")}&valor=${calificacion.toString()}',
    );

    dynamic jsondecode = json.decode(response.body);

    print(jsondecode.toString());

    if (response?.statusCode == 200) {

      if (jsondecode['status'] == true) {
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Icon(FontAwesomeIcons.solidCheckCircle, color: Colors.greenAccent, size: 30),
            content: Text("Gracias por calificar al prestador", textAlign: TextAlign.center,),
          )
        );
      }
    }
  }

  @override
  void initState() {
    setState(() {
      jsonData = json.decode(widget.jsonDataEncode);
    });
    super.initState();
  }

  Widget build(BuildContext context) {

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(70.0),
          child: Container(
              padding:EdgeInsets.only(top:5.0,bottom:10.0,left: 45.0),
              child: AppBar(
                  elevation: 0.0,
                  titleSpacing: 0.0,
                  automaticallyImplyLeading: true,
                  title: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                              child:Text(
                                "Volver",
                                style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  color:UiHelper.scAzulOscuro,
                                  fontSize: 15.0,
                                ),
                              )
                          ),
                          Padding(padding:EdgeInsets.only(right:10.0),child: SizedBox(width: 100.0,child:Image(
                            image: new AssetImage('assets/images/logo-santa-clara.png'),
                            width: (this.getSize.width * 0.10),
                            fit: BoxFit.contain,
                          ))),
                        ],
                      )
                  )
              )
          )
      ),
      body: Stack(
        children:<Widget>[
          backgroundLayout(sinLogo: true),
          _mainContent(context),
        ]
      )
    );

  }

  static Color scAzul = Color.fromRGBO(21, 61, 138, 1);


  Widget _mainContent(BuildContext ctx){

    final blocMain = AppProvider.getMainBloc(ctx);

    return Container(
        margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
        padding: EdgeInsets.all(0.0),
        width: getSize.width-45.0,
        //decoration: BoxDecoration( color:UiHelper.greenContainer, ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[

            // campo-busqueda
            SafeArea(
              child: Padding(
                padding:EdgeInsets.only(bottom:5.0),
              ),),


            Padding(
              padding:EdgeInsets.only(top: 60, bottom:5.0),
              child: new Stack(
                alignment: Alignment(0, -1.5),
                children: <Widget>[
                  new Card(
                    elevation: 3,
                    shadowColor: Colors.cyan,
                    margin: EdgeInsets.all(10),
                    child: new Container(
                        padding: EdgeInsets.only(right: 20, left: 20, top: 50, bottom: 10),
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: Color.fromRGBO(148, 204, 227, 1),
                                width: 2
                            ),
                            borderRadius: BorderRadius.circular(5)
                        ),
                        child: GestureDetector(
                            child: new Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                new Text(widget.typeBuilder == "profesionales" ? jsonData['nombre'].toString() : jsonData['centro'].toString(),
                                  style: TextStyle(
                                    fontSize: 15,
                                    fontWeight: FontWeight.w700
                                  ),
                                ),
                                new Container(
                                  padding: EdgeInsets.only(top: 20),
                                  child: new Row(
                                    children: <Widget>[
                                      new Image.asset("assets/images/map_action.png", width: 30,),
                                      new Padding(padding: EdgeInsets.all(2),),
                                      new Flexible(
                                        child: new Text(jsonData['direccion'].toString(),
                                          style: TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.w400
                                          )
                                        )
                                      )
                                    ],
                                  )
                                ),
                                new Container(
                                    padding: EdgeInsets.only(top:10, right: 15, left: 15, bottom: 10),
                                    child: RatingBar(
                                      initialRating: calificacion,
                                      minRating: 1,
                                      direction: Axis.horizontal,
                                      allowHalfRating: true,
                                      itemCount: 5,
                                      itemSize: 25,
                                      unratedColor: Colors.amber[100],
                                      itemPadding: EdgeInsets.symmetric(horizontal: 1.0),
                                      ratingWidget: RatingWidget(
                                        full: Image.asset('assets/images/high.png'),
                                        half: Image.asset('assets/images/medium.png'),
                                        empty: Image.asset('assets/images/low.png'),
                                      ),
                                      onRatingUpdate: (rating) {
                                        setState(() {
                                          calificacion = rating;
                                        });
                                      },
                                    )
                                ),
                                new InkWell(
                                    onTap: () => newRatingSend(),
                                    child: new Text("Calificar al prestador",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w700
                                      ),
                                    )
                                )
                              ],
                            )
                        )
                    ),
                  ),
                  new Container(
                    width: 80,
                    height: 80,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                        image: new AssetImage(widget.typeBuilder == "profesionales" ? "assets/images/medical.png" : "assets/images/hospital.png"),
                      ),
                      border: Border.all(
                          color: scAzul,
                          width: 3
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Flexible(
              flex: 4,
              child: new FlutterMap(
                options: new MapOptions(
                  center: new LatLng(jsonData['latitud'], jsonData['longitud']),
                  zoom: 18.0,
                ),
                layers: [
                  new TileLayerOptions(
                      urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                      subdomains: ['a', 'b', 'c']
                  ),
                  new MarkerLayerOptions(
                    markers: [
                      new Marker(
                        width: 60.0,
                        height: 60.0,
                        point: new LatLng(jsonData['latitud'], jsonData['longitud']),
                        builder: (ctx) =>
                        new Container(
                          child: new Image.asset("assets/images/marker.png", width: 30,),
                        ),
                      ),
                    ],
                  ),
                ],
              )
            ),
          ],

        )
    );
  }

}
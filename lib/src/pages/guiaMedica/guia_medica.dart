import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:santa_clara/src/pages/guiaMedica/info_guia_medica.dart';
import 'package:santa_clara/src/pages/guiaMedica/marker_guiaMedica.dart';
import 'package:santa_clara/src/pages/resultados/resultado_detail.dart';
import 'package:santa_clara/utils/uiHelper.dart';
import 'package:santa_clara/utils/constants.dart';
import 'package:santa_clara/src/modules/appProvider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';

class GuiaMedica extends StatefulWidget {

  GuiaMedica({Key key}) : super(key: key);
  @override
  createState() => GuiaMedicaState();
}


class GuiaMedicaState extends State<GuiaMedica> with UiHelper {

  List especialidadesList = List();
  List centrosAtencionList = List();
  List resultadosList = List();

  List dropdownItemsField = List();

  bool resultadosListBool = false;
  bool search = false;

  String inputValue = "";

  var selectString;


  Future<void> onUserDatas() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();

    var queryParameters = {
      'ci': prefs.getString("ci_user"),
      'uid': prefs.getString("uidUser"),
    };

    final response = await http.post(
        Constants.apiUrl+'guiaMedica/cliente',
        body : queryParameters
    );

    if(response?.statusCode != 200){
      var statusCode = response?.statusCode.toString();
      print(response.body);

    }else{
      var decodeResp = json.decode(response.body);

      dynamic datas = decodeResp['data'];

      datas.forEach((element) async {
        await onRequestEspecialidades(element['id_tarifa']);
      });
    }
    return "Listo";

  }

  Future<String> onRequestEspecialidades(int idTarifa) async {

    var queryParameters = {
      "tarifa": idTarifa.toString()
    };

    print("Body: ${queryParameters}");

    final response = await http.post(
        Constants.apiUrl+'guiaMedica/especialidades',
        body : queryParameters
    );

    if(response?.statusCode != 200){
      var statusCode = response?.statusCode.toString();
      print(response.body);

      setState(() {
        especialidadesList = List();
      });

    }else{
      var decodeResp = json.decode(response.body);

      setState(() {
        especialidadesList = decodeResp["data"]["especialidades"];
      });

    }
    await onRequestCentros();
    return "Listo";
  }

  Future<String> onRequestCentros() async {

    var queryParameters = {
      'idGuia': "PC"
    };

    final response = await http.post(
        Constants.apiUrl+'guiaMedica/tiposcentro',
        body : queryParameters
    );

    if(response?.statusCode != 200){
      var statusCode = response?.statusCode.toString();
      print(response.body.toString());

    }else{
      var decodeResp = json.decode(response.body);

      setState(() {
        centrosAtencionList = decodeResp["data"];
        resultadosListBool = true;
      });

    }

    radioButtonChanges("profesionales");
    return "Listo";
  }

  bool isNumeric(String s) {
    if(s == null) {
      return false;
    }
    return double.parse(s, (e) => null) != null;
  }

  Future<dynamic> onSearch() async {

    setState(() {
      resultadosList.clear();
    });

    String name;
    String telefono;
    String especialidadItem = selectString != null ? selectString : "";

    if(!isNumeric(inputValue)) {
      name = inputValue;
      telefono = "";
    } else if (isNumeric(inputValue)) {
      name = "";
      telefono = inputValue;
    } else {
      name = "";
      telefono = "";
    }

    if(_radioValue == "profesionales") {
      var queryParameters = {
        "idGuia": "PC",
        "nombre": "${name}",
        "direccion": "",
        "telefono": "${telefono}",
        "especialidad": "${especialidadItem}"
      };

      final response = await http.post(
          Constants.apiUrl+'guiaMedica/medicos',
          body : queryParameters
      );

      if(response?.statusCode != 200){
        var statusCode = response?.statusCode.toString();

      }else{
        dynamic decodeResp = json.decode(response.body);
        dynamic especialidadItem = "";

        for(dynamic especialidad in decodeResp['data']) {

          especialidadItem = especialidad['especialidad'];
          print("Especialidad item: "+especialidadItem.toString());

          for (dynamic medico in especialidad['medicos']) {
            medico["especialidad"] = especialidadItem;

            setState(() {
              resultadosList.add(medico);
            });
          }
        }
      }

      setState(() {
        search = true;
        resultadosListBool = true;
      });
    } else if (_radioValue == 'atencion'){

      var queryParameters = {
        "idGuia": "PC",
        "tipoCentro": "${especialidadItem}",
        "nombre": "${name}",
        "direccion": "",
        "telefono": "${telefono}",
        "servicioCentro": "-1"
      };

      final response = await http.post(
          Constants.apiUrl+'guiaMedica/centros',
          body : queryParameters
      );

      if(response?.statusCode != 200){
        var statusCode = response?.statusCode.toString();

      }else{
        dynamic decodeResp = json.decode(response.body);
        dynamic especialidadItem = "";
        dynamic departamentoItem = "";
        dynamic ciudadItem = "";

        for(dynamic especialidad in decodeResp['data']) {

          especialidadItem = especialidad['especialidad'];
          for (dynamic departamento in especialidad['departamentos']) {
            departamentoItem = departamento['departamento'];

            for (dynamic ciudad in departamento['ciudades']) {
              ciudadItem = ciudad['ciudad'];

              for (dynamic centro in ciudad["centros"]) {

                centro["especialidad"] = especialidadItem;
                centro["departamento"] = departamentoItem;
                centro['ciudad'] = ciudadItem;

                setState(() {
                  resultadosList.add(centro);
                });
              }

            }
          }
        }
      }

      setState(() {
        search = true;
        resultadosListBool = true;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    this.onUserDatas();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext context) {

    setContext(context);

    return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(70.0),
            child: Container(
                padding:EdgeInsets.only(top:10.0,bottom:10.0,left: 45.0),
                child: AppBar(
                    elevation: 0.0,
                    titleSpacing: 0.0,
                    automaticallyImplyLeading: true,
                    title: Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                                child:Text(
                                  "Volver",
                                  style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    color:UiHelper.scAzulOscuro,
                                    fontSize: 14.0,
                                  ),
                                )
                            ),
                            Padding(padding:EdgeInsets.only(right:10.0),child: SizedBox(width: 100.0,child:Image(
                              image: new AssetImage('assets/images/logo-santa-clara.png'),
                              width: (this.getSize.width * 0.10),
                              fit: BoxFit.contain,
                            ))),
                          ],
                        )
                    )
                )
            )
        ),
        body: Stack(
            children:<Widget>[
              backgroundLayout(sinLogo: true),
              _mainContent(context),
            ]
        )
    );

  }

  String _radioValue = 'profesionales'; //Initial definition of radio button value
  String choice;

  static Color scAzul = Color.fromRGBO(21, 61, 138, 1);

  void radioButtonChanges(String value) {
    setState(() {
      setState(() {
        _radioValue = value;
        selectString = null;
      });
      switch (value) {
        case 'profesionales':
          setState(() {
            choice = value;
            dropdownItemsField = especialidadesList;
            resultadosList.clear();
          });
          break;
        case 'atencion':
          setState(() {
            choice = value;
            dropdownItemsField = centrosAtencionList;
            resultadosList.clear();
          });
          break;
        default:
          setState(() {
            choice = null;
          });
      }
      print("Tipo de Consulta switch: ${choice}"); //Debug the choice in console
    });
  }



  Widget _mainContent(BuildContext ctx){

    final blocMain = AppProvider.getMainBloc(ctx);

    return Container(
        margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
        padding: EdgeInsets.all(0.0),
        width: getSize.width-45.0,
        //decoration: BoxDecoration( color:UiHelper.greenContainer, ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[

            // campo-busqueda
            SafeArea(
              child: Padding(
              padding:EdgeInsets.only(bottom:5.0),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage("assets/images/background.png"),
                            fit: BoxFit.fill
                          )
                      ),
                      height: 50,
                      padding: EdgeInsets.only(right: 12, left: 12, bottom: 5),
                      child: new Row(
                        children: <Widget>[
                          new Image.asset("assets/images/directory_medical.png", width: 20,),
                          new Padding(padding: EdgeInsets.all(5)),
                          new Text("GUIA MÉDICA",
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                              color: Colors.black
                            ),
                          )
                        ],
                      )
                  ),
                  new Padding(
                    padding: EdgeInsets.only(right: 12, left: 12, top: 10),
                    child: new Text("Buscar por...",
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w600
                      )
                    )
                  ),
                  Padding(
                    padding: EdgeInsets.all(1),
                    child: new Row(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Radio(
                              value: 'profesionales',
                              groupValue: _radioValue,
                              onChanged: radioButtonChanges,
                            ),
                            Text(
                              "Profesionales",
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Radio(
                              value: 'atencion',
                              groupValue: _radioValue,
                              onChanged: radioButtonChanges,
                            ),
                            Text(
                              "Centros de Atención",
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  new Padding(
                    padding: EdgeInsets.only(right: 10),
                    child: new Row(
                      children: <Widget>[
                        Expanded(
                          flex: 4,
                          child: inputWrapper(
                            field: StatefulBuilder(
                              builder: (BuildContext context, setState) {
                                return Container(
                                  width: MediaQuery.of(context).size.width * 0.55,
                                  child: TextField(
                                    decoration: InputDecoration(
                                      suffix: SizedBox(
                                        child: IconButton(
                                          icon: Icon(FontAwesomeIcons.search,
                                              size: 18.0, color: scAzul),
                                          onPressed: () {

                                          },
                                        ),
                                        width: 30.0,
                                        height: 20.0,
                                      ),
                                      labelText: 'Buscar por nombre o telefono',
                                      labelStyle: TextStyle(
                                          fontSize: 12
                                      ),
                                      border: InputBorder.none,),
                                    onChanged: (value) {
                                      setState(() {
                                        inputValue = value;
                                      });
                                    },
                                  ),
                                );
                              }
                            ),
                            withBoxShadow: false
                          )
                        ),
                        Expanded(
                          child: new Container(
                            child: new GestureDetector(
                              onTap: () async {
                                setState(() => resultadosListBool = false);
                                await onSearch();
                              },
                              child: new Container(
                                padding: EdgeInsets.only(top: 10, bottom: 10, right: 12, left: 10),
                                decoration: BoxDecoration(
                                    color: Color.fromRGBO(21, 61, 138, 1),
                                    borderRadius: BorderRadius.circular(10)
                                ),
                                child: new Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    new Icon(Icons.search, color: Colors.white,),
                                    new Text("Buscar", style: TextStyle(color: Colors.white),),
                                  ],
                                )
                              )
                            ),
                          )
                        )
                      ],
                    )
                  ),
                  Padding(
                    padding:EdgeInsets.only(top: 10, bottom:15.0, right: 5),
                    child: inputWrapper(
                      field: StatefulBuilder(
                        builder: (BuildContext context, setState) {
                          return new DropdownButtonFormField(
                            validator: (value) => value == null ? 'Es requerido' : null,
                            hint: new Text("Seleccionar especialidad"),
                            isExpanded: true,
                            items: dropdownItemsField.map((data) {
                              return new DropdownMenuItem(
                                child: new Text(data.containsKey("especialidad") ? data['especialidad'] : data['tipo_centro'], style: TextStyle(fontSize: 15.0),),
                                value: data['id'].toString(),
                              );
                            }).toList(),
                            onChanged: (value) {
                              setState(() {
                                selectString = value;
                                resultadosListBool = false;
                              });
                              onSearch();
                            },
                            value: selectString,
                            onSaved: (value) {
                              setState(() {
                                selectString = value;
                                resultadosListBool = false;
                              });
                              onSearch();
                            },
                          );
                        }
                      ),
                      withBoxShadow: true
                    ),
                  ),
                ],
              ),
            ),),

            new Padding(
              padding: EdgeInsets.only(right: 20, left: 20),
              child: new Text(search == true ? "Se encontraron (${resultadosList.length.toString()}) Resultados" : "",
                style: TextStyle(
                    fontWeight: FontWeight.w700
                ),
              ),
            ),

              resultadosListBool == true ? new Flexible(
              flex: 15,
              child: search == true ? new ListView.builder(
                  padding: EdgeInsets.zero,
                  shrinkWrap: true,
                  itemCount: resultadosList.length,
                  itemBuilder: (ctx, index) {
                    return new Card(
                      elevation: 3,
                      shadowColor: Colors.cyan,
                      margin: EdgeInsets.all(10),
                      child: new Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              border: Border.all(
                                  color: Color.fromRGBO(148, 204, 227, 1),
                                  width: 2
                              ),
                              borderRadius: BorderRadius.circular(5)
                          ),
                          child: GestureDetector(
                            onTap: () async => Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => InfoGuiaMedica(
                                    tipoConsulta: _radioValue,
                                    jsonDataEncode: json.encode(resultadosList[index])
                                ),
                              ),
                            ),
                            child: new Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                new Text(_radioValue == 'profesionales' ? resultadosList[index]['nombre'].toString() : resultadosList[index]['centro'].toString(),
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.w700
                                  ),
                                ),
                                new Text(resultadosList[index]['direccion'].toString(),
                                  style: TextStyle(
                                      fontSize: 12,
                                      color: Colors.grey,
                                      fontWeight: FontWeight.w600
                                  ),
                                ),
                                new Padding(
                                  padding: EdgeInsets.only(top: 10, bottom: 10),
                                  child: new Text(resultadosList[index]['telefono'].toString(),
                                    style: TextStyle(
                                        fontSize: 12,
                                        color: scAzul,
                                        fontWeight: FontWeight.w600
                                    ),
                                  ),
                                ),
                                new Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    new Flexible(
                                      child: new Container(
                                        padding: EdgeInsets.only(top: 3, bottom: 3, right: 10, left: 10),
                                        decoration: BoxDecoration(
                                            color: Colors.grey[400],
                                            border: Border.all(
                                                color: Colors.grey[400],
                                                width: 1
                                            ),
                                            borderRadius: BorderRadius.circular(15)
                                        ),
                                        child: new Text(resultadosList[index]['especialidad'].toString(),
                                          style: TextStyle(
                                              fontSize: 13,
                                              color: Colors.white,
                                              fontWeight: FontWeight.w700
                                          ),
                                        ),
                                      )
                                    ),
                                    new Container(
                                        child: new Row(
                                          children: <Widget>[
                                            new GestureDetector(
                                              onTap: () async {
                                                var phone ="tel:${resultadosList[index]['telefono'].toString()}";
                                                await canLaunch(phone) ? launch(phone) : print("Not found in phone");
                                              },
                                              child: new Image.asset("assets/images/call_action.png", width: 30,),
                                            ),
                                            new Padding(padding: EdgeInsets.all(2)),
                                            resultadosList[index]['latitud'] != null && resultadosList[index]['longitud'] != null ? new GestureDetector(
                                              onTap: () async => Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (context) => MarkerGuiaMedica(
                                                    typeBuilder: _radioValue,
                                                    jsonDataEncode: json.encode(resultadosList[index])
                                                  ),
                                                ),
                                              ),
                                              child: new Image.asset("assets/images/map_action.png", width: 30,),
                                            ) : new Padding(padding: EdgeInsets.zero),
                                          ],
                                        )
                                    )
                                  ],
                                ),
                              ],
                            )
                          )
                      ),
                    );
                  },
              ) : new Center(child: new Text("Seleccione los items a buscar")),
            ) : new Center(child:new CircularProgressIndicator())
          ],

        )
    );
  }

}
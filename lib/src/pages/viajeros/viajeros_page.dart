import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:santa_clara/src/pages/guiaMedica/info_guia_medica.dart';
import 'package:santa_clara/src/pages/guiaMedica/marker_guiaMedica.dart';
import 'package:santa_clara/src/pages/presupuestos/presupuesto_solicitud.dart';
import 'package:santa_clara/src/pages/presupuestos/presupuesto_view.dart';
import 'package:santa_clara/src/pages/resultados/resultado_detail.dart';
import 'package:santa_clara/src/pages/viajeros/asistencia/viajeros_page_one.dart';
import 'package:santa_clara/src/pages/viajeros/viajeros_view.dart';
import 'package:santa_clara/utils/uiHelper.dart';
import 'package:santa_clara/utils/constants.dart';
import 'package:santa_clara/src/modules/appProvider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';

class ViajerosPage extends StatefulWidget {

  ViajerosPage({Key key}) : super(key: key);
  @override
  createState() => ViajerosPageState();
}

class ViajerosPageState extends State<ViajerosPage> with UiHelper {

  bool listBool = true;
  List listViajeros = List();

  Future<void> onListViajeros() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();

    var queryParameters = {
      'ci': prefs.getString('ci'),
      'buscar': '',
    };

    print(queryParameters);

    final response = await http.post(
        Constants.apiUrl+'solicitudes/listarViajero',
        body : queryParameters
    );

    if(response?.statusCode != 200){
      var statusCode = response?.statusCode.toString();
      print(response.body);

    }else{

      var decodeResp = json.decode(response.body);

      print(decodeResp);

      if(decodeResp['status'] != false) {
        dynamic datas = decodeResp['data'];

        setState(() {
          listBool = false;
          listViajeros = datas;
        });
      } else {
        setState(() {
          listBool = false;
        });
      }

    }
    return "Listo";
  }

  @override
  void initState() {
    onListViajeros();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext context) {

    setContext(context);

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(70.0),
          child: Container(
              padding:EdgeInsets.only(top:10.0,bottom:10.0,left: 45.0),
              child: AppBar(
                  elevation: 0.0,
                  titleSpacing: 0.0,
                  automaticallyImplyLeading: true,
                  title: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                              child:Text(
                                "Volver",
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color:UiHelper.scAzulOscuro,
                                  fontSize: 14.0,
                                ),
                              )
                          ),
                          Padding(padding:EdgeInsets.only(right:10.0),child: SizedBox(width: 100.0,child:Image(
                            image: new AssetImage('assets/images/logo-santa-clara.png'),
                            width: (this.getSize.width * 0.10),
                            fit: BoxFit.contain,
                          ))),
                        ],
                      )
                  )
              )
          )
      ),
      body: Stack(
          children:<Widget>[
            backgroundLayout(sinLogo: true),
            _mainContent(context),
          ]
      ),
      floatingActionButton: new GestureDetector(
        onTap: () async => Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ViajerosPageOne(),
          ),
        ),
        child: Container(
            decoration: BoxDecoration(
                color: Colors.lightBlue,
                borderRadius: BorderRadius.circular(10)
            ),
            child: new Padding(
                padding: EdgeInsets.all(10),
                child: new Icon(
                  Icons.arrow_forward_ios,
                  size: 30,
                  color: Colors.white,
                )
            )
        ),
      ),
    );
  }

  static Color scAzul = Color.fromRGBO(21, 61, 138, 1);


  Widget _mainContent(BuildContext ctx){

    final blocMain = AppProvider.getMainBloc(ctx);

    return Container(
        margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
        padding: EdgeInsets.all(0.0),
        width: getSize.width-45.0,
        //decoration: BoxDecoration( color:UiHelper.greenContainer, ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[

            // campo-busqueda
            SafeArea(
              child: Padding(
                padding:EdgeInsets.only(bottom:0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage("assets/images/background.png"),
                              fit: BoxFit.fill
                          )
                      ),
                      height: 50,
                      padding: EdgeInsets.only(right: 12, left: 12, bottom: 5),
                      child: new Row(
                        children: <Widget>[
                          new Image.asset("assets/viajeros/viajeros.png", width: 30,),
                          new Padding(padding: EdgeInsets.all(5)),
                          new Text("ASISTENCIA AL VIAJERO",
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                              color: Colors.black
                            ),
                          )
                        ],
                      )
                    ),
                    new Padding(
                        padding: EdgeInsets.only(right: 12, left: 12, top: 10),
                        child: new Text("Solicitudes de asistencia",
                            style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w600
                            )
                        )
                    ),
                  ],
                ),
              ),
            ),

            !listBool ? new Flexible(
              flex: 20,
              child: listViajeros.length > 0 ? new ListView.builder(
                padding: EdgeInsets.zero,
                shrinkWrap: true,
                itemCount: listViajeros.length,
                itemBuilder: (ctx, index) {

                  // DateTime fecha_inicial = DateTime.parse(listViajeros[index]['fecha_inicial'].replaceAll('-', '/'));
                  // DateTime fecha_final = DateTime.parse(listViajeros[index]['fecha_final'].replaceAll('-', '/'));

                  // dynamic differenceDays = fecha_inicial.difference(fecha_final).inDays;
                  dynamic differenceDays = 0;

                  return new GestureDetector(
                      onTap: () async => Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ViajerosView(item: listViajeros[index]),
                        ),
                      ),
                      child: new Card(
                        elevation: 1,
                        shadowColor: Colors.cyan,
                        margin: EdgeInsets.all(10),
                        child: new ListTile(
                            leading: new Image.asset("assets/viajeros/aprobado.png", width: 40,),
                            title: new Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                new Row(
                                  children: [
                                    new Text("Días: ${differenceDays.toString()} - ",
                                      style: TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.w600
                                      ),
                                    ),
                                    new Text("No disponible",
                                      style: TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.w600,
                                          color: Colors.green
                                      ),
                                    ),
                                  ],
                                ),
                                new Text("Destino: ${listViajeros[index]['observacion']}",
                                  style: TextStyle(
                                      fontSize: 13,
                                      fontWeight: FontWeight.w500,
                                      color: scAzul
                                  ),
                                )
                              ],
                            ),
                            subtitle: new Row(
                              children: [
                                new Text("Fecha solicitud: ",
                                  style: TextStyle(
                                      fontSize: 13,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.grey
                                  ),
                                ),
                                new Expanded(
                                  child: new Text("${listViajeros[index]['fecha_emision']}",
                                    style: TextStyle(
                                        fontSize: 13,
                                        fontWeight: FontWeight.w600,
                                        color: Colors.black
                                    ),
                                  )
                                ),
                              ],
                            ),
                            trailing: new Icon(
                              Icons.arrow_forward_ios,
                              color: scAzul,
                            )
                        ),
                      )
                  );
                },
              ) : new Center(
                  child: new Padding(padding: EdgeInsets.all(10), child: new Text("No hay datos para mostrar"))
              ),
            ) : new Center(
              child: new Padding(padding: EdgeInsets.all(10), child: CircularProgressIndicator())
            )
          ],

        )
    );
  }
}
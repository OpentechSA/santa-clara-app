import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:santa_clara/src/pages/guiaMedica/info_guia_medica.dart';
import 'package:santa_clara/src/pages/guiaMedica/marker_guiaMedica.dart';
import 'package:santa_clara/src/pages/resultados/resultado_detail.dart';
import 'package:santa_clara/src/pages/viajeros/asistencia/viajeros_page_two.dart';
import 'package:santa_clara/utils/uiHelper.dart';
import 'package:santa_clara/utils/constants.dart';
import 'package:santa_clara/src/modules/appProvider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

class ViajerosPageOne extends StatefulWidget {

  ViajerosPageOne({Key key}) : super(key: key);
  @override
  createState() => ViajerosPageOneState();
}

class ViajerosPageOneState extends State<ViajerosPageOne> with UiHelper {

  final _formViajeros = new GlobalKey<FormState>();

  String destino;
  DateTime fechaDesde = DateTime.now();
  DateTime fechaHasta =  DateTime.now();
  String email;
  String telefono;
  var userDatas;
  List userPlanes = [];
  List beneficiariosList = [];

  TextEditingController fechaDesdeController = TextEditingController();
  TextEditingController fechaHastaController = TextEditingController();


  //Widget Load
  bool onLoad = true;

  Future<void> onUserDatas() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();

    var queryParameters = {
      'ci': prefs.getString("ci_user"),
      'uid': prefs.getString("uidUser"),
    };

    final response = await http.post(
        Constants.apiUrl+'solicitudes/ingresarViajero',
        body : queryParameters
    );

    if(response?.statusCode != 200){
      var statusCode = response?.statusCode.toString();

      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Error"),
            content: Text("No se pudo obtener los datos del usuario"),
            actions: <Widget>[
              FlatButton(
                child: Text("Aceptar"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );

    }else{

      var decodeResp = json.decode(response.body);

      if(decodeResp['status'] != false) {
        dynamic data = decodeResp['data'];

        setState(() {
          userDatas = data;
          userPlanes = data['planes'];
        });
      }
    }
    return "Listo";
  }

  Future<void> onListarClientes() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();

    var queryParameters = {
      'idCliente': prefs.getString("id-cliente"),
    };

    final response = await http.post(
        Constants.apiUrl+'solicitudes/listarClientesViajero',
        body : queryParameters
    );

    if(response?.statusCode != 200){
      var statusCode = response?.statusCode.toString();

      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Error"),
            content: Text("No se pudo obtener los datos del usuario"),
            actions: <Widget>[
              FlatButton(
                child: Text("Aceptar"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );

    }else{

      var decodeResp = json.decode(response.body);

      if(decodeResp['status'] != false) {
        dynamic data = decodeResp['data'];
        setState(() {
          beneficiariosList = data;
        });
      }
    }
    return "Listo";
  }

  Future<void> _selectFechaDesde(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: fechaDesde,
        firstDate: DateTime(2015),
        lastDate: DateTime(2101));
    if (picked != null && picked != fechaDesde)
      setState(() {
        fechaDesde = picked;
        fechaDesdeController.text = "${fechaDesde.day.toString()}/${fechaDesde.month.toString()}/${fechaDesde.year.toString()}";
      });
  }

  Future<void> _selectFechaHasta(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: fechaHasta,
        firstDate: DateTime(2015),
        lastDate: DateTime(2101));
    if (picked != null && picked != fechaHasta)
      setState(() {
        fechaHasta = picked;
        fechaHastaController.text = "${fechaHasta.day.toString()}/${fechaHasta.month.toString()}/${fechaHasta.year.toString()}";
      });
  }

  void userDatasReturn() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      email = prefs.getString('email') != null ? prefs.getString('email') : "";
      telefono = prefs.getString('telefono') != null ? prefs.getString('telefono') : "";

      onLoad = false;
    });
  }

  @override
  void initState() {
    onUserDatas();
    onListarClientes();
    userDatasReturn();
    super.initState();

    fechaDesdeController.text = "${fechaDesde.day.toString()}/${fechaDesde.month.toString()}/${fechaDesde.year.toString()}";
    fechaHastaController.text = "${fechaHasta.day.toString()}/${fechaHasta.month.toString()}/${fechaHasta.year.toString()}";
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext context) {

    setContext(context);

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(70.0),
          child: Container(
              padding:EdgeInsets.only(top:10.0,bottom:10.0,left: 45.0),
              child: AppBar(
                  elevation: 0.0,
                  titleSpacing: 0.0,
                  automaticallyImplyLeading: true,
                  title: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                              child:Text(
                                "Volver",
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color:UiHelper.scAzulOscuro,
                                  fontSize: 14.0,
                                ),
                              )
                          ),
                          Padding(padding:EdgeInsets.only(right:10.0),child: SizedBox(width: 100.0,child:Image(
                            image: new AssetImage('assets/images/logo-santa-clara.png'),
                            width: (this.getSize.width * 0.10),
                            fit: BoxFit.contain,
                          ))),
                        ],
                      )
                  )
              )
          )
      ),
      body: Stack(
          children:<Widget>[
            backgroundLayout(sinLogo: true),
            _mainContent(context),
          ]
      ),
      floatingActionButton: new GestureDetector(
        onTap: () async {
          if (_formViajeros.currentState.validate()) {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => ViajerosPageTwo(
                  destino: destino,
                  fechaDesde: fechaDesde,
                  fechaHasta: fechaHasta,
                  email: email,
                  telefono: telefono,
                  userDatas: userDatas,
                  userPlanes: userPlanes,
                  beneficiariosList: beneficiariosList
                ),
              ),
            );
          }
        },
        child: Container(
            decoration: BoxDecoration(
                color: Colors.lightBlue,
                borderRadius: BorderRadius.circular(10)
            ),
            child: new Padding(
                padding: EdgeInsets.all(10),
                child: new Icon(
                  Icons.arrow_forward_ios,
                  size: 30,
                  color: Colors.white,
                )
            )
        ),
      ),
    );

  }

  String _radioValue = 'profesionales'; //Initial definition of radio button value
  String choice;

  static Color scAzul = Color.fromRGBO(21, 61, 138, 1);

  var selectedString;

  Widget _mainContent(BuildContext ctx){

    final blocMain = AppProvider.getMainBloc(ctx);

    return Container(
      margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
      padding: EdgeInsets.all(0.0),
      width: getSize.width-45.0,
      //decoration: BoxDecoration( color:UiHelper.greenContainer, ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[

          // campo-busqueda
          SafeArea(
            child: Padding(
              padding:EdgeInsets.only(bottom:0),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage("assets/images/background.png"),
                              fit: BoxFit.fill
                          )
                      ),
                      height: 50,
                      padding: EdgeInsets.only(right: 12, left: 12, bottom: 5),
                      child: new Row(
                        children: <Widget>[
                          new Image.asset("assets/viajeros/viajeros.png", width: 30,),
                          new Padding(padding: EdgeInsets.all(5)),
                          new Text("ASISTENCIA AL VIAJERO",
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                                color: Colors.black
                            ),
                          )
                        ],
                      )
                  ),
                  new Padding(
                    padding: EdgeInsets.only(top: 15, right: 15, left: 15, bottom: 5),
                    child: new Row(
                      children: [
                        new Container(
                          padding: EdgeInsets.only(left: 12, right: 12, top: 3, bottom: 5),
                          decoration: BoxDecoration(
                            color: scAzul,
                            borderRadius: BorderRadius.circular(20)
                          ),
                          child: new Text("1",
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w600,
                              color: Colors.white
                            ),
                          )
                        ),
                        new Text(" Cuándo y dónde viajarán?",
                            style: TextStyle(
                                fontSize: 18,
                                color: scAzul,
                                fontWeight: FontWeight.w600
                            )
                        ),
                      ],
                    )
                  ),
                  new Padding(
                      padding: EdgeInsets.only(top: 15, right: 15, left: 15, bottom: 10),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Flexible(
                            flex: 1,
                            child: new Text("Finalización del contrato",
                                style: TextStyle(
                                    fontSize: 18,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w300
                                )
                            ),
                          ),
                          Flexible(
                            flex: 1,
                            child: new Container(
                              decoration: BoxDecoration(
                                  color: Colors.grey[600],
                                  borderRadius: BorderRadius.circular(20)
                              ),
                              padding: EdgeInsets.all(10),
                              child: new Text(userDatas != null ? userDatas['fecha_final'].toString() : "Sin fecha",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 15,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w600
                                ),
                              )
                            )
                          ),
                        ],
                      )
                  ),
                ],
              ),
            ),
          ),

          new Flexible(
            flex: 20,
            child: SingleChildScrollView(
              child: !onLoad ? new Form(
                key: _formViajeros,
                child:  new Column(
                  children: [
                    inputWrapper(
                      field: StatefulBuilder(
                        builder: (BuildContext context, setState) {
                          return Container(
                            width: MediaQuery.of(context).size.width,
                            child: TextFormField(
                              onChanged: (value) {
                                setState(() {
                                  destino = value.toString();
                                });
                              },
                              onSaved: (value) {
                                setState(() {
                                  destino = value.toString();
                                });
                              },
                              validator: (value) {
                                if(value.toString().length < 1) {
                                  return "Es requerido";
                                }else{
                                  return null;
                                }
                              },
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                labelText: 'Destino',
                                labelStyle: TextStyle(
                                    fontSize: 12
                                ),
                                border: InputBorder.none,
                              ),
                            ),
                          );
                        }
                      ),
                      withBoxShadow: false
                    ),
                    new Padding(padding: EdgeInsets.all(5)),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          flex: 1,
                          child: inputWrapper(
                              field: Container(
                                child: TextFormField(
                                  controller: fechaDesdeController,
                                  onTap: () => _selectFechaDesde(context),
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                    labelText: 'Fecha desde',
                                    labelStyle: TextStyle(
                                        fontSize: 12
                                    ),
                                    border: InputBorder.none,
                                  ),
                                  readOnly: true,
                                ),
                              ),
                              withBoxShadow: false
                          )
                        ),
                        Flexible(
                          flex: 1,
                          child: inputWrapper(
                              field: Container(
                                child: TextFormField(
                                  controller: fechaHastaController,
                                  onTap: () => _selectFechaHasta(context),
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                    labelText: 'Fecha hasta',
                                    labelStyle: TextStyle(
                                        fontSize: 12
                                    ),
                                    border: InputBorder.none,
                                  ),
                                  readOnly: true,
                                ),
                              ),
                              withBoxShadow: false
                          )
                        ),
                      ],
                    ),
                    new Padding(padding: EdgeInsets.all(5)),
                    inputWrapper(
                        field: StatefulBuilder(
                            builder: (BuildContext context, setState) {
                              return Container(
                                width: MediaQuery.of(context).size.width,
                                child: TextFormField(
                                  onChanged: (value) {
                                    setState(() {
                                      email = value.toString();
                                    });
                                  },
                                  onSaved: (value) {
                                    setState(() {
                                      email = value.toString();
                                    });
                                  },
                                  validator: (value) {
                                    if(value.toString().length < 1) {
                                      return "Es requerido";
                                    }else{
                                      return null;
                                    }
                                  },
                                  initialValue: email.toString(),
                                  keyboardType: TextInputType.emailAddress,
                                  decoration: InputDecoration(
                                    labelText: 'Correo Electrónico',
                                    labelStyle: TextStyle(
                                        fontSize: 12
                                    ),
                                    border: InputBorder.none,
                                  ),
                                ),
                              );
                            }
                        ),
                        withBoxShadow: false
                    ),
                    new Padding(padding: EdgeInsets.all(5)),
                    inputWrapper(
                        field: StatefulBuilder(
                            builder: (BuildContext context, setState) {
                              return Container(
                                width: MediaQuery.of(context).size.width,
                                child: TextFormField(
                                  onChanged: (value) {
                                    setState(() {
                                      telefono = value.toString();
                                    });
                                  },
                                  onSaved: (value) {
                                    setState(() {
                                      telefono = value.toString();
                                    });
                                  },
                                  validator: (value) {
                                    if(value.toString().length < 1) {
                                      return "Es requerido";
                                    }else{
                                      return null;
                                    }
                                  },
                                  initialValue: telefono.toString(),
                                  keyboardType: TextInputType.phone,
                                  decoration: InputDecoration(
                                    labelText: 'Telefono del solicitante',
                                    labelStyle: TextStyle(
                                        fontSize: 12
                                    ),
                                    border: InputBorder.none,
                                  ),
                                ),
                              );
                            }
                        ),
                        withBoxShadow: false
                    ),
                    new Padding(
                        padding: EdgeInsets.all(20)
                    )
                  ],
                )
              ) : Padding(padding: EdgeInsets.all(10), child: Center(child: CircularProgressIndicator()))
            ),
          )
        ],
      )
    );
  }

}
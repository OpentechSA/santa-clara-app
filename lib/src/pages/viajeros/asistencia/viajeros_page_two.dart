import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:santa_clara/src/pages/guiaMedica/info_guia_medica.dart';
import 'package:santa_clara/src/pages/guiaMedica/marker_guiaMedica.dart';
import 'package:santa_clara/src/pages/resultados/resultado_detail.dart';
import 'package:santa_clara/utils/uiHelper.dart';
import 'package:santa_clara/utils/constants.dart';
import 'package:santa_clara/src/modules/appProvider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';

class ViajerosPageTwo extends StatefulWidget {

  final destino;
  final fechaDesde;
  final fechaHasta;
  final email;
  final telefono;
  final userDatas;
  final List userPlanes;
  final List beneficiariosList;

  ViajerosPageTwo(
    {
      Key key,
      @required this.destino,
      @required this.fechaDesde,
      @required this.fechaHasta,
      @required this.email,
      @required this.telefono,
      @required this.userDatas,
      @required this.userPlanes,
      @required this.beneficiariosList
    }
  ) : super(key: key);
  @override
  createState() => ViajerosPageTwoState();
}

class ViajerosPageTwoState extends State<ViajerosPageTwo> with UiHelper {

  Future<void> solicitarAsistencia() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();

    String dayDesde = int.parse(widget.fechaDesde.day.toString()) > 9 ? widget.fechaDesde.day.toString() : "0${widget.fechaDesde.day.toString()}";
    String dayHasta = int.parse(widget.fechaHasta.day.toString()) > 9 ? widget.fechaHasta.day.toString() : "0${widget.fechaHasta.day.toString()}";

    String monthDesde = int.parse(widget.fechaDesde.month.toString()) > 9 ? widget.fechaDesde.month.toString() : "0${widget.fechaDesde.month.toString()}";
    String monthHasta = int.parse(widget.fechaHasta.month.toString()) > 9 ? widget.fechaHasta.month.toString() : "0${widget.fechaHasta.month.toString()}";;

    var queryParameters = {
      'idCliente': prefs.getString("id-cliente"),
      'secuencia': beneficiariosSelected.toString(),
      "destino": widget.destino.toString(),
      "fechaInicio": '${dayDesde}/${monthDesde}/${widget.fechaDesde.year.toString()}',
      "fechaFin": '${dayHasta}/${monthHasta}/${widget.fechaHasta.year.toString()}',
      "telefono": widget.telefono.toString(),
      "email": widget.email.toString(),
      "uid": prefs.getString("uidUser")
    };

    print(queryParameters.toString());

    final response = await http.post(
        Constants.apiUrl+'solicitudes/solicitarViajero',
        body : queryParameters
    );

    print("Status: ${response.statusCode}");

    var resp = json.decode(response.body);

    print("Response: ${resp.toString()}");

    if(response?.statusCode != 200){
      var statusCode = response?.statusCode.toString();

      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Error"),
            content: Text("No se pudo procesar la solicitud, verifique los datos e intentelo de nuevo \n Error: ${response.body}"),
            actions: <Widget>[
              FlatButton(
                child: Text("Aceptar"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );

    }else{

      var decodeResp = json.decode(response.body);

      if(decodeResp['status'] != false) {
        showDialog(
          barrierDismissible: false,
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Santa Clara"),
              content: Text("La solicitud de assitencia al viajero fue recepciada de forma exitosa."),
              actions: <Widget>[
                FlatButton(
                  child: Text("Aceptar"),
                  onPressed: () async {
                    Navigator.pushNamedAndRemoveUntil(context, 'viajeros', (Route<dynamic> route) => false);
                  },
                ),
              ],
            );
          },
        );

      } else {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Error"),
              content: Text(decodeResp['data']['msg'].toString()),
              actions: <Widget>[
                FlatButton(
                  child: Text("Aceptar"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      }
    }
    setState(() => onLoad = false);
    return "Listo";

  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext context) {

    setContext(context);

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(70.0),
          child: Container(
              padding:EdgeInsets.only(top:10.0,bottom:10.0,left: 45.0),
              child: AppBar(
                  elevation: 0.0,
                  titleSpacing: 0.0,
                  automaticallyImplyLeading: true,
                  title: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                              child:Text(
                                "Volver",
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color:UiHelper.scAzulOscuro,
                                  fontSize: 14.0,
                                ),
                              )
                          ),
                          Padding(padding:EdgeInsets.only(right:10.0),child: SizedBox(width: 100.0,child:Image(
                            image: new AssetImage('assets/images/logo-santa-clara.png'),
                            width: (this.getSize.width * 0.10),
                            fit: BoxFit.contain,
                          ))),
                        ],
                      )
                  )
              )
          )
      ),
      body: Stack(
        children:<Widget>[
          backgroundLayout(sinLogo: true),
          _mainContent(context),
        ]
      ),
      floatingActionButton: new GestureDetector(
        onTap: () async {
          if(!onLoad) {
            setState(() => onLoad = true);
            await solicitarAsistencia();
          }
        },
        child: Container(
            decoration: BoxDecoration(
                color: scAzul,
                borderRadius: BorderRadius.circular(10)
            ),
            child: new Padding(
                padding: EdgeInsets.all(10),
                child: !onLoad ? new Image.asset("assets/visaciones/save.png", width: 35,) : CircularProgressIndicator()
            )
        ),
      ),
    );

  }

  String _radioValue = 'profesionales'; //Initial definition of radio button value
  String choice;

  static Color scAzul = Color.fromRGBO(21, 61, 138, 1);

  var planesSelected;
  List beneficiariosSelected = [];
  bool onLoad = false;

  Widget _mainContent(BuildContext ctx){

    final blocMain = AppProvider.getMainBloc(ctx);

    return Container(
        margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
        padding: EdgeInsets.all(0.0),
        width: getSize.width-45.0,
        //decoration: BoxDecoration( color:UiHelper.greenContainer, ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[

            // campo-busqueda
            SafeArea(
              child: Padding(
                padding:EdgeInsets.only(bottom:0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage("assets/images/background.png"),
                              fit: BoxFit.fill
                          )
                      ),
                      height: 50,
                      padding: EdgeInsets.only(right: 12, left: 12, bottom: 5),
                      child: new Row(
                        children: <Widget>[
                          new Image.asset("assets/viajeros/viajeros.png", width: 30,),
                          new Padding(padding: EdgeInsets.all(5)),
                          new Text("ASISTENCIA AL VIAJERO",
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                                color: Colors.black
                            ),
                          )
                        ],
                      )
                    ),
                    new Padding(
                      padding: EdgeInsets.only(top: 15, right: 15, left: 15),
                      child: new Row(
                        children: [
                          new Container(
                              padding: EdgeInsets.only(left: 12, right: 12, top: 3, bottom: 5),
                              decoration: BoxDecoration(
                                  color: scAzul,
                                  borderRadius: BorderRadius.circular(20)
                              ),
                              child: new Text("2",
                                style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.white
                                ),
                              )
                          ),
                          new Text(" Quiénes viajarán?",
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w600
                              )
                          ),
                        ],
                      )
                    ),
                    new Container(
                      color: Color(0xffEFEFEF),
                      margin: EdgeInsets.only(top: 10),
                      padding: EdgeInsets.only(right: 10, left: 10, top: 20, bottom: 20),
                      child: inputWrapper(
                          field: new DropdownButtonFormField(
                            validator: (value) => planesSelected == null ? 'Es requerido' : null,
                            hint: new Text("Seleccionar plan del solicitante"),
                            isExpanded: true,
                            items: widget.userPlanes.map((data) {
                              return new DropdownMenuItem(
                                child: new Text(data['plan'].toString(), style: TextStyle(fontSize: 15.0),),
                                value: data,
                              );
                            }).toList(),
                            onChanged: (value) {
                              planesSelected = value;
                            },
                            onSaved: (value) {
                              planesSelected = value;
                            },
                            value: planesSelected,
                            // value: planesSelected,
                          ),
                          withBoxShadow: false
                      ),
                    ),
                  ],
                ),
              ),
            ),
            widget.beneficiariosList.length > 0 ? new Flexible(
              flex: 20,
              child: new ListView.builder(
                padding: EdgeInsets.zero,
                shrinkWrap: true,
                itemCount: widget.beneficiariosList.length,
                itemBuilder: (ctx, index) {
                  var item = widget.beneficiariosList[index];
                  bool itemLoad = false;
                  return new InkWell(
                    onTap: () async {
                      setState(() => itemLoad = true);
                      if(beneficiariosSelected.contains(item["secuencia"])) {
                        beneficiariosSelected.remove(item['secuencia']);
                      } else {
                        beneficiariosSelected.add(item['secuencia']);
                      }
                      setState(() => itemLoad = false);
                    },
                    child: new Card(
                      elevation: 1,
                      shadowColor: Colors.cyan,
                      margin: EdgeInsets.all(10),
                      child: new ListTile(
                          leading: new Padding(
                              padding: EdgeInsets.all(5),
                              child: new Image.asset("assets/viajeros/person_white.png")
                          ),
                          title: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              new Text("Nro de cliente: ${item['id_cliente'].toString()}",
                                style: TextStyle(
                                    fontSize: 10,
                                    fontWeight: FontWeight.w600
                                ),
                              ),
                              new Text("${item['nombres'].toString()}",
                                style: TextStyle(
                                    fontSize: 15,
                                    fontWeight: FontWeight.w500,
                                    color: scAzul
                                ),
                              )
                            ],
                          ),
                          subtitle: new Text("Cédula: ${item['ci'].toString()}",
                            style: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.w600,
                                color: Colors.black
                            ),
                          ),
                          trailing: StatefulBuilder(
                            builder: (BuildContext context, itemLoad) {
                              return new Container(
                                decoration: BoxDecoration(
                                  color: beneficiariosSelected.contains(item["secuencia"]) ? Colors.red : Colors.grey,
                                  borderRadius: BorderRadius.circular(20)
                                ),
                                child: new Padding(
                                  padding: EdgeInsets.all(5),
                                  child: new Icon(
                                    Icons.check,
                                    color: beneficiariosSelected.contains(item["secuencia"]) ? Colors.white : Colors.black,)
                                )
                              );
                            },
                          )
                      ),
                    )
                  );
                },
              ),
            ) : Flexible(flex: 15, child: Center(child: new Text("No hay beneficiarios disponibles para seleccionar")))
          ],
        )
    );
  }
}
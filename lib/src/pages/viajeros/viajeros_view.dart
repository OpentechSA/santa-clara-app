import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:santa_clara/src/pages/guiaMedica/info_guia_medica.dart';
import 'package:santa_clara/src/pages/guiaMedica/marker_guiaMedica.dart';
import 'package:santa_clara/src/pages/resultados/resultado_detail.dart';
import 'package:santa_clara/utils/uiHelper.dart';
import 'package:santa_clara/utils/constants.dart';
import 'package:santa_clara/src/modules/appProvider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';

class ViajerosView extends StatefulWidget {

  ViajerosView({Key key, this.item}) : super(key: key);

  final dynamic item;

  @override
  ViajerosViewState createState() => ViajerosViewState();
}

class ViajerosViewState extends State<ViajerosView> with UiHelper {

  List adherentesList = [];

  void listarAdherentes() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();

    String id = prefs.getString("id-cliente");

    final response = await http.get(
      Constants.apiUrl+'agenda/adherentes?idCliente=$id',
    );

    if(response?.statusCode != 200){
      var statusCode = response?.statusCode.toString();
    }else{
      var decodeResp = json.decode(response.body);

      if (decodeResp['status']  == true) {
        setState(() {
          adherentesList = decodeResp['data'];
        });
      }

    }

  }

  @override
  void initState() {
    listarAdherentes();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext context) {

    setContext(context);

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(70.0),
          child: Container(
              padding:EdgeInsets.only(top:10.0,bottom:10.0,left: 45.0),
              child: AppBar(
                  elevation: 0.0,
                  titleSpacing: 0.0,
                  automaticallyImplyLeading: true,
                  title: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                              child:Text(
                                "Volver",
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color:UiHelper.scAzulOscuro,
                                  fontSize: 14.0,
                                ),
                              )
                          ),
                          Padding(padding:EdgeInsets.only(right:10.0),child: SizedBox(width: 100.0,child:Image(
                            image: new AssetImage('assets/images/logo-santa-clara.png'),
                            width: (this.getSize.width * 0.10),
                            fit: BoxFit.contain,
                          ))),
                        ],
                      )
                  )
              )
          )
      ),
      body: Stack(
          children:<Widget>[
            backgroundLayout(sinLogo: true),
            _mainContent(context),
          ]
      ),
    );

  }

  String _radioValue = 'profesionales'; //Initial definition of radio button value
  String choice;

  static Color scAzul = Color.fromRGBO(21, 61, 138, 1);

  var selectedString;

  Widget _mainContent(BuildContext ctx){

    final blocMain = AppProvider.getMainBloc(ctx);

    return Container(
        margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
        padding: EdgeInsets.all(0.0),
        width: getSize.width-45.0,
        //decoration: BoxDecoration( color:UiHelper.greenContainer, ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[

            // campo-busqueda
            SafeArea(
              top: true,
              bottom: true,
              child: Padding(
                padding:EdgeInsets.only(bottom:0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Container(
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage("assets/images/background.png"),
                                fit: BoxFit.fill
                            )
                        ),
                        height: 50,
                        padding: EdgeInsets.only(right: 12, left: 12, bottom: 5),
                        child: new Row(
                          children: <Widget>[
                            new Image.asset("assets/viajeros/viajeros.png", width: 30,),
                            new Padding(padding: EdgeInsets.all(5)),
                            new Text("ASISTENCIA AL VIAJERO",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.black
                              ),
                            )
                          ],
                        )
                    ),
                    new Padding(
                      padding: EdgeInsets.only(top: 15, right: 15, left: 15, bottom: 12),
                      child: new Row(
                        children: [
                          new Image.asset("assets/viajeros/airplane.png", width: 30,),
                          new Text(" Datos del viaje",
                            style: TextStyle(
                              fontSize: 18,
                              color: scAzul,
                              fontWeight: FontWeight.w600
                            )
                          ),
                        ],
                      )
                    ),
                    new Padding(
                      padding: EdgeInsets.only(top: 15, right: 15, left: 15),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          new Text("Estado de la solicitud",
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w300
                              )
                          ),
                          new Container(
                              decoration: BoxDecoration(
                                  color: Color(0xffff2d55),
                                  borderRadius: BorderRadius.circular(20)
                              ),
                              padding: EdgeInsets.all(10),
                              child: new Row(
                                children: [
                                  new Icon(
                                    Icons.cancel,
                                    color: Colors.white,
                                    size: 12,
                                  ),
                                  new Text(" ANULADO",
                                    style: TextStyle(
                                        fontSize: 12,
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600
                                    ),
                                  )
                                ],
                              )
                          ),
                        ],
                      )
                    ),
                  ],
                ),
              ),
            ),

            new Flexible(
              flex: 15,
              child: SingleChildScrollView(
                  child: new Column(
                    children: [
                      inputWrapper(
                        field: StatefulBuilder(
                            builder: (BuildContext context, setState) {
                              return Container(
                                width: MediaQuery.of(context).size.width,
                                child: TextFormField(
                                  initialValue: widget.item['observacion'],
                                  decoration: InputDecoration(
                                    labelText: 'Destino',
                                    labelStyle: TextStyle(
                                        fontSize: 12
                                    ),
                                    border: InputBorder.none,
                                  ),
                                ),
                              );
                            }
                        ),
                        withBoxShadow: false
                      ),
                      new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          inputWrapper(
                            paddingRight: 0,
                            field: StatefulBuilder(
                              builder: (BuildContext context, setState) {
                                return Container(
                                  width: MediaQuery.of(context).size.width * 0.3,
                                  child: TextFormField(
                                    initialValue: widget.item['fecha_inicial'],
                                    readOnly: true,
                                    decoration: InputDecoration(
                                      suffix: SizedBox(
                                        child: new GestureDetector(
                                          onTap: () => print(""),
                                          child: new Icon(
                                            Icons.calendar_today,
                                            color: scAzul,
                                            size: 15,
                                          )
                                        )
                                      ),
                                      labelText: 'Fecha desde',
                                      labelStyle: TextStyle(
                                          fontSize: 12
                                      ),
                                      border: InputBorder.none,
                                    ),
                                  ),
                                );
                              }
                            ),
                            withBoxShadow: false
                          ),
                          inputWrapper(
                              paddingLeft: 0,
                              field: StatefulBuilder(
                                builder: (BuildContext context, setState) {
                                  return Container(
                                    width: MediaQuery.of(context).size.width * 0.3,
                                    child: TextFormField(
                                      initialValue: widget.item['fecha_final'],
                                      readOnly: true,
                                      decoration: InputDecoration(
                                        suffix: SizedBox(
                                          child: new GestureDetector(
                                            onTap: () => print(""),
                                            child: new Icon(
                                              Icons.calendar_today,
                                              color: scAzul,
                                              size: 15,
                                            )
                                          )
                                        ),
                                        labelText: 'Fecha hasta',
                                        labelStyle: TextStyle(
                                            fontSize: 12
                                        ),
                                        border: InputBorder.none,
                                      ),
                                    ),
                                  );
                                }
                              ),
                              withBoxShadow: false
                          ),
                        ],
                      ),
                      inputWrapper(
                          field: StatefulBuilder(
                            builder: (BuildContext context, setState) {
                              return Container(
                                width: MediaQuery.of(context).size.width,
                                child: TextField(
                                  readOnly: true,
                                  decoration: InputDecoration(
                                    labelText: 'Correo Electrónico',
                                    labelStyle: TextStyle(
                                        fontSize: 12
                                    ),
                                    border: InputBorder.none,
                                  ),
                                ),
                              );
                            }
                          ),
                          withBoxShadow: false
                      ),
                      inputWrapper(
                        field: StatefulBuilder(
                            builder: (BuildContext context, setState) {
                              return Container(
                                width: MediaQuery.of(context).size.width,
                                child: TextField(
                                  readOnly: true,
                                  decoration: InputDecoration(
                                    labelText: 'Telefono o Celular',
                                    labelStyle: TextStyle(
                                        fontSize: 12
                                    ),
                                    border: InputBorder.none,
                                  ),
                                ),
                              );
                            }
                        ),
                        withBoxShadow: false
                      ),
                      inputWrapper(
                        field: StatefulBuilder(
                          builder: (BuildContext context, setState) {
                            return new DropdownButtonFormField(
                              validator: (value) => value == null ? 'Es requerido' : null,
                              hint: new Text("Plan del solicitante"),
                              isExpanded: true,
                              items: ["University", "Gold"].map((data) {
                                return new DropdownMenuItem(
                                  child: new Text(data, style: TextStyle(fontSize: 15.0),),
                                  value: data,
                                );
                              }).toList(),
                              onChanged: (value) {
                                print(value);
                              },
                              value: selectedString,
                            );
                          }
                        ),
                        withBoxShadow: false
                      ),
                      new Padding(
                          padding: EdgeInsets.only(top: 20, right: 12, left: 12, bottom: 0),
                          child: new Row(
                            children: [
                              new Image.asset("assets/viajeros/person_two.png",
                                width: 30,
                              ),
                              new Text(" Viajeros",
                                style: TextStyle(
                                    fontSize: 20,
                                    color: scAzul,
                                    fontWeight: FontWeight.w700
                                )
                              )
                            ],
                          )
                      ),
                      adherentesList.length > 0 ? new ListView.builder(
                          padding: EdgeInsets.zero,
                          shrinkWrap: true,
                          itemCount: adherentesList.length,
                          itemBuilder: (context, int) {
                            return new Container(
                              decoration: BoxDecoration(
                                  border: Border(
                                      bottom: BorderSide(
                                          color: Colors.grey[600],
                                          width: 1
                                      )
                                  )
                              ),
                              child: new Row(
                                children: [
                                  new Container(
                                    padding: EdgeInsets.only(left: 12, top: 0, bottom: 8, right: 12),
                                    color: Colors.grey[300],
                                    child: new Text(adherentesList[int]['ci'],
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.w600,
                                          color: Colors.grey[600]
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: new Container(
                                      padding: EdgeInsets.only(left: 12, top: 0, bottom: 8, right: 12),
                                      color: Colors.white,
                                      child: new Text(adherentesList[int]['nombres'],
                                        maxLines: 1,
                                        style: TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.w500,
                                            color: scAzul
                                        ),
                                      ),
                                    )
                                  ),
                                ],
                              ),
                            );
                          }
                      ) : new Center(child: new Text("No hay adherentes")),
                      new Padding(
                          padding: EdgeInsets.all(20)
                      )
                    ],
                  )
              ),
            )
          ],
        )
    );
  }

}
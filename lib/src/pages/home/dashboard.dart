
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:santa_clara/src/components/menuDrawer.dart';
import 'package:santa_clara/src/components/scAppBar.dart';
import 'package:santa_clara/src/models/agendaModels.dart';
import 'package:santa_clara/src/modules/appProvider.dart';
import 'package:santa_clara/src/pages/agenda/verTareaAgenda.dart';
import 'package:santa_clara/src/pages/agenda/verTurno.dart';
import 'package:santa_clara/src/pages/guiaMedica/guia_medica.dart';
import 'package:santa_clara/src/pages/planes/document_view_planes.dart';
import 'package:santa_clara/src/pages/planes/planes_list.dart';
import 'package:santa_clara/src/pages/presupuestos/presupuestos_page.dart';
import 'package:santa_clara/src/pages/resultados/resultados.dart';
import 'package:santa_clara/src/pages/viajeros/viajeros_page.dart';
import 'package:santa_clara/src/pages/visaciones/visaciones_page.dart';
import 'package:santa_clara/src/services/bookingService.dart';
import 'package:santa_clara/utils/constants.dart';
import 'package:santa_clara/utils/uiHelper.dart';
import 'package:santa_clara/utils/userPreferences.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class Dashboard extends StatefulWidget {

  Dashboard({Key key}) : super(key: key);
  @override
  createState() => DashboardState();
}


class DashboardState extends State<Dashboard> with UiHelper {

  Widget build(BuildContext context) {

    setContext(context);

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: ScAppBar(),
      drawer: MenuDrawer(),
      body: Stack(
        children:<Widget>[
          backgroundLayout(sinLogo: true),
          _mainContent(context)
        ]
      )
    );
  }

  List resultadosList = List();

  List planesList = [];
  bool planesView = false;

  Turnos turnos = new Turnos();

  List<Evento> eventos = [];
  Map<String,dynamic> indicadores = new Map<String,dynamic>();
  int itemsCitas = 0;
  final UserPreferences userPrefs = new UserPreferences();

  @override
  void initState() {
    onListarResultados();
    onListarPlanes();
    super.initState();
  }

  Future<String> onListarPlanes() async {

    final response = await http.get(
      Constants.apiUrl+'productos/planesApp?tipo=planes',
    );

    if(response?.statusCode != 200){
      var statusCode = response?.statusCode.toString();
    }else{
      var decodeResp = json.decode(response.body);

      setState(() {
        planesList = decodeResp['data'];
        planesView = true;
      });
    }

    return "Listo";
  }

  Future<String> onListarResultados() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();

    var queryParameters = {
      'ci': prefs.getString("ci_user"),
    };

    final response = await http.post(
        Constants.apiUrl+'analisis/listar',
        body : queryParameters
    );

    print(response.statusCode);

    if(response?.statusCode != 200){
      var statusCode = response?.statusCode.toString();
    }else{
      var decodeResp = json.decode(response.body);

      print("Resultados: ${decodeResp.toString()}");

      setState(() {
        resultadosList = decodeResp['data']['resultados'];
      });
    }
    return "Listo";
  }


  Widget _mainContent(BuildContext ctx){

    final blocMain = AppProvider.getMainBloc(ctx);

    return Container(
      margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
      padding: EdgeInsets.all(0.0),
      width: getSize.width-30,
      //decoration: BoxDecoration( color:UiHelper.greenContainer, ),
      child: Column(                  
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[

          // campo-busqueda
          SafeArea(child: Padding(
            padding:EdgeInsets.only(bottom:10.0),
            child: inputWrapper(field:campoBuscarDashboard(bloc: blocMain),withBoxShadow: true),
          ),),
          
          Flexible(
            flex:12,
            
              child:ListView(
                
                padding: EdgeInsets.zero,
                children: [

                  // grilla-botones-box
                  Container(
                    padding:EdgeInsets.only(left:10.0,right:10.0),
                    child:Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children:<Widget>[

                        Padding(
                          padding: EdgeInsets.only(bottom:10.0),
                          child:Row( children: <Widget>[
                              Expanded( child: Stack( fit:StackFit.passthrough, children: <Widget>[
                                Padding(
                                    padding: EdgeInsets.only(top:10.0),
                                    child:boxDashboard(
                                      icon:FontAwesomeIcons.microscope,
                                      text: 'Resultados',
                                      onTap: () async => Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => Resultados(),
                                        ),
                                      )
                                    )
                                ),
                                badgetItems(items:double.parse(resultadosList.length.toString()),color:Colors.green,),
                              ],),),
                              Expanded( child: Stack( fit:StackFit.passthrough, children:<Widget>[
                                Padding(
                                  padding: EdgeInsets.only(top:10.0),
                                  child: boxDashboard(icon:FontAwesomeIcons.calendarAlt,text:'Citas'),
                                ),
                                badgetItems(items:1.0,color:Colors.red),
                              ],),),
                            Expanded( child: Stack( fit:StackFit.passthrough, children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(top:10.0),
                                child:boxDashboard(
                                  icon:FontAwesomeIcons.userMd,
                                  text:'Médicos',
                                    onTap: () async => Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => GuiaMedica(),
                                      ),
                                    )
                                ),
                              ),
                            ],),),

                          ],),
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom:10.0),
                          child:Row( children: <Widget>[
                            Expanded(child: Stack( fit:StackFit.passthrough, children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(top:10.0),
                                child: boxDashboard(
                                  icon:FontAwesomeIcons.fileInvoice,
                                  text:'Facturas',
                                  onTap: null,
                                ),
                              ),
                              badgetItems(items:1.0,color:Colors.lightBlue),
                            ],),),
                            Expanded(child: Stack( fit:StackFit.passthrough, children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(top:10.0),
                                child: boxDashboard(icon:FontAwesomeIcons.fileContract,text:'Visaciones',
                                  onTap: () async => Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => VisacionesPage(),
                                    ),
                                  )
                                ),
                              ),
                            ],),),
                            Expanded( child:Stack( fit:StackFit.passthrough, children: <Widget>[
                              Padding(
                                padding: EdgeInsets.only(top:10.0),
                                child: boxDashboard(
                                  icon: FontAwesomeIcons.fileInvoiceDollar,
                                  text:'Presupuestos',
                                  onTap: () async => Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => PresupuestoPage(),
                                    ),
                                  )
                                ),
                              ),
                            ],),),

                          ],),
                        ),

                      ]
                    )
                  ),

                  // slider-planes
                  Container(
                    margin:EdgeInsets.only(top:10.0),
                    padding: EdgeInsets.only(top:0.0,bottom:5.0),
                    decoration: BoxDecoration( color:Color.fromRGBO(0, 159, 227, 0.20), ),
                    child: Column(
                      children:<Widget>[
                        //header slider planes
                        headerSliderPlanes(
                          title:'Planes de Cobertura',
                          evtOnPressed:() {
                            Navigator.of(context).push(MaterialPageRoute(builder: (_) {
                              return PlanesListWidget();
                            }));
                          }
                        ),
                        //slider planes
                        Container(
                          height: 100,
                          //margin: EdgeInsets.only(left:10.0),
                          child: planesView ? ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: planesList.length,
                            itemBuilder: (context, i) {
                              return cardPlanes(
                                color: Color.fromRGBO(72, 39, 107, 1),
                                title: planesList[i]['descripcion'],
                                url: planesList[i]['archivos']['imagen']['path'],
                                evtOnPressed: () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => DocumentPlanesViewer(
                                      title: planesList[i]['descripcion'],
                                      documentType: 'pdf',
                                      document: planesList[i]['archivos']['cobertura']['path'],
                                    ),
                                  )
                                )
                              );
                            },
                            // children: <Widget>[
                            //   cardPlanes(color:Color.fromRGBO(72, 39, 107, 1),title:'Plan SANIMED Integral'),
                            //   cardPlanes(color:Color.fromRGBO(131, 58, 39, 1),title:'Plan Galeno Familiar'),
                            //   cardPlanes(color:Color.fromRGBO(198, 27, 51, 1),title:'Plan UNIVERSITY',lastItem: true)
                            // ],
                          ) : new Center(child: new CircularProgressIndicator()),
                        ),
                      ]
                    )
                  ),
                ],
              )
          ),
          headerSliderPlanes(title:'Actividades y recordatorios',evtOnPressed:(){}),

          Flexible(
            fit: FlexFit.loose,
            flex: 6,
            child: _listaActividades(),
          ),
        ],
      )
    );
  }

  Widget _listaActividades(){
    return FutureBuilder(
        future: turnos.getEventosDashboard(new RequestEventos(uid:userPrefs.id)),
        builder: (context,AsyncSnapshot<EventosResponse> snapshot) {
          if(snapshot.hasData){

            eventos.clear();
            if(snapshot.data.status){
              snapshot.data.data.forEach((key,items) {
                List<dynamic> eventosFecha = items;
                Evento evt;
                eventosFecha.forEach((element) {
                  evt = new Evento.fromJson(element);
                  eventos.add(evt);
                  if(evt.tipo == 'TURNO'){
                    itemsCitas++;
                  }
                });
              });
            }

            return Container(
                child:SingleChildScrollView(
                    child:Column(
                      children:latestEventList(eventos),
                    )
                )
            );

          }else{
            return Container(padding:EdgeInsets.symmetric(vertical:10),child:Center(child: CircularProgressIndicator()));
          }
        }
    );

  }

  List<Widget> latestEventList(List<Evento> _eventos){

    List<Widget> listWidget = [];

    if(_eventos.length > 0){

      _eventos.forEach((evento) {
        List<String> desdeData = evento.fechaDesde.split(' ');
        String desde = desdeData[0];
        String desdeHora = desdeData[1];
        Widget _card;
        switch(evento.tipo){
          case "TURNO":
            _card = customCitaCard(
                title         : evento.lugar,
                tag           : evento.especialidad,
                beneficiario  : evento.beneficiario,
                lugar         : (evento.meta['dataProfesional'] != null)?evento.meta['dataProfesional']:"",
                time          : desdeHora,
                date          : formatoFechaTurno(fecha:desde,invertido:false),
                actionLocation: false,
                evtOnTap      : (){
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute( builder: (context) => VerTurno(idTurno: evento.eid.toString()) ),
                  );
                }
            );
            break;
          case "TAREA":
            _card = customTareaCard(
                title         : evento.titulo,
                tag           : evento.tipo,
                beneficiario  : evento.desc,
                lugar         : evento.desc,
                time          : desdeHora,
                date          : formatoFechaTurno(fecha:desde,invertido:false),
                actionLocation: false,
                evtOnTap      : (){
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute( builder: (context) => VerTareaAgenda(idTarea: evento.eid.toString()) ),
                  );
                }
            );
            break;
          case "ESTUDIOS":
            _card = customTareaCard(
                title         : evento.titulo,
                tag           : evento.tipo,
                beneficiario  : evento.beneficiario,
                lugar         : evento.desc,
                time          : desdeHora,
                date          : formatoFechaTurno(fecha:desde,invertido:false),
                actionLocation: false,
                evtOnTap      : (){
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute( builder: (context) => VerTareaAgenda(idTarea: evento.eid.toString()) ),
                  );
                }
            );
            break;
          case "MEDICAMENTO":
            _card = customTareaCard(
                title         : evento.titulo,
                tag           : evento.tipo,
                beneficiario  : evento.beneficiario,
                lugar         : evento.dosis,
                time          : desdeHora,
                date          : formatoFechaTurno(fecha:desde,invertido:false),
                actionLocation: false,
                evtOnTap      : (){
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute( builder: (context) => VerTareaAgenda(idTarea: evento.eid.toString()) ),
                  );
                }
            );
            break;
          case "RECORDATORIO":
            _card = customTareaCard(
                title         : evento.titulo,
                tag           : evento.tipo,
                beneficiario  : evento.desc,
                lugar         : evento.desc,
                time          : desdeHora,
                date          : formatoFechaTurno(fecha:desde,invertido:false),
                actionLocation: false,
                evtOnTap      : (){
                  print('tapeamos el evento: '+evento.eid.toString());
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute( builder: (context) => VerTareaAgenda(idTarea: evento.eid.toString()) ),
                  );
                }
            );
            break;
        }
        listWidget.add(_card);

      });

    }else{
      listWidget.add(Container(
          width: double.infinity,
          padding:EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
          child: Text('No se registran eventos',
            textAlign: TextAlign.center,
            style:TextStyle(fontSize: 18,color: Colors.red,fontWeight: FontWeight.bold)
          )
      ));
    }

    return listWidget;

  }

}
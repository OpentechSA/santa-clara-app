
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:santa_clara/src/components/menuDrawer.dart';
import 'package:santa_clara/src/components/scAppBar.dart';
import 'package:santa_clara/src/models/agendaModels.dart';
import 'package:santa_clara/src/modules/appProvider.dart';
import 'package:santa_clara/src/modules/turnoBloc.dart';
import 'package:santa_clara/src/pages/agenda/turnoFechaHora.dart';
import 'package:santa_clara/src/services/bookingService.dart';
import 'package:santa_clara/utils/uiHelper.dart';
import 'package:santa_clara/utils/userPreferences.dart';

class TurnoConfirmar extends StatefulWidget {

  static TurnoConfirmarState state;

  TurnoConfirmar({Key key,RequestTurno requestTurno}) : super(key: key){
    state = new TurnoConfirmarState(requestTurno:requestTurno);
  }
  @override
  createState() => state;

}

class TurnoConfirmarState extends State<TurnoConfirmar> with UiHelper {

  RequestTurno requestTurno;
  TurnoBloc blocTurno;
  Turnos turnos = new Turnos();
  String email;
  String telefono;
  String valorRecordatorio;
  String valorModalidad;
  TextEditingController emailController;
  TextEditingController telefonoController;
  final UserPreferences userPrefs = new UserPreferences();

  TurnoConfirmarState({this.requestTurno});

  @override
  void initState() {
    super.initState();
    userPrefs.initPrefs();
    valorRecordatorio = null;
    valorModalidad = null;
  }

  @override
  Widget build(BuildContext context) {

    setContext(context);
    blocTurno = AppProvider.getTurnoBloc(context);
    resetTurnoRequestStatus(blocTurno);

    List<Widget> listaFloatingActionButtons = [
      botonCuadrado(
        icon:Icons.arrow_back_ios,
        evtOnPressed: (){ 
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => TurnoFechaHora(requestTurno: requestTurno),
            ),
          );
        }
      ),
      SizedBox(width: 10,),
      iconBoxButton(
        label:'',
        icon:FontAwesomeIcons.solidCalendarCheck,
        iconSize: 30, 
        width: 56, 
        height:56,
        evtOnTap:(){
          requestTurno.setDataModalidad(valorModalidad);
          requestTurno.setDataRecordatorio(valorRecordatorio);
          requestTurno.setUID(userPrefs.id);
          confirmarTurno();
        },
      ),
    ];

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: ScAppBar(masterView: false,),
      drawer: MenuDrawer(),
      body: Stack(
        children:<Widget>[
          backgroundLayout(sinLogo: true),
          _mainContent(context)
        ]
      ),
      floatingActionButton: crearBotoneraRegistro( listButtons: listaFloatingActionButtons ),
    );

  }

  Widget _mainContent(BuildContext ctx){

    String fechaTurno = formatoFechaTurno(fecha:requestTurno.dataFecha);

    return Container(
      margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
      padding: EdgeInsets.all(0.0),
      width: getSize.width-UiHelper.layoutMarginLeft,
      child: Container(
        child:Column(
          children: [

            // header index-agenda
            Container(
              margin:EdgeInsets.only(top:60),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [

                  // botonera-header 
                  Container(
                    height: 70.0,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          const Color(0xFFFFFF0),
                          const Color.fromRGBO(242, 251, 255, 1),
                          const Color.fromRGBO(206, 236, 252, 1),
                        ]
                      ),
                      border:Border(bottom: BorderSide(color:UiHelper.scAzul,width:2.0)) 
                    ),
                    child: Row(children: [
                      Expanded(child:Row(children: [
                        Padding(padding:EdgeInsets.only(left:15.0),child:SizedBox(width:24.0,child:Icon(FontAwesomeIcons.solidCalendarAlt,color:UiHelper.scAzul,size: 18))),
                        Padding(padding:EdgeInsets.only(left:10.0),child:Text('AGENDAR TURNO',style:TextStyle(color:Color.fromRGBO(51, 51, 51, 1),fontSize: 18,fontWeight: FontWeight.bold)))
                      ],)),
                    ],)
                  ),

                ],
              ),
            ),

            // wizard-step-indicator
            headerWizardTurno(step:'4',title:'Confirmación de turno'),

            Expanded(
              child:SingleChildScrollView(
                child:Column(
                  children:[

                    // profesional-especialidad-seleccionada
                    Padding(
                      padding:EdgeInsets.symmetric(vertical:0.0),
                      child: _profesionalSeleccionado()
                    ),

                    // fecha-seleccionada
                    fechaHoraTurno(fecha:fechaTurno,hora:requestTurno.dataHora),
              
                    // dropdown-modalidad
                    Padding(
                      padding:EdgeInsets.symmetric(vertical:10.0),
                      child: _dropdownModalidades(),
                    ),

                    inputWrapper(
                      field:turnoCorreoElectronico(
                        bloc:blocTurno,
                        controller: emailController,
                        evtOnChanged: (value){
                          requestTurno.setEmail(value);
                          blocTurno.emailController.value = value;
                        }
                      ),
                      withBoxShadow: true
                    ),

                    SizedBox(height: 10.0,),

                    inputWrapper(
                      field:turnoNroTelefono(
                        bloc:blocTurno,
                        controller: telefonoController,
                        evtOnChanged: (value){
                          requestTurno.setTelefono(value);
                          blocTurno.nroTelefonoController.value = value;
                        }
                      ),
                      withBoxShadow: true
                    ),

                    SizedBox(height: 10.0,),

                    msgTurnoAlert(bloc: blocTurno),

                    separadorCampos(title:'Recordatorio',icon:FontAwesomeIcons.solidBell),

                    Padding(
                      padding:EdgeInsets.symmetric(vertical:10.0),
                      child: _dropdownRecordatorio(),
                    ),
                    
                    Container(height:10.0, width: double.infinity,color:UiHelper.scSeparadorCeleste),

                    Container(
                      padding:EdgeInsets.symmetric(vertical:10.0, horizontal:15.0),
                      child:Row(
                        children: [

                          Padding(
                            padding:EdgeInsets.only(right:10.0),
                            child: Container(
                              width:24.0, 
                              height:24.0, 
                              decoration: BoxDecoration(
                                color:Colors.yellow,
                                borderRadius:BorderRadius.circular(24),
                              ),
                              child:Icon(FontAwesomeIcons.exclamationTriangle,size: 10,color:UiHelper.scAzul),
                            )
                          ),
                          Expanded(
                            child:Text(
                              'Al confirmar el turno recibirá una notificación por email o mensaje de texto.',
                              style:TextStyle(
                                fontSize: 12.0,
                                color:Colors.red
                              )
                            )
                          ),
                        
                        ],
                      )
                    ),

                    SizedBox(height: 80,),

                  ]
                ),
              ),
            ),
            

          ],
        )
      )
    );
  }

  Widget fechaHoraTurno({String fecha, String hora}){
    return Container(
      margin:EdgeInsets.symmetric(horizontal:15.0,vertical:10.0),
      child: Row(
        children: [
          SizedBox(width:20.0,child:Icon(FontAwesomeIcons.calendarAlt,color:UiHelper.scAzul,size:20)),
          Expanded(
            child: Padding(
              padding:EdgeInsets.symmetric(horizontal:10.0),
              child:Text(
                fecha,
                style:TextStyle(
                  color:UiHelper.scGrisTexto,
                  fontSize:14.0,
                  fontWeight: FontWeight.bold,
                )
              )
            )
          ),
          SizedBox(width:20.0,child:Icon(FontAwesomeIcons.clock,color:UiHelper.scAzul,size:20)),
          Padding(
            padding:EdgeInsets.symmetric(horizontal:10.0),
            child:Text(
              requestTurno.dataHora,
              style:TextStyle(
                color:UiHelper.scGrisTexto,
                fontSize:14.0,
                fontWeight: FontWeight.bold,
              )
            )
          )
          
        ],
      )
    );
  }

  Widget _profesionalSeleccionado(){

    return customProfesionalCard(
      title: requestTurno.dataSucursal,
      tag: requestTurno.dataEspecialidad,
      lugar: requestTurno.dataProfesional,
      btnCall: false,
      btnCheck: false,
      hasPicture: true
    );

  }

  Widget _dropdownRecordatorio(){
    
    return FutureBuilder(
      future: AppProvider().loadOpcionesRecordatorio(),
      builder: (context,AsyncSnapshot<List<DropdownMenuItem<String>>>snapshot) {

        if(snapshot.hasData){
          List<DropdownMenuItem<String>> _items = snapshot.data;
          if(valorRecordatorio == null){
            valorRecordatorio = _items[0].value;
          }
          return inputWrapper(
            field: DropdownButton<String>(
              hint: Text('Opciones de recordatorio'),
              value:valorRecordatorio,
              isExpanded: true,
              icon: SizedBox(width:24.0,child:Icon(FontAwesomeIcons.angleDown,color:UiHelper.scAzul,size: 16,)),
              iconSize: 16,
              elevation: 16,
              style: TextStyle( color: Color.fromRGBO(70, 70, 70, 1) ),
              underline: Container(color:Colors.transparent),
              onChanged:(opt){ 
                print(valorRecordatorio);
                setState(() { valorRecordatorio = opt; });
                print(valorRecordatorio);
              },
              items: _items
            ),
            withBoxShadow: true
          );
        }else{
          return Center(child:CircularProgressIndicator());
        }
      }
    );

  }

  Widget _dropdownModalidades(){

    return FutureBuilder(
      future: AppProvider().loadModalidadesCita(),
      builder: (context,AsyncSnapshot<List<DropdownMenuItem<String>>>snapshot) {

        if(snapshot.hasData){
          List<DropdownMenuItem<String>> _items = snapshot.data;
          if(valorModalidad == null){
            valorModalidad = _items[0].value;
          }
          return inputWrapper(
            field: DropdownButton<String>(
              hint: Text('Selecione la modalidad'),
              value:valorModalidad,
              isExpanded: true,
              icon: SizedBox(width:24.0,child:Icon(FontAwesomeIcons.angleDown,color:UiHelper.scAzul,size: 16,)),
              iconSize: 16,
              elevation: 16,
              style: TextStyle( color: Color.fromRGBO(70, 70, 70, 1) ),
              underline: Container(color:Colors.transparent),
              onChanged:(opt){ 
                setState(() { valorModalidad = opt; });
              },
              items: _items
            ),
            withBoxShadow: true
          );
        }else{
          return Center(child:CircularProgressIndicator());
        }
        
      }
    );

  }

  confirmarTurno() async {

    print(requestTurno.toJson());

    if(validateUserData()){
      if(!blocTurno.requestingStatus){
      
        // activamos el control de request
        blocTurno.requestingController.value = true;
        setBlocRequestStatus(msg:'Confirmando el turno, aguarde....',status:true,type:'loading',bloc:blocTurno);
        
        // hacemos un request para guardar el turno
        TurnoResponse respuesta = await turnos.setTurno(requestTurno);
        
        if(respuesta.status){
          // setState(() {
          //   blocTurno.emailController.value = "";
          //   blocTurno.nroTelefonoController.value = "";
          // });
          // setBlocRequestStatus(msg:respuesta.message,status:respuesta.status,type:'success',bloc:blocTurno);


          showDialog(
            context: context,
            builder: (context) => AlertDialog(
              title: Icon(FontAwesomeIcons.solidCheckCircle, color: Colors.greenAccent, size: 30),
              content: Text(respuesta.message, textAlign: TextAlign.center,),
            )
          );

          Future.delayed(Duration(seconds: 4), () {
            // 5 seconds over, navigate to Page2.
            Navigator.of(context).pop();
            Navigator.of(context).pop();
          });
        }else{
          // algo paso y te mostramos un mensaje
          setBlocRequestStatus(msg:respuesta.message,status:respuesta.status,bloc:blocTurno);
        }
        clearTurnoMsgAlert(blocTurno);
        
      }
    }else{
      setBlocRequestStatus(msg:"Los campos de email y teléfono son obligatorios!!!",bloc:blocTurno);
      clearTurnoMsgAlert(blocTurno);
    }

  }

  void setBlocRequestStatus({String msg,String type = 'info', bool status = false, TurnoBloc bloc}){
    bloc.requestStatusMsgController.value = msg;
    bloc.requestStatusTypeController.value = type;
    bloc.requestStatusController.value = status;
  }

  validateUserData(){
    if(requestTurno.email != null && requestTurno.telefono != null ){
      if(requestTurno.email.length > 0 && requestTurno.telefono.length > 0){
        return true;
      }else{
        return false;
      }
    }else{
      return false;
    }
  }

}
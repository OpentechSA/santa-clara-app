
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:santa_clara/src/components/camposTareas.dart';
import 'package:santa_clara/src/components/menuDrawer.dart';
import 'package:santa_clara/src/components/scAppBar.dart';
import 'package:santa_clara/src/models/agendaModels.dart';
import 'package:santa_clara/src/modules/appProvider.dart';
import 'package:santa_clara/src/modules/turnoBloc.dart';
import 'package:santa_clara/src/pages/agenda/turnoBeneficiario.dart';
import 'package:santa_clara/src/pages/agenda/turnoFechaHora.dart';
import 'package:santa_clara/src/services/bookingService.dart';
import 'package:santa_clara/utils/uiHelper.dart';

class TurnoEspecialidad extends StatefulWidget {

  static TurnoEspecialidadState state;
  TurnoEspecialidad({Key key, RequestTurno requestTurno}) : super(key: key){
    state = new TurnoEspecialidadState(requestTurno:requestTurno);
  }
  @override
  createState() => state;

}

class TurnoEspecialidadState extends State<TurnoEspecialidad> with UiHelper {

  int _profSelected;
  TipoBusqueda _tipoBusqueda;
  Turnos turnos = new Turnos();
  final List<Especialidad> _especialidades = new List<Especialidad>();
  final List<Profesional> _profesionales = new List<Profesional>();
  List<DropdownMenuItem<String>> _listItemsEspecialidades = [];
  List<ItemProfesional> _resultados = [];
  RequestTurno requestTurno;
  TurnoBloc blocTurno;
  TextEditingController buscarController = new TextEditingController();
  int _itemsRespuesta = 0;
  String _valorEspecialidad;

  TurnoEspecialidadState({this.requestTurno});

  @override
  void initState() { 
    super.initState();
    _tipoBusqueda = TipoBusqueda.especialidad;
    clearResultadoProfesionales();
    clearEspecialidades();
    _listItemsEspecialidades.add(DropdownMenuItem<String>(child:Text('Especialidades'),value:''));
  }

  @override
  Widget build(BuildContext context) {

    setContext(context);
    blocTurno = AppProvider.getTurnoBloc(context);
    blocTurno.requestStatusController.value = null;
    List<Widget> listaFloatingActionButtons = [
      botonCuadrado(
        icon:Icons.arrow_back_ios,
        evtOnPressed: (){ 
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => TurnoBeneficiario(),
            ),
          );
        }
      ),
      SizedBox(width: 10,),
      botonCuadrado(
        icon:Icons.arrow_forward_ios,
        evtOnPressed: (){ 
          requestTurno.setProfesional(_profesionales[_profSelected].idProfesional);
          requestTurno.setDataEspecialidad(AppProvider.getEspecialidad(_especialidades, requestTurno.especialidad).descripcion);
          requestTurno.setDataProfesional(_profesionales[_profSelected].descripcion);
          requestTurno.setDataSucursal(_profesionales[_profSelected].sucursal);
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => TurnoFechaHora(requestTurno: requestTurno),
            ),
          );
         }
      ),
    ];

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: ScAppBar(masterView: false,),
      drawer: MenuDrawer(),
      body: Stack(
        children:<Widget>[
          backgroundLayout(sinLogo: true),
          _mainContent(context)
        ]
      ),
      floatingActionButton: crearBotoneraRegistro( listButtons: listaFloatingActionButtons ),
    );

  }

  Widget _mainContent(BuildContext ctx){

    return Container(
      margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
      padding: EdgeInsets.all(0.0),
      width: getSize.width-UiHelper.layoutMarginLeft,
      child: Container(
        child:Column(
          children: [

            // header index-agenda
            Container(
              margin:EdgeInsets.only(top:60),
              decoration:BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    const Color(0xFFFFFFFF),
                    const Color.fromRGBO(230, 248, 255, 1),
                  ],
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [

                  // botonera-header 
                  Container(
                    height: 70.0,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                        const Color.fromRGBO(255, 255, 255, 1),
                        const Color.fromRGBO(206, 236, 252, 1),
                        ]
                      ),
                      border:Border(bottom: BorderSide(color:UiHelper.scAzul,width:2.0)) 
                    ),
                    child: Row(children: [
                      Expanded(child:Row(children: [
                        Padding(padding:EdgeInsets.only(left:15.0),child:SizedBox(width:24.0,child:Icon(FontAwesomeIcons.solidCalendarAlt,color:UiHelper.scAzul,size: 18))),
                        Padding(padding:EdgeInsets.only(left:10.0),child:Text('AGENDAR TURNO',style:TextStyle(color:Color.fromRGBO(51, 51, 51, 1),fontSize: 18,fontWeight: FontWeight.bold)))
                      ],)),
                    ],)
                  ),

                ],
              ),
              
            ),
            
            // wizard-step-indicator
            stepTurno(step:'2',caption:'Especialidad / Profesional'),

            Expanded(
              child:SingleChildScrollView(
                child:Column(
                  children: [
                    
                    // tipo-busqueda
                    _tipoBusquedaRadio(),

                    // buscador
                    _buscador(),

                    // dropdown-especialidad
                    _dropdownEspecialidad(),

                    headerSliderPlanes(title: 'Se encontraron $_itemsRespuesta resultados',withActionButton: false),

                    // lista-resultados
                    _listaResultados(),

                  ],
                )
              )
            ),
            
          ],
        )
      )
    );

  }

  Widget _tipoBusquedaRadio(){
    
    return Row(children: [
      
      Expanded(
        flex: 1,
        child: Row(children: [
          Radio(
            value: TipoBusqueda.especialidad,
            groupValue: _tipoBusqueda,
            onChanged: (TipoBusqueda value) {
              setState(() { 
                _tipoBusqueda = value; 
                // clearEspecialidades();
                // _listItemsEspecialidades.add(DropdownMenuItem<String>(child:Text('Especialidades'),value:''));
              });
            },
          ),
          Text('Buscar especialidad')
        ],),
      ),
      Expanded(
        flex:1,
        child: Row(children: [
          Radio(
            value: TipoBusqueda.profesional,
            groupValue: _tipoBusqueda,
            onChanged: (TipoBusqueda value) {
              setState(() { 
                clearResultadoProfesionales();
                _tipoBusqueda = value; 
              });
            },
          ),
          Text('Buscar profesional')
        ],),
      ),

    ]);

  }

  Widget _buscador(){
    return Row(children: [
      Expanded(
        child: inputWrapper(
          field:campoBuscarProfesional(
            controller: buscarController,
            caption:'Buscar especialidad o profesional',
            evtOnChanged: (value){}
          ),
          paddingRight: 5,
          withBoxShadow: true
        ),
      ),
      Padding(
        child:iconBoxButton(icon:FontAwesomeIcons.search,label: 'Buscar',evtOnTap: (){
          switch(_tipoBusqueda){
            case TipoBusqueda.profesional:
              blocTurno.requestStatusController.value = false;
              buscarProfesionales();
              break;
            case TipoBusqueda.especialidad:
              buscarEspecialidades();
              break;
          }
        }),
        padding:EdgeInsets.only(right:10)
      )
    ]);
  }

  Widget _dropdownEspecialidad(){
    
    Widget rWidget;
    if(_listItemsEspecialidades.length == 0){
      rWidget = Center(child: CircularProgressIndicator());
    }else{
      
      print('>>>> _dropdownEspecialidad - '+_valorEspecialidad);
      print(_listItemsEspecialidades);

      rWidget = Padding(
        padding:EdgeInsets.symmetric(vertical:10.0),
        child: DropdownEspecialidad(
          items:_listItemsEspecialidades,
          evtOnChanged:(value){
            if(value.toString().length > 0){
              _valorEspecialidad = _especialidades[int.parse(value)].idEspecialidad;
              requestTurno.setEspecialidad(_valorEspecialidad);
              requestTurno.setSucursal(AppProvider.getSucursalEspecialidad(_especialidades, _valorEspecialidad).toString());
            }else{
              requestTurno.especialidad = null;
              setState(() {
                _listItemsEspecialidades.clear();
                clearResultadoProfesionales();
              });
            }
          },
          selected: _valorEspecialidad,
        )
      );
      
    }
    return rWidget;

  }

  Widget _listaResultados(){

    Widget rWidget;
    if(_resultados.length == 0){
      if(blocTurno.requestStatusController.value == false){
        rWidget = Center(child: CircularProgressIndicator());
      }else{
        rWidget = ListaProfesionales( items: _resultados );  
      }
    }else{
      rWidget = ListaProfesionales( items: _resultados );
    }
    return rWidget;

  }

  void clearResultadoProfesionales(){
    requestTurno.profesional = null;
    _resultados.clear();
    _profesionales.clear();
    _itemsRespuesta = 0;
    _profSelected = -1;
  }

  void clearEspecialidades(){
    requestTurno.especialidad = null;
    _valorEspecialidad = '';
    _listItemsEspecialidades.clear();
    _especialidades.clear();
  }

  void buscarProfesionales(){

    FocusScope.of(context).requestFocus(new FocusNode());
    RequestProfesional requestProfesional = new RequestProfesional(
      idCliente:requestTurno.idCliente,
      ci: requestTurno.ci,
      sucursal: requestTurno.sucursal,
      especialidad: requestTurno.especialidad,
      buscar:buscarController.text
    );

    setState(() {
      clearResultadoProfesionales();
    });
    blocTurno.requestStatusController.value = false;
    List<ItemProfesional> _arrList = [];

    turnos.getProfesionales(requestProfesional).then((respuesta) {

      if(respuesta.data.length > 0){
        int idx = 0;
        respuesta.data.forEach((item) {
          _profesionales.add(Profesional.fromJson(item));
          _profesionales[idx].setIndex(idx);
          Widget itemFamilia = new ItemProfesional( 
            index:idx,
            title: _profesionales[idx].sucursal,
            tag: AppProvider.getTagEspecialidad(_especialidades, requestTurno.especialidad),
            lugar: _profesionales[idx].descripcion,
            btnCheck:true,
            parentState: this,
            isSelected: (_profesionales[idx].index == this.profSelected),
          );
          _arrList.add(itemFamilia);
          idx++;
        });
        blocTurno.requestStatusController.value = true;
      }else{
        blocTurno.requestStatusController.value = true;
      }

      setState(() {
        _resultados = _arrList;
        _itemsRespuesta = _resultados.length;
      });

    });

  }

  void buscarEspecialidades(){

    FocusScope.of(context).requestFocus(new FocusNode());
    RequestEspecialidad requestEspecialidad = new RequestEspecialidad(
      idCliente:requestTurno.idCliente,
      ci: requestTurno.ci,
      buscar:buscarController.text
    );
    buscarController.text = "";
    setState(() {
      clearEspecialidades();
    });
    
    turnos.getEspecialidad(requestEspecialidad).then((respuesta) {

      setState(() {
        int idx = 0;
        if(respuesta.data.length > 0){
          respuesta.data.forEach((element) { 
            _especialidades.add(Especialidad.fromJson(element));
            _especialidades[idx].setIndex(idx);
            _listItemsEspecialidades.add(new DropdownMenuItem<String>(
                child: Text(_especialidades[idx].descripcion),
                value: _especialidades[idx].index.toString(),
              )
            );
            idx++;
          });  
          buscarController.text = "";
          _valorEspecialidad = _especialidades[0].idEspecialidad;
          requestTurno.setEspecialidad(_valorEspecialidad);
          requestTurno.setSucursal(AppProvider.getSucursalEspecialidad(_especialidades, _valorEspecialidad).toString());
          _tipoBusqueda = TipoBusqueda.profesional;
          print('>>>> _buscarEspecialidades con resultados - '+_valorEspecialidad);
          print(_listItemsEspecialidades);
        }else{
          _valorEspecialidad = '';
          _listItemsEspecialidades.add(DropdownMenuItem<String>(child:Text('No se encontraron especialidades'),value:''));
          buscarController.text = "";
          print('>>>> _buscarEspecialidades sin resultados');
          print(_listItemsEspecialidades);
        }
      });

    });

  }

  void setSelected (int idx) {
    setState(() { _profSelected = idx; });
    print('ahora seleccionamos el item: $_profSelected' );
  }

  int get profSelected => _profSelected;

}

enum TipoBusqueda { profesional, especialidad }
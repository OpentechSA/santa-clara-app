
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_calendar_carousel/classes/event.dart';
import 'package:flutter_calendar_carousel/classes/event_list.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:santa_clara/src/components/menuDrawer.dart';
import 'package:santa_clara/src/components/scAppBar.dart';
import 'package:santa_clara/src/models/agendaModels.dart';
import 'package:santa_clara/src/pages/agenda/verTareaAgenda.dart';
import 'package:santa_clara/src/pages/agenda/verTurno.dart';
import 'package:santa_clara/src/services/bookingService.dart';
import 'package:santa_clara/utils/uiHelper.dart';
import 'package:intl/intl.dart' show DateFormat;
import 'package:santa_clara/utils/userPreferences.dart';

class IndexAgenda extends StatefulWidget {

  IndexAgenda({Key key}) : super(key: key);
  @override
  createState() => _IndexAgendaState();

}

class _IndexAgendaState extends State<IndexAgenda> with UiHelper {

  final UserPreferences userPrefs = new UserPreferences();
  Turnos turnos = new Turnos();
  List<Evento> eventos = [];
  DateTime _currentDate = DateTime.now();
  DateTime _currentDate2 = DateTime(DateTime.now().year,12,31);
  String _currentMonth;
  DateTime _targetDateTime;
  int cYear,cMonth,cDay;
  EventList<Event> _markedDateMap = new EventList<Event>();
  CalendarCarousel _calendarCarousel, _calendarCarouselNoHeader;
  static Widget _eventIcon = new Container(
    decoration: new BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(1000)),
        border: Border.all(color: Colors.blue, width: 2.0)),
    child: new Icon(
      Icons.person,
      color: Colors.amber,
    ),
  );

  void initDates(){

    setState(() {
      _targetDateTime = DateTime.now();
      cYear = DateTime.now().year;
      cMonth = DateTime.now().month;
      cDay = DateTime.now().day;
      _currentMonth = DateFormat('MMMM / yyyy','es').format(_targetDateTime);
    });

  }

  @override
  void initState() { 
    initDates();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    
    setContext(context);
    userPrefs.initPrefs();

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: ScAppBar(),
      drawer: MenuDrawer(),
      body: Stack(
        children:<Widget>[
          backgroundLayout(sinLogo: true),
          _mainContent(context)
        ]
      )
    );
  }

  Widget _mainContent(BuildContext ctx){

    // print('>>> cYear: $cYear');
    // print('>>> cMonth: $cMonth');
    // print('>>> cDay: $cDay');
    // print('>>> _targetDateTime: '+DateTime.now().toString());

    return Container(
      margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
      padding: EdgeInsets.all(0.0),
      width: getSize.width-UiHelper.layoutMarginLeft,
      child: Container(
        child: Column(
          children: [

            // header index-agenda
            Container(
              margin: EdgeInsets.only(top:80),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  // botonera-header
                  Container(
                    height: 75.0,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          const Color(0xFFFFFF0),
                          const Color.fromRGBO(242, 251, 255, 1),
                          const Color.fromRGBO(206, 236, 252, 1),
                        ]
                      ),
                      border:Border(bottom: BorderSide(color:UiHelper.scAzul,width:2.0))
                    ),
                    child: Row(children: [
                      Expanded(child:Row(children: [
                        Padding(padding:EdgeInsets.only(left:15.0),child:SizedBox(width:24.0,child:Icon(FontAwesomeIcons.solidCalendarAlt,color:UiHelper.scAzul,size: 18))),
                        Padding(padding:EdgeInsets.only(left:10.0),child:Text('AGENDAR',style:TextStyle(color:Color.fromRGBO(51, 51, 51, 1),fontSize: 18,fontWeight: FontWeight.bold)))
                      ],)),
                      // boton-turno
                      iconBoxButton(label:'Turno',icon:FontAwesomeIcons.bookMedical,evtOnTap:(){
                        Navigator.pushNamed(context, 'turno-beneficiario');
                      }),
                      // boton-tarea
                      iconBoxButton(label:'Tarea',icon:FontAwesomeIcons.calendarAlt,evtOnTap:(){
                        Navigator.pushReplacementNamed(context,'tarea-agenda');
                      }),
                      SizedBox(width: 10.0,),
                    ],)
                  ),
                ],
              ),
            ),
            // container
            Expanded(
              flex: 1,
              child:_calendarEvents(),
            )
          ],
        )
      )
    );
  
  }

  Widget _separadorCeleste(){
    return Container( 
      height: 10.0,
      color:UiHelper.scSeparadorCeleste
    );
  }

  List<Widget> latestEventList(List<Evento> _eventos){

    List<Widget> listWidget = [];

    _eventos.forEach((evento) {
      List<String> desdeData = evento.fechaDesde.split(' ');
      String desde = desdeData[0];
      String desdeHora = desdeData[1];
      Widget _card;
      switch(evento.tipo){
        case "TURNO":
        _card = customCitaCard(
          title         : evento.lugar,
          tag           : evento.especialidad,
          beneficiario  : evento.beneficiario,
          lugar         : (evento.meta['dataProfesional'] != null)?evento.meta['dataProfesional']:"",
          time          : desdeHora,
          date          : formatoFechaTurno(fecha:desde,invertido:false),
          actionLocation: false,
          evtOnTap      : (){
            print('tapeamos el turno: '+evento.eid.toString());
            Navigator.pushReplacement(
              context,
              MaterialPageRoute( builder: (context) => VerTurno(idTurno: evento.eid.toString()) ),
            );
          }
        );
        break;
        case "TAREA":
        _card = customTareaCard(
          title         : evento.titulo,
          beneficiario  : evento.desc,
          lugar         : evento.desc,
          time          : desdeHora,
          date          : formatoFechaTurno(fecha:desde,invertido:false),
          actionLocation: false,
          evtOnTap      : (){
            print('tapeamos el evento: '+evento.eid.toString());
            Navigator.pushReplacement(
              context,
              MaterialPageRoute( builder: (context) => VerTareaAgenda(idTarea: evento.eid.toString()) ),
            );
          }
        );
        break;
      }
      listWidget.add(_card);
      
    });


    return listWidget;

  }

  Widget _calendarControl(){
    return Container(
      margin: EdgeInsets.only(
        top: 10.0,
        bottom: 10.0,
        left: 10.0,
        right: 10.0,
      ),
      child: new Row(
        children: <Widget>[
          InkWell(
            child: SizedBox(width: 40, height: 30, child: Icon(FontAwesomeIcons.angleLeft,size: 26,color:UiHelper.scAzul),),
            onTap: () {
              setState(() {
                _targetDateTime = DateTime(_targetDateTime.year, _targetDateTime.month -1);
                _currentMonth = DateFormat('MMMM / yyyy','es').format(_targetDateTime);
              });
            },
          ),
          Expanded(
            flex: 1,
            child: Center(child:Text(
              _currentMonth,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
              ),
            )
          )),
          InkWell(
            child: SizedBox(width: 40, height: 30, child: Icon(FontAwesomeIcons.angleRight,size: 26,color:UiHelper.scAzul),),
            onTap: () {
              setState(() {
                _targetDateTime = DateTime(_targetDateTime.year, _targetDateTime.month +1);
                _currentMonth = DateFormat('MMMM / yyyy','es').format(_targetDateTime);
              });
            },
          )
        ],
      ),
    );
  }

  Widget _calendarEvents(){

    return FutureBuilder(
      future: turnos.getEventos(new RequestEventos(uid:userPrefs.id)),
      builder: (context,AsyncSnapshot<EventosResponse> snapshot) {
        if(snapshot.hasData){

          _markedDateMap.clear();
          eventos.clear();

          snapshot.data.data.forEach((key,items) {

            List<dynamic> eventosFecha = items;
            List<Event> _items = [];
            int aaaa = int.parse(key.split('-')[0]);
            int mm = int.parse(key.split('-')[1]);
            int dd = int.parse(key.split('-')[2]);
            DateTime date = new DateTime(aaaa,mm,dd);
            eventosFecha.forEach((element) {
              eventos.add(new Evento.fromJson(element));
              DateTime eventDate = DateTime.fromMillisecondsSinceEpoch(int.parse(element['ts-desde']));
              _items.add(
                new Event(
                  date: eventDate,
                  title: element['id'].toString()+" "+element['titulo'],
                  icon: _eventIcon,
                )
              );
            });
            _markedDateMap.addAll(date, _items);

          });

          initCalendarWidget();

          return SingleChildScrollView(
            child: Container(
              child:Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[

                  //custom icon
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 16.0),
                    child: _calendarCarousel,
                  ), // This trailing comma makes auto-formatting nicer for build methods.
                  //custom icon without header
                  _calendarControl(),
                  Container(
                    //color:UiHelper.greenContainer,
                    margin: EdgeInsets.symmetric(horizontal: 16.0),
                    padding: EdgeInsets.only(top:10.0),
                    child: Center(
                      child:_calendarCarouselNoHeader
                    ),
                  ),
                  _separadorCeleste(),
                  headerSliderPlanes(title:'Actividades y recordatorios',evtOnPressed:(){}),
                  // Column(children: latestEventList(eventos)),

                ],

              ),
            )
          );

        }else{
          return Center(child: CircularProgressIndicator());
        }
      }
    );
    
  }

  void initCalendarWidget(){

    double widgetWidth = getSize.width-UiHelper.layoutMarginLeft-40;
    double dayWidth = widgetWidth/7;
    double widgetHeight = widgetWidth;
    // print("$widgetWidth - $dayWidth");

    _calendarCarouselNoHeader = CalendarCarousel<Event>(
      showOnlyCurrentMonthDate: false,
      weekendTextStyle: TextStyle( color: Colors.greenAccent ),
      markedDatesMap: _markedDateMap,
      height: widgetHeight,
      width: widgetWidth,
      locale: 'es', 
      selectedDateTime: _currentDate,
      targetDateTime: _targetDateTime,
      customGridViewPhysics: NeverScrollableScrollPhysics(),
      markedDateCustomTextStyle: TextStyle( fontSize: 18, color: Colors.blue, ),
      showHeader: false,
      todayTextStyle: TextStyle( color: Colors.white, ),
      todayButtonColor: Colors.transparent,
      todayBorderColor: Colors.transparent,
      selectedDayTextStyle: TextStyle( color: Colors.yellow, ),
      selectedDayButtonColor: Colors.transparent,
      selectedDayBorderColor: Colors.transparent,
      minSelectedDate: _currentDate.subtract(Duration(days: 360)),
      maxSelectedDate: _currentDate.add(Duration(days: 360)),
      prevDaysTextStyle: TextStyle( fontSize: 16, color: Colors.pinkAccent, ),
      inactiveDaysTextStyle: TextStyle( color: Colors.red, fontSize: 16, ),
      onCalendarChanged: (DateTime date) {
        this.setState(() {
          _targetDateTime = date;
          _currentMonth = DateFormat('MMMM / yyyy','es').format(_targetDateTime);
        });
      },
      onDayPressed: (DateTime date, List<Event> events) {
        this.setState(() => _currentDate2 = date);
        events.forEach((event) => print(event.title));
      },
      onDayLongPressed: (DateTime date) { print('long pressed date $date'); },
      weekFormat: false,
      weekDayFormat: WeekdayFormat.narrow,
      customWeekDayBuilder:(
        int pday,
        String label
      ){
        BoxDecoration weekDayBoxDecoration;
        if(pday == 7){
          weekDayBoxDecoration = BoxDecoration(
            color:UiHelper.scAzul,
            border:null
          );
        }else{
          weekDayBoxDecoration = BoxDecoration(
            color:UiHelper.scAzul,
            border: null
          );
        }
        return Container(
          alignment: Alignment.centerLeft,
          margin: EdgeInsets.symmetric(horizontal:0,vertical: 0),
          padding: EdgeInsets.symmetric(vertical:8),
          decoration: weekDayBoxDecoration,
          width: dayWidth-1,
          child: Center(
            child:Text(label.toString(),style:TextStyle(color:Colors.white,fontWeight: FontWeight.bold,fontSize: 12))
          ),
        );
      },
      customDayBuilder: (
        bool isSelectable,
        int index,
        bool isSelectedDay,
        bool isToday,
        bool isPrevMonthDay,
        TextStyle textStyle,
        bool isNextMonthDay,
        bool isThisMonthDay,
        DateTime day
      ) {
          /// If you return null, [CalendarCarousel] will build container for current [day] with default function.
          /// This way you can build custom containers for specific days only, leaving rest as default.

          BoxDecoration dayBoxDecoration;
          Color cBorderColor = UiHelper.scBordeCeleste;
          TextStyle dayTextStyle;
          Widget dayContent;

          if(day.weekday == 7){
            dayBoxDecoration = BoxDecoration(
              border: Border(
                right:BorderSide(color:cBorderColor,width:1),
                left:BorderSide(color:cBorderColor,width:1),
                bottom:BorderSide(color:cBorderColor,width:1),
              )
            );
          }else{
            dayBoxDecoration = BoxDecoration(
              border: Border(
                left:BorderSide(color:cBorderColor,width:1),
                bottom:BorderSide(color:cBorderColor,width:1),
              )
            );
          }
          dayTextStyle = TextStyle(color:UiHelper.scGris);
          if(isNextMonthDay || isPrevMonthDay){ dayTextStyle = TextStyle(color:Color.fromRGBO(200, 200, 200, 1)); }

          dayContent = Text(day.day.toString(),style: dayTextStyle,);
          if(_markedDateMap.getEvents(day).length > 0){
            dayTextStyle = TextStyle(color:UiHelper.scAzul);
            dayContent = Container(
              width: 30,
              height: 30,
              padding: EdgeInsets.all(4),
              decoration:BoxDecoration(
                color:UiHelper.scBordeCeleste,
                border:Border.all(color:UiHelper.scBordeCeleste,width: 1.0),
                borderRadius: BorderRadius.circular(20.0),
              ),
              child:Text(day.day.toString(),style: dayTextStyle,textAlign: TextAlign.center,),
            );
          }
          if(isToday){
            dayTextStyle = TextStyle(color:Colors.white);
            dayContent = Container(
              width: 30,
              height: 30,
              padding: EdgeInsets.all(4),
              decoration:BoxDecoration(
                color:UiHelper.scCheckRojo,
                border:Border.all(color:UiHelper.scCheckRojo,width: 1.0),
                borderRadius: BorderRadius.circular(20.0),
              ),
              child:Text(day.day.toString(),style: dayTextStyle,textAlign: TextAlign.center,),
            );
          }
          
          return Container(
            width: dayWidth,
            height: dayWidth,
            margin: EdgeInsets.symmetric(horizontal:0,vertical: 0),
            decoration: dayBoxDecoration,
            child: Center(
              child:dayContent
            ),
          );
      },
      daysHaveCircularBorder: false,
      dayPadding: 0, 
      firstDayOfWeek: 1,
    );
  
  }

}
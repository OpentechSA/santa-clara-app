
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:santa_clara/src/components/listaGrupoFamiliar.dart';
import 'package:santa_clara/src/components/menuDrawer.dart';
import 'package:santa_clara/src/components/scAppBar.dart';
import 'package:santa_clara/src/models/agendaModels.dart';
import 'package:santa_clara/src/pages/agenda/turnoEspecialidad.dart';
import 'package:santa_clara/src/services/bookingService.dart';
import 'package:santa_clara/utils/uiHelper.dart';
import 'package:santa_clara/utils/userPreferences.dart';

class TurnoBeneficiario extends StatefulWidget {

  static TurnoBeneficiarioState state;

  TurnoBeneficiario({Key key}) : super(key: key);
  @override
  createState() => state = new TurnoBeneficiarioState();

}

class TurnoBeneficiarioState extends State<TurnoBeneficiario> with UiHelper {

  int _userSelected;
  Widget _lista;
  Turnos turnos = new Turnos();
  final List<Adherente> _beneficiarios = new List<Adherente>();
  final UserPreferences userPrefs = new UserPreferences();

  @override
  void initState() { 
    super.initState();
    this._userSelected = -1;
  }

  @override
  Widget build(BuildContext context) {

    setContext(context);
    userPrefs.initPrefs();
    List<Widget> listaFloatingActionButtons = [
      botonCuadrado(
        icon:Icons.arrow_forward_ios,
        evtOnPressed: (){   
          RequestTurno request = new RequestTurno();
          request.setIdCliente(this._beneficiarios[_userSelected].idCliente);
          request.setCi(this._beneficiarios[_userSelected].ci);
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => TurnoEspecialidad(requestTurno: request),
            ),
          ); 
        }
      ),
    ];

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: ScAppBar(masterView: false,),
      drawer: MenuDrawer(),
      body: Stack(
        children:<Widget>[
          backgroundLayout(sinLogo: true),
          _mainContent(context)
        ]
      ),
      floatingActionButton: crearBotoneraRegistro( listButtons: listaFloatingActionButtons ),
    );

  }

  Widget _mainContent(BuildContext ctx){

    return Container(
      margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
      padding: EdgeInsets.all(0.0),
      width: getSize.width-UiHelper.layoutMarginLeft,
      child: Container(
        child:Column(
          children: [

            // header index-agenda
            Container(
              margin:EdgeInsets.only(top:60),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [

                  // botonera-header 
                  Container(
                    height: 70.0,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          const Color(0xFFFFFF0),
                          const Color.fromRGBO(242, 251, 255, 1),
                          const Color.fromRGBO(206, 236, 252, 1),
                        ]
                      ),
                      border:Border(bottom: BorderSide(color:UiHelper.scAzul,width:2.0)) 
                    ),
                    child: Row(children: [
                      Expanded(child:Row(children: [
                        Padding(padding:EdgeInsets.only(left:15.0),child:SizedBox(width:24.0,child:Icon(FontAwesomeIcons.solidCalendarAlt,color:UiHelper.scAzul,size: 18))),
                        Padding(padding:EdgeInsets.only(left:10.0),child:Text('AGENDAR TURNO',style:TextStyle(color:Color.fromRGBO(51, 51, 51, 1),fontSize: 18,fontWeight: FontWeight.bold)))
                      ],)),
                    ],)
                  ),

                ],
              ),
            ),
            
            // wizard-step-indicator
            Container(
              padding: EdgeInsets.symmetric(vertical:10.0),
              child:Row(children: [

                Flexible(
                  flex:2,
                  child:Center(
                    child:Container(
                      height: 30.0,
                      width: 30.0,
                      decoration: BoxDecoration(
                        color:UiHelper.scAzul,
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                      alignment: Alignment.center,
                      margin: EdgeInsets.zero,
                      child:Text(
                        '1',
                        style:TextStyle(fontSize:16,color:Colors.white,fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    )
                  )
                ),

                Flexible(
                  flex:9,
                  child:Container(
                    child:Text(
                      'Seleccionar beneficiario',
                      style:TextStyle(fontSize: 18,fontWeight: FontWeight.bold,color:UiHelper.scGris)
                    )
                  )
                ),

              ],)
            ),

            // lista-horas-eventos-dia
            Flexible(
              fit: FlexFit.loose,
              flex:9,
              child: Container(
                child:_listaBeneficiarios()
              ),
            ),

          ],
        )
      )
    );
  }

  Widget _listaBeneficiarios(){
    
    return FutureBuilder(
      future: turnos.getBeneficiarios('177768'),
      builder: (context,AsyncSnapshot<AdherentesResponse> snapshot) {

        List<ItemGrupoFamiliar> arrItems = new List<ItemGrupoFamiliar>();
        if(snapshot.hasData){
          arrItems.clear();
          _beneficiarios.clear();
          int idx = 0;

          snapshot.data.data.forEach((item) {
            _beneficiarios.add(Adherente.fromJson(item));
            _beneficiarios[idx].setIndex(idx);
            Widget itemFamilia = new ItemGrupoFamiliar(
              key         : UniqueKey(),
              index       : idx,
              title       : item['nombres'],
              data        : "Nro. cliente: "+item['id_cliente'],
              dataID      : "Nro. documento: "+item['ci'],
              parentState :this,
              isSelected  : (_beneficiarios[idx].index == this.userSelected)
            );
            arrItems.add(itemFamilia);
            idx++;
          });
          _lista = new ListaGrupoFamiliar( items: arrItems );
        }else{
          _lista = Center(child: CircularProgressIndicator());
        }

        return _lista;

      },
    );

  }

  void setSelected (int idx) {
    setState(() { 
      _userSelected = idx;
    });
    print('>>>>> item seleccionado: $_userSelected' );
  }

  int get userSelected => _userSelected;

}
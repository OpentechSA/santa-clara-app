import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:santa_clara/src/components/camposTareas.dart';
import 'package:santa_clara/src/components/menuDrawer.dart';
import 'package:santa_clara/src/components/scAppBar.dart';
import 'package:santa_clara/src/models/agendaModels.dart';
import 'package:santa_clara/src/modules/appProvider.dart';
import 'package:santa_clara/src/modules/turnoBloc.dart';
import 'package:santa_clara/src/services/bookingService.dart';
import 'package:santa_clara/utils/uiHelper.dart';
import 'package:santa_clara/utils/userPreferences.dart';

class TareaAgenda extends StatefulWidget {
  static TareaAgendaState state;
  TareaAgenda({Key key}) : super(key: key);
  @override
  createState() => state = new TareaAgendaState();
}

class TareaAgendaState extends State<TareaAgenda> with UiHelper {

  final UserPreferences userPrefs = new UserPreferences();
  final List<Adherente> _beneficiarios = new List<Adherente>();
  TurnoBloc blocTarea;
  Turnos turnos = new Turnos();
  AppProvider provider = new AppProvider();
  RequestTarea requestTarea = new RequestTarea();
  List<String> _daysSelected = [];
  List<DropdownMenuItem<String>> _itemsTipoRecordatorio = [];
  List<DropdownMenuItem<String>> _itemsRecordatorio = [];
  List<DropdownMenuItem<String>> _itemsFrecuencia = [];
  List<DropdownMenuItem<String>> _itemsUsuarios = [];
  List<Tags> arrItems;
  List<Tags> arrItemsDias;
  TagsTareas _lista;
  TagsTareas _dias;
  Widget _usuarios;
  String _tagSelected = "";
  String valorUsuarioMedicamento = "";
  String valorTipoRecordatorio = "";
  String valorRecordatorio = "";
  String valorFrecuencia = "";
  bool frecuenciaLoaded = false;
  bool recordatorioLoaded = false;
  bool tipoRecordatorioLoaded = false;
  bool usuariosLoaded = false;
  bool tagsLoaded;
  bool tagsDiasLoaded;

  @override
  void initState() {
    super.initState();
    valorRecordatorio = null;
    valorTipoRecordatorio = null;
    valorFrecuencia = null;
    valorUsuarioMedicamento = null;
    tagsLoaded = false;
    tagsDiasLoaded = false;
  }

  @override
  Widget build(BuildContext context) {

    setContext(context);
    userPrefs.initPrefs();
    blocTarea = AppProvider.getTurnoBloc(context);
    resetTurnoRequestStatus(blocTarea);

    List<Widget> listaFloatingActionButtons = [
      iconBoxButton(
        label: '',
        icon: FontAwesomeIcons.solidCalendarCheck,
        iconSize: 35,
        width: 60,
        height: 60,
        evtOnTap: () {
          requestTarea.setTitulo(blocTarea.tituloController.value);
          requestTarea.setDescripcion(blocTarea.descripcionController.value);
          requestTarea.setRecordatorioTipo(valorTipoRecordatorio);
          requestTarea.setRecordatorioFrecuencia(valorFrecuencia);
          requestTarea.setRecordatorioPrevio(valorRecordatorio);
          requestTarea.setDosisMedicacion(blocTarea.dosificacionController.value);
          requestTarea.setBeneficiario(valorUsuarioMedicamento);
          requestTarea.setDias(_daysSelected);
          requestTarea.setTipo(_tagSelected);
          requestTarea.setUID(userPrefs.id);
          confirmarTarea();
        },
      ),
    ];

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: ScAppBar(
        masterView: false,
        customAction: (){
          Navigator.of(context).pushReplacementNamed('agenda');
        },
      ),
      drawer: MenuDrawer(),
      body: Stack(children: <Widget>[
        backgroundLayout(sinLogo: true),
        _mainContent(context)
      ]),
      floatingActionButton:
          crearBotoneraRegistro(listButtons: listaFloatingActionButtons),
    );
  }

  Widget _mainContent(BuildContext ctx) {
    blocTarea = AppProvider.getTurnoBloc(ctx);

    return Container(
        margin: EdgeInsets.only(left: UiHelper.layoutMarginLeft),
        padding: EdgeInsets.all(0.0),
        width: getSize.width - UiHelper.layoutMarginLeft,
        child: Container(
            child: Column(
          children: [
            // header index-agenda
            Container(
              margin: EdgeInsets.only(top: 60),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  // botonera-header
                  Container(
                      height: 70.0,
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                const Color(0xFFFFFF0),
                                const Color.fromRGBO(242, 251, 255, 1),
                                const Color.fromRGBO(206, 236, 252, 1),
                              ]),
                          border: Border(
                              bottom: BorderSide(
                                  color: UiHelper.scAzul, width: 2.0))),
                      child: Row(
                        children: [
                          Expanded(
                              child: Row(
                            children: [
                              Padding(
                                  padding: EdgeInsets.only(left: 15.0),
                                  child: SizedBox(
                                      width: 24.0,
                                      child: Icon(
                                          FontAwesomeIcons.solidCalendarAlt,
                                          color: UiHelper.scAzul,
                                          size: 18))),
                              Padding(
                                  padding: EdgeInsets.only(left: 10.0),
                                  child: Text('AGENDA',
                                      style: TextStyle(
                                          color: Color.fromRGBO(51, 51, 51, 1),
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold)))
                            ],
                          )),
                        ],
                      )),
                ],
              ),
            ),

            Expanded(
                child: SingleChildScrollView(
              child: Column(children: [
                separadorCampos(
                    title: 'Datos de la tarea',
                    icon: FontAwesomeIcons.edit,
                    marginTop: 10.0),

                _camposByTag(),

                separadorCampos(
                  title: 'Categorías',
                  icon: FontAwesomeIcons.tag,
                  actionButton: btnLimpiarSeleccion(
                      label: 'LIMPIAR SELECCION',
                      evtOnTap: () {
                        setState(() {
                          _tagSelected = "";
                        });
                      }),
                ),

                _tagsTareas(),

                separadorCampos(
                    title: 'Recordatorio',
                    icon: FontAwesomeIcons.solidBell,
                    marginTop: 5.0),

                SizedBox(height: 10),

                msgTurnoAlert(bloc: blocTarea),

                SizedBox(height: 10),

                // campo-tipo-recordatorio
                _dropdownTipoRecordatorio(),

                _camposTipoRecordatorio(),

                SizedBox(height: 100),
              ]),
            )),
          ],
        )));
  }

  Widget _tagsTareas() {
    return FutureBuilder(
      future: provider.loadTagsTareas(),
      initialData: [],
      builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
        if (snapshot.hasData) {
          tagsLoaded = true;
          arrItems = new List<Tags>();
          snapshot.data.forEach((item) {
            Widget itemTag;
            itemTag = new Tags(
                index: item['tag'],
                label: item['nombre'],
                parentState: this,
                selected: (item['tag'] == this.tagSelected));
            arrItems.add(itemTag);
          });
          _lista = new TagsTareas(
              title: "Seleccione la categoría a la que corresponde la tarea",
              items: arrItems);
          return _lista;
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  Widget _tagsDias() {
    return FutureBuilder(
      future: provider.loadDiasSemana(),
      initialData: [],
      builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
        if (snapshot.hasData) {
          tagsDiasLoaded = true;
          bool isSelected = false;
          arrItemsDias = new List<Tags>();
          snapshot.data.forEach((item) {
            isSelected = (this.daysSelected.indexOf(item['tag']) > -1);
            Widget itemTag;
            itemTag = new Tags(
                index: item['tag'],
                label: item['nombre'],
                parentState: this,
                withIcon: false,
                isMultiple: true,
                selected: isSelected);
            arrItemsDias.add(itemTag);
          });
          _dias = new TagsTareas(
              title: "Seleccione el o los días de la semana",
              items: arrItemsDias);
          return _dias;
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  Widget _camposTipoRecordatorio() {
    Widget rWidget;
    switch (valorTipoRecordatorio) {
      case RequestTarea.TIPO_RECORDATORIO_UNICO:
        rWidget = Column(children: _recordatorioUnico());
        break;
      case RequestTarea.TIPO_RECORDATORIO_INDEFINIDO:
        rWidget = Column(children: _recordatorioIndefinido());
        break;
      case RequestTarea.TIPO_RECORDATORIO_PROGRAMADO:
        rWidget = Column(children: _recordatorioProgramado());
        break;
      default:
        rWidget = Column(children: _recordatorioUnico());
        break;
    }
    return rWidget;
  }

  Widget _camposByTag() {
    Widget rWidget;
    switch (_tagSelected) {
      case 'MEDICAMENTO':
        rWidget = Column(children: _camposTagMedicamento());
        break;
      default:
        rWidget = Column(children: _camposTagTarea());
        break;
    }
    return rWidget;
  }

  List<Widget> _camposTagMedicamento() {
    final List<Widget> _widgetList = [
      SizedBox(height: 10),
      // campo-titulo
      inputWrapper(field: campoTitulo(bloc: blocTarea), withBoxShadow: true),
      SizedBox(height: 10),
      // campo-dosificacion
      inputWrapper(
          field: campoDosificacion(bloc: blocTarea), withBoxShadow: true),
      SizedBox(height: 10),
      _dropdownUsuarios(),
    ];
    return _widgetList;
  }

  List<Widget> _camposTagTarea() {
    final List<Widget> _widgetList = [
      SizedBox(height: 10),
      // campo-titulo
      inputWrapper(field: campoTitulo(bloc: blocTarea), withBoxShadow: true),
      SizedBox(height: 10),
      // campo-descripcion
      inputWrapper(
          field: campoDescripcion(bloc: blocTarea, lines: 3),
          withBoxShadow: true),
    ];
    return _widgetList;
  }

  List<Widget> _recordatorioUnico() {
    final List<Widget> _widgetList = [
      SizedBox(height: 10),
      Row(children: [
        Expanded(
            child: CampoFechaTarea(
          caption: 'Fecha Inicio',
          type: 'start',
          parentState: this,
        ))
      ]),
      SizedBox(
        height: 10,
      ),
      Row(children: [
        Expanded(
            child: CampoHoraTarea(
          caption: 'Hora Inicio',
          type: 'start',
          parentState: this,
        )),
        Expanded(
            child: CampoHoraTarea(
          caption: 'Hora Fin',
          type: 'end',
          parentState: this,
        )),
      ]),
      SizedBox(
        height: 10,
      ),
      Row(children: [
        // dropdown-recordatorio
        Flexible(flex: 5, child: _dropdownRecordatorio()),
        Flexible(
            flex: 5,
            child: SizedBox(
              width: 1,
            ))
      ]),
    ];
    return _widgetList;
  }

  List<Widget> _recordatorioProgramado() {
    final List<Widget> _widgetList = [
      SizedBox(height: 10),
      Row(children: [
        Expanded(
            child: CampoFechaTarea(
          caption: 'Fecha inicio',
          type: 'start',
          parentState: this,
        )),
        Expanded(
            child: CampoFechaTarea(
          caption: 'Fecha fin',
          type: 'end',
          parentState: this,
        ))
      ]),
      SizedBox(
        height: 10,
      ),
      Row(children: [
        Expanded(
            child: CampoHoraTarea(
          caption: 'Hora Inicio',
          type: 'start',
          parentState: this,
        )),
        Expanded(child: _dropdownFrecuencia()),
      ]),
      SizedBox(
        height: 10,
      ),
      Row(children: [
        // dropdown-recordatorio
        Flexible(flex: 5, child: _dropdownRecordatorio()),
        Flexible(
            flex: 5,
            child: SizedBox(
              width: 1,
            ))
      ]),
    ];
    return _widgetList;
  }

  List<Widget> _recordatorioIndefinido() {
    final List<Widget> _widgetList = [
      SizedBox(height: 10),
      Row(children: [
        Expanded(
            child: CampoFechaTarea(
          caption: 'Fecha desde',
          type: 'start',
          parentState: this,
        )),
        Expanded(
            child: CampoHoraTarea(
          caption: 'Hora Inicio',
          type: 'start',
          parentState: this,
        ))
      ]),
      SizedBox(
        height: 10,
      ),
      _tagsDias(),
      SizedBox(
        height: 10,
      ),
      Row(children: [
        // dropdown-recordatorio
        Flexible(flex: 5, child: _dropdownRecordatorio()),
        Flexible(
            flex: 5,
            child: SizedBox(
              width: 1,
            ))
      ]),
    ];
    return _widgetList;
  }

  Widget _dropdownTipoRecordatorio() {
    Widget rWidget;
    if (!tipoRecordatorioLoaded) {
      rWidget = FutureBuilder(
          future: provider.loadOpcionesTipoRecordatorio(),
          builder: (context,
              AsyncSnapshot<List<DropdownMenuItem<String>>> snapshot) {
            if (snapshot.hasData) {
              tipoRecordatorioLoaded = true;
              _itemsTipoRecordatorio.clear();
              _itemsTipoRecordatorio = snapshot.data;
              if (valorTipoRecordatorio == null) {
                valorTipoRecordatorio = _itemsTipoRecordatorio[0].value;
              }
              return inputWrapper(
                  field: DropdownButton<String>(
                      hint: Text('Tipos de recordatorio'),
                      value: valorTipoRecordatorio,
                      isExpanded: true,
                      icon: SizedBox(
                          width: 24.0,
                          child: Icon(
                            FontAwesomeIcons.angleDown,
                            color: UiHelper.scAzul,
                            size: 16,
                          )),
                      iconSize: 16,
                      elevation: 16,
                      style: TextStyle(color: Color.fromRGBO(70, 70, 70, 1)),
                      underline: Container(color: Colors.transparent),
                      onChanged: (opt) {
                        setState(() {
                          valorTipoRecordatorio = opt;
                          requestTarea
                              .setRecordatorioTipo(valorTipoRecordatorio);
                        });
                      },
                      items: _itemsTipoRecordatorio),
                  withBoxShadow: true);
            } else {
              return Center(child: CircularProgressIndicator());
            }
          });
    } else {
      if (valorTipoRecordatorio == null) {
        valorTipoRecordatorio = _itemsTipoRecordatorio[0].value;
      }
      rWidget = inputWrapper(
          field: DropdownButton<String>(
              hint: Text('Tipos de recordatorio'),
              value: valorTipoRecordatorio,
              isExpanded: true,
              icon: SizedBox(
                  width: 24.0,
                  child: Icon(
                    FontAwesomeIcons.angleDown,
                    color: UiHelper.scAzul,
                    size: 16,
                  )),
              iconSize: 16,
              elevation: 16,
              style: TextStyle(color: Color.fromRGBO(70, 70, 70, 1)),
              underline: Container(color: Colors.transparent),
              onChanged: (opt) {
                setState(() {
                  valorTipoRecordatorio = opt;
                  requestTarea.setRecordatorioTipo(valorTipoRecordatorio);
                });
              },
              items: _itemsTipoRecordatorio),
          withBoxShadow: true);
    }

    return rWidget;
  }

  Widget _dropdownUsuarios() {
    Widget rWidget;
    if (!usuariosLoaded) {
      rWidget = FutureBuilder(
        future: turnos.getBeneficiarios(userPrefs.idCliente),
        builder: (context, AsyncSnapshot<AdherentesResponse> snapshot) {
          _itemsUsuarios = [];
          if (snapshot.hasData) {
            usuariosLoaded = true;
            _beneficiarios.clear();
            int idx = 0;

            snapshot.data.data.forEach((item) {
              _beneficiarios.add(Adherente.fromJson(item));
              _beneficiarios[idx].setIndex(idx);
              _itemsUsuarios.add(new DropdownMenuItem(
                  child: Text(_beneficiarios[idx].nombres),
                  value: _beneficiarios[idx].nombres));
              idx++;
            });

            if (valorUsuarioMedicamento == null) {
              valorUsuarioMedicamento = _itemsUsuarios[0].value;
            }

            _usuarios = inputWrapper(
                field: DropdownButton<String>(
                    hint: Text('Beneficiarios'),
                    value: valorUsuarioMedicamento,
                    isExpanded: true,
                    icon: SizedBox(
                        width: 24.0,
                        child: Icon(
                          FontAwesomeIcons.angleDown,
                          color: UiHelper.scAzul,
                          size: 16,
                        )),
                    iconSize: 16,
                    elevation: 16,
                    style: TextStyle(color: Color.fromRGBO(70, 70, 70, 1)),
                    underline: Container(color: Colors.transparent),
                    onChanged: (opt) {
                      setState(() {
                        valorUsuarioMedicamento = opt;
                      });
                      print(valorUsuarioMedicamento);
                    },
                    items: _itemsUsuarios),
                withBoxShadow: true);
          } else {
            _usuarios = Center(child: CircularProgressIndicator());
          }
          return _usuarios;
        },
      );
    } else {
      if (valorUsuarioMedicamento == null) {
        valorUsuarioMedicamento = _itemsUsuarios[0].value;
      }
      rWidget = _usuarios;
    }

    return rWidget;
  }

  Widget _dropdownRecordatorio() {
    Widget rWidget;
    if (!recordatorioLoaded) {
      rWidget = FutureBuilder(
          future: provider.loadOpcionesRecordatorio(),
          builder: (context,
              AsyncSnapshot<List<DropdownMenuItem<String>>> snapshot) {
            if (snapshot.hasData) {
              recordatorioLoaded = true;
              _itemsRecordatorio.clear();
              _itemsRecordatorio = snapshot.data;
              if (valorRecordatorio == null) {
                valorRecordatorio = _itemsRecordatorio[0].value;
              }
              return inputWrapper(
                  field: DropdownButton<String>(
                      hint: Text('Opciones de recordatorio'),
                      value: valorRecordatorio,
                      isExpanded: true,
                      icon: SizedBox(
                          width: 24.0,
                          child: Icon(
                            FontAwesomeIcons.angleDown,
                            color: UiHelper.scAzul,
                            size: 16,
                          )),
                      iconSize: 16,
                      elevation: 16,
                      style: TextStyle(color: Color.fromRGBO(70, 70, 70, 1)),
                      underline: Container(color: Colors.transparent),
                      onChanged: (opt) {
                        setState(() {
                          valorRecordatorio = opt;
                        });
                      },
                      items: _itemsRecordatorio),
                  withBoxShadow: true);
            } else {
              return Center(child: CircularProgressIndicator());
            }
          });
    } else {
      if (valorRecordatorio == null) {
        valorRecordatorio = _itemsRecordatorio[0].value;
      }
      return inputWrapper(
          field: DropdownButton<String>(
              hint: Text('Opciones de recordatorio'),
              value: valorRecordatorio,
              isExpanded: true,
              icon: SizedBox(
                  width: 24.0,
                  child: Icon(
                    FontAwesomeIcons.angleDown,
                    color: UiHelper.scAzul,
                    size: 16,
                  )),
              iconSize: 16,
              elevation: 16,
              style: TextStyle(color: Color.fromRGBO(70, 70, 70, 1)),
              underline: Container(color: Colors.transparent),
              onChanged: (opt) {
                setState(() {
                  valorRecordatorio = opt;
                });
              },
              items: _itemsRecordatorio),
          withBoxShadow: true);
    }

    return rWidget;
  }

  Widget _dropdownFrecuencia() {
    Widget rWidget;
    if (!frecuenciaLoaded) {
      rWidget = FutureBuilder(
          future: provider.loadOpcionesFrecuencia(),
          builder: (context,
              AsyncSnapshot<List<DropdownMenuItem<String>>> snapshot) {
            if (snapshot.hasData) {
              frecuenciaLoaded = true;
              _itemsFrecuencia = snapshot.data;
              if (valorFrecuencia == null) {
                valorFrecuencia = _itemsFrecuencia[0].value;
              }
              return inputWrapper(
                  field: DropdownButton<String>(
                      hint: Text('Opciones de Frecuencia'),
                      value: valorFrecuencia,
                      isExpanded: true,
                      icon: SizedBox(
                          width: 24.0,
                          child: Icon(
                            FontAwesomeIcons.angleDown,
                            color: UiHelper.scAzul,
                            size: 16,
                          )),
                      iconSize: 16,
                      elevation: 16,
                      style: TextStyle(color: Color.fromRGBO(70, 70, 70, 1)),
                      underline: Container(color: Colors.transparent),
                      onChanged: (opt) {
                        setState(() {
                          valorFrecuencia = opt;
                        });
                      },
                      items: _itemsFrecuencia),
                  withBoxShadow: true);
            } else {
              return Center(child: CircularProgressIndicator());
            }
          });
    } else {
      if (valorFrecuencia == null) {
        setState(() {
          valorFrecuencia = _itemsFrecuencia[0].value;
        });
      }
      rWidget = inputWrapper(
          field: DropdownButton<String>(
              hint: Text('Opciones de Frecuencia'),
              value: valorFrecuencia,
              isExpanded: true,
              icon: SizedBox(
                  width: 24.0,
                  child: Icon(
                    FontAwesomeIcons.angleDown,
                    color: UiHelper.scAzul,
                    size: 16,
                  )),
              iconSize: 16,
              elevation: 16,
              style: TextStyle(color: Color.fromRGBO(70, 70, 70, 1)),
              underline: Container(color: Colors.transparent),
              onChanged: (opt) {
                setState(() {
                  valorFrecuencia = opt;
                });
              },
              items: _itemsFrecuencia),
          withBoxShadow: true);
    }
    return rWidget;
  }

  String get tagSelected => _tagSelected;
  List<String> get daysSelected => _daysSelected;

  void setSelectedTag(String tag) {
    setState(() {
      _tagSelected = tag;
    });
    print('>>>>> tag seleccionado: $_tagSelected');
  }

  void setSelectedDay(String tag) {
    setState(() {
      if (_daysSelected.indexOf(tag) < 0) {
        _daysSelected.add(tag);
      } else {
        _daysSelected.remove(tag);
      }
    });
    print('>>>>> tags días seleccionados: $_daysSelected');
  }

  void setRecordatorioHoraInicio(String hora) {
    setState(() {
      requestTarea.setRecordatorioHoraInicio(hora);
    });
  }

  void setRecordatorioHoraFin(String hora) {
    setState(() {
      requestTarea.setRecordatorioHoraFin(hora);
    });
  }

  void setRecordatorioFechaInicio(String fecha) {
    setState(() {
      requestTarea.setRecordatorioFechaInicio(fecha);
    });
  }

  void setRecordatorioFechaFin(String fecha) {
    setState(() {
      requestTarea.setRecordatorioFechaFin(fecha);
    });
  }

  confirmarTarea() async {
    print(requestTarea.toJson());

    if (validateTaskData()) {
      if (!blocTarea.requestingStatus) {
        if (requestTarea.recordatorioValido()) {
          // activamos el control de request
          blocTarea.requestingController.value = true;
          setBlocRequestStatus(
              msg: 'Guardando la tarea, aguarde....',
              status: true,
              type: 'loading',
              bloc: blocTarea);

          // hacemos un request para guardar la tarea
          TareaResponse respuesta = await turnos.setTarea(requestTarea);

          if (respuesta.status) {
            blocTarea.tituloController.value = "";
            blocTarea.descripcionController.value = "";
            setBlocRequestStatus(
                msg: respuesta.message,
                status: respuesta.status,
                type: 'success',
                bloc: blocTarea);
          } else {
            // algo paso y te mostramos un mensaje
            setBlocRequestStatus(
                msg: respuesta.message,
                status: respuesta.status,
                bloc: blocTarea);
          }
          clearTurnoMsgAlert(blocTarea);
        } else {
          setBlocRequestStatus(
              msg: "Campos de recordatorio incompletos!!!", bloc: blocTarea);
          clearTurnoMsgAlert(blocTarea);
        }
      }
    } else {
      setBlocRequestStatus(
          msg: "Complete los campos de la tarea!!!", bloc: blocTarea);
      clearTurnoMsgAlert(blocTarea);
    }
  }

  void setBlocRequestStatus( {String msg, String type = 'info', bool status = false, TurnoBloc bloc} ) {
    bloc.requestStatusMsgController.value = msg;
    bloc.requestStatusTypeController.value = type;
    bloc.requestStatusController.value = status;
  }

  bool validateTaskData() {
    bool valid = false;
    switch (_tagSelected) {
      case 'MEDICAMENTO':
        valid = validateCamposMedicamento();
        break;
      default:
        valid = validateCamposTarea();
        break;
    }
    return valid;
  }

  bool validateCamposMedicamento() {
    bool valid = false;
    if (requestTarea.titulo != null &&
        requestTarea.descripcion != null &&
        requestTarea.dosisMedicacion != null &&
        valorUsuarioMedicamento != null) {
      if (requestTarea.titulo.length > 0 &&
          requestTarea.descripcion.length > 0 &&
          requestTarea.dosisMedicacion.length > 0 &&
          valorUsuarioMedicamento.length > 0) {
        valid = true;
      }
    }
    return valid;
  }

  bool validateCamposTarea() {
    bool valid = false;
    if (requestTarea.titulo != null && requestTarea.descripcion != null) {
      if (requestTarea.titulo.length > 0 &&
          requestTarea.descripcion.length > 0) {
        valid = true;
      }
    }
    return valid;
  }

}

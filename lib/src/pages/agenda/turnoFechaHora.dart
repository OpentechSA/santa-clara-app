
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:santa_clara/src/components/camposTareas.dart';
import 'package:santa_clara/src/components/menuDrawer.dart';
import 'package:santa_clara/src/components/scAppBar.dart';
import 'package:santa_clara/src/models/agendaModels.dart';
import 'package:santa_clara/src/modules/appProvider.dart';
import 'package:santa_clara/src/modules/turnoBloc.dart';
import 'package:santa_clara/src/pages/agenda/turnoConfirmar.dart';
import 'package:santa_clara/src/pages/agenda/turnoEspecialidad.dart';
import 'package:santa_clara/src/services/bookingService.dart';
import 'package:santa_clara/utils/uiHelper.dart';

class TurnoFechaHora extends StatefulWidget {

  static TurnoFechaHoraState state;

  TurnoFechaHora({Key key, RequestTurno requestTurno}) : super(key: key){
    state = new TurnoFechaHoraState(requestTurno:requestTurno);
  }
  @override
  createState() => state;

}

class TurnoFechaHoraState extends State<TurnoFechaHora> with UiHelper {

  String _timeSelected;
  RequestTurno requestTurno;
  Turnos turnos = new Turnos();
  TurnoBloc blocTurno;
  List<DropdownMenuItem<String>> _listItemsFechasTurno = [];
  List<Fecha> _fechas = [];
  List<Hora> _horas = [];
  List<TagHora> arrItems = [];
  //TagsHoras _tagsHoras;

  TurnoFechaHoraState({this.requestTurno});

  @override
  void initState() {
    super.initState();
    this._timeSelected = "";
    requestTurno.fecha = null;
  }

  @override
  Widget build(BuildContext context) {

    setContext(context);
    blocTurno = AppProvider.getTurnoBloc(context);
    blocTurno.requestStatusController.value = false;
    List<Widget> listaFloatingActionButtons = [
      botonCuadrado(
        icon:Icons.arrow_back_ios,
        evtOnPressed: (){ 
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => TurnoEspecialidad(requestTurno: requestTurno),
            ),
          );
        }
      ),
      SizedBox(width: 10,),
      botonCuadrado(
        icon:Icons.arrow_forward_ios,
        evtOnPressed: (){ 
          requestTurno.setIdReserva(this.timeSelected);
          requestTurno.setDataFecha(AppProvider.getTagFecha(_fechas, requestTurno.fecha));
          requestTurno.setDataHora(AppProvider.getTagHora(_horas, this.timeSelected));
          print(requestTurno.toJson());
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => TurnoConfirmar(requestTurno: requestTurno),
            ),
          );
         }
      ),
    ];

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: ScAppBar(),
      drawer: MenuDrawer(),
      body: Stack(
        children:<Widget>[
          backgroundLayout(sinLogo: true),
          _mainContent(context)
        ]
      ),
      floatingActionButton: crearBotoneraRegistro( listButtons: listaFloatingActionButtons ),
    );

  }

  Widget _mainContent(BuildContext ctx){

    return Container(
      margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
      padding: EdgeInsets.all(0.0),
      width: getSize.width-UiHelper.layoutMarginLeft,
      child: Container(
        child:Column(
          children: [

            // header index-agenda
            Container(
              margin:EdgeInsets.only(top:60),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [

                  // botonera-header 
                  Container(
                    height: 70.0,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          const Color(0xFFFFFF0),
                          const Color.fromRGBO(242, 251, 255, 1),
                          const Color.fromRGBO(206, 236, 252, 1),
                        ]
                      ),
                      border:Border(bottom: BorderSide(color:UiHelper.scAzul,width:2.0)) 
                    ),
                    child: Row(children: [
                      Expanded(child:Row(children: [
                        Padding(padding:EdgeInsets.only(left:15.0),child:SizedBox(width:24.0,child:Icon(FontAwesomeIcons.solidCalendarAlt,color:UiHelper.scAzul,size: 18))),
                        Padding(padding:EdgeInsets.only(left:10.0),child:Text('AGENDAR TURNO',style:TextStyle(color:Color.fromRGBO(51, 51, 51, 1),fontSize: 18,fontWeight: FontWeight.bold)))
                      ],)),
                    ],)
                  ),

                ],
              ),
            ),

            // wizard-step-indicator
            headerWizardTurno(step:'3',title:'Seleccionar fecha y hora'),

            Expanded(
              child:SingleChildScrollView(
                child:Column(children: [
                  
                  // profesional-y-especialidad
                  Padding(
                    padding:EdgeInsets.symmetric(vertical:0.0),
                    child: _profesionalSeleccionado()
                  ),

                  // picker-fechas-disponibles
                  _pickerFecha(),

                  // lista-resultados
                  // StreamBuilder(
                  //   stream: blocTurno.requestStatusStream,
                  //   builder: (context,AsyncSnapshot snapshot){
                  //     Widget rWidget;
                  //     if(snapshot.hasData){
                  //       rWidget = _listaResultadosHoras();
                  //       // if(snapshot.data){
                  //       // }else{
                  //       //   rWidget = Container(child:null);
                  //       // }
                  //     }else{
                  //       rWidget = Center(child:CircularProgressIndicator());
                  //     }
                  //     return rWidget;
                  //   },
                  // ),
                  _listaResultadosHoras(),

                  _footerLeyenda(),

                  SizedBox(height:80),

                ],)
              )
            ),
            
          ],
        )
      )
    );

  }

  Widget _listaResultadosHoras(){

    RequestHoras requestHoras = new RequestHoras(
      idCliente:requestTurno.idCliente,
      ci: requestTurno.ci,
      sucursal:requestTurno.sucursal,
      especialidad:requestTurno.especialidad,
      profesional:requestTurno.profesional,
      fecha:requestTurno.fecha
    );

    arrItems.clear();
    Widget rWidget;

    return FutureBuilder(
      future: turnos.getHorasDisponibles(requestHoras),
      builder: (context,AsyncSnapshot<HorasResponse> snapshot) {
        if(snapshot.hasData){
          _fillTagsHoras(snapshot.data.data);
          rWidget = new TagsHoras( title:"Horarios disponibles en "+requestTurno.dataSucursal, items: arrItems );
        }else{
          rWidget = new TagsHoras( title:"Horarios disponibles en "+requestTurno.dataSucursal, items: arrItems );
        }
        return rWidget;
      },
    );

  }

  Widget _footerLeyenda(){
    return Container(
      padding:EdgeInsets.symmetric(vertical:10.0, horizontal:15.0),
      child:Row(
        children: [
          Padding(
            padding:EdgeInsets.only(right:5.0),
            child: Container(
              width:15.0, 
              height:15.0, 
              decoration: BoxDecoration(
                color:UiHelper.scCheckRojo,
                border:Border.all(width:1.0,color:UiHelper.scGrisTexto),
                borderRadius:BorderRadius.circular(15),
              ),
            )
          ),
          Expanded(child:Text('Horario seleccionado',style:TextStyle(fontSize: 12.0))),
          Padding(
            padding:EdgeInsets.only(right:5.0),
            child:Container(
              width:15.0, 
              height:15.0, 
              decoration: BoxDecoration(
                color:UiHelper.scCheckGris,
                border:Border.all(width:1.0,color:UiHelper.scGrisTexto),
                borderRadius:BorderRadius.circular(15),
              ),
            )
          ),
          Expanded(child:Text('Horario disponible',style:TextStyle(fontSize: 12.0))),
        ],
      )
    );
  }

  Widget _dropdownFechasDisponibles(){

    RequestFechas requestFechas = new RequestFechas(
      idCliente:requestTurno.idCliente,
      ci: requestTurno.ci,
      sucursal:requestTurno.sucursal,
      especialidad:requestTurno.especialidad,
      profesional:requestTurno.profesional
    );
    print(requestFechas.toString());
    
    return FutureBuilder(
      future: turnos.getFechasDisponibles(requestFechas),
      builder: (context,AsyncSnapshot<FechasResponse> snapshot) {

        _fechas.clear();
        Widget rWidget;
        if(snapshot.hasData){
          _listItemsFechasTurno.clear();
          _fillFechasDropdown(snapshot.data.data);
          rWidget = Padding(
            padding:EdgeInsets.symmetric(vertical:10.0),
            child: DropdownFechasTurno(
              items:_listItemsFechasTurno,
              evtOnChanged:(value){
                  print('Fecha seleccionada: $value pidamos las horas en esa fecha dude!');
                  requestTurno.fecha = value;
                  setState(() { 
                    blocTurno.requestStatusController.value = null;
                    this._timeSelected = "";
                    buscarHoras(); 
                  });
              },
              selected: (requestTurno.fecha != null)?requestTurno.fecha:'',
            ),
          );
          blocTurno.requestStatusController.value = true;
        }else{
          blocTurno.requestStatusController.value = true;
          rWidget = Center(child: CircularProgressIndicator());
        }
        return rWidget;
        
      }

    );
  }

  Widget _pickerFecha(){
    return Container(
      margin:EdgeInsets.symmetric(horizontal:15.0,vertical:10.0),
      child: Row(
        children: [
          SizedBox(width:18.0,child:Icon(FontAwesomeIcons.calendarAlt,color:UiHelper.scAzul,size:18)),
          Expanded( child: _dropdownFechasDisponibles() ),
          btnLimpiarSeleccion(label:'LIMPIAR SELECCION',evtOnTap:(){
            setState(() {
              this._timeSelected = "";
            });
          }),
        ],
      )
    );
  }

  Widget _profesionalSeleccionado(){

    return customProfesionalCard(
      title: requestTurno.dataSucursal,
      tag: requestTurno.dataEspecialidad,
      lugar: requestTurno.dataProfesional,
      btnCall: false,
      btnCheck: false,
      hasPicture: true
    );

  }

  void _fillFechasDropdown(Map<String,dynamic> items){

    print(items['detalle']);
    int idx=0;
    _fechas.clear();
    items['detalle'].forEach((element) { 
      _fechas.add(new Fecha.fromJson(element));
      _fechas[idx].setIndex(idx);
      print(_fechas[idx].fecha+" - "+_fechas[idx].descDia);
      _listItemsFechasTurno.add(new DropdownMenuItem<String>(
          child: Text(_fechas[idx].descDia),
          value: _fechas[idx].fecha,
        )
      );
      idx++;
    });

  }

  _fillTagsHoras(Map<String,dynamic> data){

    List<TagHora> items = [];
    int idx=0;
    _horas.clear();
    data['detalle'].forEach((element) {
      _horas.add(new Hora.fromJson(element));
      _horas[idx].setIndex(idx);
      Widget itemTag = new TagHora(
        index       : _horas[idx].idRes,
        label       : _horas[idx].descHora,
        withIcon    : false,
        parentState : this,
        selected    : ( _horas[idx].idRes == this.timeSelected )
      );
      items.add(itemTag);
      idx++;
    });
    arrItems = items;

  }

  void buscarHoras(){

    RequestHoras requestHoras = new RequestHoras(
      idCliente:requestTurno.idCliente,
      ci: requestTurno.ci,
      sucursal:requestTurno.sucursal,
      especialidad:requestTurno.especialidad,
      profesional:requestTurno.profesional,
      fecha:requestTurno.fecha
    );

    turnos.getHorasDisponibles(requestHoras).then((respuesta) {
      if(respuesta.status){
        setState(() {
          _fillTagsHoras(respuesta.data);
        });
      }else{
        setState(() { arrItems.clear(); });
      }
      blocTurno.requestStatusController.value = true;
    });

  }

  void setSelected (String idx) {
    setState(() {  _timeSelected = idx; });
    print('ahora seleccionamos el item: $_timeSelected' );
  }

  String get timeSelected => _timeSelected;

}
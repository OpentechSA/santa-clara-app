
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:santa_clara/src/components/menuDrawer.dart';
import 'package:santa_clara/src/components/scAppBar.dart';
import 'package:santa_clara/src/models/agendaModels.dart';
import 'package:santa_clara/src/modules/appProvider.dart';
import 'package:santa_clara/src/modules/turnoBloc.dart';
import 'package:santa_clara/src/services/bookingService.dart';
import 'package:santa_clara/utils/uiHelper.dart';
import 'package:santa_clara/utils/userPreferences.dart';

class VerTurno extends StatefulWidget {

  static VerTurnoState state;

  VerTurno({Key key, String idTurno}) : super(key: key){
    state = new VerTurnoState(idTurno:idTurno);
  }
  @override
  createState() => state;

}

class VerTurnoState extends State<VerTurno> with UiHelper {

  String idTurno;
  RequestTurno requestTurno;
  TurnoBloc blocTurno;
  Turnos turnos = new Turnos();
  AppProvider provider = new AppProvider();
  List<DropdownMenuItem<String>> _itemsRecordatorio = [];
  List<DropdownMenuItem<String>> _itemsModalidad = [];
  String email;
  String telefono;
  String valorRecordatorio;
  String valorModalidad;
  TextEditingController emailController;
  TextEditingController telefonoController;
  final UserPreferences userPrefs = new UserPreferences();
  bool dataLoaded = false;
  bool recordatorioLoaded = false;
  bool modalidadLoaded = false;
  Widget widgetTarea;
  Evento evento;
  EventoResponse eventoResponse;

  VerTurnoState({this.idTurno});

  @override
  void initState() {
    super.initState();
    userPrefs.initPrefs();
    valorRecordatorio = null;
    valorModalidad = null;
  }

  @override
  Widget build(BuildContext context) {

    setContext(context);
    blocTurno = AppProvider.getTurnoBloc(context);
    resetTurnoRequestStatus(blocTurno);

    List<Widget> listaFloatingActionButtons = [
      iconBoxButton(
        label: '',
        icon: FontAwesomeIcons.solidCalendarTimes,
        iconSize: 35,
        width: 60,
        height: 60,
        color: UiHelper.scCheckRojo,
        evtOnTap: () {
          alertaSiNo(
            title: 'Cancelar Turno', 
            txtMessage: 'Está seguro que dese cancelar el turno', 
            ctx: context, 
            yesAction:() {
              Navigator.of(context).pop();
              cancelarTurno();
            }
          );
        }
      )
    ];

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: ScAppBar(
        masterView: false,
        customAction: (){
          Navigator.of(context).pushReplacementNamed('agenda');
        },
      ),
      drawer: MenuDrawer(),
      body: Stack(
        children:<Widget>[
          backgroundLayout(sinLogo: true),
          _mainContent(context)
        ]
      ),
      floatingActionButton: crearBotoneraRegistro( listButtons: listaFloatingActionButtons ),
    );

  }

  Widget _mainContent(BuildContext ctx){

    return Container(
      margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
      padding: EdgeInsets.all(0.0),
      width: getSize.width-UiHelper.layoutMarginLeft,
      child: Container(
        child:Column(
          children: [

            // header index-agenda
            Container(
              margin:EdgeInsets.only(top:60),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [

                  // botonera-header 
                  Container(
                    height: 70.0,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          const Color(0xFFFFFF0),
                          const Color.fromRGBO(242, 251, 255, 1),
                          const Color.fromRGBO(206, 236, 252, 1),
                        ]
                      ),
                      border:Border(bottom: BorderSide(color:UiHelper.scAzul,width:2.0)) 
                    ),
                    child: Row(children: [
                      Expanded(child:Row(children: [
                        Padding(padding:EdgeInsets.only(left:15.0),child:SizedBox(width:24.0,child:Icon(FontAwesomeIcons.solidCalendarAlt,color:UiHelper.scAzul,size: 18))),
                        Padding(padding:EdgeInsets.only(left:10.0),child:Text('AGENDAR TURNO',style:TextStyle(color:Color.fromRGBO(51, 51, 51, 1),fontSize: 18,fontWeight: FontWeight.bold)))
                      ],)),
                    ],)
                  ),

                ],
              ),
            ),

            // wizard-step-indicator
            headerWizardTurno(step:'',title:'Datos del Turno'),

            // body campos-turno
            Expanded(
              child:SingleChildScrollView(
                child: _turno(),
              ),
            ),
            

          ],
        )
      )
    );
  }

  void loadDataEvento(Evento evento){
    
    String email = evento.meta['request']['email'];
    String telefono = evento.meta['request']['telefono'];
    emailController = new TextEditingController.fromValue( new TextEditingValue(
      text: email,
      selection: new TextSelection.collapsed(offset: email.length))
    );
    telefonoController = new TextEditingController.fromValue( new TextEditingValue(
      text: telefono,
      selection: new TextSelection.collapsed(offset:telefono.length))
    );
    requestTurno = new RequestTurno.fromJson(evento.meta['request']);
    requestTurno.setDataProfesional(evento.meta['dataProfesional']);
    requestTurno.setDataEspecialidad(evento.meta['dataEspecialidad']);
    requestTurno.setDataSucursal(evento.meta['dataSucursal']);
    requestTurno.setDataFecha(evento.meta['dataFecha']);
    requestTurno.setDataHora(evento.meta['dataHora']);
    requestTurno.setDataRecordatorio(evento.meta['dataRecordatorio']);
    requestTurno.setDataModalidad(evento.meta['dataModalidad']);
    valorRecordatorio = evento.meta['dataRecordatorio'];
    valorModalidad = evento.meta['dataModalidad'];

  }
  
  Widget camposTurno(){

    String fechaTurno = formatoFechaTurno(fecha:requestTurno.dataFecha);

    return Column(
      children:[

        // profesional-especialidad-seleccionada
        Padding(
          padding:EdgeInsets.symmetric(vertical:0.0),
          child: _profesionalSeleccionado()
        ),

        // fecha-seleccionada
        fechaHoraTurno(fecha:fechaTurno,hora:requestTurno.dataHora),
  
        // dropdown-modalidad
        Padding(
          padding:EdgeInsets.symmetric(vertical:10.0),
          child: _dropdownModalidades(),
        ),

        inputWrapper(
          field:turnoCorreoElectronico(
            bloc:blocTurno,
            controller: emailController,
            evtOnChanged: (value){
              requestTurno.setEmail(value);
              blocTurno.emailController.value = value;
            }
          ),
          withBoxShadow: true
        ),

        SizedBox(height: 10.0,),

        inputWrapper(
          field:turnoNroTelefono(
            bloc:blocTurno,
            controller: telefonoController,
            evtOnChanged: (value){
              requestTurno.setTelefono(value);
              blocTurno.nroTelefonoController.value = value;
            }
          ),
          withBoxShadow: true
        ),

        SizedBox(height: 10.0,),

        msgTurnoAlert(bloc: blocTurno),

        separadorCampos(title:'Recordatorio',icon:FontAwesomeIcons.solidBell),

        Padding(
          padding:EdgeInsets.symmetric(vertical:10.0),
          child: _dropdownRecordatorio(),
        ),
        
        Container(height:10.0, width: double.infinity,color:UiHelper.scSeparadorCeleste),

        Container(
          padding:EdgeInsets.symmetric(vertical:10.0, horizontal:15.0),
          child:Row(
            children: [

              Padding(
                padding:EdgeInsets.only(right:10.0),
                child: Container(
                  width:24.0, 
                  height:24.0, 
                  decoration: BoxDecoration(
                    color:Colors.yellow,
                    borderRadius:BorderRadius.circular(24),
                  ),
                  child:Icon(FontAwesomeIcons.exclamationTriangle,size: 10,color:UiHelper.scAzul),
                )
              ),
              Expanded(
                child:Text(
                  'Al confirmar el turno recibirá una notificación por email o mensaje de texto.',
                  style:TextStyle(
                    fontSize: 12.0,
                    color:Colors.red
                  )
                )
              ),
            
            ],
          )
        ),

        SizedBox(height: 80,),

      ]
    );
  }

  Widget _turno(){

    if (!dataLoaded) {
      widgetTarea = FutureBuilder(
        future: turnos.getEvento(idTurno, userPrefs.id),
        builder: (context, AsyncSnapshot<EventoResponse> snapshot) {
          Widget rWidget;
          if (snapshot.hasData) {
            dataLoaded = true;
            evento = Evento.fromJson(snapshot.data.data);
            loadDataEvento(evento);
            rWidget = camposTurno();
            //rWidget = Center(child: CircularProgressIndicator());
          }else{
            rWidget = Center(child: CircularProgressIndicator());
          }
          return rWidget;
        }
      );
    }else {
      loadDataEvento(evento);
      widgetTarea = camposTurno();
      //widgetTarea = Center(child: CircularProgressIndicator());
    }
    return widgetTarea;

  }
  
  Widget fechaHoraTurno({String fecha, String hora}){
    return Container(
      margin:EdgeInsets.symmetric(horizontal:15.0,vertical:10.0),
      child: Row(
        children: [

          SizedBox(width:20.0,child:Icon(FontAwesomeIcons.calendarAlt,color:UiHelper.scAzul,size:20)),

          Expanded(
            child: Padding(
              padding:EdgeInsets.symmetric(horizontal:10.0),
              child:Text(
                fecha,
                style:TextStyle(
                  color:UiHelper.scGrisTexto,
                  fontSize:14.0,
                  fontWeight: FontWeight.bold,
                )
              )
            )
          ),

          SizedBox(width:20.0,child:Icon(FontAwesomeIcons.clock,color:UiHelper.scAzul,size:20)),

          Padding(
            padding:EdgeInsets.symmetric(horizontal:10.0),
            child:Text(
              requestTurno.dataHora,
              style:TextStyle(
                color:UiHelper.scGrisTexto,
                fontSize:14.0,
                fontWeight: FontWeight.bold,
              )
            )
          )
          
        ],
      )
    );
  }

  Widget _profesionalSeleccionado(){

    return customProfesionalCard(
      title: requestTurno.dataSucursal,
      tag: requestTurno.dataEspecialidad,
      lugar: requestTurno.dataProfesional,
      btnCall: false,
      btnCheck: false,
      hasPicture: true
    );

  }

  Widget _dropdownRecordatorio() {
    Widget rWidget;
    if (!recordatorioLoaded) {
      rWidget = FutureBuilder(
          future: provider.loadOpcionesRecordatorio(),
          builder: (context,
              AsyncSnapshot<List<DropdownMenuItem<String>>> snapshot) {
            if (snapshot.hasData) {
              recordatorioLoaded = true;
              _itemsRecordatorio.clear();
              _itemsRecordatorio = snapshot.data;
              if (valorRecordatorio == null) {
                valorRecordatorio = _itemsRecordatorio[0].value;
              }
              return inputWrapper(
                  field: DropdownButton<String>(
                      hint: Text('Opciones de recordatorio'),
                      value: valorRecordatorio,
                      isExpanded: true,
                      icon: SizedBox(
                          width: 24.0,
                          child: Icon(
                            FontAwesomeIcons.angleDown,
                            color: UiHelper.scAzul,
                            size: 16,
                          )),
                      iconSize: 16,
                      elevation: 16,
                      style: TextStyle(color: Color.fromRGBO(70, 70, 70, 1)),
                      underline: Container(color: Colors.transparent),
                      onChanged: (opt) {
                        setState(() {
                          valorRecordatorio = opt;
                          evento.meta['previo'] = valorRecordatorio;
                        });
                      },
                      items: _itemsRecordatorio),
                  withBoxShadow: true);
            } else {
              return Center(child: CircularProgressIndicator());
            }
          });
    } else {
      if (valorRecordatorio == null) {
        valorRecordatorio = _itemsRecordatorio[0].value;
      }
      rWidget = inputWrapper(
        field: DropdownButton<String>(
          hint: Text('Opciones de recordatorio'),
          value: valorRecordatorio,
          isExpanded: true,
          icon: SizedBox(
              width: 24.0,
              child: Icon(
                FontAwesomeIcons.angleDown,
                color: UiHelper.scAzul,
                size: 16,
              )),
          iconSize: 16,
          elevation: 16,
          style: TextStyle(color: Color.fromRGBO(70, 70, 70, 1)),
          underline: Container(color: Colors.transparent),
          onChanged: (opt) {
            setState(() {
              valorRecordatorio = opt;
              evento.meta['previo'] = valorRecordatorio;
            });
          },
          items: _itemsRecordatorio
        ),
        withBoxShadow: true
      );
    }

    return rWidget;
  }

  Widget _dropdownModalidades(){

    Widget rWidget;
    if (!modalidadLoaded) {

      rWidget = FutureBuilder(
        future: AppProvider().loadModalidadesCita(),
        builder: (context,AsyncSnapshot<List<DropdownMenuItem<String>>>snapshot) {

          if(snapshot.hasData){
            
            _itemsModalidad.clear();
            _itemsModalidad = snapshot.data;
            if(valorModalidad == null){
              valorModalidad = _itemsModalidad[0].value;
            }
            return inputWrapper(
              field: DropdownButton<String>(
                hint: Text('Selecione la modalidad'),
                value:valorModalidad,
                isExpanded: true,
                icon: SizedBox(width:24.0,child:Icon(FontAwesomeIcons.angleDown,color:UiHelper.scAzul,size: 16,)),
                iconSize: 16,
                elevation: 16,
                style: TextStyle( color: Color.fromRGBO(70, 70, 70, 1) ),
                underline: Container(color:Colors.transparent),
                onChanged:(opt){ 
                  setState(() { valorModalidad = opt; });
                },
                items: _itemsModalidad
              ),
              withBoxShadow: true
            );
          }else{
            return Center(child:CircularProgressIndicator());
          }
          
        }
      );

    }else{
      if(valorModalidad == null){
        valorModalidad = _itemsModalidad[0].value;
      }
      rWidget = inputWrapper(
        field: DropdownButton<String>(
          hint: Text('Selecione la modalidad'),
          value:valorModalidad,
          isExpanded: true,
          icon: SizedBox(width:24.0,child:Icon(FontAwesomeIcons.angleDown,color:UiHelper.scAzul,size: 16,)),
          iconSize: 16,
          elevation: 16,
          style: TextStyle( color: Color.fromRGBO(70, 70, 70, 1) ),
          underline: Container(color:Colors.transparent),
          onChanged:(opt){ 
            setState(() { valorModalidad = opt; });
          },
          items: _itemsModalidad
        ),
        withBoxShadow: true
      );
    }

    return rWidget;

  }

  cancelarTurno(){

  }

  confirmarTurno() async {

    print(requestTurno.toJson());

    if(validateUserData()){
      if(!blocTurno.requestingStatus){
      
        // activamos el control de request
        blocTurno.requestingController.value = true;
        setBlocRequestStatus(msg:'Confirmando el turno, aguarde....',status:true,type:'loading',bloc:blocTurno);
        
        // hacemos un request para guardar el turno
        TurnoResponse respuesta = await turnos.setTurno(requestTurno);
        
        if(respuesta.status){
          setState(() {
            blocTurno.emailController.value = "";
            blocTurno.nroTelefonoController.value = "";
          });
          setBlocRequestStatus(msg:respuesta.message,status:respuesta.status,type:'success',bloc:blocTurno);
        }else{
          // algo paso y te mostramos un mensaje
          setBlocRequestStatus(msg:respuesta.message,status:respuesta.status,bloc:blocTurno);
        }
        clearTurnoMsgAlert(blocTurno);
        
      }
    }else{
      setBlocRequestStatus(msg:"Los campos de email y teléfono son obligatorios!!!",bloc:blocTurno);
      clearTurnoMsgAlert(blocTurno);
    }

  }

  void setBlocRequestStatus({String msg,String type = 'info', bool status = false, TurnoBloc bloc}){
    bloc.requestStatusMsgController.value = msg;
    bloc.requestStatusTypeController.value = type;
    bloc.requestStatusController.value = status;
  }

  validateUserData(){
    if(requestTurno.email != null && requestTurno.telefono != null ){
      if(requestTurno.email.length > 0 && requestTurno.telefono.length > 0){
        return true;
      }else{
        return false;
      }
    }else{
      return false;
    }
  }

}
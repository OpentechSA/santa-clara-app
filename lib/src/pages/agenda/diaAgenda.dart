
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:santa_clara/src/components/menuDrawer.dart';
import 'package:santa_clara/src/components/scAppBar.dart';
import 'package:santa_clara/utils/uiHelper.dart';
import 'package:intl/intl.dart';

class DiaAgenda extends StatefulWidget {

  DiaAgenda({Key key}) : super(key: key);
  @override
  createState() => _IndexAgendaState();

}

class _IndexAgendaState extends State<DiaAgenda> with UiHelper {

  DateTime _now;
  DateFormat _formatter;

  @override
  void initState() { 
    super.initState();
    _now = new DateTime.now();
    _formatter = new DateFormat('dd/MM/yyyy');
  }

  @override
  Widget build(BuildContext context) {

    setContext(context);
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: ScAppBar(),
      drawer: MenuDrawer(),
      body: Stack(
        children:<Widget>[
          backgroundLayout(sinLogo: true),
          _mainContent(context)
        ]
      )
    );

  }

  Widget _mainContent(BuildContext ctx){

    return Container(
      margin: EdgeInsets.only(left:45.0),
      padding: EdgeInsets.all(0.0),
      width: getSize.width-45.0,
      child: Container(
        child:Column(
          children: [

            // header index-agenda
            Flexible(
              flex:3,
              fit:FlexFit.tight,
              child: Container(
                decoration:BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      const Color(0xFFFFFFFF),
                      const Color.fromRGBO(230, 248, 255, 1),
                    ],
                  ),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [

                    // botonera-header 
                    Container(
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [
                            const Color.fromRGBO(242, 251, 255, 1),
                            const Color.fromRGBO(206, 236, 252, 1),
                          ]
                        ),
                        border:Border(bottom: BorderSide(color:UiHelper.scAzul,width:2.0)) 
                      ),
                      child: Row(children: [
                        Expanded(child:Row(children: [
                          Padding(padding:EdgeInsets.only(left:15.0),child:SizedBox(width:24.0,child:Icon(FontAwesomeIcons.solidCalendarAlt,color:UiHelper.scAzul,size: 18))),
                          Padding(padding:EdgeInsets.only(left:10.0),child:Text('AGENDA',style:TextStyle(color:Color.fromRGBO(51, 51, 51, 1),fontSize: 18,fontWeight: FontWeight.bold)))
                        ],)),
                        // boton-turno
                        InkWell(
                          onTap: (){}, 
                          child: Container(
                            margin: EdgeInsets.symmetric(horizontal:5.0,vertical:10.0),
                            padding:EdgeInsets.symmetric(horizontal:10.0,vertical:5.0),
                            decoration:BoxDecoration(
                              color:UiHelper.scAzul,
                              borderRadius: BorderRadius.circular(10.0),
                              boxShadow: [ BoxShadow(color: Colors.grey,offset: Offset(1,1),blurRadius: 1.0), ],
                            ),
                            child:Column(children:[
                              Padding(padding:EdgeInsets.symmetric(vertical:5.0),child:Icon(FontAwesomeIcons.bookMedical,color:Colors.white,size: 16,)),
                              Text('Turno',style:TextStyle(color:Colors.white,fontSize: 11))
                            ])
                          ),
                        ),
                        // boton-tarea
                        InkWell(
                          onTap: (){}, 
                          child: Container(
                            margin: EdgeInsets.symmetric(horizontal:5.0,vertical:10.0),
                            padding:EdgeInsets.symmetric(horizontal:10.0,vertical:5.0),
                            decoration:BoxDecoration(
                              color:UiHelper.scAzul,
                              borderRadius: BorderRadius.circular(10.0),
                              boxShadow: [ BoxShadow(color: Colors.grey,offset: Offset(1,1),blurRadius: 1.0), ],
                            ),
                            child:Column(children:[
                              Padding(padding:EdgeInsets.symmetric(vertical:5.0),child:Icon(FontAwesomeIcons.calendarAlt,color:Colors.white,size: 16,)),
                              Text('Tarea',style:TextStyle(color:Colors.white,fontSize: 11))
                            ])
                          )
                        ),
                        SizedBox(width: 10.0,),
                      ],)
                    ),

                  ],
                ),
                
              ),
            ),

            // date-nav
            Container(
              padding: EdgeInsets.symmetric(vertical:5.0),
              child:Row(children: [

                Flexible(
                  flex:4,
                  child:Center(
                    child:IconButton(
                      onPressed:(){},
                      icon:Container(
                        alignment: Alignment.center,
                        margin: EdgeInsets.zero,
                        child:Icon(FontAwesomeIcons.angleLeft,size:24,color:UiHelper.scAzul)
                      )
                    )
                  )
                ),

                Flexible(
                  flex:4,
                  child:Center(
                    child:Container(
                      child:Text(
                        _formatter.format(_now),
                        style:TextStyle(fontSize: 18,fontWeight: FontWeight.bold,color:UiHelper.scGrisTexto)
                      )
                    )
                  )
                ),

                Flexible(
                  flex:4,
                  child:Center(
                    child:IconButton(
                      onPressed:(){},
                      icon:Container(
                        alignment: Alignment.center,
                        margin: EdgeInsets.zero,
                        child:Icon(FontAwesomeIcons.angleRight,size:24,color:UiHelper.scAzul)
                      )
                    )
                  )
                ),

              ],)
            ),

            //separador-celeste
            Container(height: 10.0,color:UiHelper.scSeparadorCeleste),
          
            // lista-horas-eventos-dia
            Flexible(
              fit: FlexFit.tight,
              flex:9,
              child: Container(color:UiHelper.scSeparadorCeleste,child:_listaDiaAgenda()),
            ),

          ],
        )
      )
    );
  }

  Widget _listaDiaAgenda(){
    
    final List<Widget> _arrWidgets = new List<Widget>();
    final BorderSide _bordeCeleste = BorderSide(width:1.0,color:UiHelper.scBordeCeleste);

    for(int i=0;i<24;i++){
      String _hora = (i<10)?"0$i:00":"$i:00";
      _arrWidgets.add(
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Flexible(
              flex:3,
              child:Container(
                decoration: BoxDecoration(
                  color:UiHelper.scAmarilloHora,
                  border:Border( bottom: _bordeCeleste, right: _bordeCeleste)
                ),
                height: 70.0,
                child:Center(
                  child:Text(
                    _hora,
                    style:TextStyle( color: UiHelper.scGris,fontSize: 20,fontWeight: FontWeight.bold )
                  )
                )
              ),
            ),
            Flexible(
              flex:7,
              child:Container(
                height: 70.0,
                decoration: BoxDecoration(
                  color:UiHelper.scHoraGris,
                  border:Border(bottom: _bordeCeleste)
                ),
              )
            )
          ],
        )
      );
    }
    final ListView _lista = new ListView(children: _arrWidgets,);
    return MediaQuery.removePadding(
      context: context,
      removeTop: true,
      child: _lista
    );
  }

}
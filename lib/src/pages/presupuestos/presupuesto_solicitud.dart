import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:santa_clara/src/pages/guiaMedica/info_guia_medica.dart';
import 'package:santa_clara/src/pages/guiaMedica/marker_guiaMedica.dart';
import 'package:santa_clara/src/pages/resultados/resultado_detail.dart';
import 'package:santa_clara/utils/uiHelper.dart';
import 'package:santa_clara/utils/constants.dart';
import 'package:santa_clara/src/modules/appProvider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';

class PresupuestoSolicitudPage extends StatefulWidget {

  PresupuestoSolicitudPage({Key key}) : super(key: key);
  @override
  createState() => PresupuestoSolicitudState();
}


class PresupuestoSolicitudState extends State<PresupuestoSolicitudPage> with UiHelper {

  final _keyForm = new GlobalKey<FormState>();

  String nombre;
  String cedula;
  String email;
  String telefono;
  String mensaje;
  File adjunto;

  bool onLoad = true;
  bool onUpload = false;

  @override
  void initState() {
    userDatasReturn();
    super.initState();
  }

  void userDatasReturn() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      nombre = prefs.getString('nombre-completo') != null ? prefs.getString('nombre-completo') : "";
      cedula = prefs.getString('ci_user') != null ? prefs.getString('ci_user') : "";
      email = prefs.getString('email') != null ? prefs.getString('email') : "";
      telefono = prefs.getString('telefono') != null ? prefs.getString('telefono') : "";

      onLoad = false;
    });
  }

  void documentUpload() async {

    FilePickerResult result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['jpg', 'pdf', 'doc', 'docx', 'png', 'xlsx', 'csv'],
    );

    if(result != null) {
      setState(() => adjunto = File(result.files.single.path));
    }
  }

  Future<void> onPresupuestoSave() async {

    _keyForm.currentState.save();

    SharedPreferences prefs = await SharedPreferences.getInstance();

    FormData formData = FormData.fromMap({
      "nombre": nombre.toString(),
      "email": email.toString(),
      "ci": cedula.toString(),
      "telefono": telefono.toString(),
      "consulta": mensaje.toString(),
      "uid": prefs.getString("uidUser"),
      "adjunto": await MultipartFile.fromFile(adjunto.path)
    });

    final response = await Dio().post(
        Constants.apiUrl+"solicitudes/presupuesto",
        data: formData
    );

    if(response?.statusCode != 200){
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Error"),
            content: Text(response.data.toString()),
            actions: <Widget>[
              FlatButton(
                child: Text("Aceptar"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );

    }else{
      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Santa Clara"),
            content: Text("La solicitud de presupuesto fue recepciada de forma exitosa."),
            actions: <Widget>[
              FlatButton(
                child: Text("Aceptar"),
                onPressed: () async {
                  Navigator.pushNamedAndRemoveUntil(context, 'presupuestos', (Route<dynamic> route) => false);
                },
              ),
            ],
          );
        },
      );

      _keyForm.currentState.reset();
    }
    setState(() => onUpload = false);
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext context) {

    setContext(context);

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(70.0),
          child: Container(
              padding:EdgeInsets.only(top:10.0,bottom:10.0,left: 45.0),
              child: AppBar(
                  elevation: 0.0,
                  titleSpacing: 0.0,
                  automaticallyImplyLeading: true,
                  title: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                              child:Text(
                                "Volver",
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color:UiHelper.scAzulOscuro,
                                  fontSize: 14.0,
                                ),
                              )
                          ),
                          Padding(padding:EdgeInsets.only(right:10.0),child: SizedBox(width: 100.0,child:Image(
                            image: new AssetImage('assets/images/logo-santa-clara.png'),
                            width: (this.getSize.width * 0.10),
                            fit: BoxFit.contain,
                          ))),
                        ],
                      )
                  )
              )
          )
      ),
      body: Stack(
        children:<Widget>[
          backgroundLayout(sinLogo: true),
          _mainContent(context),
        ]
      ),
      floatingActionButton: !onLoad ? InkWell(
        onTap: () async {
          if (!onUpload) {
            if(_keyForm.currentState.validate() && adjunto != null) {
              setState(() => onUpload = true);
              await onPresupuestoSave();
            } else {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Text("Error"),
                    content: Text("Todos los campos son requeridos."),
                    actions: <Widget>[
                      FlatButton(
                        child: Text("Aceptar"),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  );
                },
              );
            }
          }
        },
        child: Container(
            decoration: BoxDecoration(
                color: scAzul,
                borderRadius: BorderRadius.circular(10)
            ),
            child: new Padding(
                padding: EdgeInsets.all(10),
                child: !onUpload ? new Image.asset(
                  "assets/visaciones/save.png",
                  width: 35,
                ) : CircularProgressIndicator()
            )
        ),
      ) : new Padding(padding: EdgeInsets.zero),
    );

  }

  String _radioValue = 'profesionales'; //Initial definition of radio button value
  String choice;

  static Color scAzul = Color.fromRGBO(21, 61, 138, 1);


  Widget _mainContent(BuildContext ctx){

    final blocMain = AppProvider.getMainBloc(ctx);

    return Container(
      margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
      padding: EdgeInsets.all(0.0),
      width: getSize.width-45.0,
      //decoration: BoxDecoration( color:UiHelper.greenContainer, ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[

          // campo-busqueda
          SafeArea(
            child: Padding(
              padding:EdgeInsets.only(bottom:0),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Container(
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage("assets/images/background.png"),
                            fit: BoxFit.fill
                        )
                    ),
                    height: 50,
                    padding: EdgeInsets.only(right: 12, left: 12, bottom: 5),
                    child: new Row(
                      children: <Widget>[
                        new Image.asset("assets/presupuesto/presupuesto.png", width: 30,),
                        new Padding(padding: EdgeInsets.all(5)),
                        new Text("SOLICITUD DE PRESUPUESTO",
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                              color: Colors.black
                          ),
                        )
                      ],
                    )
                  ),
                  new Padding(
                    padding: EdgeInsets.only(right: 12, left: 12, top: 10),
                    child: new Row(
                      children: [
                        new Image.asset("assets/presupuesto/person.png",
                          width: 30,
                        ),
                        new Text(" Datos de la solicitud",
                            style: TextStyle(
                              fontSize: 20,
                              color: scAzul,
                              fontWeight: FontWeight.w600
                            )
                        )
                      ],
                    )
                  ),
                ],
              ),
            ),
          ),

          new Flexible(
            flex: 20,
            child: SingleChildScrollView(
              child: !onLoad ? new Form(
                key: _keyForm,
                child: new Column(
                  children: [
                    inputWrapper(
                        field: StatefulBuilder(
                            builder: (BuildContext context, setState) {
                              return Container(
                                width: MediaQuery.of(context).size.width,
                                child: TextFormField(
                                  onChanged: (value) {
                                    setState(() {
                                      nombre = value.toString();
                                    });
                                  },
                                  onSaved: (value) {
                                    setState(() {
                                      nombre = value.toString();
                                    });
                                  },
                                  validator: (value) {
                                    if(value.toString().length < 1) {
                                      return "Es requerido";
                                    }else{
                                      return null;
                                    }
                                  },
                                  initialValue: nombre.toString(),
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                    labelText: 'Nombre y Apellido',
                                    labelStyle: TextStyle(
                                        fontSize: 12
                                    ),
                                    border: InputBorder.none,
                                  ),
                                ),
                              );
                            }
                        ),
                        withBoxShadow: false
                    ),
                    inputWrapper(
                        field: StatefulBuilder(
                            builder: (BuildContext context, setState) {
                              return Container(
                                width: MediaQuery.of(context).size.width,
                                child: TextFormField(
                                  onChanged: (value) {
                                    setState(() {
                                      cedula = value.toString();
                                    });
                                  },
                                  onSaved: (value) {
                                    setState(() {
                                      cedula = value.toString();
                                    });
                                  },
                                  validator: (value) {
                                    if(value.toString().length < 1) {
                                      return "Es requerido";
                                    }else{
                                      return null;
                                    }
                                  },
                                  initialValue: cedula.toString(),
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                    labelText: 'Nro de cédula',
                                    labelStyle: TextStyle(
                                        fontSize: 12
                                    ),
                                    border: InputBorder.none,
                                  ),
                                ),
                              );
                            }
                        ),
                        withBoxShadow: false
                    ),
                    inputWrapper(
                        field: StatefulBuilder(
                            builder: (BuildContext context, setState) {
                              return Container(
                                width: MediaQuery.of(context).size.width,
                                child: TextFormField(
                                  onChanged: (value) {
                                    setState(() {
                                      email = value.toString();
                                    });
                                  },
                                  onSaved: (value) {
                                    setState(() {
                                      email = value.toString();
                                    });
                                  },
                                  validator: (value) {
                                    if(value.toString().length < 1) {
                                      return "Es requerido";
                                    }else{
                                      return null;
                                    }
                                  },
                                  initialValue: email.toString(),
                                  keyboardType: TextInputType.emailAddress,
                                  decoration: InputDecoration(
                                    labelText: 'Correo Electrónico',
                                    labelStyle: TextStyle(
                                        fontSize: 12
                                    ),
                                    border: InputBorder.none,
                                  ),
                                ),
                              );
                            }
                        ),
                        withBoxShadow: false
                    ),
                    inputWrapper(
                        field: StatefulBuilder(
                            builder: (BuildContext context, setState) {
                              return Container(
                                width: MediaQuery.of(context).size.width,
                                child: TextFormField(
                                  onChanged: (value) {
                                    setState(() {
                                      telefono = value.toString();
                                    });
                                  },
                                  onSaved: (value) {
                                    setState(() {
                                      telefono = value.toString();
                                    });
                                  },
                                  validator: (value) {
                                    if(value.toString().length < 1) {
                                      return "Es requerido";
                                    }else{
                                      return null;
                                    }
                                  },
                                  initialValue: telefono.toString(),
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                    labelText: 'Celular o Telefono',
                                    labelStyle: TextStyle(
                                        fontSize: 12
                                    ),
                                    border: InputBorder.none,
                                  ),
                                ),
                              );
                            }
                        ),
                        withBoxShadow: false
                    ),
                    inputWrapper(
                        field: StatefulBuilder(
                            builder: (BuildContext context, setState) {
                              return Container(
                                width: MediaQuery.of(context).size.width,
                                child: TextFormField(
                                  onChanged: (value) {
                                    setState(() {
                                      mensaje = value.toString();
                                    });
                                  },
                                  onSaved: (value) {
                                    setState(() {
                                      mensaje = value.toString();
                                    });
                                  },
                                  validator: (value) {
                                    if(value.toString().length < 1) {
                                      return "Es requerido";
                                    }else{
                                      return null;
                                    }
                                  },
                                  keyboardType: TextInputType.text,
                                  maxLines: 6,
                                  decoration: InputDecoration(
                                    labelText: 'Texto de la consulta',
                                    labelStyle: TextStyle(
                                        fontSize: 12
                                    ),
                                    border: InputBorder.none,
                                  ),
                                ),
                              );
                            }
                        ),
                        withBoxShadow: false
                    ),
                    new Padding(
                        padding: EdgeInsets.only(top: 20, right: 12, left: 12, bottom: 20),
                        child: new Row(
                          children: [
                            new Image.asset("assets/presupuesto/file.png",
                              width: 30,
                            ),
                            new Text("  Adjuntos",
                                style: TextStyle(
                                    fontSize: 20,
                                    color: scAzul,
                                    fontWeight: FontWeight.w700
                                )
                            )
                          ],
                        )
                    ),
                    inputWrapper(
                        field: StatefulBuilder(
                            builder: (BuildContext context, setState) {
                              return GestureDetector(
                                  onTap: () => documentUpload(),
                                  child: Container(
                                    padding: EdgeInsets.only(top: 15, bottom:15),
                                    width: MediaQuery.of(context).size.width,
                                    child: Text(adjunto != null ? adjunto.path.toString() : "Subir un archivo \nAdmitidos: JPG, PDF, DOC, DOCX, PNG, XLSX, CSV",
                                        style:TextStyle(
                                            fontSize: 15
                                        )
                                    ),
                                  )
                              );
                            }
                        ),
                        withBoxShadow: false
                    ),
                  ],
                )
              )  : Padding(padding: EdgeInsets.all(10), child: Center(child: CircularProgressIndicator()))
            ),
          )
        ],
      )
    );
  }

}
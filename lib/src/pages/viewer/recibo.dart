import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:santa_clara/src/components/menuDrawer.dart';
import 'package:santa_clara/src/components/scAppBar.dart';
import 'package:santa_clara/src/models/genericModels.dart';
import 'package:santa_clara/src/services/userService.dart';
import 'package:santa_clara/utils/uiHelper.dart';
import 'package:santa_clara/utils/userPreferences.dart';
import 'package:webview_flutter/webview_flutter.dart';

class Recibo extends StatefulWidget {
  
  static ReciboState _state;
  Recibo({Key key,@required ReciboCliente recibo}) : super(key: key){
    _state = new ReciboState(reciboCliente:recibo);
  }
  @override
  ReciboState createState() => _state;

}

class ReciboState extends State<Recibo> with UiHelper{
  
  static UserProfile user = new UserProfile();
  final UserPreferences userPrefs = new UserPreferences();
  final Completer<WebViewController> _controller = Completer<WebViewController>();
  ReciboCliente reciboCliente;
  String urlRecibo;


  ReciboState({this.reciboCliente}){
    urlRecibo = "https://www.santaclara.com.py/sitio/factura/facturas.php?metodo=visualizar_recibo&cod_recibo="+reciboCliente.idPagoPrepaga;
  }

  @override
  void initState() { 
    super.initState(); 
    userPrefs.initPrefs();
  }

  @override
  Widget build(BuildContext context) {

    setContext(context);

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: ScAppBar(masterView: false,),
      drawer: MenuDrawer(),
      body: Stack(
        children:<Widget>[
          backgroundLayout(sinLogo: true),
          _mainContent(context)
        ]
      )
    );

  }

  Widget _mainContent(BuildContext ctx){

    return Container(
      margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
      padding: EdgeInsets.all(0.0),
      width: getSize.width-UiHelper.layoutMarginLeft,
      child: Container(
        child:Column(
          children: [

            // header visor-recibo
            Container(
                width: UiHelper.size.width-UiHelper.layoutMarginLeft,
                padding: EdgeInsets.only(top:50),
                decoration:BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      const Color(0xFFFFFFFF),
                      const Color.fromRGBO(230, 248, 255, 1),
                    ],
                  ),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [

                    // data-recibo
                    headerRecibo(),
                    // botonera-acciones
                    botoneraAcciones(),

                  ],
                )
              ),

            // content visor-factura
            Expanded(
              child:WebView(
                initialUrl: urlRecibo,
                javascriptMode: JavascriptMode.unrestricted,
                onWebViewCreated: (WebViewController webViewController) {
                  _controller.complete(webViewController);
                },
                // ignore: prefer_collection_literals
                javascriptChannels: <JavascriptChannel>[
                  _toasterJavascriptChannel(context),
                ].toSet(),
                navigationDelegate: (NavigationRequest request) {
                  if (request.url.startsWith('https://www.youtube.com/')) {
                    print('blocking navigation to $request}');
                    return NavigationDecision.prevent;
                  }
                  print('allowing navigation to $request');
                  return NavigationDecision.navigate;
                },
                onPageStarted: (String url) {
                  print('Page started loading: $url');
                },
                onPageFinished: (String url) {
                  print('Page finished loading: $url');
                },
                gestureNavigationEnabled: true,
              ),
            ),

          ],
        )
      )
    );

  }

  Widget botoneraAcciones(){

    return Container(
      decoration: BoxDecoration( border: Border(bottom:BorderSide(width: 2.0,color:UiHelper.scAzul)), ),
      padding:EdgeInsets.symmetric(horizontal:15.0,vertical: 10.0),
      child:Row(
        children: [
          Container(child:tagVisor(label:'RECIBO')),
          Spacer(flex: 4),
          btnAccionVisor(label:'Descargar',icon:FontAwesomeIcons.solidSave,evtOnTap:(){
            print('iniciamos el proceso de descarga');
          }),
        ],
      )
    );

  }

  Widget headerRecibo(){

    return Column(
      children: [
        
        Container(
          width: double.infinity,
          margin: EdgeInsets.only(top: 30),
          padding: EdgeInsets.symmetric(horizontal:15.0),
          child:Text(
            'Comprobante Nro: '+reciboCliente.nroRecibo,
            style:TextStyle(
              color:UiHelper.scGris,
              fontSize: 16.0,
              fontWeight: FontWeight.bold,
            ),
            textAlign: TextAlign.left,
            overflow:TextOverflow.ellipsis,
            maxLines: 1,
          )
        ),
        
        Container(
          width: double.infinity,
          margin: EdgeInsets.only(top: 10),
          padding: EdgeInsets.symmetric(horizontal:15.0),
          child:Text(
            'Fecha: '+reciboCliente.fechaPago+' - Monto: '+reciboCliente.total.toString(),
            style:TextStyle(
              color:UiHelper.scGrisTexto,
              fontSize: 14.0,
            ),
            textAlign: TextAlign.left,
            overflow:TextOverflow.ellipsis,
            maxLines: 1,
          )
        ),
      
      ],
    );
  }

  JavascriptChannel _toasterJavascriptChannel(BuildContext context) {
    return JavascriptChannel(
        name: 'Toaster',
        onMessageReceived: (JavascriptMessage message) {
          Scaffold.of(context).showSnackBar(
            SnackBar(content: Text(message.message)),
          );
        });
  }

}



import 'dart:io';

import 'package:advance_pdf_viewer/advance_pdf_viewer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:santa_clara/src/components/menuDrawer.dart';
import 'package:santa_clara/src/components/scAppBar.dart';
import 'package:santa_clara/src/models/genericModels.dart';
import 'package:santa_clara/src/modules/appProvider.dart';
import 'package:santa_clara/src/pages/viewer/recibo.dart';
import 'package:santa_clara/src/services/userService.dart';
import 'package:santa_clara/utils/uiHelper.dart';
import 'package:santa_clara/utils/userPreferences.dart';

class Factura extends StatefulWidget {
  
  static FacturaState _state;
  Factura({Key key,@required FacturaCliente factura}) : super(key: key){
    _state = new FacturaState(facturaCliente:factura);
  }
  @override
  FacturaState createState() => _state;

}

class FacturaState extends State<Factura> with UiHelper{
  
  static UserProfile user = new UserProfile();
  final UserPreferences userPrefs = new UserPreferences();
  bool _isLoading = true;
  PDFDocument document;
  File factura;
  FacturaCliente facturaCliente;
  String urlFactura;

  FacturaState({this.facturaCliente}){
    urlFactura = "https://www.santaclara.com.py/sitio/factura/facturas_old.php?metodo=visualizar_factura&xTg="+facturaCliente.idFactura;
  }

  @override
  void initState() { 
    super.initState(); 
    userPrefs.initPrefs();
    loadDocument();
  }

  loadDocument() async {
    factura = await AppProvider().createFileOfPdfUrl(pUri:urlFactura);
    document =  await PDFDocument.fromFile(factura);
    setState(() => _isLoading = false );
  }

  @override
  Widget build(BuildContext context) {

    setContext(context);

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: ScAppBar(masterView: false,),
      drawer: MenuDrawer(),
      body: Stack(
        children:<Widget>[
          backgroundLayout(sinLogo: true),
          _mainContent(context)
        ]
      )
    );

  }

  Widget _mainContent(BuildContext ctx){

    return Container(
      margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
      padding: EdgeInsets.all(0.0),
      width: getSize.width-UiHelper.layoutMarginLeft,
      child: Container(
        child:Column(
          children: [

            // header visor-factura
            Container(
                width: UiHelper.size.width-UiHelper.layoutMarginLeft,
                padding: EdgeInsets.only(top:50),
                decoration:BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      const Color(0xFFFFFFFF),
                      const Color.fromRGBO(230, 248, 255, 1),
                    ],
                  ),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [

                    // data-factura
                    headerFactura(),
                    // botonera-acciones
                    botoneraAcciones(),

                  ],
                )
              ),

            // content visor-factura
            Expanded(
              child:Center(
                child: _isLoading
                ? Center(child: CircularProgressIndicator())
                : PDFViewer(
                    document: document,
                    zoomSteps: 1,
                    showPicker: true,
                  ),
              ),
            ),

          ],
        )
      )
    );

  }

  Widget botoneraAcciones(){

    return Container(
      decoration: BoxDecoration(
        border: Border(bottom:BorderSide(width: 2.0,color:UiHelper.scAzul)),
      ),
      padding:EdgeInsets.symmetric(horizontal:15.0,vertical: 10.0),
      child:Row(
        children: [
          Container(child:tagVisor(label:'FACTURA')),
          Spacer(flex: 3),
          (facturaCliente.recibos.length > 0) ? btnAccionVisor(label:'Ver recibo',icon:FontAwesomeIcons.eye,evtOnTap :(){ 
            openDocument(facturaCliente);
          }):null,
          SizedBox(width: 10,),
          btnAccionVisor(label:'Descargar',icon:FontAwesomeIcons.solidSave,evtOnTap:(){
            savePdf();
          }),
        ],
      )
    );

  }

  Widget headerFactura(){

    return Column(
      children: [
        
        Container(
          width: double.infinity,
          margin: EdgeInsets.only(top: 30),
          padding: EdgeInsets.symmetric(horizontal:15.0),
          child:Text(
            'Comprobante Nro: '+facturaCliente.nroFactura,
            style:TextStyle(
              color:UiHelper.scGris,
              fontSize: 16.0,
              fontWeight: FontWeight.bold,
            ),
            textAlign: TextAlign.left,
            overflow:TextOverflow.ellipsis,
            maxLines: 1,
          )
        ),
        
        Container(
          width: double.infinity,
          margin: EdgeInsets.only(top: 10),
          padding: EdgeInsets.symmetric(horizontal:15.0),
          child:Text(
            'Fecha: '+facturaCliente.fecha+' - Saldo: '+facturaCliente.saldo.toString(),
            style:TextStyle(
              color:UiHelper.scGrisTexto,
              fontSize: 14.0,
            ),
            textAlign: TextAlign.left,
            overflow:TextOverflow.ellipsis,
            maxLines: 1,
          )
        ),
      
      ],
    );
  }

  savePdf()async{
    
    print('full path '+factura.path);

  }

  openDocument(FacturaCliente factura){
    ReciboCliente pRecibo = new ReciboCliente.fromJson(factura.recibos[0]);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => Recibo(recibo: pRecibo),
      ),
    );
  }

}



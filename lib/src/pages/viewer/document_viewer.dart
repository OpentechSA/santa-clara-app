import 'dart:convert';

import 'package:advance_pdf_viewer/advance_pdf_viewer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:santa_clara/src/pages/guiaMedica/info_guia_medica.dart';
import 'package:santa_clara/src/pages/guiaMedica/marker_guiaMedica.dart';
import 'package:santa_clara/src/pages/presupuestos/presupuesto_solicitud.dart';
import 'package:santa_clara/src/pages/presupuestos/presupuesto_view.dart';
import 'package:santa_clara/src/pages/resultados/resultado_detail.dart';
import 'package:santa_clara/utils/uiHelper.dart';
import 'package:santa_clara/utils/constants.dart';
import 'package:santa_clara/src/modules/appProvider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';

class DocumentViewer extends StatefulWidget {

  final documentType;
  final document;

  DocumentViewer({
    Key key,
    @required this.documentType,
    @required this.document
  })
      : super(key: key);

  @override
  createState() => DocumentViewerState();
}

class DocumentViewerState extends State<DocumentViewer> with UiHelper {

  List presupuestosList = List();

  bool presupuestosListBool = false;

  static Color scAzul = Color.fromRGBO(21, 61, 138, 1);

  PDFDocument documentPdf;
  bool viewLoad = false;

  Future<String> onViewer() async {

    if(widget.documentType == "pdf") {
      dynamic fromUrl = await PDFDocument.fromURL(widget.document);

      setState(() {
        documentPdf = fromUrl;
        viewLoad = true;
      });
    } else {
      setState(() {
        viewLoad = true;
      });
    }
  }

  @override
  void initState() {
    onViewer();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget build(BuildContext context) {

    setContext(context);

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(70.0),
          child: Container(
              padding:EdgeInsets.only(top:10.0,bottom:10.0,left: 45.0),
              child: AppBar(
                  elevation: 0.0,
                  titleSpacing: 0.0,
                  automaticallyImplyLeading: true,
                  title: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                              child:Text(
                                "Volver",
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color:UiHelper.scAzulOscuro,
                                  fontSize: 14.0,
                                ),
                              )
                          ),
                          Padding(padding:EdgeInsets.only(right:10.0),child: SizedBox(width: 100.0,child:Image(
                            image: new AssetImage('assets/images/logo-santa-clara.png'),
                            width: (this.getSize.width * 0.10),
                            fit: BoxFit.contain,
                          ))),
                        ],
                      )
                  )
              )
          )
      ),
      body: Stack(
          children:<Widget>[
            backgroundLayout(sinLogo: true),
            _mainContent(context),
          ]
      ),
    );
  }

  Widget _mainContent(BuildContext ctx){

    final blocMain = AppProvider.getMainBloc(ctx);

    return Container(
        margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
        padding: EdgeInsets.all(0.0),
        width: getSize.width-45.0,
        //decoration: BoxDecoration( color:UiHelper.greenContainer, ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[

            // campo-busqueda
            SafeArea(
              child: Padding(
                padding:EdgeInsets.only(bottom:0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Container(
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage("assets/images/background.png"),
                                fit: BoxFit.fill
                            )
                        ),
                        height: 50,
                        padding: EdgeInsets.only(right: 12, left: 12, bottom: 5),
                        child: new Row(
                          children: <Widget>[
                            new Image.asset("assets/visaciones/file.png", width: 30,),
                            new Padding(padding: EdgeInsets.all(5)),
                            new Text("ADJUNTOS",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.black
                              ),
                            )
                          ],
                        )
                    ),
                  ],
                ),
              ),
            ),

            new Flexible(
              flex: 20,
              child: viewLoad ? new Padding(
                padding: EdgeInsets.all(5),
                child: widget.documentType == "pdf" ? PDFViewer(
                  document: documentPdf,
                ) : widget.documentType == "png" ? new Padding(
                    padding: EdgeInsets.all(5),
                    child: Image.network(
                      widget.document,
                      fit: BoxFit.fill,
                    )
                ) : new Center(
                    child: new Text("No se puede ver el documento solicitado")
                )
              ) : new Center(
                child: CircularProgressIndicator()
              ),
            )
          ],

        )
    );
  }

}
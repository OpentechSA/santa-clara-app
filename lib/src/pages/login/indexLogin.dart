import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:santa_clara/src/models/loginModels.dart';
import 'package:santa_clara/src/modules/appProvider.dart';
import 'package:santa_clara/src/services/authenticationService.dart';
import 'package:santa_clara/utils/uiHelper.dart';
import 'package:santa_clara/utils/userPreferences.dart';

class IndexLogin extends StatefulWidget {
  
  IndexLogin({Key key}) : super(key: key);
  IndexLoginState _state = new IndexLoginState();
  @override
  IndexLoginState createState() => _state;
  
}

class IndexLoginState extends State<IndexLogin> with UiHelper{

  static Auth auth = new Auth();
  static AuthBloc blocLogin;
  final UserPreferences userPrefs = new UserPreferences();
  static bool _showPass;

  @override
  void initState() { 
    super.initState();
    _showPass = true;
  }

  Widget build(BuildContext context) {

    setContext(context);
    blocLogin = AppProvider.getAuthBloc(context);
    userPrefs.initPrefs();
    resetAuthRequestStatus(blocLogin);

    List<Widget> listaFloatingActionButtons = [
      StreamBuilder(
        stream: blocLogin.validLoginForm,
        builder: (context,AsyncSnapshot snapshot){
          return botonCuadrado(
            icon:Icons.arrow_forward_ios,
            evtOnPressed: (snapshot.hasData)? (){ login(context); }: null
          );
        },
      ),
    ];

    return Scaffold(
      body:Stack(
        children:<Widget>[
          backgroundLayout(sinLogo: true),
          _loginForm(context)
        ]
      ),
      floatingActionButton: crearBotoneraRegistro(listButtons:listaFloatingActionButtons ),
    );

  }

  Widget _loginForm(BuildContext ctx){

    double maxTextButtonSize = getSize.width-130;

    return Center(
      
      child:SingleChildScrollView(
          
        child: Column(
          
          children: <Widget>[

            Container(
              margin: EdgeInsets.only(left:50.0),
              width: getSize.width-50.0,
              child: Column(
                children: <Widget>[

                  Container(alignment:Alignment.centerRight, padding: EdgeInsets.only(bottom:50,right:20),child:logoSantaClara()),
                  crearTituloGrande(caption:'Ingresar'),
                  SizedBox(height: 15.0,),
                  inputWrapper(field:campoCorreoElectronico(bloc:blocLogin),withBoxShadow: true),
                  SizedBox(height: 10.0,),
                  inputWrapper(field:campoPassword(bloc: blocLogin,showPass:_showPass,evtOnPressed: (){
                    setState(() {
                      _showPass = (!_showPass)?true:false;
                    });
                  }),withBoxShadow: true),
                  SizedBox(height: 10.0,),
                  msgAlert(bloc: blocLogin),
                  SizedBox(height: 15.0,),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal:15.0),
                    child: Column(children: [
                      crearBotonTexto(
                        caption:'No tengo una cuenta',
                        icon:FontAwesomeIcons.userEdit,
                        evtOnTap: (){ Navigator.pushNamed(ctx, 'registro'); }
                      ),
                      crearBotonTexto(
                        caption:'Olvide mi clave',
                        icon:FontAwesomeIcons.unlock,
                        maxSize:maxTextButtonSize,
                        evtOnTap: (){ Navigator.pushNamed(ctx, 'recuperar-clave'); }
                      ),
                      crearBotonTexto(
                        caption:'Condiciones de uso y términos legales',
                        icon:FontAwesomeIcons.fileContract,
                        evtOnTap: (){ Navigator.pushNamed(ctx, 'terminos-legales'); }
                      )
                    ],),
                  ),
                  SizedBox(height: 50.0,),
                  
                ],
              ),
            )

          ],
        ),
      ),
    );

  }

  login(BuildContext context) async{

    if(blocLogin.validLoginForm != null){

      String email    = blocLogin.email;
      String password = blocLogin.password;

      // String email    = "k4rkuxx@gmail.com";
      // String password = "123456";
      print("$email - $password");
      
      if(!blocLogin.requestingStatus){
        
        // activamos el control de request
        blocLogin.requestStatusTypeController.value = 'loading';
        blocLogin.requestingController.value = true;
        blocLogin.requestStatusController.value = true;
        blocLogin.requestStatusMsgController.value = "Verificando, aguarde ...";
        
        // login request
        LoginResponse respuesta = await auth.signIn(email,password);
        
        if(respuesta.status){
          // todo ok nos vamos al dashboard
          blocLogin.emailController.value="";
          blocLogin.passwordController.value="";
          Map<String,dynamic> userData = respuesta.data['data'];
          userPrefs.setPrefData(userData);

          Navigator.pushReplacementNamed(context, 'dashboard');
        }else{
          // algo paso y te mostramos un mensaje
          blocLogin.requestStatusTypeController.value = 'info';
          blocLogin.requestStatusMsgController.value = respuesta.message;
          blocLogin.requestStatusController.value = respuesta.status;
          clearAuthMsgAlert(blocLogin);
        }
        
      }
    }else{
      blocLogin.requestStatusTypeController.value = 'info';
      blocLogin.requestStatusMsgController.value = "Ingresar datos de usuario!";
      blocLogin.requestStatusController.value = false;
    }

  }
}
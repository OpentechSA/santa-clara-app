import 'package:flutter/material.dart';
import 'package:santa_clara/src/models/registrationModels.dart';
import 'package:santa_clara/src/modules/appProvider.dart';
import 'package:santa_clara/src/services/authenticationService.dart';
import 'package:santa_clara/utils/uiHelper.dart';


class RecuperarClave extends StatelessWidget with UiHelper {
  
  RecuperarClave({Key key}) : super(key: key);
  static Auth auth = new Auth();
  static AuthBloc blocRecuperar;

  Widget build(BuildContext context) {

    setContext(context);
    blocRecuperar = AppProvider.getAuthBloc(context);
    blocRecuperar.requestStatusController.value = null;

    List<Widget> listaFloatingActionButtons = [
      botonCuadrado(
        icon:Icons.arrow_back_ios,
        evtOnPressed: (){ 
          resetAuthRequestStatus(blocRecuperar);
          Navigator.pop(context);
        }
      ),
      SizedBox(width:5.0),
      StreamBuilder(
        stream: blocRecuperar.emailRecoveryStream,
        builder: (context,AsyncSnapshot snapshot){
          Function _evtOnPressed;
          _evtOnPressed = null;
          if(snapshot.hasData){
            if(!snapshot.hasError){
               _evtOnPressed = (){ recuperar(context); };
            }
          }
          return botonCuadrado(
            icon:Icons.arrow_forward_ios,
            evtOnPressed: _evtOnPressed
          );
        }
      )
    ];

    return Scaffold(
      body:Stack(
        children:<Widget>[
          backgroundLayout(sinLogo: true),
          _recuperarForm(context)
        ]
      ),
      floatingActionButton: crearBotoneraRegistro(listButtons:listaFloatingActionButtons ),
    );
  }

  Widget _recuperarForm(BuildContext ctx){

    final blocRecuperar = AppProvider.getAuthBloc(ctx);
    return Center(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[

            Container(
              margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
              width: getSize.width-UiHelper.layoutMarginLeft,
              child: Column(
                children: <Widget>[

                  Container(alignment:Alignment.centerRight, padding: EdgeInsets.only(bottom:50,right:20),child:logoSantaClara()),
                  crearTituloGrande(caption:'Recuperar acceso',textSize: 30),
                  SizedBox(height: 10.0,),
                  crearTexto(text:'Enviaremos un mensaje a tu dirección de correo para resetear la clave de acceso.',textSize: 14.0),
                  SizedBox(height: 15.0,),
                  inputWrapper(field:campoCorreoElectronicoRecovery(bloc:blocRecuperar),withBoxShadow: true),
                  SizedBox(height: 10.0,),
                  msgAlert(bloc: blocRecuperar),
                  
                ],
              ),
            )

          ],
        ),
      )
      
    );
    
  }

  recuperar(BuildContext context) async {

    if(blocRecuperar.emailRecoveryStream != null){

      String email = blocRecuperar.emailRecovery;

      print("$email");
      RecoveryRequest request = new RecoveryRequest(
        email : email
      );

      if(!blocRecuperar.requestingStatus){
        
        // activamos el control de request
        blocRecuperar.requestStatusTypeController.value = 'loading';
        blocRecuperar.requestingController.value = true;
        blocRecuperar.requestStatusController.value = true;
        blocRecuperar.requestStatusMsgController.value = "Enviado datos ...";
        
        // recuperar request
        RecoveryResponse respuesta = await auth.recoveryUp(request);

        // procesamos la respuesta del request
        if(respuesta.status){
          NotificationRecovery notiRecovery = respuesta.data;
          blocRecuperar.requestStatusTypeController.value = 'success';
          blocRecuperar.requestStatusMsgController.value = notiRecovery.message;
          blocRecuperar.requestStatusController.value = respuesta.status;
          
        }else{
          blocRecuperar.requestStatusTypeController.value = 'info';
          blocRecuperar.requestStatusMsgController.value = respuesta.message;
          blocRecuperar.requestStatusController.value = respuesta.status;
        }
        clearAuthMsgAlert(blocRecuperar);

      }

    }else{

      blocRecuperar.requestStatusMsgController.value = "Ingresar datos de usuario!";
      blocRecuperar.requestStatusController.value = false;

    }

  }

}
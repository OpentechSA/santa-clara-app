import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:santa_clara/src/pages/resultados/resultado_detail.dart';
import 'package:santa_clara/utils/uiHelper.dart';
import 'package:santa_clara/utils/constants.dart';
import 'package:santa_clara/src/modules/appProvider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class Resultados extends StatefulWidget {

  Resultados({Key key}) : super(key: key);
  @override
  createState() => ResultadosState();
}


class ResultadosState extends State<Resultados> with UiHelper {

  List resultadosList = List();

  List resultadosListFilter = List();

  bool resultadosListBool = false;

  String inputValue = "";

  Future<String> onListarResultados() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();

    var queryParameters = {
      'ci': prefs.getString("ci_user"),
    };

    final response = await http.post(
        Constants.apiUrl+'analisis/listar',
        body : queryParameters
    );

    if(response?.statusCode != 200){
      var statusCode = response?.statusCode.toString();
      print(response.body);

      setState(() {
        resultadosListBool = true;
      });
    }else{
      var decodeResp = json.decode(response.body);

      setState(() {
        resultadosList = decodeResp['data']['resultados'];
        resultadosListFilter = resultadosList;
        resultadosListBool = true;
      });

      print("Resultados: "+resultadosListFilter.toString());
    }
    return "Listo";
  }

  @override
  void initState() {
    onListarResultados();
    super.initState();
  }

  Widget build(BuildContext context) {

    setContext(context);

    return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(70.0),
          child: Container(
            padding:EdgeInsets.only(top:10.0,bottom:10.0,left: 45.0),
            child: AppBar(
              elevation: 0.0,
              titleSpacing: 0.0,
              automaticallyImplyLeading: true,
              title: Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                        child:Text(
                          "Volver",
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            color:UiHelper.scAzulOscuro,
                            fontSize: 16.0,
                          ),
                        )
                    ),
                    Padding(padding:EdgeInsets.only(right:10.0),child: SizedBox(width: 100.0,child:Image(
                      image: new AssetImage('assets/images/logo-santa-clara.png'),
                      width: (this.getSize.width * 0.10),
                      fit: BoxFit.contain,
                    ))),
                  ],
                )
              )
            )
          )
        ),
        body: Stack(
            children:<Widget>[
              backgroundLayout(sinLogo: true),
              _mainContent(context),
            ]
        )
    );

  }

  Future<String> filterItems(String value) async {

    if(value.length != 0) {
      List tempList = new List();
      for (int i = 0; i < resultadosList.length; i++) {
        if (resultadosList[i]['codigo'].toString().toLowerCase().contains(value.toLowerCase())) {
          tempList.add(resultadosList[i]);
        }
      }
      setState(() {
        resultadosListBool = true;
        resultadosListFilter = tempList;
      });
    } else {
      setState(() {
        resultadosListBool = true;
        resultadosListFilter = resultadosList;
      });
    }
  }

  static Color scAzul = Color.fromRGBO(21, 61, 138, 1);


  Widget _mainContent(BuildContext ctx){

    final blocMain = AppProvider.getMainBloc(ctx);

    return Container(
        margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
        padding: EdgeInsets.all(0.0),
        width: getSize.width-45.0,
        //decoration: BoxDecoration( color:UiHelper.greenContainer, ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[

            // campo-busqueda
            SafeArea(child: Padding(
              padding:EdgeInsets.only(bottom:15.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Padding(
                    padding: EdgeInsets.only(right: 20, left: 20, bottom: 10),
                    child: new Text("Resultados de Análisis",
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w700
                      ),
                    ),
                  ),
                  inputWrapper(
                      field: StatefulBuilder(
                          builder: (BuildContext context, setState) {
                            return TextField(
                              decoration: InputDecoration(
                                suffix: SizedBox(
                                  child: IconButton(
                                    icon: Icon(FontAwesomeIcons.search,
                                        size: 18.0, color: scAzul),
                                    onPressed: () {
                                      setState(() {
                                        resultadosListBool = false;
                                      });
                                      filterItems(inputValue);
                                    },
                                  ),
                                  width: 30.0,
                                  height: 20.0,
                                ),
                                labelText: 'Filtrar por código',
                                border: InputBorder.none,),
                              onChanged: (value) {
                                setState(() {
                                  inputValue = value;
                                  resultadosListBool = false;
                                });
                                filterItems(value);
                              },
                            );
                          }
                      ),
                      withBoxShadow: true
                  )
                ],
              ),
            ),),

            new Padding(
              padding: EdgeInsets.only(right: 20, left: 20),
              child: new Text("Se encontraron (${resultadosListFilter.length.toString()}) Resultados",
                style: TextStyle(
                    fontWeight: FontWeight.w700
                ),
              ),
            ),

            new Flexible(
              flex: 10,
              child: resultadosListBool == true ? new ListView.builder(
                padding: EdgeInsets.zero,
                shrinkWrap: true,
                itemCount: resultadosListFilter.length,
                itemBuilder: (ctx, index) {
                  return GestureDetector(
                    onTap: () async {
                      Navigator.push(context, new MaterialPageRoute(builder: (context){
                        return ResultadosDetail(
                          archivos: json.encode(resultadosListFilter[index]['archivos']),
                          fecha: resultadosListFilter[index]['fecha_recibido'].toString()
                        );
                      }));
                    },
                    child: new Container(
                      color: !index.isOdd ? Color(0xffF9F9F9) : Colors.white,
                      child: new Column(
                        children: <Widget>[
                          new Container(
                              padding: EdgeInsets.all(10),
                              child: new Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  new Row(
                                    children: <Widget>[
                                      new Image.asset("assets/images/file_upload.png", width: 40,),
                                      new Container(
                                        padding: EdgeInsets.all(10),
                                        child: new Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: <Widget>[
                                            new Text(resultadosListFilter[index]['codigo'].toString(),
                                              style: TextStyle(
                                                  color: Colors.grey
                                              ),
                                            ),
                                            new Text(resultadosListFilter[index]['fecha_recibido'].toString(),
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.w800
                                              ),
                                            ),
                                            new Row(
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: <Widget>[
                                                Image.asset("assets/images/file_count.png", width: 30,),
                                                new Text(" ${resultadosListFilter[index]['archivos'].length} archivo${ resultadosListFilter[index]['archivos'].length > 1 ? "s" : "" }",
                                                  style: TextStyle(
                                                      color: Colors.blueAccent
                                                  ),
                                                )
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                  new Container(
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                            width: 1.0
                                        ),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5.0) //                 <--- border radius here
                                        ),
                                      ),
                                      padding: EdgeInsets.all(2),
                                      child: new Text("Analisis")
                                  ),
                                ],
                              )
                          ),
                          new Container(
                            color: Colors.grey,
                            width: MediaQuery.of(context).size.width * 0.85,
                            height: 0.5,
                          )
                        ],
                      )
                    )
                  );
                }
              ) : new Center(child:new CircularProgressIndicator()),
            )
          ],

        )
    );
  }

}
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:santa_clara/utils/uiHelper.dart';
import 'package:santa_clara/utils/constants.dart';
import 'package:santa_clara/src/modules/appProvider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:advance_pdf_viewer/advance_pdf_viewer.dart';

class ResultadosDetail extends StatefulWidget {

  final archivos;
  final fecha;

  ResultadosDetail({Key key, this.archivos, this. fecha}) : super(key: key);
  @override
  createState() => ResultadosState();
}

class ResultadosState extends State<ResultadosDetail> with UiHelper {

  List pdfList = List();

  bool pdfListBool = false;

  var archivoString;

  PDFDocument documentPdf;

  Future<void> onListarResultados() async {

    List tempList = List();

    json.decode(widget.archivos).forEach((element) async {
      final responsePdf = await http.post(
          Constants.apiUrl+'analisis/archivo',
          body : {
            'archivo': element,
          }
      );

      var responseVar = json.decode(responsePdf.body);

      setState(() {
        tempList.add({
          "name": element,
          "url": responseVar['data']['url'].toString()
        });
      });
    });

    setState(() {
      pdfList = tempList;
    });

    return "Listo";
  }

  Future<String> onPdfViewer() async {

    String urlPdfDownload;

    pdfList.forEach((element) {
      if (element['url'].toString().toLowerCase().contains(archivoString.toLowerCase())) {
        setState(() {
          urlPdfDownload = element['url'].toString();
        });
      }
    });

    dynamic fromUrl = await PDFDocument.fromURL(urlPdfDownload);

    setState(() {
      documentPdf = fromUrl;
      pdfListBool = true;
    });
  }

  @override
  void initState() {
    onListarResultados();
    super.initState();
  }

  Widget build(BuildContext context) {

    setContext(context);

    return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(70.0),
            child: Container(
                padding:EdgeInsets.only(top:10.0,bottom:10.0,left: 45.0),
                child: AppBar(
                    elevation: 0.0,
                    titleSpacing: 0.0,
                    automaticallyImplyLeading: true,
                    title: Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                                child:Text(
                                  "Volver",
                                  style: TextStyle(
                                    color:UiHelper.scAzulOscuro,
                                    fontSize: 14.0,
                                  ),
                                )
                            ),
                            Padding(padding:EdgeInsets.only(right:10.0),child: SizedBox(width: 100.0,child:Image(
                              image: new AssetImage('assets/images/logo-santa-clara.png'),
                              width: (this.getSize.width * 0.10),
                              fit: BoxFit.contain,
                            ))),
                          ],
                        )
                    )
                )
            )
        ),
        body: Stack(
          children:<Widget>[
            backgroundLayout(sinLogo: true),
            _mainContent(context),
          ]
        )
    );

  }


  static Color scAzul = Color.fromRGBO(21, 61, 138, 1);


  Widget _mainContent(BuildContext ctx){

    final blocMain = AppProvider.getMainBloc(ctx);

    return Container(
        margin: EdgeInsets.only(left:UiHelper.layoutMarginLeft),
        padding: EdgeInsets.all(0.0),
        width: getSize.width-45.0,
        //decoration: BoxDecoration( color:UiHelper.greenContainer, ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[

            // campo-busqueda
            SafeArea(child: Padding(
              padding:EdgeInsets.only(bottom:15.0),
              child: inputWrapper(
                  field: StatefulBuilder(
                    builder: (BuildContext context, setState) {
                      return new DropdownButtonFormField(
                        validator: (value) => value == null ? 'Es requerido' : null,
                        hint: new Text("Seleccionar archivo"),
                        isExpanded: true,
                        items: pdfList.map((data) {
                          return new DropdownMenuItem(
                            child: new Text(data['name'], style: TextStyle(fontSize: 15.0),),
                            value: data['name'],
                          );
                        }).toList(),
                        onChanged: (newVal) {
                          setState(() {
                            pdfListBool = false;
                            archivoString = newVal;
                            onPdfViewer();
                          });
                        },
                        value: archivoString,
                        onSaved: (value) {
                          setState(() {
                            pdfListBool = false;
                            archivoString = value;
                            onPdfViewer();
                          });
                        },
                      );
                    }
                  ),
                  withBoxShadow: true
              ),
            ),),
            new Padding(
              padding: EdgeInsets.only(right: 15, left: 15),
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.greenAccent,
                  borderRadius: BorderRadius.circular(5),
                ),
                padding: EdgeInsets.only(top: 5, bottom: 5, left: 5, right: 5),
                child: new Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    new Text("Fecha de recepción: ",
                      style: TextStyle(
                          fontWeight: FontWeight.w300
                      ),
                    ),
                    new Text("${widget.fecha.toString()} ",
                      style: TextStyle(
                          fontWeight: FontWeight.w700
                      ),
                    )
                  ],
                ),
              ),
            ),
            new Flexible(
              flex: 10,
              child: pdfListBool == true ? PDFViewer(
                document: documentPdf,
              ) : Center(child: new Text("Selecciona un documento")),
            )
          ],

        )
    );
  }

}
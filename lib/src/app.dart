import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'package:santa_clara/src/pages/agenda/diaAgenda.dart';
import 'package:santa_clara/src/pages/agenda/indexAgenda.dart';
import 'package:santa_clara/src/pages/agenda/tareaAgenda.dart';
import 'package:santa_clara/src/pages/agenda/turnoConfirmar.dart';
import 'package:santa_clara/src/pages/agenda/turnoEspecialidad.dart';
import 'package:santa_clara/src/pages/agenda/turnoFechaHora.dart';
import 'package:santa_clara/src/pages/guiaMedica/guia_medica.dart';
import 'package:santa_clara/src/pages/guiaMedica/info_guia_medica.dart';
import 'package:santa_clara/src/pages/guiaMedica/marker_guiaMedica.dart';
import 'package:santa_clara/src/pages/home/dashboard.dart';
import 'package:santa_clara/src/pages/institucional/terminosLegales.dart';
import 'package:santa_clara/src/pages/login/indexLogin.dart';
import 'package:santa_clara/src/pages/login/recuperarClave.dart';
import 'package:santa_clara/src/pages/presupuestos/presupuestos_page.dart';
import 'package:santa_clara/src/pages/registro/clavesRegistro.dart';
import 'package:santa_clara/src/pages/registro/confirmaRegistro.dart';
import 'package:santa_clara/src/pages/registro/indexRegistro.dart';
import 'package:santa_clara/src/pages/resultados/resultados.dart';
import 'package:santa_clara/src/pages/solicitudes/solicitudes_page.dart';
import 'package:santa_clara/src/pages/usuario/carnet_usuario.dart';
import 'package:santa_clara/src/pages/usuario/indexPerfilUsuario.dart';
import 'package:santa_clara/src/pages/agenda/turnoBeneficiario.dart';
import 'package:santa_clara/src/pages/viajeros/viajeros_page.dart';
import 'package:santa_clara/src/pages/visaciones/visaciones_page.dart';
import 'package:santa_clara/utils/uiHelper.dart';
import 'package:santa_clara/utils/userPreferences.dart';

import 'classes/pdf_downloader.dart';
import 'modules/appProvider.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    final userPrefs = new UserPreferences();

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => FileDownloaderProvider(),
        ),
      ],
      child: AppProvider(
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          theme:ThemeData(
              fontFamily: 'SegoeUI',
              appBarTheme: AppBarTheme( color:UiHelper.bgAppbar ),
              primaryIconTheme: IconThemeData( color:UiHelper.scAzul )
          ),
          localizationsDelegates: [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate
          ],
          supportedLocales: [
            const Locale('es','ES'),
          ],
          initialRoute: userPrefs.ci != '######' ? 'dashboard' : 'login',
          routes: {
            'login':(BuildContext context)=> IndexLogin(),
            'terminos-legales':(BuildContext context)=> TerminosLegales(),
            'recuperar-clave':(BuildContext context)=> RecuperarClave(),
            'registro':(BuildContext context)=> IndexRegistro(),
            'registro-password':(BuildContext context)=> ClavesRegistro(),
            'confirma-registro':(BuildContext context)=> ConfirmaRegistro(),
            'dashboard':(BuildContext context)=> Dashboard(),
            'perfil-usuario':(BuildContext context)=> IndexPerfilUsuario(),
            'agenda':(BuildContext context)=> IndexAgenda(),
            'dia':(BuildContext context)=> DiaAgenda(),
            'turno-beneficiario':(BuildContext context)=> TurnoBeneficiario(),
            'turno-especialidad':(BuildContext context)=> TurnoEspecialidad(),
            'turno-fecha-hora':(BuildContext context)=> TurnoFechaHora(),
            'turno-confirmar':(BuildContext context)=> TurnoConfirmar(),
            'tarea-agenda':(BuildContext context)=> TareaAgenda(),
            'resultados':(BuildContext context)=> Resultados(),
            'guia_medica':(BuildContext context)=> GuiaMedica(),
            'marker_guiaMedica': (BuildContext context)=> MarkerGuiaMedica(),
            'info_guiaMedica': (BuildContext context)=> InfoGuiaMedica(),
            'visaciones': (BuildContext context)=> VisacionesPage(),
            'presupuestos': (BuildContext context)=> PresupuestoPage(),
            'solicitudes': (BuildContext context)=> SolicitudesPage(),
            'viajeros': (BuildContext context)=> ViajerosPage(),
            'carnet_digital': (BuildContext context) => CarnetUsuario()
          },
        )
      ),
    );
  }
}
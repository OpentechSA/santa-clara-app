class LoginResponse
{
    String message;
    int tipoRespuesta;
    bool status;
    Map<String,dynamic> data;
    Object params;
    String codStatus;

    LoginResponse({this.message,this.codStatus,this.data, this.params, this.status, this.tipoRespuesta});

    factory LoginResponse.fromJson(Map<String, dynamic> json) {
      return LoginResponse(
        message: json['message'],
        tipoRespuesta: json['tipoRespuesta'],
        codStatus: json['codStatus'],
        data: json['data'],
        params: json['params'],
        status: json['status']
      );
    }

    void setStatus(bool data) => status = data;
    void setMessage(String data) => message = data;

    String get getMessage => this.message;

}

class LoginUserData{

  int id;
  String email;
  String nombreCompleto;
  String ci;
  String estadoZonaAsegurados;

  LoginUserData({this.id,this.email,this.nombreCompleto,this.ci,this.estadoZonaAsegurados});

  factory LoginUserData.fromJson(Map<String,dynamic> data){
    return LoginUserData(
      id:data['id'],
      email:data['email'],
      nombreCompleto: data['nombre'],
      ci:data['ci'],
      estadoZonaAsegurados: data['status-zona-asegurados']
    );
  }

}

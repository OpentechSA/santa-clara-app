import 'dart:convert';

class AdherentesResponse {
  String message;
  int tipoRespuesta;
  bool status;
  List<dynamic> data;
  Object params;
  String codStatus;

  AdherentesResponse(
      {this.message,
      this.codStatus,
      this.data,
      this.params,
      this.status,
      this.tipoRespuesta});

  factory AdherentesResponse.fromJson(Map<String, dynamic> json) {
    return AdherentesResponse(
        message: json['message'],
        tipoRespuesta: json['tipoRespuesta'],
        codStatus: json['codStatus'],
        data: json['data'],
        params: json['params'],
        status: json['status']);
  }

  void setStatus(bool data) => status = data;
  void setMessage(String data) => message = data;

  String get getMessage => this.message;
}

class Adherente {
  int index = 0;
  String idCliente;
  String secuencia;
  String ci;
  String nombres;
  String descripcion;

  Adherente(
      {this.idCliente,
      this.secuencia,
      this.ci,
      this.nombres,
      this.descripcion});
  factory Adherente.fromJson(Map<String, dynamic> json) {
    return Adherente(
        idCliente: json['id_cliente'],
        secuencia: json['secuencia'],
        ci: json['ci'],
        nombres: json['nombres'],
        descripcion: json['descripcion']);
  }
  void setIndex(int value) => this.index = value;
}

class RequestEspecialidad {
  String idCliente;
  String ci;
  String buscar;

  RequestEspecialidad({this.idCliente, this.ci, this.buscar});
  factory RequestEspecialidad.fromJson(Map<String, dynamic> json) {
    return RequestEspecialidad(
      idCliente: json['idCliente'],
      ci: json['ci'],
      buscar: json['buscar'],
    );
  }

  Map toJson() =>
      {'idCliente': this.idCliente, 'ci': this.ci, 'buscar': this.buscar};
}

class RequestProfesional {
  String idCliente;
  String ci;
  String sucursal;
  String especialidad;
  String buscar;

  RequestProfesional(
      {this.idCliente, this.ci, this.sucursal, this.especialidad, this.buscar});
  factory RequestProfesional.fromJson(Map<String, dynamic> json) {
    return RequestProfesional(
      idCliente: json['idCliente'],
      ci: json['ci'],
      sucursal: json['sucursal'],
      especialidad: json['especialidad'],
      buscar: json['buscar'],
    );
  }

  Map toJson() => {
        'idCliente': this.idCliente,
        'ci': this.ci,
        'sucursal': this.sucursal,
        'especialidad': this.especialidad,
        'buscar': this.buscar,
      };
}

class EspecialidadesResponse {
  String message;
  int tipoRespuesta;
  bool status;
  List<dynamic> data;
  Object params;
  String codStatus;

  EspecialidadesResponse(
      {this.message,
      this.codStatus,
      this.data,
      this.params,
      this.status,
      this.tipoRespuesta});

  factory EspecialidadesResponse.fromJson(Map<String, dynamic> json) {
    return EspecialidadesResponse(
        message: json['message'],
        tipoRespuesta: json['tipoRespuesta'],
        codStatus: json['codStatus'],
        data: json['data'],
        params: json['params'],
        status: json['status']);
  }

  void setStatus(bool data) => status = data;
  void setMessage(String data) => message = data;

  String get getMessage => this.message;
}

class Especialidad {
  int index = 0;
  String idEspecialidad;
  String descripcion;
  int codSucursal;
  String sucursal;

  Especialidad(
      {this.idEspecialidad, this.descripcion, this.codSucursal, this.sucursal});
  factory Especialidad.fromJson(Map<String, dynamic> json) {
    return Especialidad(
        idEspecialidad: json['esp'],
        descripcion: json['desc_espe'],
        codSucursal: int.parse(json['suc'].toString()),
        sucursal: json['nombre_suc']);
  }
  void setIndex(int value) => this.index = value;
}

class Profesional {
  int index = 0;
  String idProfesional;
  String descripcion;
  int codSucursal;
  String sucursal;

  Profesional(
      {this.idProfesional, this.descripcion, this.codSucursal, this.sucursal});
  factory Profesional.fromJson(Map<String, dynamic> json) {
    return Profesional(
        idProfesional: json['prof'],
        descripcion: json['desc_prof'],
        codSucursal: int.parse(json['suc'].toString()),
        sucursal: json['nom_suc']);
  }
  void setIndex(int value) => this.index = value;
}

class ProfesionalesResponse {
  String message;
  int tipoRespuesta;
  bool status;
  List<dynamic> data;
  Object params;
  String codStatus;

  ProfesionalesResponse(
      {this.message,
      this.codStatus,
      this.data,
      this.params,
      this.status,
      this.tipoRespuesta});

  factory ProfesionalesResponse.fromJson(Map<String, dynamic> json) {
    return ProfesionalesResponse(
        message: json['message'],
        tipoRespuesta: json['tipoRespuesta'],
        codStatus: json['codStatus'],
        data: json['data'],
        params: json['params'],
        status: json['status']);
  }

  void setStatus(bool data) => status = data;
  void setMessage(String data) => message = data;

  String get getMessage => this.message;
}

class Fecha {
  int index = 0;
  String fecha;
  String descDia;

  Fecha({this.fecha, this.descDia});
  factory Fecha.fromJson(Map<String, dynamic> json) {
    return Fecha(fecha: json['fecha'], descDia: json['desc_dia']);
  }
  void setIndex(int value) => this.index = value;
}

class RequestFechas {
  String idCliente;
  String ci;
  String sucursal;
  String especialidad;
  String profesional;

  RequestFechas(
      {this.idCliente,
      this.ci,
      this.sucursal,
      this.especialidad,
      this.profesional});
  factory RequestFechas.fromJson(Map<String, dynamic> json) {
    return RequestFechas(
      idCliente: json['idCliente'],
      ci: json['ci'],
      sucursal: json['sucursal'],
      especialidad: json['especialidad'],
      profesional: json['profesional'],
    );
  }

  Map toJson() => {
        'idCliente': this.idCliente,
        'ci': this.ci,
        'sucursal': this.sucursal,
        'especialidad': this.especialidad,
        'profesional': this.profesional,
      };
}

class FechasResponse {
  String message;
  int tipoRespuesta;
  bool status;
  Map<String, dynamic> data;
  Object params;
  String codStatus;

  FechasResponse(
      {this.message,
      this.codStatus,
      this.data,
      this.params,
      this.status,
      this.tipoRespuesta});

  factory FechasResponse.fromJson(Map<String, dynamic> json) {
    return FechasResponse(
        message: json['message'],
        tipoRespuesta: json['tipoRespuesta'],
        codStatus: json['codStatus'],
        data: json['data'],
        params: json['params'],
        status: json['status']);
  }

  void setStatus(bool data) => status = data;
  void setMessage(String data) => message = data;

  String get getMessage => this.message;
}

class Hora {
  int index = 0;
  String idRes;
  String descHora;

  Hora({this.idRes, this.descHora});
  factory Hora.fromJson(Map<String, dynamic> json) {
    return Hora(idRes: json['id_res'], descHora: json['desc_hora']);
  }
  void setIndex(int value) => this.index = value;
}

class RequestHoras {
  String idCliente;
  String ci;
  String sucursal;
  String especialidad;
  String profesional;
  String fecha;

  RequestHoras(
      {this.idCliente,
      this.ci,
      this.sucursal,
      this.especialidad,
      this.profesional,
      this.fecha});
  factory RequestHoras.fromJson(Map<String, dynamic> json) {
    return RequestHoras(
      idCliente: json['idCliente'],
      ci: json['ci'],
      sucursal: json['sucursal'],
      especialidad: json['especialidad'],
      profesional: json['profesional'],
      fecha: json['fecha'],
    );
  }

  Map toJson() => {
        'idCliente': this.idCliente,
        'ci': this.ci,
        'sucursal': this.sucursal,
        'especialidad': this.especialidad,
        'profesional': this.profesional,
        'fecha': this.fecha,
      };
}

class HorasResponse {
  String message;
  int tipoRespuesta;
  bool status;
  Map<String, dynamic> data;
  Object params;
  String codStatus;

  HorasResponse(
      {this.message,
      this.codStatus,
      this.data,
      this.params,
      this.status,
      this.tipoRespuesta});

  factory HorasResponse.fromJson(Map<String, dynamic> json) {
    return HorasResponse(
        message: json['message'],
        tipoRespuesta: json['tipoRespuesta'],
        codStatus: json['codStatus'],
        data: json['data'],
        params: json['params'],
        status: json['status']);
  }

  void setStatus(bool data) => status = data;
  void setMessage(String data) => message = data;

  String get getMessage => this.message;
}

class TurnoResponse {
  String message;
  int tipoRespuesta;
  bool status;
  Map<String, dynamic> data;
  Object params;
  String codStatus;

  TurnoResponse(
      {this.message,
      this.codStatus,
      this.data,
      this.params,
      this.status,
      this.tipoRespuesta});

  factory TurnoResponse.fromJson(Map<String, dynamic> json) {
    return TurnoResponse(
        message: json['message'],
        tipoRespuesta: json['tipoRespuesta'],
        codStatus: json['codStatus'],
        data: json['data'],
        params: json['params'],
        status: json['status']);
  }

  void setStatus(bool data) => status = data;
  void setMessage(String data) => message = data;

  String get getMessage => this.message;
}

class RequestTurno {
  String idCliente;
  String ci;
  String sucursal;
  String especialidad;
  String profesional;
  String fecha;
  String idReserva;
  String email;
  String telefono;
  String dataEspecialidad;
  String dataProfesional;
  String dataSucursal;
  String dataFecha;
  String dataHora;
  String dataModalidad;
  String dataRecordatorio;
  String uid;

  RequestTurno(
      {this.idCliente,
      this.ci,
      this.sucursal,
      this.especialidad,
      this.profesional,
      this.fecha,
      this.idReserva,
      this.email,
      this.telefono});
  factory RequestTurno.fromJson(Map<String, dynamic> json) {
    return RequestTurno(
      idCliente: json['esp'],
      ci: json['desc_espe'],
      sucursal: json['sucursal'],
      especialidad: json['especialidad'],
      profesional: json['profesional'],
      fecha: json['fecha'],
      idReserva: json['idReserva'],
      email: json['email'],
      telefono: json['telefono'],
    );
  }

  Map toJson() => {
        'idCliente': this.idCliente,
        'ci': this.ci,
        'sucursal': this.sucursal,
        'especialidad': this.especialidad,
        'profesional': this.profesional,
        'fecha': this.fecha,
        'idReserva': this.idReserva,
        'email': this.email,
        'telefono': this.telefono,
        'uid': this.uid,
        'dataProfesional': "$dataProfesional",
        'dataEspecialidad': "$dataEspecialidad",
        'dataSucursal': "$dataSucursal",
        'dataFecha': "$dataFecha",
        'dataHora': "$dataHora",
        'dataRecordatorio': "$dataRecordatorio",
        'dataModalidad': "$dataModalidad"
      };

  void setIdCliente(String value) => this.idCliente = value;
  void setCi(String value) => this.ci = value;
  void setSucursal(String value) => this.sucursal = value;
  void setEspecialidad(String value) => this.especialidad = value;
  void setProfesional(String value) => this.profesional = value;
  void setFecha(String value) => this.fecha = value;
  void setIdReserva(String value) => this.idReserva = value;
  void setEmail(String value) => this.email = value;
  void setTelefono(String value) => this.telefono = value;
  void setUID(String value) => this.uid = value;
  void setDataProfesional(String value) => this.dataProfesional = value;
  void setDataEspecialidad(String value) => this.dataEspecialidad = value;
  void setDataSucursal(String value) => this.dataSucursal = value;
  void setDataFecha(String value) => this.dataFecha = value;
  void setDataHora(String value) => this.dataHora = value;
  void setDataRecordatorio(String value) => this.dataRecordatorio = value;
  void setDataModalidad(String value) => this.dataModalidad = value;
}

class TareaResponse {
  String message;
  int tipoRespuesta;
  bool status;
  Map<String, dynamic> data;
  Object params;
  String codStatus;

  TareaResponse(
      {this.message,
      this.codStatus,
      this.data,
      this.params,
      this.status,
      this.tipoRespuesta});

  factory TareaResponse.fromJson(Map<String, dynamic> json) {
    return TareaResponse(
        message: json['message'],
        tipoRespuesta: json['tipoRespuesta'],
        codStatus: json['codStatus'],
        data: json['data'],
        params: json['params'],
        status: json['status']);
  }

  void setStatus(bool data) => status = data;
  void setMessage(String data) => message = data;

  String get getMessage => this.message;
}

class RequestTarea {
  static const TIPO_RECORDATORIO_UNICO = 'UNICA_VEZ';
  static const TIPO_RECORDATORIO_INDEFINIDO = 'INDEFINIDO';
  static const TIPO_RECORDATORIO_PROGRAMADO = 'PROGRAMADO';
  String titulo;
  String descripcion;
  String tipo;
  String dosisMedicacion;
  String beneficiario;
  String recordatorioTipo;
  String recordatorioFechaInicio;
  String recordatorioFechaFin;
  String recordatorioHoraInicio;
  String recordatorioHoraFin;
  String recordatorioPrevio;
  String recordatorioFrecuencia;
  String uid;
  int eid;
  List<String> dias;

  RequestTarea(
      {this.titulo,
      this.descripcion,
      this.tipo,
      this.dosisMedicacion,
      this.beneficiario,
      this.recordatorioTipo,
      this.recordatorioFechaInicio,
      this.recordatorioFechaFin,
      this.recordatorioHoraInicio,
      this.recordatorioHoraFin,
      this.recordatorioPrevio,
      this.recordatorioFrecuencia,
      this.uid,
      this.eid,
      this.dias});
  factory RequestTarea.fromJson(Map<String, dynamic> json) {
    return RequestTarea(
        titulo: json['titulo'],
        descripcion: json['descripcion'],
        tipo: json['tipo'],
        dosisMedicacion: json['dosisMedicacion'],
        beneficiario: json['beneficiario'],
        recordatorioTipo: json['recordatorioTipo'],
        recordatorioFechaInicio: json['recordatorioFechaInicio'],
        recordatorioFechaFin: json['recordatorioFechaFin'],
        recordatorioHoraInicio: json['recordatorioHoraInicio'],
        recordatorioHoraFin: json['recordatorioHoraFin'],
        recordatorioPrevio: json['recordatorioPrevio'],
        recordatorioFrecuencia: json['recordatorioFrecuencia'],
        uid: json['uid'],
        eid: json['eid'],
        dias: json['dias'].join(','));
  }

  void setTitulo(String value) => this.titulo = value;
  void setDescripcion(String value) => this.descripcion = value;
  void setTipo(String value) => this.tipo = value;
  void setDosisMedicacion(String value) => this.dosisMedicacion = value;
  void setBeneficiario(String value) => this.beneficiario = value;
  void setRecordatorioTipo(String value) => this.recordatorioTipo = value;
  void setRecordatorioFechaInicio(String value) => this.recordatorioFechaInicio = value;
  void setRecordatorioFechaFin(String value) => this.recordatorioFechaFin = value;
  void setRecordatorioHoraInicio(String value) => this.recordatorioHoraInicio = value;
  void setRecordatorioHoraFin(String value) => this.recordatorioHoraFin = value;
  void setRecordatorioPrevio(String value) => this.recordatorioPrevio = value;
  void setRecordatorioFrecuencia(String value) => this.recordatorioFrecuencia = value;
  void setUID(String value) => this.uid = value;
  void setEID(int value) => this.eid = value;
  void setDias(List<String> value) => this.dias = value;

  Map toJson() => {
        'titulo': this.titulo,
        'descripcion': this.descripcion,
        'tipo': this.tipo,
        'dosisMedicacion': (this.dosisMedicacion != null) ? this.dosisMedicacion : "",
        'beneficiario': (this.beneficiario != null) ? this.beneficiario : "",
        'recordatorioTipo': this.recordatorioTipo,
        'recordatorioFechaInicio': (this.recordatorioFechaInicio != null)? this.recordatorioFechaInicio : "",
        'recordatorioFechaFin': (this.recordatorioFechaFin != null)? this.recordatorioFechaFin : "",
        'recordatorioHoraInicio': (this.recordatorioHoraInicio != null) ? this.recordatorioHoraInicio : "",
        'recordatorioHoraFin': (this.recordatorioHoraFin != null) ? this.recordatorioHoraFin : "",
        'recordatorioPrevio': (this.recordatorioPrevio != null) ? this.recordatorioPrevio : "",
        'recordatorioFrecuencia': (this.recordatorioFrecuencia != null) ? this.recordatorioFrecuencia : "",
        'uid': this.uid,
        'eid': (this.eid != null) ? this.eid.toString() : "",
        'dias': (this.dias != null) ? this.dias.join(',') : "",
      };

  bool recordatorioValido() {
    bool valid = false;
    switch (this.recordatorioTipo) {
      case TIPO_RECORDATORIO_UNICO:
        valid = validarRecordatorioUnico();
        break;
      case TIPO_RECORDATORIO_PROGRAMADO:
        valid = validarRecordatorioProgramado();
        break;
      case TIPO_RECORDATORIO_INDEFINIDO:
        valid = validarRecordatorioIndefinido();
        break;
    }
    return valid;
  }

  bool validarRecordatorioUnico() {
    bool valid = false;
    if (recordatorioFechaInicio != null &&
        recordatorioHoraInicio != null &&
        recordatorioHoraFin != null) {
      if (recordatorioFechaInicio.length > 0 &&
          recordatorioHoraInicio.length > 0 &&
          recordatorioHoraFin.length > 0) {
        valid = true;
      }
    }
    return valid;
  }

  bool validarRecordatorioProgramado() {
    bool valid = false;
    if (recordatorioFechaInicio != null &&
        recordatorioFechaFin != null &&
        recordatorioHoraInicio != null &&
        recordatorioFrecuencia != null) {
      if (recordatorioFechaInicio.length > 0 &&
          recordatorioFechaFin.length > 0 &&
          recordatorioHoraInicio.length > 0 &&
          recordatorioFrecuencia.length > 0) {
        valid = true;
      }
    }
    return valid;
  }

  bool validarRecordatorioIndefinido() {
    bool valid = false;
    print(recordatorioFechaInicio);
    print(recordatorioHoraInicio);
    print(dias.join(','));

    print(recordatorioFechaInicio);
    if ( recordatorioFechaInicio != null && recordatorioHoraInicio != null && dias != null ) {
      if ( recordatorioFechaInicio.length > 0 && recordatorioHoraInicio.length > 0 && dias.length > 0 ) {
        valid = true;
      }
    }
    return valid;
  }
}

class RequestEventos {
  String uid;

  RequestEventos({this.uid});
  factory RequestEventos.fromJson(Map<String, dynamic> json) {
    return RequestEventos(
      uid: json['uid'],
    );
  }

  Map toJson() => {'uid': this.uid};
}

class EventosResponse {
  String message;
  int tipoRespuesta;
  bool status;
  Map<String, dynamic> data;
  Object params;
  String codStatus;

  EventosResponse(
      {this.message,
      this.codStatus,
      this.data,
      this.params,
      this.status,
      this.tipoRespuesta});

  factory EventosResponse.fromJson(Map<String, dynamic> json) {
    return EventosResponse(
        message: json['message'],
        tipoRespuesta: json['tipoRespuesta'],
        codStatus: json['codStatus'],
        data: json['data'],
        params: json['params'],
        status: json['status']);
  }

  void setStatus(bool data) => status = data;
  void setMessage(String data) => message = data;

  String get getMessage => this.message;
}

class Evento {
  int eid;
  String titulo;
  String desc;
  String fechaDesde;
  String fechaHasta;
  String tsDesde;
  String tsHasta;
  String lugar;
  String especialidad;
  String dosis;
  String beneficiario;
  String tipo;
  String ref;
  dynamic recordatorio;
  Map<String, dynamic> meta;

  Evento(
      {this.eid,
      this.titulo,
      this.desc,
      this.fechaDesde,
      this.fechaHasta,
      this.tsDesde,
      this.tsHasta,
      this.lugar,
      this.especialidad,
      this.dosis,
      this.beneficiario,
      this.tipo,
      this.ref,
      this.recordatorio,
      this.meta});

  factory Evento.fromJson(Map<String, dynamic> json) {
    return Evento(
        eid: json['id'],
        titulo: json['titulo'],
        desc: json['desc'],
        fechaDesde: json['fecha-desde'],
        fechaHasta: json['fecha-hasta'],
        tsDesde: json['ts-desde'],
        tsHasta: json['ts-hasta'],
        lugar: (json['lugar'] != null) ? json['lugar'] : "",
        especialidad: (json['especialidad'] != null) ? json['especialidad'] : "",
        dosis: (json['dosis'] != null) ? json['dosis'] : "",
        beneficiario: (json['beneficiario'] != null) ? json['beneficiario'] : "",
        tipo: json['tipo'],
        ref: (json['ref'] != null) ? json['ref'] : "",
        recordatorio: json['recordatorio'],
        meta: json['meta']);
  }
}

class EventoResponse {
  String message;
  int tipoRespuesta;
  bool status;
  Map<String, dynamic> data;
  Object params;
  String codStatus;

  EventoResponse(
      {this.message,
      this.codStatus,
      this.data,
      this.params,
      this.status,
      this.tipoRespuesta});

  factory EventoResponse.fromJson(Map<String, dynamic> json) {
    return EventoResponse(
        message: json['message'],
        tipoRespuesta: json['tipoRespuesta'],
        codStatus: json['codStatus'],
        data: json['data'],
        params: json['params'],
        status: json['status']);
  }

  void setStatus(bool data) => status = data;
  void setMessage(String data) => message = data;

  String get getMessage => this.message;
}

class DeleteEventoResponse {
  String message;
  int tipoRespuesta;
  bool status;
  dynamic data;
  Object params;
  String codStatus;

  DeleteEventoResponse(
      {this.message,
      this.codStatus,
      this.data,
      this.params,
      this.status,
      this.tipoRespuesta});

  factory DeleteEventoResponse.fromJson(Map<String, dynamic> json) {
    return DeleteEventoResponse(
        message: json['message'],
        tipoRespuesta: json['tipoRespuesta'],
        codStatus: json['codStatus'],
        data: json['data'],
        params: json['params'],
        status: json['status']);
  }

  void setStatus(bool data) => status = data;
  void setMessage(String data) => message = data;

  String get getMessage => this.message;
}

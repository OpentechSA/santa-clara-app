class SignUpResponse
{
  String message;
  int tipoRespuesta;
  bool status;
  Object data;
  Object params;
  String codStatus;

  SignUpResponse({this.message,this.codStatus,this.data, this.params, this.status, this.tipoRespuesta});
  factory SignUpResponse.fromJson(Map<String, dynamic> json) {
    return SignUpResponse(
      message: json['message'],
      tipoRespuesta: json['tipoRespuesta'],
      codStatus: json['codStatus'],
      data: json['data'],
      params: json['params'],
      status: json['status']
    );
  }

  void setStatus(bool data) => status = data;
  void setMessage(String data) => message = data;
}

class EditUserResponse
{
  String message;
  int tipoRespuesta;
  bool status;
  Object params;
  String codStatus;

  EditUserResponse({this.message,this.codStatus, this.params, this.status, this.tipoRespuesta});
  factory EditUserResponse.fromJson(Map<String, dynamic> json) {
    return EditUserResponse(
      message: json['message'],
      tipoRespuesta: json['tipoRespuesta'],
      codStatus: json['codStatus'],
      params: json['params'],
      status: json['status']
    );
  }

  void setStatus(bool data) => status = data;
  void setMessage(String data) => message = data;
}

class SignUpRequest
{
  String nombre;
  String ci;
  String email;
  String password;

  SignUpRequest({this.ci,this.email,this.nombre,this.password});

  factory SignUpRequest.fromJson(Map<String, dynamic> json) {
    return SignUpRequest(
      ci: json['ci'],
      email: json['email'],
      nombre: json['nombre'],
      password: json['password'],
    );
  }

  Map toJson() => {
    'ci': this.ci,
    'email': this.email,
    'nombre': this.nombre,
    'password': this.password,
  };

}

class EditUserRequest
{
  String uid;
  String nombre;
  String ci;
  String email;
  String telefono;

  EditUserRequest({this.uid,this.ci,this.email,this.nombre,this.telefono});

  factory EditUserRequest.fromJson(Map<String, dynamic> json) {
    return EditUserRequest(
      uid: json['id'],
      ci: json['ci'],
      email: json['email'],
      nombre: json['nombre'],
      telefono: json['telefono']
    );
  }

  Map toJson() => {
    'uid': this.uid,
    'ci': this.ci,
    'email': this.email,
    'nombre': this.nombre,
    'telefono': this.telefono,
  };

}

class ChangePassRequest
{
  String uid;
  String password;

  ChangePassRequest({this.uid,this.password});

  factory ChangePassRequest.fromJson(Map<String, dynamic> json) {
    return ChangePassRequest(
      uid: json['uid'],
      password: json['password']
    );
  }

  Map toJson() => {
    'uid': this.uid,
    'password': this.password
  };

}

class RecoveryRequest
{
  
  String email;
  RecoveryRequest({this.email});

  factory RecoveryRequest.fromJson(Map<String, dynamic> json) {
    return RecoveryRequest(
      email: json['email']
    );
  }
  
  Map toJson() => {
    'email': this.email
  };

}

class RecoveryResponse
{
  
  String message;
  int tipoRespuesta;
  bool status;
  NotificationRecovery data;
  Object params;
  String codStatus;
  
  RecoveryResponse({this.message,this.codStatus,this.data, this.params, this.status, this.tipoRespuesta});
  
  factory RecoveryResponse.fromJson(Map<String, dynamic> json) {
    NotificationRecovery paramData;
    if(json['data'] != null){
      paramData = NotificationRecovery.fromJson(json['data']); 
    }else{
      paramData = new NotificationRecovery(status: json['status'],message: json['message']);
    }
    return RecoveryResponse(
      message: json['message'],
      tipoRespuesta: json['tipoRespuesta'],
      codStatus: json['codStatus'],
      data: paramData,
      params: json['params'],
      status: json['status']
    );
  }

  void setStatus(bool data) => status = data;
  void setMessage(String data) => message = data;

}

class NotificationRecovery
{
  
  String message;
  int tipoRespuesta;
  bool status;
  Object data;
  
  NotificationRecovery({this.message,this.data, this.status, this.tipoRespuesta});
  
  factory NotificationRecovery.fromJson(Map<String, dynamic> json) {
    return NotificationRecovery(
      message: json['msg'],
      tipoRespuesta: json['tipoRespuesta'],
      data: json['data'],
      status: json['status']
    );
  }

  void setStatus(bool data) => status = data;
  void setMessage(String data) => message = data;

}

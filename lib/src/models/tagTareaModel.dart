class TagTarea{

  String tag;
  String nombre;
  int index;

  TagTarea({
    String tag,
    String nombre,
    int index
  }){
    tag = tag;
    nombre = nombre;
    index = index;
  }

  void setIndex (int idx) => index = idx;
  void setNombre (String data) => nombre = data;
  void setTag (String data) => tag = data;

}
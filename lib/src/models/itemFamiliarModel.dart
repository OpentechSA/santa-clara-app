class ItemFamiliar{

  String nroCliente;
  String nroDocumento;
  int index;
  String nombreCompleto;

  ItemFamiliar({
    String nroCliente,
    String nroDocumento,
    int index,
    String nombreCompleto,
  }){
    nroCliente = nroCliente;
    nroDocumento = nroDocumento;
    index = index;
    nombreCompleto = nombreCompleto;
  }

  void setIndex (int idx) => index = idx;
  void setNroCliente (String data) => nroCliente = data;
  void setNroDocumento (String data) => nroDocumento = data;
  void setNombre (String data) => nombreCompleto = data;
  
  @override
  String toString() {
    return "{index:$index,nroCliente:$nroCliente,nroDocumento:$nroDocumento,nombreCompleto:$nombreCompleto}";
  }

}
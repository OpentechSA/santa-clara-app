class DefaultResponse
{
    String message;
    int tipoRespuesta;
    bool status;
    Map<String,dynamic> data;
    Object params;
    String codStatus;

    DefaultResponse({this.message,this.codStatus,this.data, this.params, this.status, this.tipoRespuesta});

    factory DefaultResponse.fromJson(Map<String, dynamic> json) {
      return DefaultResponse(
        message: json['message'],
        tipoRespuesta: json['tipoRespuesta'],
        codStatus: json['codStatus'],
        data: json['data'],
        params: json['params'],
        status: json['status']
      );
    }

    void setStatus(bool data) => status = data;
    void setMessage(String data) => message = data;

    String get getMessage => this.message;

}


class FacturasResponse
{
    String message;
    int tipoRespuesta;
    bool status;
    List<dynamic> data;
    Object params;
    String codStatus;

    FacturasResponse({this.message,this.codStatus,this.data, this.params, this.status, this.tipoRespuesta});

    factory FacturasResponse.fromJson(Map<String, dynamic> json) {
      return FacturasResponse(
        message: json['message'],
        tipoRespuesta: json['tipoRespuesta'],
        codStatus: json['codStatus'],
        data: json['data'],
        params: json['params'],
        status: json['status']
      );
    }

    void setStatus(bool data) => status = data;
    void setMessage(String data) => message = data;

    String get getMessage => this.message;

}

class FacturaCliente{

  String idFactura;
  String nroFactura;
  String fecha;
  double saldo;
  String estado;
  String codigo;
  List<dynamic> recibos;

  FacturaCliente({
    this.idFactura,
    this.nroFactura,
    this.fecha, 
    this.saldo, 
    this.estado, 
    this.codigo,
    this.recibos
  });

  factory FacturaCliente.fromJson(Map<String, dynamic> json) {
    return FacturaCliente(
      idFactura: json['id_factura'],
      nroFactura: json['nro_fact'],
      fecha: json['fecha'],
      saldo: double.parse(json['saldo'].toString()),
      estado: json['txt_estado'],
      codigo: (json['txt_codigo'] != null)?json['txt_codigo']:'---',
      recibos: json['recibos']
    );
  }

}

/*
{
  "id_factura":"1440740",
  "nro_fact":"10140060567",
  "fecha":"20 de abril 2020",
  "saldo":0,
  "txt_estado":"CANCELADO",
  "txt_codigo":null,
  "recibos":[
    {
      "id_pago_prepaga":"740162",
      "nro_recibo":"10080108990",
      "fecha_pago":"2020-05-07",
      "total_rec":503800
    }
  ]
}
*/

class ReciboCliente{

  String idPagoPrepaga;
  String nroRecibo;
  String fechaPago;
  double total;

  ReciboCliente({
    this.idPagoPrepaga,
    this.nroRecibo,
    this.fechaPago, 
    this.total
  });

  factory ReciboCliente.fromJson(Map<String, dynamic> json) {
    return ReciboCliente(
      idPagoPrepaga: json['id_pago_prepaga'],
      nroRecibo: json['nro_recibo'],
      fechaPago: json['fecha_pago'],
      total: double.parse(json['total_rec'].toString())
    );
  }

}

import 'package:intl/intl.dart' as intl;
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:santa_clara/src/modules/appProvider.dart';
import 'package:santa_clara/src/modules/turnoBloc.dart';
import 'package:santa_clara/src/pages/agenda/turnoEspecialidad.dart';
import 'package:santa_clara/src/pages/agenda/turnoFechaHora.dart';
import 'package:santa_clara/src/pages/agenda/verTareaAgenda.dart';
import 'package:santa_clara/utils/uiHelper.dart';

class TipoRecordatorio extends StatefulWidget {
  _TipoRecordatorioState _state;

  TipoRecordatorio({Key key, Function evtOnChanged}) : super(key: key) {
    _state = new _TipoRecordatorioState(evtOnChanged: evtOnChanged);
  }

  @override
  _TipoRecordatorioState createState() => _state;
}

class _TipoRecordatorioState extends State<TipoRecordatorio> with UiHelper {
  String _valor = 'UNICA_VEZ';
  TurnoBloc _bloc;
  Function _evtOnChanged;

  _TipoRecordatorioState({Function evtOnChanged}) {
    _evtOnChanged = evtOnChanged;
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _bloc = AppProvider.getTurnoBloc(context);

    return StreamBuilder(
        stream: _bloc.tipoRecordatorioStream,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return Container(
            child: DropdownButton<String>(
                hint: Text('Recordatorio previo'),
                value: _valor,
                isExpanded: true,
                icon: SizedBox(
                    width: 24.0,
                    child: Icon(
                      FontAwesomeIcons.angleDown,
                      color: UiHelper.scAzul,
                      size: 16,
                    )),
                iconSize: 16,
                elevation: 16,
                style: TextStyle(color: Color.fromRGBO(70, 70, 70, 1)),
                underline: Container(color: Colors.transparent),
                onChanged: (opt) {
                  setState(() {
                    _valor = opt;
                  });
                  _bloc.changeTipoRecordatorio(opt);
                  _evtOnChanged(opt);
                },
                items: dropdownItems
                    .map((String _k, String _v) {
                      return MapEntry(_k,
                          DropdownMenuItem<String>(value: _k, child: Text(_v)));
                    })
                    .values
                    .toList()),
          );
        });
  }
}

class CampoHoraTarea extends StatefulWidget {
  _CampoHoraTareaState _state;
  CampoHoraTarea(
      {Key key,
      String caption,
      VerTareaAgendaState parentState,
      String type,
      String value})
      : super(key: key) {
    _state = new _CampoHoraTareaState(
        caption: caption, parentState: parentState, type: type, value: value);
  }
  @override
  _CampoHoraTareaState createState() => _state;
}

class _CampoHoraTareaState extends State<CampoHoraTarea> with UiHelper {
  TextEditingController _inputTimeFieldController = new TextEditingController();
  String _value = "";
  String _caption = "";
  String _type = "";
  VerTareaAgendaState _parentState;
  _CampoHoraTareaState(
      {String caption,
      VerTareaAgendaState parentState,
      String type,
      String value}) {
    _caption = caption;
    _type = type;
    _parentState = parentState;
    _value = value;
  }

  @override
  void initState() {
    super.initState();
    if (_value.length > 0) {
      _inputTimeFieldController.text = _value;
    }
  }

  @override
  Widget build(BuildContext context) {
    return inputWrapper(
      field: TextField(
        controller: _inputTimeFieldController,
        decoration: InputDecoration(
          suffix: SizedBox(
            child: Icon(FontAwesomeIcons.clock,
                size: 18.0, color: UiHelper.scAzul),
            width: 30.0,
            height: 20.0,
          ),
          labelText: _caption,
          border: InputBorder.none,
        ),
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
          selectTime(context);
        },
      ),
      withBoxShadow: true,
    );
  }

  Future<String> selectTime(BuildContext context) async {
    String rValue = "";
    TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: new TimeOfDay.now(),
    );
    if (picked != null) {
      rValue = picked.format(context);
      setState(() {
        _inputTimeFieldController.text = rValue;
        switch (_type) {
          case 'start':
            _parentState.setRecordatorioHoraInicio(rValue);
            break;
          case 'end':
            _parentState.setRecordatorioHoraFin(rValue);
            break;
        }
      });
    }
    return rValue;
  }
}

class CampoFechaTarea extends StatefulWidget {
  _CampoFechaTareaState _state;
  CampoFechaTarea(
      {Key key,
      String caption,
      VerTareaAgendaState parentState,
      String type,
      String value})
      : super(key: key) {
    _state = new _CampoFechaTareaState(
        caption: caption, parentState: parentState, type: type, value: value);
  }
  @override
  _CampoFechaTareaState createState() => _state;
}

class _CampoFechaTareaState extends State<CampoFechaTarea> with UiHelper {
  TextEditingController _inputDateFieldController = new TextEditingController();
  String _caption = "";
  String _type = "";
  VerTareaAgendaState _parentState;

  _CampoFechaTareaState(
      {String caption,
      VerTareaAgendaState parentState,
      String type,
      String value}) {
    _caption = caption;
    _parentState = parentState;
    _type = type;
    _inputDateFieldController.text = value;
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return inputWrapper(
        field: TextField(
          controller: _inputDateFieldController,
          decoration: InputDecoration(
            suffix: SizedBox(
              child: Icon(FontAwesomeIcons.calendarAlt,
                  size: 18.0, color: UiHelper.scAzul),
              width: 30.0,
              height: 20.0,
            ),
            labelText: _caption,
            border: InputBorder.none,
          ),
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
            selectDate(context);
          },
        ),
        withBoxShadow: true);
  }

  Future<String> selectDate(BuildContext context) async {
    String rValue = "";
    final formatter = new intl.DateFormat('dd/MM/yyyy');
    DateTime picked = await showDatePicker(
      context: context,
      initialDate: new DateTime.now(),
      firstDate: new DateTime(2018),
      lastDate: new DateTime(2025),
      locale: Locale('es', 'ES'),
    );
    if (picked != null) {
      rValue = formatter.format(picked);
      setState(() {
        _inputDateFieldController.text = rValue;
        switch (_type) {
          case 'start':
            _parentState.setRecordatorioFechaInicio(rValue);
            break;
          case 'end':
            _parentState.setRecordatorioFechaFin(rValue);
            break;
        }
      });
    }
    return rValue;
  }
}

class TagsTareas extends StatelessWidget {
  List<Tags> _items = [];
  String _title = "";
  TagsTareas({Key key, String title, List<Tags> items}) : super(key: key) {
    _items = items;
    _title = title;
  }

  @override
  Widget build(BuildContext context) {
    final Widget rWidget = Container(
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.only(bottom: 10.0),
            child: Text(
              _title,
              style: TextStyle(color: UiHelper.scGrisTexto, fontSize: 12.0),
            ),
          ),
          Wrap(
            spacing: 5,
            runSpacing: 5,
            runAlignment: WrapAlignment.start,
            children: _items,
          ),
        ],
      ),
    );

    return rWidget;
  }
}

class Tags extends StatefulWidget {
  
  _TagsState _state;

  Tags({
    Key key,
    String label,
    String index,
    Function evtOnTap,
    VerTareaAgendaState parentState,
    bool selected = false,
    bool isMultiple = false,
    bool withIcon = true,
  }) : super(key: key) {
    _state = new _TagsState(
        label: label,
        index: index,
        evtOnTap: evtOnTap,
        parentState: parentState,
        isSelected: selected,
        isMultiple: isMultiple,
        withIcon: withIcon);
  }

  @override
  _TagsState createState() => _state;

  void unSelect() => _state.unSelect();
  void setSelect() => _state.setItemState();
  String getIndex() => _state.getIndex;
  String getLabel() => _state.getLabel;
}

class _TagsState extends State<Tags> {
  String _index;
  bool _selected = false;
  bool _multiple = false;
  bool _hasIcon = false;
  String _label;
  VerTareaAgendaState _parentState;

  _TagsState(
      {String label,
      String index,
      Function evtOnTap,
      VerTareaAgendaState parentState,
      bool isSelected = false,
      bool isMultiple = false,
      bool withIcon = true}) {
    this.setLabel(label);
    this.setIndex(index);
    this.setParentState(parentState);
    this.setSelected(isSelected);
    this.setMultiple(isMultiple);
    this.setHasIcon(withIcon);
  }

  @override
  void initState() {
    super.initState();
  }

  void setIndex(String data) => _index = data;
  void setSelected(bool data) => _selected = data;
  void setMultiple(bool data) => _multiple = data;
  void setHasIcon(bool data) => _hasIcon = data;
  void setLabel(String data) => _label = data;
  void setParentState(VerTareaAgendaState data) => _parentState = data;
  void setSelectedState(String data) {
    if (_multiple) {
      _selected = (_parentState.daysSelected.indexOf(_index) >= 0);
    } else {
      _selected = (_parentState.tagSelected == data) ? true : false;
    }
  }

  bool get getItemState => _selected;
  String get getIndex => _index;
  String get getLabel => _label;
  void unSelect() {
    setState(() {
      _selected = false;
    });
  }

  void setItemState() {
    setState(() {
      _selected = true;
    });
  }

  void setItemSelected(String tag) {
    if (_multiple) {
      _parentState.setSelectedDay(tag);
    } else {
      _parentState.setSelectedTag(tag);
    }
  }

  final BoxDecoration boxDeco = BoxDecoration( color: UiHelper.scCheckGris, borderRadius: BorderRadius.circular(15.0));
  final TextStyle textNormal = TextStyle( color: UiHelper.scGrisTexto, fontSize: 10, fontWeight: FontWeight.bold);
  final Icon iconNormal = Icon(FontAwesomeIcons.checkCircle, color: UiHelper.scGrisTexto, size: 12.0);
  final BoxDecoration boxDecoSelected = BoxDecoration( color: UiHelper.scCheckRojo, borderRadius: BorderRadius.circular(15.0));
  final TextStyle textSelected = TextStyle(color: Colors.white, fontSize: 10, fontWeight: FontWeight.bold);
  final Icon iconSelected = Icon(FontAwesomeIcons.checkCircle, color: Colors.white, size: 12.0);
  final TextStyle onlyTextNormal = TextStyle( color: UiHelper.scGrisTexto, fontSize: 14, fontWeight: FontWeight.bold);
  final TextStyle onlyTextSelected = TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.bold);

  List<Widget> _children = [];

  @override
  Widget build(BuildContext context) {
    _selected = false;
    if (!_multiple) {
      if (_parentState.tagSelected == this._index) {
        _selected = true;
      }
    } else {
      _selected = (_parentState.daysSelected.indexOf(_index) >= 0);
    }

    if (_hasIcon) {
      _children = [
        SizedBox(
          width: 25.0,
          child: (!_selected) ? iconNormal : iconSelected,
        ),
        Text(_label, style: (!_selected) ? textNormal : textSelected),
      ];
    } else {
      _children = [
        Text(_label, style: (!_selected) ? onlyTextNormal : onlyTextSelected),
      ];
    }

    Widget rWidget = InkResponse(
        onTap: () {
          if (_multiple) {
            this.setItemSelected(_index);
            setState(() {
              _selected = (_parentState.daysSelected.indexOf(_index) >= 0);
            });
          } else {
            this.setItemSelected(_index);
            setState(() {
              _selected = (_parentState.tagSelected == _index);
            });
          }
        },
        child: Container(
          padding: (_hasIcon)
              ? EdgeInsets.only(right: 10.0, top: 5.0, bottom: 5.0)
              : EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
          decoration: (!_selected) ? boxDeco : boxDecoSelected,
          child: Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: _children),
        ));
    return rWidget;
  }
}

class TagsHoras extends StatelessWidget {
  List<TagHora> _items = [];
  String _title = "";
  TagsHoras({Key key, String title, List<TagHora> items}) : super(key: key) {
    _items = items;
    _title = title;
  }

  @override
  Widget build(BuildContext context) {
    final Widget rWidget = Container(
      alignment: Alignment.topLeft,
      //color:UiHelper.greenContainer,
      child: Expanded(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(bottom: 10.0),
              child: UiHelper().headerLocation(caption: _title),
            ),
            Container(
              alignment: Alignment.topLeft,
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: Wrap(
                spacing: 5,
                runSpacing: 5,
                children: _items,
              ),
            ),
          ],
        ),
      ),
    );

    return rWidget;
  }
}

class TagHora extends StatefulWidget {
  _TagHoraState _state;

  TagHora({
    Key key,
    String label,
    String index,
    Function evtOnTap,
    TurnoFechaHoraState parentState,
    bool selected = false,
    bool isMultiple = false,
    bool withIcon = true,
  }) : super(key: key) {
    _state = new _TagHoraState(
        label: label,
        index: index,
        evtOnTap: evtOnTap,
        parentState: parentState,
        isSelected: selected,
        isMultiple: isMultiple,
        withIcon: withIcon);
  }

  @override
  _TagHoraState createState() => _state;

  void unSelect() => _state.unSelect();
  void setSelect() => _state.setItemState();
  String getIndex() => _state.getIndex;
}

class _TagHoraState extends State<TagHora> {
  String _index;
  bool _selected = false;
  bool _multiple = false;
  bool _hasIcon = false;
  String _label;
  TurnoFechaHoraState _parentState;

  _TagHoraState(
      {String label,
      String index,
      Function evtOnTap,
      TurnoFechaHoraState parentState,
      bool isSelected = false,
      bool isMultiple = false,
      bool withIcon = true}) {
    this.setLabel(label);
    this.setIndex(index);
    this.setParentState(parentState);
    this.setSelected(isSelected);
    this.setMultiple(isMultiple);
    this.setHasIcon(withIcon);
  }

  @override
  void initState() {
    super.initState();
  }

  void setIndex(String data) => _index = data;
  void setSelected(bool data) => _selected = data;
  void setMultiple(bool data) => _multiple = data;
  void setHasIcon(bool data) => _hasIcon = data;
  void setLabel(String data) => _label = data;
  void setParentState(TurnoFechaHoraState data) => _parentState = data;
  void setSelectedState(String data) {
    if (_multiple) {
      _selected = (_parentState.timeSelected.indexOf(_index) >= 0);
    } else {
      _selected = (_parentState.timeSelected == data) ? true : false;
    }
  }

  bool get getItemState => _selected;
  String get getIndex => _index;
  void unSelect() {
    setState(() {
      _selected = false;
    });
  }

  void setItemState() {
    setState(() {
      _selected = true;
    });
  }

  void setItemSelected(String tag) {
    if (_multiple) {
      _parentState.setSelected(tag);
    } else {
      _parentState.setSelected(tag);
    }
  }

  final BoxDecoration boxDeco = BoxDecoration(
      color: UiHelper.scCheckGris, borderRadius: BorderRadius.circular(15.0));
  final TextStyle textNormal = TextStyle(
      color: UiHelper.scGrisTexto, fontSize: 12, fontWeight: FontWeight.bold);
  final Icon iconNormal = Icon(FontAwesomeIcons.checkCircle,
      color: UiHelper.scGrisTexto, size: 12.0);
  final BoxDecoration boxDecoSelected = BoxDecoration(
      color: UiHelper.scCheckRojo, borderRadius: BorderRadius.circular(15.0));
  final TextStyle textSelected =
      TextStyle(color: Colors.white, fontSize: 12, fontWeight: FontWeight.bold);
  final Icon iconSelected =
      Icon(FontAwesomeIcons.checkCircle, color: Colors.white, size: 12.0);
  final TextStyle onlyTextNormal = TextStyle(
      color: UiHelper.scGrisTexto, fontSize: 14, fontWeight: FontWeight.bold);
  final TextStyle onlyTextSelected =
      TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.bold);

  List<Widget> _children = [];

  @override
  Widget build(BuildContext context) {
    //this.setSelectedState(_index);
    _selected = false;
    if (_parentState.timeSelected == this._index) {
      _selected = true;
    }

    if (_hasIcon) {
      _children = [
        SizedBox(
          width: 25.0,
          child: (!_selected) ? iconNormal : iconSelected,
        ),
        Text(_label, style: (!_selected) ? textNormal : textSelected),
      ];
    } else {
      _children = [
        Text(_label, style: (!_selected) ? onlyTextNormal : onlyTextSelected),
      ];
    }

    Widget rWidget = InkResponse(
        onTap: () {
          if (_multiple) {
            this.setItemSelected(_index);
            setState(() {
              _selected = (_parentState.timeSelected.indexOf(_index) >= 0)
                  ? true
                  : false;
            });
          } else {
            this.setItemSelected(_index);
            setState(() {
              _selected = (_parentState.timeSelected == _index) ? true : false;
            });
          }
        },
        child: Container(
          padding: (_hasIcon)
              ? EdgeInsets.only(right: 10.0, top: 5.0, bottom: 5.0)
              : EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
          decoration: (!_selected) ? boxDeco : boxDecoSelected,
          child: Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: _children),
        ));
    return rWidget;
  }
}

class DropdownGrupoFamiliar extends StatefulWidget {
  _DropdownGrupoFamiliarState _state;
  DropdownGrupoFamiliar(
      {Key key,
      Function evtOnChanged,
      List<DropdownMenuItem<String>> items,
      String selected})
      : super(key: key) {
    _state = new _DropdownGrupoFamiliarState(
        evtOnChanged: evtOnChanged, mapItems: items, selected: selected);
  }

  @override
  _DropdownGrupoFamiliarState createState() => _state;
}

class _DropdownGrupoFamiliarState extends State<DropdownGrupoFamiliar>
    with UiHelper {
  String _valor;
  Function _evtOnChanged;
  List<DropdownMenuItem<String>> _dropdownItems;

  _DropdownGrupoFamiliarState(
      {Function evtOnChanged,
      List<DropdownMenuItem<String>> mapItems,
      String selected}) {
    _evtOnChanged = evtOnChanged;
    _dropdownItems = mapItems;
    if (selected.length > 0) {
      _valor = selected;
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final Widget rWidget = inputWrapper(
        field: DropdownButton<String>(
            hint: Text('Usuario Beneficiario'),
            value: _valor,
            isExpanded: true,
            icon: SizedBox(
                width: 24.0,
                child: Icon(
                  FontAwesomeIcons.angleDown,
                  color: UiHelper.scAzul,
                  size: 16,
                )),
            iconSize: 16,
            elevation: 16,
            style: TextStyle(color: Color.fromRGBO(70, 70, 70, 1)),
            underline: Container(color: Colors.transparent),
            onChanged: (opt) {
              setState(() {
                _valor = opt;
              });
              _evtOnChanged(_valor);
            },
            items: _dropdownItems),
        withBoxShadow: true);

    return rWidget;
  }
}

class DropdownEspecialidad extends StatefulWidget {
  _DropdownEspecialidadState _state;
  DropdownEspecialidad(
      {Key key,
      Function evtOnChanged,
      List<DropdownMenuItem<String>> items,
      String selected})
      : super(key: key) {
    _state = new _DropdownEspecialidadState(
        evtOnChanged: evtOnChanged, mapItems: items, selected: selected);
  }

  @override
  _DropdownEspecialidadState createState() => _state;
}

class _DropdownEspecialidadState extends State<DropdownEspecialidad>
    with UiHelper {
  String _valor;
  Function _evtOnChanged;
  List<DropdownMenuItem<String>> _dropdownItems;

  _DropdownEspecialidadState(
      {Function evtOnChanged,
      List<DropdownMenuItem<String>> mapItems,
      String selected}) {
    _evtOnChanged = evtOnChanged;
    _dropdownItems = mapItems;
    if (selected.length > 0) {
      if (existeValor(selected)) {
        _valor = selected;
      } else {
        _valor = _dropdownItems[0].value;
      }
    } else {
      if (_dropdownItems.length > 0) {
        _valor = _dropdownItems[0].value;
      }
    }
  }

  bool existeValor(String valor) {
    bool result = false;
    for (final element in _dropdownItems) {
      if (element.value == valor) {
        result = true;
        break;
      }
    }
    return result;
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final Widget rWidget = inputWrapper(
        field: DropdownButton<String>(
            hint: Text('Seleccionar Especialidad'),
            value: _valor,
            isExpanded: true,
            icon: SizedBox(
                width: 24.0,
                child: Icon(
                  FontAwesomeIcons.angleDown,
                  color: UiHelper.scAzul,
                  size: 16,
                )),
            iconSize: 16,
            elevation: 16,
            style: TextStyle(color: Color.fromRGBO(70, 70, 70, 1)),
            underline: Container(color: Colors.transparent),
            onChanged: (opt) {
              setState(() {
                _valor = opt;
              });
              _evtOnChanged(_valor);
            },
            items: _dropdownItems),
        withBoxShadow: true);

    return rWidget;
  }
}

class ListaProfesionales extends StatelessWidget {
  List<ItemProfesional> _items = [];
  ListaProfesionales({Key key, List<ItemProfesional> items}) : super(key: key) {
    _items = items;
  }

  List<ItemProfesional> get getItems => _items;

  @override
  Widget build(BuildContext context) {
    final SingleChildScrollView rWidget = SingleChildScrollView(
        child: Container(
            child: Column(
              children: _items,
            ),
            margin: EdgeInsets.only(bottom: 80)));
    return rWidget;
  }
}

class ItemProfesional extends StatefulWidget {
  ItemProfesionalState _state;

  ItemProfesional(
      {Key key,
      String title,
      String tag,
      String lugar,
      int index,
      bool btnCheck,
      bool isSelected,
      TurnoEspecialidadState parentState})
      : super(key: key) {
    _state = new ItemProfesionalState(
        title: title,
        tag: tag,
        lugar: lugar,
        index: index,
        parentState: parentState,
        key: key,
        btnCheck: btnCheck,
        isSelected: isSelected);
  }

  @override
  ItemProfesionalState createState() => _state;

  void unSelect() => _state.unSelect();
  void setSelect() => _state.setItemState();
  bool getState() => _state.getItemState;
  int getIndex() {
    return _state.getIndex;
  }
}

class ItemProfesionalState extends State<ItemProfesional> with UiHelper {
  String _title;
  String _tag;
  String _lugar;
  bool _btnCheck;
  int _index = 0;
  bool _selected = false;
  TurnoEspecialidadState _parentState;

  ItemProfesionalState(
      {Key key,
      int index,
      String title,
      String tag,
      String lugar,
      bool isSelected = false,
      bool btnCheck = true,
      TurnoEspecialidadState parentState}) {
    this.setTitle(title);
    this.setTag(tag);
    this.setLugar(lugar);
    this.setIndex(index);
    this.setParentState(parentState);
    this.setBtnCheck(btnCheck);
    this.setSelected(isSelected);
  }

  @override
  void initState() {
    super.initState();
    _selected = false;
  }

  void setTitle(String data) => _title = data;
  void setTag(String data) => _tag = data;
  void setLugar(String data) => _lugar = data;
  void setIndex(int data) => _index = data;
  void setSelected(bool data) => _selected = data;
  void setBtnCheck(bool data) => _btnCheck = data;
  void setParentState(TurnoEspecialidadState data) => _parentState = data;

  @override
  Widget build(BuildContext context) {
    List<Widget> acciones = [];
    double _boxMarginTop = 5.0;
    double _boxPaddingTop = 0.0;
    _selected = false;
    if (_parentState.profSelected == this._index) {
      _selected = true;
    }

    if (_btnCheck) {
      acciones.add(this.btnActionCard(
        icon: FontAwesomeIcons.check,
        iconColor: (!_selected) ? UiHelper.scGris : Colors.white,
        btnColor: (!_selected) ? UiHelper.scBtnGris : UiHelper.scCheckRojo,
        evtOnPressed: () {
          this.setItemSelected(_index);
          setState(() {
            _selected = (_parentState.profSelected == _index) ? true : false;
          });
        },
      ));
    }

    return Container(
      margin: EdgeInsets.only(
          left: 15.0, top: _boxMarginTop, bottom: 5.0, right: 15.0),
      decoration: boxDecoProfesionalCard,
      child: Column(
        children: <Widget>[
          // titulo+tag
          Padding(
              padding: EdgeInsets.only(top: _boxPaddingTop),
              child: rowTitleCard(title: _title)),

          // dirección
          Padding(
            padding: EdgeInsets.only(top: 10.0),
            child: rowDireccionCard(text: _lugar),
          ),

          // footer-fecha-botonera-acciones
          footerProfesionalCard(
            direccion: _lugar,
            tag: _tag,
            actionList: acciones,
          ),
        ],
      ),
    );
  }

  int get getIndex => _index;
  bool get getItemState => _selected;

  void unSelect() {
    setState(() {
      _selected = false;
    });
  }

  void setItemState() {
    setState(() {
      _selected = true;
    });
  }

  void setItemSelected(int idx) {
    _parentState.setSelected(idx);
  }
}

class DropdownFechasTurno extends StatefulWidget {
  _DropdownFechasTurnoState _state;
  DropdownFechasTurno(
      {Key key,
      Function evtOnChanged,
      List<DropdownMenuItem<String>> items,
      String selected})
      : super(key: key) {
    _state = new _DropdownFechasTurnoState(
        evtOnChanged: evtOnChanged, mapItems: items, selected: selected);
  }

  @override
  _DropdownFechasTurnoState createState() => _state;
}

class _DropdownFechasTurnoState extends State<DropdownFechasTurno>
    with UiHelper {
  String _valor;
  Function _evtOnChanged;
  List<DropdownMenuItem<String>> _dropdownItems;

  _DropdownFechasTurnoState(
      {Function evtOnChanged,
      List<DropdownMenuItem<String>> mapItems,
      String selected}) {
    _evtOnChanged = evtOnChanged;
    _dropdownItems = mapItems;
    if (selected.length > 0) {
      _valor = selected;
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final Widget rWidget = inputWrapper(
        field: DropdownButton<String>(
            hint: Text('DD/MM/AAAA'),
            value: _valor,
            isExpanded: true,
            icon: SizedBox(
                width: 24.0,
                child: Icon(
                  FontAwesomeIcons.angleDown,
                  color: UiHelper.scAzul,
                  size: 16,
                )),
            iconSize: 16,
            elevation: 16,
            style: TextStyle(color: Color.fromRGBO(70, 70, 70, 1)),
            underline: Container(color: Colors.transparent),
            onChanged: (opt) {
              setState(() {
                _valor = opt;
              });
              _evtOnChanged(_valor);
            },
            items: _dropdownItems),
        withBoxShadow: true);
    return rWidget;
  }
}

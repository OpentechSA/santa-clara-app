import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:santa_clara/src/pages/usuario/indexPerfilUsuario.dart';
import 'package:santa_clara/utils/uiHelper.dart';

class PageViewNav extends StatelessWidget{

  List<BtnPageViewNav> _items = [];
  PageViewNav({Key key, List<BtnPageViewNav> items}) : super(key: key){ _items = items; }

  @override
  Widget build(BuildContext context) {

    final Widget rWidget = ListView(
      scrollDirection: Axis.horizontal,
      children: _items,
    );

    return rWidget;

  }

}

class BtnPageViewNav extends StatefulWidget{
  
  BtnPageViewNavState _state;
  
  BtnPageViewNav({ 
    Key key, 
    int index = 0, 
    String caption, 
    bool activo = false, 
    IndexPerfilUsuarioState parentState,
    Function evtOnPressed
  }):super(key:key){
    _state = new BtnPageViewNavState(
      caption       : caption,
      activo        : activo,
      index         : index,
      parentState   : parentState,
      evtOnPressed  : evtOnPressed
    );
  }
  
  @override
  createState() => _state;

}

class BtnPageViewNavState extends State<BtnPageViewNav> with UiHelper{
  
  bool activo = false;
  int index = 0;
  String caption;
  Function evtOnPressed;
  IndexPerfilUsuarioState parentState;

  BtnPageViewNavState({ int index, String caption, Function evtOnPressed, bool activo, IndexPerfilUsuarioState parentState }){
    this.parentState  = parentState;
    this.index        = index;
    this.caption      = caption;
    this.activo       = (this.parentState.currentPage == this.index);
  }

  void setItemSelected(int value) => parentState.setCurrentPage(value);

  @override
  void initState() => super.initState();

  @override
  Widget build(BuildContext context){

    this.activo = (parentState.currentPage == this.index);

    return ButtonTheme(
      padding: EdgeInsets.all(0.0),
      child: FlatButton(
        onPressed: (){
          this.setItemSelected(this.index);
          activo = (parentState.currentPage == index);
        }, 
        child: Container(
          margin: EdgeInsets.zero,
          padding:EdgeInsets.symmetric(horizontal:16.0,vertical:15.0),
          decoration:BoxDecoration( 
            border:Border(bottom: BorderSide(color:(activo)?UiHelper.scAzulOscuro:Colors.transparent,width:3.0))
          ),
          child:Text(caption,style: TextStyle(fontWeight: (activo)?FontWeight.bold:FontWeight.normal,fontSize: 12.0),),
        )
      ),
    );
  }

}

class ItemTransaccion extends StatefulWidget with UiHelper{

  ItemTransaccionState _state;
  
  ItemTransaccion({ 
    Key key, 
    int index,
    String title,
    String data,
    String dataID,
    bool altBg = false,
    bool inactive = false,
    EstadoTsx status = EstadoTsx.Pendiente,
    Function evtOnTap
  }):super(key:key){
    _state = new ItemTransaccionState(
      index: index,
      title: title,
      data: data,
      dataID: dataID,
      altBg: altBg,
      inactive: inactive,
      status: status,
      evtOnTap: evtOnTap
    );
  }
  
  @override
  createState() => _state;

}

class ItemTransaccionState extends State<ItemTransaccion> with UiHelper{

  int index;
  String title;
  String data;
  String dataID;
  bool altBg = false;
  bool inactive = false;
  EstadoTsx status = EstadoTsx.Pendiente;
  Function evtOnTap;

  ItemTransaccionState({this.index,this.title,this.data,this.dataID,this.altBg,this.inactive,this.status,this.evtOnTap});
  
  @override
  void initState() => super.initState();

  @override
  Widget build(BuildContext context){

    BoxDecoration trailingBoxDeco = BoxDecoration(color:Colors.orangeAccent,borderRadius:BorderRadius.circular(24.0));
    Color iconColor = Color.fromRGBO(55, 55, 55, 1);
    switch(status){
      case  EstadoTsx.Pendiente:
        break;
      case EstadoTsx.Realizada:
        trailingBoxDeco = BoxDecoration(color:Colors.green,borderRadius:BorderRadius.circular(24.0));
        iconColor = Colors.white;
        break;
      case EstadoTsx.Vencida:
        trailingBoxDeco = BoxDecoration(color:Colors.red,borderRadius:BorderRadius.circular(24.0));
        iconColor = Colors.white;
        break;
    }

    final Widget itemTrailing = Container(
      width: 24,
      height: 24,
      decoration: trailingBoxDeco,
      child:Icon(
        FontAwesomeIcons.eye,
        color:iconColor,
        size:14
      )
    );

    return Container(
      padding: EdgeInsets.symmetric(vertical:10.0),
      margin: EdgeInsets.zero,
      decoration: BoxDecoration(
        color:(altBg)?Color.fromRGBO(249, 249, 249, 1):Colors.transparent,
        border:Border(bottom: BorderSide(color: Color.fromRGBO(233, 233, 233, 1),width: 1.0 ))
      ),
      child:ListTile(
        trailing: itemTrailing,
        leading:Container(
          width: 40.0,
          height: 40.0,
          decoration: BoxDecoration(
            color:Color.fromRGBO(235, 255, 229, 1),
            borderRadius: BorderRadius.circular(40.0),
            border: Border.all(color: UiHelper.scAzul,width: 2.0),
          ),
          child:Icon(FontAwesomeIcons.moneyBillAlt,color:Color.fromRGBO(80, 80, 80, 1),size:16)
        ),
        title:Column(
          crossAxisAlignment: CrossAxisAlignment.start,  
          children:[
            Text( data, style:TextStyle(fontSize:11.0,color:Color.fromRGBO(90,90,90,1))),
            Text(title,style:TextStyle(fontSize:16.0,color:UiHelper.scAzul,fontWeight: FontWeight.bold)),
            Text( dataID, style:TextStyle(fontSize:12.0,color:Color.fromRGBO(90,90,90,1),fontWeight: FontWeight.bold)),
          ]
        ),
        onTap: (){
          evtOnTap(index);
        } ,
      ),
    );

  }

}


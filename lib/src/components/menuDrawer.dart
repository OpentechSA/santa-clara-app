import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:santa_clara/utils/uiHelper.dart';
import 'package:santa_clara/utils/userPreferences.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MenuDrawer extends StatelessWidget with UiHelper {
  
  MenuDrawer({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    final UserPreferences userPrefs = new UserPreferences();
    userPrefs.initPrefs();

    return Drawer(
      child: Stack(
        children: <Widget>[
          
          getDrawerBackground(),
          SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[

                SafeArea(
                  child: userProfileBox(
                    nombreCompleto: userPrefs.nombreCompleto,
                    userProfilePic: userPrefs.imagenPerfil,
                    profileComplete: userPrefs.userPrefsComplete(),
                    evtPressed: (){ Navigator.pushNamed(context, 'perfil-usuario'); }
                  ),
                ),
                SizedBox(height: 35.0,),
                // userPlanBox(evtOnpressed: (){},planes: userPrefs.userPlanes),
                separadorDrawer(),

                // opciones del menu (agenda,solicitudes,programa-referidos,e-commerce)
                Container(
                  margin:EdgeInsets.only(right:40.0,left:20.0),
                  width: UiHelper.size.width * 0.6,
                  child:Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('Servicios',style:TextStyle(fontSize: 16.0,color:UiHelper.scCeleste,fontWeight: FontWeight.bold)),
                      SizedBox(height: 10.0,),
                      customDrawerNavItem(icon:FontAwesomeIcons.cube,text:'Inicio',evtPressed: (){
                        Navigator.pushNamed(context, 'dashboard');
                      }),
                      customDrawerNavItem(icon:FontAwesomeIcons.solidCalendarAlt,text:'Agenda',evtPressed: (){
                        Navigator.pushNamed(context, 'agenda');
                      }),
                      customDrawerNavItem(icon:FontAwesomeIcons.solidFileAlt,text:'Solicitudes',evtPressed: (){
                        Navigator.pushNamed(context, 'solicitudes');
                      }),
                      customDrawerNavItem(icon:FontAwesomeIcons.search,text:'Consultas',evtPressed: (){}),
                      customDrawerNavItem(icon:FontAwesomeIcons.plane,text:'Asistencia al viajero',evtPressed: (){
                        Navigator.pushNamed(context, "viajeros");
                      }),
                      customDrawerNavItem(icon:FontAwesomeIcons.userFriends,text:'Programa de referidos',evtPressed: (){}),
                      customDrawerNavItem(icon:FontAwesomeIcons.shoppingBasket,text:'E-commerce',evtPressed: (){}),
                      customDrawerNavItem(
                          icon:FontAwesomeIcons.userAlt,
                          text:'Carnet Digital',
                          evtPressed: (){
                            Navigator.pushNamed(context, "carnet_digital");
                          }
                      ),
                    ],
                  )
                ),

                separadorDrawer(),

                // opciones del menu (atc,faq,chat,ayuda)
                Container(
                  margin:EdgeInsets.only(right:40.0,left:20.0),
                  width: UiHelper.size.width * 0.6,
                  child:Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('Atención al cliente',style:TextStyle(fontSize: 16.0,color:UiHelper.scCeleste,fontWeight: FontWeight.bold)),
                      SizedBox(height: 10.0,),
                      customDrawerNavItem(icon:FontAwesomeIcons.questionCircle,text:'FAQ',evtPressed: (){}),
                      customDrawerNavItem(icon:FontAwesomeIcons.solidComments,text:'Chat',evtPressed: (){}),
                      customDrawerNavItem(icon:FontAwesomeIcons.solidLifeRing,text:'Centro de ayuda',evtPressed: (){}),
                    ],
                  )
                ),

                separadorDrawer(),

                // opciones del menu (salir)
                Container(
                  margin:EdgeInsets.only(right:40.0,left:20.0,bottom: 30.0),
                  width: UiHelper.size.width * 0.6,
                  child:Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      customDrawerNavItem(
                        icon:FontAwesomeIcons.signOutAlt,
                        text:'Cerrar sesión',
                        evtPressed: () async {
                          SharedPreferences prefs = await SharedPreferences.getInstance();

                          prefs.clear();

                          Navigator.pushNamedAndRemoveUntil(context, 'login', (route) => false);
                        }
                      ),
                    ],
                  )
                ),

                SizedBox(height:30.0),

              ],
            )
          ,),
          
        ],
      ),
    );
    
  }

}
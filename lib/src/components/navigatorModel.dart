import 'package:flutter/material.dart';

class NavigatorModel with ChangeNotifier{

  int _currentPage = 0;
  PageController _pageController = new PageController(initialPage: 0);
  int get currentPage  => this._currentPage;
  set currentPage(int value) {
    this._currentPage = value;
    _pageController.animateToPage(value, duration: Duration(milliseconds: 250), curve: Curves.easeInOut);
    notifyListeners();
  }
  PageController get pageController => _pageController;

}
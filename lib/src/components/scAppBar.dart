import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:santa_clara/utils/uiHelper.dart';
import 'package:santa_clara/utils/userPreferences.dart';

class ScAppBar extends StatelessWidget with UiHelper implements PreferredSizeWidget {
  
  bool masterView;
  String altCaption;
  Function customAction;

  ScAppBar({Key key, bool masterView = true, String altCaption, Function customAction }) : preferredSize = Size.fromHeight(kToolbarHeight), super(key: key){
    this.masterView = masterView;
    this.altCaption = altCaption;
    this.customAction = customAction;
  }

  @override
  final Size preferredSize;
  
  @override
  PreferredSizeWidget build(BuildContext context) {

    final UserPreferences userPrefs = new UserPreferences();
    userPrefs.initPrefs();

    final logoSantaClaraAppBar = Image(
      image: new AssetImage('assets/images/logo-santa-clara.png'),
      width: (this.getSize.width * 0.10),
      fit: BoxFit.contain,
    );

    final customLeadingIconButton = Builder(
      builder: (BuildContext context) {
        return IconButton(
          icon: (masterView)?const Icon(FontAwesomeIcons.bars):const Icon(FontAwesomeIcons.angleLeft),
          onPressed: () { 
            if(masterView){
              Scaffold.of(context).openDrawer(); 
            }else{
              if(this.customAction != null){
                customAction();
              }else{
                Navigator.pop(context);
              }
            }
          },
          tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
        );
      },
    );

    final Widget customAppBar = PreferredSize(
      preferredSize: Size.fromHeight(70.0),
      child: Container(
        padding:EdgeInsets.only(top:10.0,bottom:10.0,left: 45.0),
        child:AppBar(
          elevation: 0.0,
          titleSpacing: 0.0,
          automaticallyImplyLeading: true,
          leading: customLeadingIconButton,
          title: Container(
            child:Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child:Text(
                    (altCaption != null)?altCaption:'Hola, '+userPrefs.nombreCompleto,
                    style: TextStyle(
                      color:UiHelper.scAzulOscuro,
                      fontSize: 14.0,
                    ),
                  )
                ),
                Padding(padding:EdgeInsets.only(right:10.0),child: SizedBox(width: 100.0,child:logoSantaClaraAppBar)),
              ],
            )
          )
        )
      )
    );
    
    return customAppBar;
    
  }

}
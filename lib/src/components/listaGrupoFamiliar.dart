import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:santa_clara/src/pages/agenda/turnoBeneficiario.dart';
import 'package:santa_clara/utils/uiHelper.dart';

//******************************
// ItemGrupoFamiliar
//******************************

class ListaGrupoFamiliar extends StatelessWidget{
  
  List<ItemGrupoFamiliar> _items = [];
  ListaGrupoFamiliar ({Key key,List<ItemGrupoFamiliar> items}):super(key:key){ _items = items; }

  List<ItemGrupoFamiliar> get getItems => _items;
  
  @override
  Widget build(BuildContext context) {
    final SingleChildScrollView rWidget = SingleChildScrollView(child: Column(children: _items,),);
    return rWidget;
  }
  
}

class ItemGrupoFamiliar extends StatefulWidget{

  ItemGrupoFamiliarState _state;

  ItemGrupoFamiliar({
    Key key,
    String title,
    String data,
    String dataID,
    int index,
    bool altBg,
    bool isSelected,
    TurnoBeneficiarioState parentState
  }):super(key:key){
    _state = new ItemGrupoFamiliarState(
      title       : title,
      data        : data,
      dataID      : dataID,
      index       : index,
      parentState : parentState,
      key         : key,
      isSelected  : isSelected
    );
  }

  @override
  ItemGrupoFamiliarState createState() => _state;

  void unSelect() => _state.unSelect();
  void setSelect() => _state.setItemState();
  bool getState() => _state.getItemState;
  int getIndex () { return _state.getIndex; }
  
}

class ItemGrupoFamiliarState extends State<ItemGrupoFamiliar> with UiHelper{
  
  String _title;
  String _data;
  String _dataID;
  int _index = 0;
  bool _altBg = false;
  bool _selected = false;
  TurnoBeneficiarioState _parentState;
  Key _key;
  ListTile _listTile = new ListTile();

  ItemGrupoFamiliarState({
    Key key,
    int index,
    String title,
    String data,
    String dataID,
    bool altBg = false,
    bool isSelected = false,
    TurnoBeneficiarioState parentState
  }){
    this.setTitle(title);
    this.setData(data);
    this.setDataID(dataID);
    this.setAltBg(altBg);
    this.setIndex(index);
    this.setParentState(parentState);
    this.setKey(key);
    this.setSelected(isSelected);
  }

  @override
  void initState(){
    super.initState();
  }

  void setTitle (String data) => _title = data;
  void setData (String data) => _data = data;
  void setDataID (String data) => _dataID = data;
  void setIndex (int data) => _index = data;
  void setAltBg (bool data) => _altBg = data;
  void setSelected (bool data) => _selected = data;
  void setParentState (TurnoBeneficiarioState data) => _parentState = data;
  void setKey (Key data) => _key = data;

  final activeTrailing = Icon(
    FontAwesomeIcons.solidCheckCircle,
    color:Colors.green,
    size:24
  );

  final Widget inactiveTrailing = Container(
    width: 24,
    height: 24,
    decoration: BoxDecoration(color:Color.fromRGBO(150, 150, 150, 1),borderRadius:BorderRadius.circular(24.0)),
    child:Icon(
      FontAwesomeIcons.ban,
      color:Color.fromRGBO(60, 60, 60, 1),
      size:14
    )
  );

  final Widget selectedTrailing = Container(
    width: 24,
    height: 24,
    decoration: BoxDecoration(color:Color.fromRGBO(235, 87, 87, 1),borderRadius:BorderRadius.circular(24.0)),
    child:Icon(
      FontAwesomeIcons.check,
      color:Colors.white,
      size:14
    )
  );

  final Widget unselectedTrailing = Container(
    width: 24,
    height: 24,
    decoration: BoxDecoration(color:UiHelper.scBtnGris,borderRadius:BorderRadius.circular(24.0)),
    child:Icon(
      FontAwesomeIcons.check,
      color:UiHelper.scGris,
      size:14
    )
  );

  final Widget itemLeading = Container(
    width: 40.0,
    height: 40.0,
    decoration: BoxDecoration(
      color:Color.fromRGBO(235, 255, 229, 1),
      borderRadius: BorderRadius.circular(40.0),
      border: Border.all(color: UiHelper.scAzul,width: 2.0),
    ),
    child:Icon(FontAwesomeIcons.solidUser,color:Color.fromRGBO(80, 80, 80, 1),size:16)
  );

  @override
  Widget build(BuildContext context) {

    _listTile = new ListTile(
      key: _key,
      onTap: (){ 
        this.setItemSelected(_index);
        setState( () {
          _selected = (_parentState.userSelected == _index)?true:false;
        });
      },
      trailing: (!_selected)?unselectedTrailing:selectedTrailing,
      leading:itemLeading,
      title:Text(_title,style:TextStyle(fontSize:16.0,color:UiHelper.scAzul)),
      subtitle: Column(
        crossAxisAlignment: CrossAxisAlignment.start,  
        children:[
          Text( _data, style:TextStyle(fontSize:11.0,color:Color.fromRGBO(90,90,90,1))),
          Text( _dataID, style:TextStyle(fontSize:12.0,color:Color.fromRGBO(90,90,90,1),fontWeight: FontWeight.bold)),
        ]
      ),
    );

    Widget rWidget = Container(
      padding: EdgeInsets.symmetric(vertical:10.0),
      margin: EdgeInsets.zero,
      decoration: BoxDecoration(
        color:(_altBg)?Color.fromRGBO(249, 249, 249, 1):Colors.transparent,
        border:Border(bottom: BorderSide(color: Color.fromRGBO(233, 233, 233, 1),width: 1.0 ))
      ),
      child:_listTile,
    );

    return rWidget;
  }

  int get getIndex => _index;
  bool get getItemState => _selected;

  void unSelect(){  setState(() {  _selected = false; });  }
  void setItemState(){ setState(() { _selected = true; });  }
  void setItemSelected(int idx){  _parentState.setSelected(idx); }

}
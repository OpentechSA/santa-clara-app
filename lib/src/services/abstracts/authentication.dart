import 'package:santa_clara/src/models/loginModels.dart';
import 'package:santa_clara/src/models/registrationModels.dart';

abstract class BaseAuth {
  
  Future<LoginResponse> signIn(String email, String password);
  Future<SignUpResponse> signUp(SignUpRequest request);
  Future<RecoveryResponse> recoveryUp(RecoveryRequest request);
  Future<EditUserResponse> editUser(EditUserRequest request);
  Future<EditUserResponse> changePass(ChangePassRequest request);

}
import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:santa_clara/src/models/agendaModels.dart';
import 'package:santa_clara/utils/constants.dart';

class Turnos {
  Future<AdherentesResponse> getBeneficiarios(String idCliente) async {
    AdherentesResponse resp = new AdherentesResponse();
    Map<String, dynamic> decodeResp = {};
    var queryParameters = {'idCliente': idCliente};

    print(queryParameters.toString());

    final response = await http.post(Constants.apiUrl + 'agenda/adherentes',
        body: queryParameters);

    print(response.statusCode);

    var respu = json.decode(response.body);

    print(respu.toString());

    if (response?.statusCode != 200) {
      var statusCode = response?.statusCode.toString();
      resp.setStatus(false);
      resp.setMessage("response: $statusCode");
    } else {
      if (respu['status'] == true) {
        decodeResp = json.decode(response.body);
        resp = AdherentesResponse.fromJson(decodeResp);
      } else {
        resp = AdherentesResponse(
          message: respu['message'],
          codStatus: respu['codStatus'],
          data: [],
          params: [],
          status: false,
          tipoRespuesta: 1
        );
      }
    }

    return resp;
  }

  Future<EventosResponse> getEventosDashboard(RequestEventos request) async {

    EventosResponse resp = new EventosResponse();
    Map<String, dynamic> decodeResp = {};
    var queryParameters = request.toJson();
    print(queryParameters);

    final response = await http.post(Constants.apiUrl + 'agenda/dashboard',
        body: queryParameters);

    if (response?.statusCode != 200) {
      var statusCode = response?.statusCode.toString();
      resp.setStatus(false);
      resp.setMessage("response: $statusCode");
    } else {
      decodeResp = json.decode(response.body);
      resp = EventosResponse.fromJson(decodeResp);
    }
    return resp;

  }

  Future<EspecialidadesResponse> getEspecialidad( RequestEspecialidad request ) async {
    EspecialidadesResponse resp = new EspecialidadesResponse();
    Map<String, dynamic> decodeResp = {};
    var queryParameters = request.toJson();
    // print(queryParameters);

    final response = await http.post(Constants.apiUrl + 'agenda/especialidad',
        body: queryParameters);

    if (response?.statusCode != 200) {
      var statusCode = response?.statusCode.toString();
      resp.setStatus(false);
      resp.setMessage("response: $statusCode");
      //print(response.body);
    } else {
      decodeResp = json.decode(response.body);
      resp = EspecialidadesResponse.fromJson(decodeResp);
      //print(response.body);
      //print(resp.message);
    }

    return resp;
  }

  Future<ProfesionalesResponse> getProfesionales( RequestProfesional request ) async {
    ProfesionalesResponse resp = new ProfesionalesResponse();
    Map<String, dynamic> decodeResp = {};
    var queryParameters = request.toJson();
    // print(queryParameters);

    final response = await http.post(Constants.apiUrl + 'agenda/profesional',
        body: queryParameters);

    if (response?.statusCode != 200) {
      var statusCode = response?.statusCode.toString();
      resp.setStatus(false);
      resp.setMessage("response: $statusCode");
      // print(response.body);
    } else {
      decodeResp = json.decode(response.body);
      resp = ProfesionalesResponse.fromJson(decodeResp);
      // print(response.body);
      // print(resp.message);
    }

    return resp;
  }

  Future<FechasResponse> getFechasDisponibles(RequestFechas request) async {
    FechasResponse resp = new FechasResponse();
    Map<String, dynamic> decodeResp = {};
    var queryParameters = request.toJson();
    // print(queryParameters);

    final response = await http.post(Constants.apiUrl + 'agenda/fechaTurno',
        body: queryParameters);

    if (response?.statusCode != 200) {
      var statusCode = response?.statusCode.toString();
      resp.setStatus(false);
      resp.setMessage("response: $statusCode");
      // print(response.body);
    } else {
      decodeResp = json.decode(response.body);
      resp = FechasResponse.fromJson(decodeResp);
      // print(response.body);
      // print(resp.message);
    }

    return resp;
  }

  Future<HorasResponse> getHorasDisponibles(RequestHoras request) async {
    HorasResponse resp = new HorasResponse();
    Map<String, dynamic> decodeResp = {};
    var queryParameters = request.toJson();
    // print(queryParameters);

    final response = await http.post(Constants.apiUrl + 'agenda/horaTurno',
        body: queryParameters);

    if (response?.statusCode != 200) {
      var statusCode = response?.statusCode.toString();
      resp.setStatus(false);
      resp.setMessage("response: $statusCode");
      // print(response.body);
    } else {
      decodeResp = json.decode(response.body);
      resp = HorasResponse.fromJson(decodeResp);
      // print(response.body);
      // print(resp.message);
    }

    return resp;
  }

  Future<TurnoResponse> setTurno(RequestTurno request) async {
    TurnoResponse resp = new TurnoResponse();
    Map<String, dynamic> decodeResp = {};
    var queryParameters = request.toJson();
    print(queryParameters);

    final response = await http.post(Constants.apiUrl + 'agenda/guardarTurno',
        body: queryParameters);

    if (response?.statusCode != 200) {
      var statusCode = response?.statusCode.toString();
      resp.setStatus(false);
      resp.setMessage("response: $statusCode");
      print(response.body);
    } else {
      decodeResp = json.decode(response.body);
      resp = TurnoResponse.fromJson(decodeResp);
      print(response.body);
      print(resp.message);
    }

    return resp;
  }

  Future<TareaResponse> setTarea(RequestTarea request) async {
    TareaResponse resp = new TareaResponse();
    Map<String, dynamic> decodeResp = {};
    var queryParameters = request.toJson();
    print(queryParameters);

    final response = await http.post(Constants.apiUrl + 'agenda/guardarTarea', body: queryParameters);

    if (response?.statusCode != 200) {
      var statusCode = response?.statusCode.toString();
      resp.setStatus(false);
      resp.setMessage("response: $statusCode");
      print(response.body);
    } else {
      decodeResp = json.decode(response.body);
      resp = TareaResponse.fromJson(decodeResp);
      print(response.body);
      print(resp.message);
    }

    return resp;
  }

  Future<TareaResponse> updateTarea(RequestTarea request) async {
    TareaResponse resp = new TareaResponse();
    Map<String, dynamic> decodeResp = {};
    var queryParameters = request.toJson();
    print(queryParameters);

    final response = await http.post(Constants.apiUrl + 'agenda/editarTarea',
        body: queryParameters);

    if (response?.statusCode != 200) {
      var statusCode = response?.statusCode.toString();
      resp.setStatus(false);
      resp.setMessage("response: $statusCode");
      print(response.body);
    } else {
      decodeResp = json.decode(response.body);
      resp = TareaResponse.fromJson(decodeResp);
      print(response.body);
      print(resp.message);
    }

    return resp;
  }

  Future<EventosResponse> getEventos(RequestEventos request) async {
    EventosResponse resp = new EventosResponse();
    Map<String, dynamic> decodeResp = {};
    var queryParameters = request.toJson();
    print(queryParameters);

    final response = await http.post(Constants.apiUrl + 'agenda/eventos',
        body: queryParameters);

    if (response?.statusCode != 200) {
      var statusCode = response?.statusCode.toString();
      resp.setStatus(false);
      resp.setMessage("response: $statusCode");
      // print(response.body);
    } else {
      decodeResp = json.decode(response.body);

      if (decodeResp['status'] == true) {
        resp = EventosResponse.fromJson(decodeResp);
      } else {
        dynamic value = {"message": "Ocurrió un error en la petición!", "tipoRespuesta": 1, "status": false, "data": [], "params": [], "codStatus": 02};
        resp = EventosResponse.fromJson(value);
      }
    }

    return resp;
  }

  Future<EventoResponse> getEvento(String idEvento, String userID) async {
    EventoResponse resp = new EventoResponse();
    Map<String, dynamic> decodeResp = {};
    var queryParameters = {'eid': idEvento, 'uid': userID};

    final response = await http.post(Constants.apiUrl + 'agenda/evento',
        body: queryParameters);

    if (response?.statusCode != 200) {
      var statusCode = response?.statusCode.toString();
      resp.setStatus(false);
      resp.setMessage("response: $statusCode");
    } else {
      decodeResp = json.decode(response.body);
      resp = EventoResponse.fromJson(decodeResp);
    }

    return resp;
  }

  Future<DeleteEventoResponse> deleteTarea( String idEvento, String userID ) async {
    DeleteEventoResponse resp = new DeleteEventoResponse();
    Map<String, dynamic> decodeResp = {};
    var queryParameters = {'eid': idEvento, 'uid': userID};

    final response = await http.post(Constants.apiUrl + 'agenda/eliminarTarea',
        body: queryParameters);

    if (response?.statusCode != 200) {
      var statusCode = response?.statusCode.toString();
      resp.setStatus(false);
      resp.setMessage("response: $statusCode");
    } else {
      decodeResp = json.decode(response.body);
      resp = DeleteEventoResponse.fromJson(decodeResp);
    }

    return resp;
  }
}

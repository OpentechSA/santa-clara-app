import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:mime_type/mime_type.dart';
import 'package:santa_clara/src/models/genericModels.dart';
import 'package:santa_clara/utils/constants.dart';

class UserProfile {

  Future<DefaultResponse> uploadUserAvatar(File imagen,String userId) async{

    final url = Uri.parse(Constants.apiUrl+'usuarios/profilepic');
    final mimeType = mime(imagen.path).split('/');
    final imageUploadRequest = http.MultipartRequest('POST',url);
    final uploadParams = {
      'uid':userId
    };
    
    DefaultResponse resp = new DefaultResponse();
    Map<String,dynamic> decodeResp = {};

    final file = await http.MultipartFile.fromPath(
      'file', 
      imagen.path,
      contentType: MediaType(mimeType[0],mimeType[1])
    );

    imageUploadRequest.files.add(file);
    imageUploadRequest.fields.addAll(uploadParams);
    

    final streamResponse = await imageUploadRequest.send();
    final response = await http.Response.fromStream(streamResponse);

    if(response.statusCode != 200 && response.statusCode != 201){
      var statusCode = response?.statusCode.toString();
      resp.setStatus(false);
      resp.setMessage("response: $statusCode");
      print(response.body);
    }else{
      decodeResp = json.decode(response.body);
      resp = DefaultResponse.fromJson(decodeResp);
    }

    return resp;

  }

  Future<DefaultResponse> getClienteIds(String ci) async {
    
    DefaultResponse resp = new DefaultResponse();
    Map<String,dynamic> decodeResp = {};
    var queryParameters = {
      'ci' : ci
    };
    
    final response = await http.post(
      Constants.apiUrl+'facturas/clienteids',
      body : queryParameters
    );

    if(response?.statusCode != 200){
      var statusCode = response?.statusCode.toString();
      resp.setStatus(false);
      resp.setMessage("response: $statusCode");
      print(response.body);
    }else{
      decodeResp = json.decode(response.body);
      resp = DefaultResponse.fromJson(decodeResp);
      //print(response.body);
      print(resp.message);
    }

    return resp;
    
  }

  Future<FacturasResponse> getFacturas(String idCliente) async {
    
    FacturasResponse resp = new FacturasResponse();
    Map<String,dynamic> decodeResp = {};
    var queryParameters = {
      'idCliente' : idCliente
    };
    
    final response = await http.post(
      Constants.apiUrl+'facturas/operaciones',
      body : queryParameters
    );

    if(response?.statusCode != 200){
      var statusCode = response?.statusCode.toString();
      resp.setStatus(false);
      resp.setMessage("response: $statusCode");
      print(response.body);
    }else{
      decodeResp = json.decode(response.body);
      resp = FacturasResponse.fromJson(decodeResp);
      //print(response.body);
      print(resp.message);
    }

    return resp;
    
  }

  

}
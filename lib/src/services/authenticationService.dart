import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:santa_clara/utils/constants.dart';
import 'package:santa_clara/src/models/loginModels.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:santa_clara/src/models/registrationModels.dart';
import 'package:santa_clara/src/services/abstracts/authentication.dart';

class Auth implements BaseAuth {

  @override
  Future<LoginResponse> signIn(String email, String password) async {
    
    LoginResponse resp = new LoginResponse();
    Map<String,dynamic> decodeResp = {};
    var queryParameters = {
      'usuario'   : email,
      'password'  : password,
    };
    
    final response = await http.post(
      Constants.apiUrl+'usuarios/login',
      body : queryParameters
    );

    if(response?.statusCode != 200){
      var statusCode = response?.statusCode.toString();
      dynamic decodeRespValue = json.decode(response.body);
      resp.setStatus(false);
      print(response.body.toString());
    }else{
      decodeResp = json.decode(response.body);

      if(decodeResp['status'] == false) {
        dynamic decodeRespValue = json.decode(response.body);
        resp.setStatus(false);
        resp.setMessage(decodeRespValue['message'].toString());
      } else {
        // Save in sharedPreference
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString("ci_user", decodeResp['data']['data']['ci'].toString());
        prefs.setString("uidUser", decodeResp['data']['data']['uid'].toString());

        print(decodeResp.toString());

        //Response
        resp = LoginResponse.fromJson(decodeResp);
      }
    }

    return resp;
    
  }

  @override
  Future<SignUpResponse> signUp(SignUpRequest request) async {
    
    SignUpResponse resp = new SignUpResponse();
    Map<String,dynamic> decodeResp = {};
    var queryParameters = request.toJson();

    print(queryParameters);
    
    final response = await http.post(
      Constants.apiUrl+'usuarios/registro',
      body : queryParameters
    );
    
    if(response?.statusCode != 200){
      var statusCode = response?.statusCode.toString();
      resp.setStatus(false);
      resp.setMessage("response: $statusCode");
      print(response.body);
    }else{
      decodeResp = json.decode(response.body);
      resp = SignUpResponse.fromJson(decodeResp);
      print(resp.message);
    }
    
    return resp;
    
  }

  @override
  Future<RecoveryResponse> recoveryUp(RecoveryRequest request) async {
    
    RecoveryResponse resp = new RecoveryResponse();
    Map<String,dynamic> decodeResp = {};
    var queryParameters = request.toJson();

    print(queryParameters);
    
    final response = await http.post(
      Constants.apiUrl+'usuarios/recuperar',
      body : queryParameters
    );
    
    if(response?.statusCode != 200){
      var statusCode = response?.statusCode.toString();
      resp.setStatus(false);
      resp.setMessage("response: $statusCode");
      print(json.decode(response.body));
    }else{
      decodeResp = json.decode(response.body);
      print(decodeResp);
      resp = RecoveryResponse.fromJson(decodeResp);
      print(resp.message);
    }
    
    return resp;
    
  }

  @override
  Future<EditUserResponse> editUser(EditUserRequest request) async {
    
    EditUserResponse resp = new EditUserResponse();
    Map<String,dynamic> decodeResp = {};
    var queryParameters = request.toJson();

    print(queryParameters);
    
    final response = await http.post(
      Constants.apiUrl+'usuarios/update',
      body : queryParameters
    );
    
    if(response?.statusCode != 200){
      var statusCode = response?.statusCode.toString();
      resp.setStatus(false);
      resp.setMessage("response: $statusCode");
      print(json.decode(response.body));
    }else{
      decodeResp = json.decode(response.body);
      print(decodeResp);
      resp = EditUserResponse.fromJson(decodeResp);
      print(resp.message);
    }
    
    return resp;
    
  }

  @override
  Future<EditUserResponse> changePass(ChangePassRequest request) async {
    
    EditUserResponse resp = new EditUserResponse();
    Map<String,dynamic> decodeResp = {};
    var queryParameters = request.toJson();

    print(queryParameters);
    
    final response = await http.post(
      Constants.apiUrl+'usuarios/updatepwd',
      body : queryParameters
    );
    
    if(response?.statusCode != 200){
      var statusCode = response?.statusCode.toString();
      resp.setStatus(false);
      resp.setMessage("response: $statusCode");
      print(json.decode(response.body));
    }else{
      decodeResp = json.decode(response.body);
      print(decodeResp);
      resp = EditUserResponse.fromJson(decodeResp);
      print(resp.message);
    }
    
    return resp;
    
  }

}